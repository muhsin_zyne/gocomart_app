//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Muhsin Zyne on 09/03/21.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
