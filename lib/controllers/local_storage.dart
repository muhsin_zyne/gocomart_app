import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:graphql/client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  dynamic orderStatusConst;
  dynamic constSyncTime;
  BuildContext context;
  LocalStorage({this.context}) {
    orderStatusConst = {};
  }
  AppServices _appServices;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

//  loadOrderStatusConst() async {
//    final SharedPreferences prefs = await _prefs;
//    try {
//      orderStatusConst = prefs.getString('orderStatusConst');
//    } catch (e) {
//      orderStatusConst = {};
//    }
//  }

  updateAppConst() async {
    await this.getAppConstantSyncTime();
  }

  getAppConstantSyncTime() async {
    final SharedPreferences prefs = await _prefs;
    try {
      var constSyncTime = prefs.getString('constSyncTime');
      if (constSyncTime != null) {
        var lastSyncDate = DateTime.parse(constSyncTime);
        int diffDays = lastSyncDate.difference(new DateTime.now()).inDays;
        if (diffDays > 0) {
          await this._syncAppConst();
        }
      } else {
        await this._syncAppConst();
      }
    } catch (e) {
      await this._syncAppConst();
    }
  }

  getOrderStatus() async {
    final SharedPreferences prefs = await _prefs;
    try {
      orderStatusConst = jsonDecode(prefs.getString('orderStatusConst'));
    } catch (e) {
      await this._syncAppConst();
    }
    return orderStatusConst;
  }

  _syncAppConst() async {
    final SharedPreferences prefs = await _prefs;

    _appServices = AppServices(context: this.context);
    await Future.delayed(Duration(milliseconds: 500));
    QueryResult appConst = await _appServices.appConstLoad();
    if (!appConst.hasException) {
      await this._formatConst(appConst.data);
      var constUpdated = await prefs.setString('orderStatusConst', jsonEncode(orderStatusConst));
      var date = new DateTime.now();
      await prefs.setString('constSyncTime', date.toIso8601String());
      if (constUpdated) {
        return true;
      }
    }
  }

  _formatConst(dynamic constData) async {
    for (int i = 0; i < constData['appOrderConst']['orderStatus'].length; i++) {
      dynamic currentItem = constData['appOrderConst']['orderStatus'][i];
      orderStatusConst[currentItem['code']] = currentItem['order_status_id'];
    }
  }
}
