import 'package:intl/intl.dart';

class CurrencyFormat {
  String format;
  String symbol;
  NumberFormat instance;
  String decimal;
  CurrencyFormat({this.format = '##,##,###', this.symbol = '₹', this.decimal = '00'}) {
    this.instance = new NumberFormat("${this.format}.${this.decimal}", "en_US");
  }

  String formatCurrency(double value) {
    var data = instance.format(value);
    return this.symbol + '' + data.toString();
  }

  String formatToNumber(double value) {
    var data = instance.format(value);
    return data.toString();
  }
}
