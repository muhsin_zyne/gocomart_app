import 'package:device_info/device_info.dart';
import 'dart:io';

class DeviseInfo {
  String uniqueID;
  String type;
  fetchInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      type = 'ios';
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      uniqueID = iosInfo.identifierForVendor;
    }
    if (Platform.isAndroid) {
      type = 'android';
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      uniqueID = androidInfo.androidId;
    }
  }
}
