import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

class InAppNotificator {
  void showInAppNotification({message = '', color = Colors.green}) {
    showSimpleNotification(
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                AutoSizeText(
                  message,
                  presetFontSizes: [15, 14, 13, 12, 11, 10, 9, 8],
                ),
              ],
            )
          ],
        ),
      ),
      background: color,
    );
  }
}
