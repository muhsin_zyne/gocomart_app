import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

class UINotification {
  static const Duration defaultDuration = Duration(seconds: 3);
  static showInAppNotification({title: '', message = '', color = Colors.green, Duration duration}) {
    showSimpleNotification(
      Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
        child: Column(
          children: [
            title != ''
                ? Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            child: Text(title),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: AutoSizeText(
                      message,
                      minFontSize: 12,
                      maxLines: 20,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      background: color,
      duration: duration ?? UINotification.defaultDuration,
    );
  }
}
