import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/src/screens/wallet/wallet_home_screen.dart';

class ActionManager {
  BuildContext context;
  ActionManager(BuildContext context) {
    this.context = context;
  }

  void runAction(String identification) {
    switch (identification) {
      case 'invite_now':
        this._inviteNow();
        break;
      case 'test':
        this._test();
        break;
      case 'wallet_home':
        this._walletHome();
        break;
      default:
        this._updateNow();
        break;
    }
  }

  _walletHome() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => WalletHomeScreen()));
  }

  _inviteNow() {
    print("Invite Trigger");
  }

  _test() {}

  _updateNow() {}
}
