import 'dart:io';
import 'package:flutter/material.dart';
import 'package:gocomartapp/screens/error/connection_lost.dart';

class Connectivity {
  static void checkConnection(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {}
    } on SocketException catch (_) {
      Navigator.pushNamed(context, ConnectionLostScreen.id);
    }
  }
}
