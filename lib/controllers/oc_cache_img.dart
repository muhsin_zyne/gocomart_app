import 'package:flutter/cupertino.dart';

class FileCoustomData {
  String fileName;
  String fileExt;
}

class OCImageCache {
  String imgSuffix = '-100x100';

  String getCachedImage({
    @required String path,
    @required ImageQuality imageQuality,
  }) {
    if (imageQuality == ImageQuality.low) {
      imgSuffix = '-40x40';
    } else if (imageQuality == ImageQuality.med) {
      imgSuffix = '-50x50';
    } else if (imageQuality == ImageQuality.high) {
      imgSuffix = '-100x100';
    } else {
      return path;
    }

    dynamic filePathList = path.split('/');
    FileCoustomData fileData = _splitFileNameWithExt(filePathList.last);
    filePathList.removeLast();
    var newFilePath = filePathList.join('/');
    var cachedPath = _joinCustomFile(newFilePath, fileData, this.imgSuffix);
    return cachedPath;
  }

  _splitFileNameWithExt(String filename) {
    FileCoustomData fileData = FileCoustomData();
    dynamic filePathList = filename.split('.');
    fileData.fileExt = filePathList.last;
    filePathList.removeLast();
    fileData.fileName = filePathList.join('.');
    return fileData;
  }

  _joinCustomFile(String prefix, FileCoustomData fileData, String suffix) {
    return prefix + "/" + fileData.fileName + suffix + "." + fileData.fileExt;
  }
}

enum ImageQuality { low, med, high, sup, supFine }
