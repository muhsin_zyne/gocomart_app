import 'dart:convert';

import 'package:gocomartapp/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseController {
  String authToken;
  String refreshToken;
  User userData;

  Future<bool> isLoggedIn() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    try {
      authToken = _prefs.getString('auth_token');
      refreshToken = _prefs.getString('refreshToken');
      var userData = _prefs.getString('userData');
      User.fromJson(jsonDecode(userData));
      if (this.authToken != null && this.refreshToken != null) {
        return true;
      } else {
        return false;
      }
    } on NoSuchMethodError catch (e) {
      return false;
    } on Exception catch (e) {
      return false;
    }
  }
}
