import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gocomartapp/models/user.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/src/screens/auth/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'connectivity.dart';

mixin AuthManager {
  String authToken;
  User userData;

  Future connectivityCheck(BuildContext context) async {
    await new Future.delayed(const Duration(seconds: 1));
    Connectivity.checkConnection(context);
  }

  void canAccess(BuildContext context, bool forceLogin) async {
    connectivityCheck(context);
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    authToken = _prefs.getString('auth_token');
    var userData = _prefs.getString('userData');
    if (userData != null) {
      User.fromJson(jsonDecode(userData));
      if (this.authToken == null && forceLogin == true) {
        Navigator.pushNamed(context, LoginScreen.id);
        // error pass
      } else {
        // account validation check
        if (forceLogin == false) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomeScreen(),
            ),
          );
        }
      }
    } else {
      print("login data is null now");
    }
  }
}

class AuthData {
  String authToken;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  loadAuthToken() async {
    final SharedPreferences prefs = await _prefs;
    try {
      authToken = prefs.getString('auth_token');
    } catch (e) {
      authToken = '';
    }
  }
}
