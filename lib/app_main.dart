import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/core/PushNotificationData.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/providers/app_provider.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/category_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/providers/product_provider.dart';
import 'package:gocomartapp/providers/screen_caching_provider.dart';
import 'package:gocomartapp/providers/shopping_home_provider.dart';
import 'package:gocomartapp/providers/user_profile_provider.dart';
import 'package:gocomartapp/screens/empty_page.dart';
import 'package:gocomartapp/screens/error/connection_lost.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/screens/profile/address/address_screen.dart';
import 'package:gocomartapp/screens/profile/invite_earn.dart';
import 'package:gocomartapp/screens/profile/order/order_status.dart';
import 'package:gocomartapp/screens/profile/order/orders_screen.dart';
import 'package:gocomartapp/screens/profile/profile_home/profile_screen.dart';
import 'package:gocomartapp/screens/profile/rating_review.dart';
import 'package:gocomartapp/screens/splash/splash_screen.dart';
import 'package:gocomartapp/screens/wallet/cash_wallet/cash_wallet_offer.dart';
import 'package:gocomartapp/screens/wallet/test_page.dart';
import 'package:gocomartapp/src/controllers/page_dynamic_route_controller.dart';
import 'package:gocomartapp/src/screens/auth/login_screen.dart';
import 'package:gocomartapp/src/screens/auth/otp_verification_screen.dart';
import 'package:gocomartapp/src/screens/tests/test_screen.dart';
import 'package:gocomartapp/src/screens/wallet/cashwallet/cash_wallet_rewards.dart';
import 'package:intl/intl.dart';
import 'package:load/load.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

import 'controllers/curency_format.dart';
import 'src/screens/wallet/wallet_home_screen.dart';

export 'theme/hexcolor.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    print('MyApp starts now | ${DateTime.now()} ');
    this._loadOneSignalConnectedServices();
    super.initState();
  }

  void _loadOneSignalConnectedServices() async {
    /// on Notification Recived handler
    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      print("you have recived a notification");
      print("\n \n \n \n \n ");
      print(notification.jsonRepresentation());
      print("\n \n \n \n \n ");
      print(notification.payload.additionalData);
      //TODO need to add a new concept
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) async {
      var notificationData = result.notification.payload.additionalData;
      final PushNotificationData pushNotificationData = PushNotificationData.fromJSON(notificationData);
      if (pushNotificationData.type == PushNotificationTypeConst.NAVIGATOR) {
        showLoadingDialog();
        await Future.delayed(Duration(seconds: 1));
        print(pushNotificationData.route.params.getArguments());
        if (navigatorKey.currentState.canPop()) {
          PageDynamicRouteController.pushReplacementNamed(
            navigatorKey,
            pushNotificationData.route.screenId,
            pushNotificationData.route.params.getArguments(),
          );
        } else {
          PageDynamicRouteController.pushNamed(
            navigatorKey,
            pushNotificationData.route.screenId,
            pushNotificationData.route.params.getArguments(),
          );
        }
        hideLoadingDialog();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
      child: LoadingProvider(
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider(
              create: (_) => GlobalProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => ProductProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => CartProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => AddressProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => AppProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => CategoryProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => UserProfileProvider(),
              child: ProfileScreen(),
            ),
            ChangeNotifierProvider(
              create: (_) => ShoppingHomeProvider(),
            ),
            ChangeNotifierProvider(
              create: (_) => ScreenCachingProvider(),
            ),
          ],
          child: Consumer<GlobalProvider>(
            builder: (context, auth, _) {
              return MaterialApp(
                localizationsDelegates: translator.delegates,
                locale: translator.locale,
                supportedLocales: translator.locals(),
                navigatorKey: navigatorKey,
                title: getTranslate('appTitle'),
                theme: ThemeData(
                  primaryColor: Color(0xff3b5998),
                  fontFamily: 'Product-Sans',
                  //platform: TargetPlatform.iOS,
                ),
                debugShowCheckedModeBanner: false,
                initialRoute: SplashScreen.id,
                routes: {
                  SplashScreen.id: (context) => SplashScreen(),
                  LoginScreen.id: (context) => LoginScreen(),
                  TestPage.id: (context) => TestPage(),
                  HomeScreen.id: (context) => HomeScreen(),
                  WalletHomeScreen.id: (context) => WalletHomeScreen(),
                  CashWalletRewardsScreen.id: (context) => CashWalletRewardsScreen(),
                  ProfileScreen.id: (context) => ProfileScreen(),
                  OtpVerificationScreen.id: (context) => OtpVerificationScreen(),
                  EmptyPage.id: (context) => EmptyPage(),
                  ConnectionLostScreen.id: (context) => ConnectionLostScreen(),
                  CashWalletOfferScreen.id: (context) => CashWalletOfferScreen(),
                  AddressScreen.id: (context) => AddressScreen(),
                  OrdersScreen.id: (context) => OrdersScreen(),
                  RatingReview.id: (context) => RatingReview(),
                  InviteScreen.id: (context) => InviteScreen(),
                  TestScreenNew.id: (context) => TestScreenNew(),
                  OrderStatus.id: (context) => OrderStatus(),
                },
              );
            },
          ),
        ),
      ),
    );
  }
}

formatTime(timeStamp, {format}) {
  var stdDateTime = new DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  return new DateFormat("dd LLL, yyyy").add_jm().format(stdDateTime);
}

dateFormat({format = 'LLL dd, yyyy', type = 'DateTime', String dateTime, timeStamp}) {
  var stdDateTime;
  if (type == 'DateTime') {
    stdDateTime = DateTime.parse(dateTime);
  } else {
    stdDateTime = new DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  }
  return new DateFormat(format).format(stdDateTime);
}

discountPercentage(var orgPrice, var offerPrice) {
  var percentage = ((orgPrice - offerPrice) / orgPrice) * 100;
  return percentage.round();
}

String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

String getTranslate(String key) {
  try {
    return translator.translate(key);
  } on Exception catch (e) {
    return key;
  }
}

typedef void RatingCallback(double rating);

class RatingBar extends StatefulWidget {
  RatingBar({
    Key key,
    this.maxRating = 5,
    @required this.onRatingChanged,
    @required this.filledIcon,
    @required this.emptyIcon,
    this.halfFilledIcon,
    this.isHalfAllowed = false,
    this.initialRating = 0.0,
    this.filledColor,
    this.emptyColor = Colors.grey,
    this.halfFilledColor,
    this.size = 40,
  })  : _readOnly = false,
        assert(maxRating != null),
        assert(initialRating != null),
        assert(filledIcon != null),
        assert(emptyIcon != null),
        assert(isHalfAllowed != null),
        assert(!isHalfAllowed || halfFilledIcon != null),
        assert(size != null),
        super(key: key);

  RatingBar.readOnly({
    Key key,
    this.maxRating = 5,
    @required this.filledIcon,
    @required this.emptyIcon,
    this.halfFilledIcon,
    this.isHalfAllowed = false,
    this.initialRating = 0.0,
    this.filledColor,
    this.emptyColor = Colors.grey,
    this.halfFilledColor,
    this.size = 40,
  })  : _readOnly = true,
        onRatingChanged = null,
        assert(maxRating != null),
        assert(initialRating != null),
        assert(filledIcon != null),
        assert(emptyIcon != null),
        assert(isHalfAllowed != null),
        assert(!isHalfAllowed || halfFilledIcon != null),
        assert(size != null),
        super(key: key);

  final int maxRating;
  final IconData filledIcon;
  final IconData emptyIcon;
  final IconData halfFilledIcon;
  final RatingCallback onRatingChanged;
  final double initialRating;
  final Color filledColor;
  final Color emptyColor;
  final Color halfFilledColor;
  final double size;
  final bool isHalfAllowed;
  final bool _readOnly;

  @override
  _RatingBarState createState() {
    return _RatingBarState();
  }
}

class _RatingBarState extends State<RatingBar> {
  double _currentRating;

  @override
  void initState() {
    super.initState();
    if (widget.isHalfAllowed) {
      _currentRating = widget.initialRating;
    } else {
      _currentRating = widget.initialRating.roundToDouble();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: List.generate(widget.maxRating, (index) {
          return Builder(
            builder: (rowContext) => widget._readOnly ? buildIcon(context, index + 1) : buildStar(rowContext, index + 1),
          );
        }),
      ),
    );
  }

  Widget buildIcon(BuildContext context, int position) {
    IconData iconData;
    Color color;
    double rating;
    if (widget._readOnly) {
      if (widget.isHalfAllowed) {
        rating = widget.initialRating;
      } else {
        rating = widget.initialRating.roundToDouble();
      }
    } else {
      rating = _currentRating;
    }
    if (position > rating + 0.5) {
      iconData = widget.emptyIcon;
      color = widget.emptyColor ?? Colors.grey;
    } else if (position == rating + 0.5) {
      iconData = widget.halfFilledIcon;
      color = widget.halfFilledColor ?? widget.filledColor ?? Theme.of(context).primaryColor;
    } else {
      iconData = widget.filledIcon;
      color = widget.filledColor ?? Theme.of(context).primaryColor;
    }
    return Icon(iconData, color: color, size: widget.size);
  }

  Widget buildStar(BuildContext context, int position) {
    return GestureDetector(
      child: buildIcon(context, position),
      onTap: () {
        setState(() => _currentRating = position.toDouble());
        widget?.onRatingChanged(_currentRating);
      },
      onHorizontalDragUpdate: (details) {
        RenderBox renderBox = context.findRenderObject();
        var localPosition = renderBox.globalToLocal(details.globalPosition);
        var rating = localPosition.dx / widget.size;

        if (rating < 1) {
          rating = 1;
        } else if (rating > widget.maxRating) {
          rating = widget.maxRating.toDouble();
        } else {
          rating = widget.isHalfAllowed ? (2 * rating).ceilToDouble() / 2 : rating.ceilToDouble();
        }

        if (_currentRating != rating) {
          setState(() => _currentRating = rating);
          widget?.onRatingChanged(_currentRating);
        }
      },
    );
  }
}

Future<bool> onBackPressed(context) {
  return showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text('Are you sure?'),
          content: new Text('Do you want to exit an App'),
          actions: <Widget>[
            new GestureDetector(
              onTap: () => Navigator.of(context).pop(false),
              child: Text("NO"),
            ),
            SizedBox(height: 16),
            new GestureDetector(
              onTap: () => Navigator.of(context).pop(true),
              child: Text("YES"),
            ),
          ],
        ),
      ) ??
      false;
}

generateAuthHash(String data) {
  var key = utf8.encode(FlavorConfig.instance.flavorValues.oneSignalRestApiKey);
  var hmacSha256 = Hmac(sha256, key);
  var digest = hmacSha256.convert(utf8.encode(data));
  return digest;
}

CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
