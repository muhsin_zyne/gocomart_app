import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/theme/hexcolor.dart';

Icon getIcon({@required string, @required String hexColor}) {
  IconData iconData;
  switch (string) {
    case "phone_book":
      iconData = FontAwesomeIcons.addressBook;
      break;

    case "order":
      iconData = Icons.receipt;
      break;

    case "like":
      iconData = Icons.thumb_up;
      break;

    case "invite_earn":
      iconData = FontAwesomeIcons.paperPlane;
      break;

    default:
  }
  return Icon(
    iconData,
    color: Color(getColor(hexColor: hexColor)),
  );
}
