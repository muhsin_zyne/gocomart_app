import 'package:flutter/material.dart';

import 'package:auto_size_text/auto_size_text.dart';

class ScratchCardDetailItem extends StatelessWidget {
  final Function onTap;
  final FadeInImage image;
  final IconData iconData;
  final String scratchedValue;
  final String offerText;
  final String offerProvider;
  final bool isScratched;
  final Color unScratchedColor;
  final bool isDetails;
  final double priceFontSize;
  final double iconDataSize;
  final String heroTag;
  final String cdnPoint;
  final String texturePng;
  final String cardDisabledText;
  final bool locationDisabled;
  ScratchCardDetailItem({
    @required this.image,
    this.onTap,
    this.iconData,
    @required this.scratchedValue,
    this.offerProvider,
    this.offerText,
    this.unScratchedColor,
    this.isScratched = false,
    this.isDetails = false,
    this.priceFontSize,
    this.iconDataSize,
    this.heroTag,
    this.cdnPoint,
    this.texturePng,
    this.cardDisabledText = '',
    this.locationDisabled = false,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Container(
        height: MediaQuery.of(context).size.width * .76,
        width: MediaQuery.of(context).size.width * .76,
        child: Stack(
          children: <Widget>[
            Positioned(
              bottom: 35,
              width: MediaQuery.of(context).size.width * .76,
              child: Container(
                margin: EdgeInsets.all(30),
                padding: EdgeInsets.all(30),
                child: image,
              ),
            ),
            Positioned(
              bottom: 20,
              width: MediaQuery.of(context).size.width * .76,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          iconData,
                          size: iconDataSize != null ? iconDataSize : 12,
                          color: Colors.black,
                        ),
                        AutoSizeText(
                          scratchedValue,
                          presetFontSizes: [26, 28, 30],
                          maxLines: 1,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    AutoSizeText(
                      offerText,
                      presetFontSizes: [12, 14, 16, 18],
                      maxLines: 1,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: AutoSizeText(
                        offerProvider,
                        presetFontSizes: [12, 14, 16, 18],
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                  ],
                ),
              ),
            ),
            (isScratched == false && locationDisabled == true)
                ? Positioned.fill(
                    child: Container(
                      color: unScratchedColor,
                      child: Image.asset('assets/images/texture/$texturePng'),
                    ),
                  )
                : Container(),
            (isScratched == false && locationDisabled == true)
                ? Positioned(
                    bottom: 20,
                    child: Container(
                      width: MediaQuery.of(context).size.width * .76,
                      height: 40,
                      color: Colors.black.withOpacity(0.4),
                      child: AutoSizeText(
                        cardDisabledText,
                        textAlign: TextAlign.center,
                        presetFontSizes: [16, 15, 14, 13, 12],
                        style: TextStyle(color: Colors.white.withOpacity(0.7)),
                      ),
                    ),
                  )
                : Container(),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: onTap,
                child: Container(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
