import 'package:flutter/material.dart';

import 'package:auto_size_text/auto_size_text.dart';

class ScratchCardListItem extends StatelessWidget {
  final Function onTap;
  final Image image;
  final IconData iconData;
  final String scratchedValue;
  final String offerText;
  final String offerProvider;
  final bool isScratched;
  final Color unScratchedColor;
  final bool isDetails;
  final double priceFontSize;
  final double iconDataSize;
  final String texturePng;
  final String cdnPoint;
  final String heroTag;

  ScratchCardListItem({
    @required this.image,
    this.onTap,
    this.iconData,
    @required this.scratchedValue,
    this.offerProvider,
    this.offerText,
    this.unScratchedColor,
    this.isScratched = false,
    this.isDetails = false,
    this.priceFontSize,
    this.iconDataSize,
    this.texturePng,
    this.cdnPoint,
    this.heroTag,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Container(
        child: Hero(
          tag: heroTag,
          child: Stack(
            children: <Widget>[
              Container(),
              isScratched
                  ? Positioned(
                      width: MediaQuery.of(context).size.width * 0.40,
                      bottom: 30,
                      child: Container(
                        margin: EdgeInsets.all(5.0),
                        padding: EdgeInsets.all(15.0),
                        child: image,
                      ),
                    )
                  : Container(
                      color: unScratchedColor ?? Colors.blue.shade300,
                      child: Image.asset('assets/images/texture/$texturePng'),
                    ),
              isScratched
                  ? Positioned(
                      bottom: 10.0,
                      width: MediaQuery.of(context).size.width * 0.40,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  iconData,
                                  size:
                                      iconDataSize != null ? iconDataSize : 12,
                                  color:
                                      isScratched ? Colors.black : Colors.white,
                                ),
                                AutoSizeText(
                                  scratchedValue,
                                  presetFontSizes: [12, 14, 16],
                                  maxLines: 1,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1,
                            ),
                            AutoSizeText(
                              offerText,
                              presetFontSizes: [10, 12, 14, 16, 18],
                              maxLines: 1,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: AutoSizeText(
                                offerProvider,
                                presetFontSizes: [10, 12, 14, 16, 18],
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: onTap,
                  child: Container(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
