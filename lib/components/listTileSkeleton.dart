import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ListTileSkeleton extends StatelessWidget {
  const ListTileSkeleton(
      {Key key, @required this.width, @required this.boxImage})
      : super(key: key);

  final double width;
  final bool boxImage;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SkeletonAnimation(
          child: boxImage
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    color: Colors.grey[300],
                    height: width / 7.5,
                    width: width / 8,
                  ),
                )
              : CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.grey[300],
                ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SkeletonAnimation(
                child: Container(
                  height: 10,
                  width: width / 5,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: SkeletonAnimation(
                  child: Container(
                    height: 10,
                    width: width / 2.5,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
