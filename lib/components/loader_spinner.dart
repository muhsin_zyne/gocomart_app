import 'package:flutter/material.dart';
import 'package:load/load.dart';

class LoaderSpinner {
  activateLoader() async {
    showCustomLoadingWidget(
      Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
      tapDismiss: false,
    );
  }

  dismiss() async {
    hideLoadingDialog();
    return true;
  }
}
