import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SwipeCard extends StatefulWidget {
  // class named parameters
  final Color color;
  final String cardLabel;
  final String cardNumber;
  final String nameOnCard;
  final String valid;
  final String cardValue;
  final bool enabled;
  final String cardID;
  // local parameter

  SwipeCard({
    this.color = Colors.white,
    this.valid,
    this.cardLabel,
    this.cardNumber,
    this.nameOnCard,
    this.cardValue = '',
    this.cardID = '',
    this.enabled = false,
  });

  @override
  _SwipeCardState createState() => _SwipeCardState();
}

class _SwipeCardState extends State<SwipeCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
      child: Stack(
        children: <Widget>[
          Container(
            height: 170,
            width: MediaQuery.of(context).size.width * .7,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/card_bg.jpg'),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          Positioned(
            child: Container(
              decoration: BoxDecoration(
                color: widget.enabled ? widget.color.withOpacity(0.4) : widget.color.withOpacity(0.2),
                borderRadius: BorderRadius.circular(20),
              ),
              height: 170,
              width: MediaQuery.of(context).size.width * .7,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * .7,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        'assets/images/goco_card_logo.png',
                        scale: 5,
                      ),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * .23,
                            child: AutoSizeText(
                              widget.cardLabel,
                              presetFontSizes: [18, 16, 14, 12, 10],
                              maxLines: 1,
                              style: TextStyle(
                                color: Color(0xfff4f4f4),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .23,
                            child: AutoSizeText(
                              widget.cardValue,
                              presetFontSizes: [24, 23, 22, 21, 20, 19, 18, 17],
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xfff4f4f4),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * .49,
                      child: AutoSizeText(
                        widget.cardID,
                        presetFontSizes: [22, 21, 20, 19, 18, 17],
                        maxLines: 1,
                        style: TextStyle(
                          color: Color(0xfff4f4f4),
                          letterSpacing: 10,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * .52,
                      child: AutoSizeText(
                        widget.nameOnCard,
                        presetFontSizes: [12, 11, 10, 9],
                        maxLines: 1,
                        style: TextStyle(
                          color: Color(0xfff4f4f4),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SwipeCardSkeletonAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
      child: Stack(
        children: <Widget>[
          Container(
            height: 140,
            width: MediaQuery.of(context).size.width * .7,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(.4),
                borderRadius: BorderRadius.circular(20),
              ),
              height: 140,
              width: MediaQuery.of(context).size.width * .7,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * .7,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 90,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 1,
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class RadiusMenuSkeletonAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SkeletonAnimation(
          child: CircleAvatar(
            radius: 30,
            backgroundColor: Colors.grey[300],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        SkeletonAnimation(
          child: Container(
            height: 10,
            width: 50,
            decoration: BoxDecoration(
              color: Colors.grey[300],
            ),
          ),
        ),
      ],
    );
  }
}

class TransactionSkeletonAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: SkeletonAnimation(
              child: Container(
                height: 20,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Expanded(
          flex: 9,
          child: Container(
            height: 60,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 5,
                ),
                SkeletonAnimation(
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                SkeletonAnimation(
                  child: Container(
                    height: 10,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
