import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class PriceMetaLabel extends StatelessWidget {
  const PriceMetaLabel({
    Key key,
    @required this.width,
    this.labelItem = '',
    this.labelValue = '',
    this.labelColor = Colors.black,
    this.fontWeight = FontWeight.normal,
  }) : super(key: key);

  final double width;
  final String labelItem;
  final String labelValue;
  final Color labelColor;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(top: 8),
          width: width / 1.8,
          child: AutoSizeText(
            labelItem,
            presetFontSizes: [16, 15, 14, 13, 12, 11],
            style: TextStyle(color: this.labelColor, fontWeight: fontWeight),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 8, top: 8),
          child: AutoSizeText(
            labelValue,
            presetFontSizes: [16, 15, 14, 13, 12, 11],
            style: TextStyle(color: this.labelColor, fontWeight: fontWeight),
          ),
        ),
      ],
    );
  }
}
