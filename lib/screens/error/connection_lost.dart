import 'package:flutter/material.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/main.dart' as main;

class ConnectionLostScreen extends StatefulWidget {
  static const String id = 'connection_lost_screen';
  @override
  _ConnectionLostScreenState createState() => _ConnectionLostScreenState();
}

class _ConnectionLostScreenState extends State<ConnectionLostScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: GocoMartAppTheme.buildLightTheme().primaryColor,
          ),
          onPressed: () {
            Navigator.pop(context, () {
              setState(() {});
            });
          },
        ),
        iconTheme: new IconThemeData(
          color: GocoMartAppTheme.buildLightTheme().primaryColor,
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(''),
      ),
      body: PageContent(),
    );
  }
}

class PageContent extends StatelessWidget {
  void actionRestartApp() {
    main.main();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * .1,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Image.asset(
              'assets/images/no_internet.gif',
              width: 300,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              'No Internet Connection',
              style: TextStyle(
                color: Color(0xff8d9293),
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontFamily: 'Lato',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: Text(
              'Please check your internet settings!',
              style: TextStyle(
                color: Color(0xff8d9293),
                fontSize: 16,
                fontFamily: 'Lato',
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .1,
          ),
          FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white),
            ),
            onPressed: () {
              actionRestartApp();
            },
            color: GocoMartAppTheme.buildLightTheme().primaryColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
              child: Text(
                'Reload',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Lato',
                  fontSize: 22,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
