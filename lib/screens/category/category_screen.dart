import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/models/category/appCategories.dart';
import 'package:gocomartapp/models/category/category.dart';
import 'package:gocomartapp/providers/category_provider.dart';
import 'package:gocomartapp/screens/category/widgets/child_categories.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:graphql/client.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Categories',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: CategoryContent(),
    );
  }
}

class CategoryContent extends StatefulWidget {
  @override
  _CategoryContentState createState() => _CategoryContentState();
}

class _CategoryContentState extends State<CategoryContent> {
  HtmlUnescape _htmlUnescape = HtmlUnescape();

  CategoryProvider _categoryProvider;
  AppServices _appServices;
  bool pageReady = false;
  AppCategories _appCategories;
  int _selectedCategoryId = 0;
  Category _selectedCategory;

  @override
  void initState() {
    _appServices = AppServices(context: context);
    super.initState();
    this._loadClient();
  }

  @override
  void didChangeDependencies() {
    //_cartProvider = Provider.of<CartProvider>(context);
    _categoryProvider = Provider.of<CategoryProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    this.networkFetch();
  }

  void networkFetch() async {
    setState(() {
      _appCategories = _categoryProvider.appCategories;
    });
    this._setFistCategoryAsSelected();

    if (_appCategories != null) {
      setState(() {
        pageReady = true;
      });
    }
    QueryResult appCategoriesResult = await _appServices.appCategories();
    if (!appCategoriesResult.hasException) {
      _categoryProvider.appCategories = AppCategories.fromJson(appCategoriesResult.data['appCategories']);
      setState(() {
        this._appCategories = _categoryProvider.appCategories;
      });
      this._setFistCategoryAsSelected();
      setState(() {
        pageReady = true;
      });
    }
  }

  void _setFistCategoryAsSelected() {
    if (_appCategories?.categories != null) {
      setState(() {
        if (_categoryProvider.selectedCategoryId != null) {
          _selectedCategoryId = _categoryProvider.selectedCategoryId;
          _selectedCategory = _categoryProvider.selectedCategory;
        } else {
          _selectedCategoryId = _appCategories?.categories[0]?.categoryId ?? 0;
          _selectedCategory = _appCategories?.categories[0];
        }
      });
    }
  }

  void _selectSideCategory(Category onSelectedCategory) {
    setState(() {
      _selectedCategoryId = onSelectedCategory.categoryId;
      _categoryProvider.selectedCategoryId = _selectedCategoryId;
      _categoryProvider.selectedCategory = onSelectedCategory;
      _selectedCategory = onSelectedCategory;
    });
  }

  @override
  Widget build(BuildContext context) {
    return pageReady
        ? Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    color: Color(0xfff6f6f6),
                    child: Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Color(0xfff6f6f6),
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemBuilder: (BuildContext context, int index) {
                          Category baseCategory = this._appCategories.categories[index];
                          return Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    left: BorderSide(
                                      width: _selectedCategoryId == baseCategory.categoryId ? 6 : 0,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  color: _selectedCategoryId == baseCategory.categoryId ? Colors.white : Color(0xfff6f6f6),
                                ),
                                margin: EdgeInsets.only(top: 2),
                                child: Container(
                                  margin: EdgeInsets.only(left: 5, top: 5, right: 2),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          child: Center(
                                            child: AutoSizeText(
                                              _htmlUnescape.convert(baseCategory.description.name) ?? '',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                              ),
                                            ),
                                          ),
                                          height: 60,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Positioned.fill(
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      _selectSideCategory(baseCategory);
                                    },
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                        itemCount: _appCategories?.categories?.length ?? 0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    color: Colors.white,
                    child: ChildCategoryExpandable(
                      selectedCategory: _selectedCategory,
                    ),
                  ),
                )
              ],
            ),
          )
        : loaderBlock();
  }

  Widget loaderBlock() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
