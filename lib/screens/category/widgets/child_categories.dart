import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/models/category/category.dart';
import 'package:gocomartapp/providers/category_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/product-list/product_list_screen.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';

class ChildCategoryExpandable extends StatefulWidget {
  const ChildCategoryExpandable({Key key, @required this.selectedCategory}) : super(key: key);
  final Category selectedCategory;
  @override
  _ChildCategoryExpandableState createState() => _ChildCategoryExpandableState();
}

class _ChildCategoryExpandableState extends State<ChildCategoryExpandable> {
  CategoryProvider _categoryProvider; // ignore: unused_field
  GlobalProvider _globalProvider;
  HtmlUnescape _htmlUnescape = HtmlUnescape();
  @override
  void didChangeDependencies() {
    _categoryProvider = Provider.of<CategoryProvider>(context, listen: true);
    _globalProvider = Provider.of<GlobalProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  void _categorySelected(Category selectedCategory) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductListScreen(
          title: _htmlUnescape.convert(selectedCategory.description.name.toString()),
          categoryId: selectedCategory.categoryId,
        ),
      ),
    );
  }

  Widget _generateWrapItem(Category item) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * .2,
          child: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width * .29,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: CachedNetworkImage(
                        imageUrl: "${_globalProvider.cdnImagePoint}${item.image}",
                        width: MediaQuery.of(context).size.width * .2,
                        placeholder: (context, url) => ImagePreLoader(),
                        errorWidget: (context, url, error) => ImageError(),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              _htmlUnescape.convert(item.description.name),
                              minFontSize: 8,
                              maxFontSize: 16,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned.fill(
            child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              _categorySelected(item);
            },
          ),
        ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        Category sChildCategory = this.widget.selectedCategory.children[index];
        List<Widget> childList = [];
        Widget banner = Container();
        sChildCategory.children.forEach((Category childElement) {
          childList.add(this._generateWrapItem(childElement));
        });
        if (sChildCategory.banner != null) {
          banner = Container(
            width: MediaQuery.of(context).size.width * .67,
            child: Stack(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: "${_globalProvider.cdnImagePoint}${sChildCategory.banner}",
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Center(
                    child: ImagePreLoader(),
                  ),
                  errorWidget: (context, url, error) => Container(
                    child: ImageError(),
                  ),
                ),
                Positioned.fill(
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        _categorySelected(sChildCategory);
                      },
                    ),
                  ),
                )
              ],
            ),
          );
        }

        return ExpansionTile(
          initiallyExpanded: true,
          title: AutoSizeText(
            _htmlUnescape.convert(sChildCategory.description.name),
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * .035,
            ),
          ),
          children: <Widget>[
            banner,
            Container(
              child: Wrap(
                //alignment: WrapAlignment.end,
                spacing: 10,
                children: childList,
              ),
            )
          ],
        );
      },
      itemCount: this.widget.selectedCategory?.children?.length ?? 0,
    );
  }
}
