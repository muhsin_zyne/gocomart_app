import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ScratchCardSkeletonAnimation extends StatelessWidget {
  const ScratchCardSkeletonAnimation({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Stack(
        children: <Widget>[
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: Container(
              color: Colors.grey,
            ),
            period: Duration(milliseconds: 800),
          ),
          Positioned.fill(
            child: Image.asset('assets/images/texture/texture_01.png'),
          )
        ],
      ),
    );
  }
}

class ProductListSkeletonAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Stack(
        children: <Widget>[
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: Container(
              color: Colors.grey,
            ),
            period: Duration(milliseconds: 800),
          ),
          Positioned.fill(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      LabelSkeleton(
                        width: MediaQuery.of(context).size.width / 3,
                        height: MediaQuery.of(context).size.width / 4,
                        color: Colors.grey.withOpacity(.5),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned.fill(
              child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                LabelSkeleton(
                  width: 150,
                  height: 15,
                  color: Colors.grey.withOpacity(.5),
                ),
                SizedBox(
                  height: 10,
                ),
                LabelSkeleton(
                  width: 60,
                  height: 10,
                  color: Colors.grey.withOpacity(.5),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: LabelSkeleton(
                    width: 100,
                    height: 15,
                    color: Colors.grey.withOpacity(.5),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}

class LabelSkeleton extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  const LabelSkeleton({
    Key key,
    this.width = 100,
    this.height = 10,
    this.color = Colors.grey,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: width,
          height: height,
          child: SkeletonAnimation(
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: color,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
