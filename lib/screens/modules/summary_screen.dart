// import 'package:flutter/material.dart';
// import 'package:auto_size_text/auto_size_text.dart';
// import 'package:gocomart/screens/modules/payment_method_screen.dart';
// import 'package:gocomart/services/shopping_services.dart';
// import 'package:graphql/client.dart';
// import 'package:gocomart/models/address/get_address.dart';
// import 'package:gocomart/screens/modules/address_add_screen.dart';
// import 'package:gocomart/providers/address_provider.dart';
// import 'package:provider/provider.dart';
// import 'package:gocomart/providers/global_provider.dart';

// // ignore: must_be_immutable
// class SummaryScreen extends StatefulWidget {
//   var products = [];

//   SummaryScreen({@required productIds}) {
//     products = productIds;
//   }

//   @override
//   _SummaryScreenState createState() => _SummaryScreenState();
// }

// class _SummaryScreenState extends State<SummaryScreen> {
//   ShoppingServices _shoppingServices;

//   GetAddress _address;
//   AddressProvider _addressProvider;
//   GlobalProvider _globalProvider;

//   bool firstLoading = true;
//   bool addressLoading=true;
//   @override
//   void initState() {

//     super.initState();

//     loadClient();
//   }

//   loadClient() async {
//     print("load client");
//     _shoppingServices = ShoppingServices(context: context);

//     await Future.delayed(Duration(microseconds: 10));
//     _globalProvider = Provider.of<GlobalProvider>(context);
//     _addressProvider = Provider.of<AddressProvider>(context);
//     print("---------------------object");
//     QueryResult addressResult = await _shoppingServices.getAddress();
//     if (!addressResult.hasErrors) {
//       setState(() {
//         _address = GetAddress.fromJson(addressResult.data['getAddress']);
//         addressLoading=false;
//       });

//       if (_address.address.length != 0) {
//         print("nulladdressError");
//         _addressProvider.nullAddress=false;
//       }
//       print(_address.address[0].address_1);

//       if (firstLoading == true) {
//         _addressProvider.currentAddress=_address.address[0];
//       }
//     }
//     // build(context);
//     return true;
//   }

//   @override
//   Widget build(BuildContext context) {
//     print(widget.products);

//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Summary"),
//       ),
//       body:_addressProvider?.isReady==true? body(context):Container(
//         child: Center(
//           child: CircularProgressIndicator(),
//         ),
//       ),
//       // SummaryScreenBody(
//       //   productIds: products,
//       // ),
//       bottomSheet: Row(
//         //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         mainAxisAlignment: MainAxisAlignment.end,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Expanded(
//             flex: 6,
//             child: Container(
//                 padding: EdgeInsets.only(left: 20, top: 5),
//                 height: MediaQuery.of(context).size.width / 6,
//                 width: MediaQuery.of(context).size.width / 2,
//                 //color: Colors.yellow[900],
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     AutoSizeText(
//                       "Total ",
//                       style: TextStyle(
//                           color: Colors.grey[600],
//                           fontSize: MediaQuery.of(context).size.width / 20,
//                           fontWeight: FontWeight.w600),
//                       maxLines: 2,
//                     ),
//                     AutoSizeText(
//                       "₹1888888",
//                       style: TextStyle(
//                           color: Colors.grey[700],
//                           fontSize: MediaQuery.of(context).size.width / 15,
//                           fontWeight: FontWeight.w700),
//                       maxLines: 2,
//                     ),
//                   ],
//                 )),
//           ),
//           Expanded(
//             flex: 4,
//             child: InkWell(
//               onTap: _addressProvider?.nullAddress==false
//                   ? () {
//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                             builder: (context) => PaymentMethodScreen(
//                                   productIds: widget.products,
//                                   shipping_id: _addressProvider.currentAddress.address_id,
//                                 )),
//                       );
//                     }
//                   : null,
//               child: Container(
//                   height: MediaQuery.of(context).size.width / 6,
//                   width: MediaQuery.of(context).size.width / 2,
//                   color: _addressProvider?.nullAddress==true
//                       ? Colors.grey
//                       : Theme.of(context).primaryColor,
//                   child: Center(
//                       child: AutoSizeText(
//                     "BUY NOW",
//                     style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 18,
//                         fontWeight: FontWeight.w700),
//                     maxLines: 1,
//                   ))),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   body(context) {
//     double width = MediaQuery.of(context).size.width;

//     return SingleChildScrollView(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Container(
//             margin: EdgeInsets.only(top: 20, left: 10, right: 10),
//             //height: MediaQuery.of(context).size.width/3.2,
//             //width: MediaQuery.of(context).size.width / 1.111,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(5),
//                 border: Border.all(color: Colors.grey[400])),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: <Widget>[
//                 Container(
//                   padding: EdgeInsets.only(top: 10, left: 10),
//                   child: Text(
//                     "Shipping Address",
//                     style: TextStyle(
//                         fontSize: width / 20,
//                         letterSpacing: .03,
//                         color: Theme.of(context).primaryColor,
//                         fontWeight: FontWeight.w400),
//                   ),
//                 ),
//                 Divider(
//                   thickness: 1,
//                   color: Colors.grey[400],
//                 ),
//                 //address pading
//                 Container(
//                   padding: EdgeInsets.only(left: 10, bottom: 10),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Container(
//                         height: width / 15,
//                         child: !_addressProvider.nullAddress
//                             ? AutoSizeText(
//                                 "${_addressProvider.currentAddress.name }",
//                                 presetFontSizes: [22, 20, 18, 17, 16],
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.w500,
//                                     color: Colors.black54),
//                               )
//                             : Container(
//                                 child: Text("No address"),
//                               ),
//                       ),
//                       Container(
//                         width: width / 1.3,
//                         child: !_addressProvider.nullAddress
//                             ? AutoSizeText(
//                                 "${_addressProvider.currentAddress.address_1}, ${_addressProvider.currentAddress.address_2},\n${_addressProvider.currentAddress.postcode}, ${_addressProvider.currentAddress.city},${_addressProvider.currentAddress.landmark}\n ${_addressProvider.currentAddress.mobile_number}\n ${_addressProvider.currentAddress.alt_contact} ",
//                                 maxLines: null,
//                                 style: TextStyle(
//                                     fontSize: width / 26,
//                                     color: Colors.grey[600]),
//                               )
//                             : Container(),
//                       ),
//                     ],
//                   ),
//                 ),

//                 Row(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       margin: EdgeInsets.only(left: width / 3.5, bottom: 7),
//                       height: width / 13,
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(5),
//                         color: Theme.of(context).primaryColor,
//                       ),
//                       child: FlatButton(
//                         onPressed: () async {
//                           // addAddressAlertDialog(context);

//                           Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => AddressAddScreen(
//                                       )));
//                         },
//                         child: Text(
//                           "Add",
//                           style: TextStyle(
//                             color: Colors.white,
//                           ),
//                         ),
//                       ),
//                     ),
//                     !_addressProvider.nullAddress
//                         ? Container(
//                             margin: EdgeInsets.only(
//                               left: 10,
//                             ),
//                             height: width / 13,
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(5),
//                               color: Theme.of(context).primaryColor,
//                             ),
//                             child: FlatButton(
//                               onPressed: () async {
//                                 showDialog(
//                                     context: context,
//                                     barrierDismissible: true,
//                                     builder: (context) {
//                                       return AlertDialog(
//                                         shape: RoundedRectangleBorder(
//                                           borderRadius: BorderRadius.all(
//                                             Radius.circular(10.0),
//                                           ),
//                                         ),
//                                         content: CircularProgressIndicator(),
//                                       );
//                                     });
//                                 bool response = await loadClient();
//                                 //await Future.delayed(Duration(seconds: 1));

//                                 if (response == true) {
//                                   Navigator.pop(context);
//                                   print(_address.address.length);
//                                   showAlertDialog(context);
//                                 }
//                               },
//                               child: Text(
//                                 "Change",
//                                 style: TextStyle(
//                                   color: Colors.white,
//                                 ),
//                               ),
//                             ),
//                           )
//                         : Container()
//                   ],
//                 ),

//                 //end of shipping address card
//               ],
//             ),
//           ),
//           Container(
//             padding: EdgeInsets.only(top: 20, left: 10),
//             child: Text(
//               "Items",
//               style: TextStyle(
//                   fontSize: width / 16,
//                   letterSpacing: .03,
//                   color: Theme.of(context).primaryColor,
//                   fontWeight: FontWeight.w400),
//             ),
//           ),
//           Divider(
//             thickness: 1,
//             color: Colors.grey[400],
//           ),
//           Column(
//             children: items(width),
//           ),
//           Container(
//             margin: EdgeInsets.only(top: 20, left: 10, right: 10),
//             //height: MediaQuery.of(context).size.width/3.2,
//             //width: MediaQuery.of(context).size.width / 1.111,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(5),
//                 border: Border.all(color: Colors.grey[400])),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: <Widget>[
//                 Container(
//                   padding: EdgeInsets.only(top: 10, left: 10),
//                   child: Text(
//                     "Price details",
//                     style: TextStyle(
//                         fontSize: width / 20,
//                         letterSpacing: .03,
//                         color: Theme.of(context).primaryColor,
//                         fontWeight: FontWeight.w400),
//                   ),
//                 ),
//                 Divider(
//                   thickness: 1,
//                   color: Colors.grey[400],
//                 ),
//                 //address pading
//                 Container(
//                   padding: EdgeInsets.only(left: 10, bottom: 10),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Container(
//                             margin: EdgeInsets.only(top: 8),
//                             height: width / 17,
//                             width: width / 1.8,
//                             child: AutoSizeText(
//                               "Price",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(right: 8, top: 8),
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "₹220",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                         ],
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Container(
//                             margin: EdgeInsets.only(top: 8),
//                             height: width / 17,
//                             width: width / 1.8,
//                             child: AutoSizeText(
//                               "Delivery Fee",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(right: 8, top: 8),
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "₹220",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                         ],
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Container(
//                             margin: EdgeInsets.only(top: 8),
//                             height: width / 17,
//                             width: width / 1.8,
//                             child: AutoSizeText(
//                               "Other Amount",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(right: 8, top: 8),
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "₹220",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(color: Colors.grey[600]),
//                             ),
//                           ),
//                         ],
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Container(
//                             margin: EdgeInsets.only(top: 8),
//                             height: width / 17,
//                             width: width / 1.8,
//                             child: AutoSizeText(
//                               "Total Payable Amount",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(
//                                 fontWeight: FontWeight.w600,
//                                 color: Color(0xFF4c4d52),
//                               ),
//                             ),
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(right: 8, top: 8),
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "₹220",
//                               presetFontSizes: [18, 16, 14],
//                               style: TextStyle(
//                                 fontWeight: FontWeight.w600,
//                                 color: Color(0xFF4c4d52),
//                               ),
//                             ),
//                           ),
//                         ],
//                       )
//                     ],
//                   ),
//                 ),

//                 //end of shipping address card
//               ],
//             ),
//           ),
//           SizedBox(
//             height: 80,
//           )
//         ],
//       ),
//     );
//   }

//   List<Widget> items(width) {
//     List<Widget> addItems = [];
//     for (int i = 0; i < widget.products.length; i++) {
//       addItems.add(Card(
//         child: Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             Expanded(
//               flex: 2,
//               child: Container(
//                 //  color:Colors.red,
//                 alignment: Alignment.center,
//                 padding: const EdgeInsets.all(5),
//                 height: width / 4,
//                 child: Image.asset(
//                   "assets/images/home/bag2.jpg",
//                   fit: BoxFit.fill,
//                 ),
//               ),
//             ),
//             Expanded(
//               flex: 4,
//               child: Container(
//                 //padding: EdgeInsets.only(left: 10),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       // color: Colors.red,
//                       height: width / 16,
//                       width: width / 1.6,
//                       child: AutoSizeText(
//                         '${widget.products[0]} ',
//                         style: TextStyle(
//                           color: Color(0xff73767a),
//                         ),
//                         overflow: TextOverflow.ellipsis,
//                         presetFontSizes: [19, 18, 17, 16],
//                       ),
//                     ),
//                     Container(
//                       height: width / 18,
//                       width: width / 1.6,
//                       child: AutoSizeText(
//                         'Fasttrack',
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: Color(0xff969798),
//                         ),
//                         presetFontSizes: [
//                           17,
//                           14,
//                           12,
//                         ],
//                       ),
//                     ),
//                     Container(
//                       height: width / 18,
//                       width: width / 1.6,
//                       child: AutoSizeText(
//                         'Size:9',
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: Color(0xff969798),
//                         ),
//                         presetFontSizes: [
//                           17,
//                           14,
//                           12,
//                         ],
//                       ),
//                     ),
//                     Row(
//                       children: <Widget>[
//                         Container(
//                           height: MediaQuery.of(context).size.width / 18,
//                           child: AutoSizeText(
//                             "₹249",
//                             overflow: TextOverflow.ellipsis,
//                             presetFontSizes: [16, 15, 14, 13, 12],
//                             textAlign: TextAlign.center,
//                             style: TextStyle(
//                                 fontWeight: FontWeight.w600,
//                                 color: Colors.green),
//                           ),
//                         ),
//                         Container(
//                           margin: EdgeInsets.only(left: 8),
//                           height: MediaQuery.of(context).size.width / 19,
//                           width: MediaQuery.of(context).size.width / 9,
//                           child: AutoSizeText(
//                             "₹999",
//                             overflow: TextOverflow.ellipsis,
//                             presetFontSizes: [14, 12, 10],
//                             style: TextStyle(
//                                 decoration: TextDecoration.lineThrough,
//                                 color: Colors.black54),
//                           ),
//                         ),
//                       ],
//                     ),
//                     Container(
//                       height: width / 18,
//                       width: width / 1.6,
//                       child: AutoSizeText(
//                         'You Saved ₹999 on this order',
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: Colors.yellow[800],
//                         ),
//                         presetFontSizes: [
//                           17,
//                           14,
//                           12,
//                         ],
//                       ),
//                     ),
//                     Container(
//                       height: width / 18,
//                       width: width / 1.6,
//                       child: AutoSizeText(
//                         'Deliverd on 02 January 2020',
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: Colors.grey,
//                         ),
//                         presetFontSizes: [
//                           17,
//                           14,
//                           12,
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//                 // height: width / 3,

//                 // color: Colors.blue,
//               ),
//             )
//           ],
//         ),
//       ));
//     }
//     return addItems;
//   }

//   showAlertDialog(context) {
//     SimpleDialog dialog = SimpleDialog(
//       title: Text("Select Address"),
//       children: address(),
//     );
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return dialog;
//       },
//     );
//   }

//   List<Widget> address() {
//     List<Widget> addresses = [];
//     for (int i = 0; i < _address.address.length; i++) {
//       addresses.add(SimpleDialogOption(
//         child: Container(
//           color: Colors.black38,
//           child: AutoSizeText(
//             "${_address.address[i].name}\n${_address.address[i].address_1}, ${_address.address[i].address_2},\n${_address.address[i].postcode}, ${_address.address[i].city},${_address.address[i].landmark}\n ${_address.address[i].mobile_number}\n ${_address.address[i].alt_contact} ",
//             maxLines: null,
//           ),
//         ),
//         onPressed: () {
//           _addressProvider.currentAddress=_address.address[i];

//           Navigator.of(context).pop();
//           print(_address.address[i].address_1);
//         },
//       ));
//     }
//     return addresses;
//   }

//   addAddressAlertDialog(context) {
//     return showDialog(
//         context: context,
//         barrierDismissible: true,
//         builder: (context) {
//           return AlertDialog(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(
//                 Radius.circular(10.0),
//               ),
//             ),
//             title: Text(
//               'Add Address',
//               style: TextStyle(color: Color(0xff3a3a3a)),
//             ),
//             content: Column(
//               children: <Widget>[],
//             ),
//             actions: <Widget>[
//               MaterialButton(
//                 elevation: 5.0,
//                 child: Text("Save"),
//                 onPressed: () {
//                   // Navigator.pop(context);
//                   Navigator.pop(context);
//                 },
//               ),
//             ],
//           );
//         });
//   }
// }
