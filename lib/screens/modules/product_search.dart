import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/app_search/searchProductResponse.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/product-list/product_list_screen.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductSearch extends SearchDelegate<String> {
  //ShoppingServices _shoppingServices;
  Stream<int> testData;
  List<String> items = ["1", "2", "Third", "4"];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // returing to filter page for searching relating items
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return SearchSuggestionBlock(
      query: query,
    );
  }

  getAppSearch() async {}
}

class SearchSuggestionBlock extends StatefulWidget {
  final String query;
  SearchSuggestionBlock({this.query = ''});
  @override
  _SearchSuggestionBlockState createState() => _SearchSuggestionBlockState();
}

class _SearchSuggestionBlockState extends State<SearchSuggestionBlock> {
  HtmlUnescape _htmlUnescape = HtmlUnescape();

  OCImageCache ocImageCache = OCImageCache();
  bool pageReady = false;
  bool apiCacheUpdated = false;
  ShoppingServices _shoppingServices;
  SearchProductResponse _searchProductResponse;
  List<Widget> searchResults = [];
  SharedPreferences _prefs;
  GlobalProvider _globalProvider;

  @override
  void initState() {
    super.initState();

    _shoppingServices = ShoppingServices(context: context);
    loadClient();
  }

  loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _globalProvider = Provider.of<GlobalProvider>(context);
    _prefs = await SharedPreferences.getInstance();
    pageReady = true;
  }

  @override
  Widget build(BuildContext context) {
    if (pageReady == true) {
      getSearchApi();
    }
    return ListView.builder(
      itemCount: searchResults.length,
      itemBuilder: (BuildContext context, int index) {
        return searchResults[index];
      },
    );
  }

  getSearchApi() async {
    if (widget.query != '') {
      QueryResult result = await _shoppingServices.appSearchResult(term: widget.query);
      if (!result.hasException) {
        cacheSearchResult(result.data);
        _searchProductResponse = SearchProductResponse.fromJson(result.data);
        buildSearchResult();
      }
    } else if (apiCacheUpdated == false) {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      final apiCacheData = jsonDecode(_prefs.getString('app_search_cache'));
      if (apiCacheData != null) {
        _searchProductResponse = SearchProductResponse.fromJson(apiCacheData);
        buildSearchResult();
        setState(() {
          apiCacheUpdated = true;
        });
      }
    }
  }

  buildSearchResult() {
    setState(() {
      searchResults = [];
    });
    for (int i = 0; i < _searchProductResponse.searchProduct.category.length; i++) {
      final cCategoryItem = _searchProductResponse.searchProduct.category[i];
      final String parentName = cCategoryItem.getParentName ?? cCategoryItem.name;
      setState(() {
        searchResults.add(
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProductListScreen(
                    title: cCategoryItem.name.toString(),
                    categoryId: cCategoryItem.category_id,
                  ),
                ),
              );
            },
            leading: Container(
              child: Icon(FontAwesomeIcons.list),
            ),
            title: AutoSizeText(
              _htmlUnescape.convert(cCategoryItem.name),
              presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
              style: TextStyle(color: Colors.black54, fontFamily: 'Product-Sans'),
            ),
            subtitle: AutoSizeText(
              "in ${_htmlUnescape.convert(parentName)}",
              presetFontSizes: [10, 9, 8, 7],
              style: TextStyle(color: Colors.black54, fontFamily: 'Product-Sans'),
            ),
            trailing: Icon(Icons.arrow_forward),
          ),
        );
        searchResults.add(Divider());
      });
    }

    for (int i = 0; i < _searchProductResponse.searchProduct.products.length; i++) {
      final cProductItem = _searchProductResponse.searchProduct.products[i];
      var imageCache = ocImageCache.getCachedImage(
        path: cProductItem.details.image,
        imageQuality: ImageQuality.high,
      );
      setState(() {
        searchResults.add(
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProductDetailScreen(
                    productId: cProductItem.product_id,
                  ),
                ),
              );
            },
            leading: Container(
              child: Image.network(
                "${_globalProvider.cdnImagePoint}cache/$imageCache",
                width: 40,
              ),
            ),
            title: AutoSizeText(
              cProductItem.name,
              presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
              style: TextStyle(color: Colors.black54, fontFamily: 'Product-Sans'),
            ),
            trailing: Icon(Icons.arrow_forward),
          ),
        );
        searchResults.add(Divider());
      });
    }
  }

  cacheSearchResult(dynamic jsonData) async {
    _prefs.setString('app_search_cache', jsonEncode(jsonData));
  }
}
