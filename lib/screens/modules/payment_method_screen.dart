import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

dynamic sel;

// ignore: must_be_immutable
class PaymentMethodScreen extends StatefulWidget {
  var products = [];
  // ignore: non_constant_identifier_names
  int shipping_address_id;
  // ignore: non_constant_identifier_names
  PaymentMethodScreen({@required productIds, @required shipping_id}) {
    products = productIds;
    shipping_address_id = shipping_id;
  }

  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen> {
  bool _useGocoCash = false;
  //dynamic _selectedCoupon = '';
  bool proceedButtonClicked = false;

  ShoppingServices _shoppingServices;

  @override
  void initState() {
    super.initState();
    sel = null;
    _shoppingServices = ShoppingServices(context: context);
  }

  orderPlaceRequest() async {
    var productsTOBePlaced = [
      {"product_id": widget.products[0], "purchase_quantity": 2}
    ];
    QueryResult orderPlaceResult =
        await _shoppingServices.orderPlaceRequest(shippingAddressId: widget.shipping_address_id, products: productsTOBePlaced, productOptions: []);
    if (!orderPlaceResult.hasException) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              title: Text(
                'Order Placed',
                style: TextStyle(color: Color(0xff3a3a3a)),
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text("Ok"),
                  onPressed: () {
                    // Navigator.replace(context, oldRoute: null, newRoute: null)
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomeScreen(),
                      ),
                      (r) => false,
                    );
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("Payments Options"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: body(width),
      ),
      bottomSheet: InkWell(
        onTap: proceedButtonClicked == true
            ? null
            : () async {
                setState(() {
                  proceedButtonClicked = true;
                });
                await orderPlaceRequest();
              },
        child: Container(
            height: MediaQuery.of(context).size.width / 7,
            //width: MediaQuery.of(context).size.width / 2,
            color: proceedButtonClicked == true ? Colors.grey : Theme.of(context).primaryColor,
            child: Center(
                child: AutoSizeText(
              "Proceed",
              style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width / 20, fontWeight: FontWeight.w700),
              maxLines: 1,
            ))),
      ),
    );
  }

  TextStyle optionTextStyle() {
    return TextStyle(
      color: Colors.grey[600],
    );
  }

  body(width) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.green[50],
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                AutoSizeText(
                  "Total Payable Amount",
                  style: TextStyle(fontSize: width / 24),
                  maxFontSize: 20,
                ),
                AutoSizeText(
                  "₹1,750",
                  style: TextStyle(fontSize: width / 24, fontWeight: FontWeight.bold),
                  maxFontSize: 20,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20, left: 15),
            child: Row(
              children: <Widget>[
                Container(
                  width: width / 2.2,
                  height: width / 3.8,
                  padding: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(border: Border.all(color: Colors.black38), borderRadius: BorderRadius.circular(10)),
                  child: CustomListViewTile(
                    title: "Goco Wallet",
                    imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTzY4XUZKsc6bf80E1TfoZqkgxohO_6K_D0i59VkD-1jMZnVKT_",
                    subTitle: "Balance",
                    width: width,
                    bottom: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          width: width / 3.4,
                          child: AutoSizeText(
                            "You can use ₹1250 Goco Cash for this order",
                            maxLines: 2,
                            presetFontSizes: [12, 11, 10, 9, 8],
                          ),
                        ),
                        Checkbox(
                          value: _useGocoCash,
                          activeColor: Colors.green,
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          onChanged: (value) {
                            setState(() {
                              _useGocoCash = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  padding: const EdgeInsets.only(top: 10, left: 10),
                  width: width / 2.2,
                  height: width / 3.8,
                  decoration: BoxDecoration(border: Border.all(color: Colors.black38), borderRadius: BorderRadius.circular(10)),
                  child: CustomListViewTile(
                    title: "Gift Voucher",
                    imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRF8eEjp7pHDxdwx-yuvBcBFGe-MrZiGQUWjDDggJ38ykEBC4EV",
                    subTitle: "Promo codes",
                    width: width,
                    bottom: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        SizedBox(
                          // alignment: Alignment.centerRight,
                          // color: Colors.red,
                          width: width / 5,
                          height: width / 12,
                          child: TextField(
                            controller: null,
                            style: TextStyle(fontSize: 12),
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                counterText: "",
                                hintText: "Enter Coupon",
                                hintStyle: TextStyle(fontSize: width / 30),
                                disabledBorder: InputBorder.none,
                                fillColor: null,
                                contentPadding: EdgeInsets.only(bottom: 3),
                                labelStyle: TextStyle(fontSize: width / 30)),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 10),
                          color: Colors.green,
                          height: width / 20,
                          width: width / 7.2,
                          child: Center(
                            child: Text(
                              "Apply",
                              style: TextStyle(fontSize: width / 35, color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 12, top: 25, bottom: 20),
            // padding:const EdgeInsets.only(top:10),
            height: width / 15,
            child: AutoSizeText(
              "Payment Options",
              presetFontSizes: [22, 20, 18, 16],
            ),
          ),
          Wrap(
            direction: Axis.horizontal,
            children: payment(width),
          ),
          SizedBox(
            height: 80,
          )
        ],
      ),
    );
  }

  List<Widget> payment(width) {
    List<Widget> options = [];

    for (int i = 1; i < 8; i++) {
      options.add(PaymentOption(
          width: width,
          id: i,
          content: Image.network(
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT6r3Nl7dQb4zlslLFMmEFx0u_j8SzIztdNIS61L7ULXjqx2yy6",
            fit: BoxFit.fill,
          ),
          name: "i",
          onPressed: () {
            setState(() {
              sel = i;
              //print(sel);
            });
          },
          selected: sel));
    }
    return options;
  }
}

class CustomListViewTile extends StatelessWidget {
  final String title;
  final String imageUrl;
  final String subTitle;
  final double width;
  final Widget bottom;
  CustomListViewTile({@required this.title, @required this.imageUrl, @required this.subTitle, @required this.width, this.bottom});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(
              height: width / 9,
              width: width / 7,
              child: Image.network(
                "$imageUrl",
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AutoSizeText(
                    "$title",
                    style: TextStyle(
                      fontSize: width / 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                    ),
                    maxFontSize: 20,
                  ),
                  AutoSizeText(
                    "$subTitle",
                    style: TextStyle(fontSize: width / 30, color: Colors.grey),
                    maxFontSize: 15,
                  ),
                ],
              ),
            )
          ],
        ),
        bottom != null ? bottom : SizedBox()
      ],
    );
  }
}

class PaymentOption extends StatelessWidget {
  final double width;
  final dynamic id;
  final Widget content;
  final String name;
  final dynamic selected;
  final VoidCallback onPressed;

  PaymentOption({@required this.width, @required this.id, @required this.content, @required this.name, @required this.selected, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    tick() {
      if (sel == id) {
        return SizedBox(
          height: width / 4.5,
          width: width / 4.5,
          child: Container(
            decoration: BoxDecoration(color: Colors.white70, borderRadius: BorderRadius.circular(10)),
            padding: const EdgeInsets.all(10.0),
            child: Center(
                child: Icon(
              Icons.check,
              color: Colors.green,
              size: width / 10,
            )),
          ),
        );
      }
      return Container();
    }

    return InkWell(
      onTap: onPressed,
      child: Container(
        //color: Colors.red,
        margin: const EdgeInsets.all(5),
        height: width / 3.5,
        width: width / 4.5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: width / 4.5,
              width: width / 4.5,
              decoration: BoxDecoration(border: Border.all(color: Colors.grey[300]), borderRadius: BorderRadius.circular(10)),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Center(child: content),
                  ),
                  tick(),
                ],
              ),
            ),
            AutoSizeText("name")
          ],
        ),
      ),
    );
  }
}
