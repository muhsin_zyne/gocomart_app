import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/wishlist_screen_model/product_wishlist.dart';
import 'package:gocomartapp/providers/screen_caching_provider.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:skeleton_text/skeleton_text.dart';

class WishlistScreen extends StatefulWidget {
  WishlistScreen({Key key}) : super(key: key);

  @override
  _WishlistScreenState createState() => _WishlistScreenState();
}

class _WishlistScreenState extends State<WishlistScreen> {
  ShoppingServices _shoppingServices;
  ScreenCachingProvider _screenCachingProvider;
  //ProductWishlist _productWishlist;
  final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
  // bool loaded = false;
  num displayprice;
  num specialprice;
  num orginalprice;
  bool emptyProducts = false;

  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();
  }

  @override
  void didChangeDependencies() {
    _screenCachingProvider = Provider.of<ScreenCachingProvider>(
      context,
    );
    super.didChangeDependencies();
  }

  loadClient() async {
    initWishListProducts();
  }

  initWishListProducts() async {
    await Future.delayed(Duration(microseconds: 100));
    // if (_screenCachingProvider.productWishlist !=
    //     null) if (_screenCachingProvider
    //         ?.productWishlist?.products.length ==
    //     0) {
    //   setState(() {
    //     emptyProducts = true;
    //   });
    // }
    QueryResult productResult = await _shoppingServices.productWishList();
    setState(() {
      if (!productResult.hasException) {
        _screenCachingProvider.productWishlist = ProductWishlist.fromJson(productResult.data['productWishlist']);
        if (_screenCachingProvider.productWishlist.products.length == 0) {
          setState(() {
            emptyProducts = true;
          });
        } else {
          setState(() {
            emptyProducts = false;
          });
        }
        // setState(() {
        //   loaded = true;
        // });
      }
    });
  }

  removeFromWishList(productId) async {
    await _shoppingServices.removeFromWishList(productId: productId);
    initWishListProducts();
  }

  Widget body() {
    return ListView.builder(
      itemCount: (_screenCachingProvider.productWishlist != null) ? _screenCachingProvider.productWishlist.products.length : 3,
      itemBuilder: (context, int i) {
        bool removingFromWishlist = false;
        if ((_screenCachingProvider.productWishlist != null) == true) {
          orginalprice = _screenCachingProvider.productWishlist.products[i].productDetails.price;
          specialprice = _screenCachingProvider.productWishlist.products[i].productDetails.special.price;
          if (specialprice != null) {
            displayprice = specialprice;
          } else {
            displayprice = orginalprice;
          }
        }
        return Container(
          color: Colors.transparent,
          margin: const EdgeInsets.only(top: 8, left: 8, right: 8),
          height: MediaQuery.of(context).size.width / 3,
          child: InkWell(
            onTap: () {
              productDetailNavigator(_screenCachingProvider.productWishlist.products[i].product_id);
            },
            child: Card(
              elevation: 3,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    padding: EdgeInsets.all(10),
                    height: MediaQuery.of(context).size.width / 3,
                    width: MediaQuery.of(context).size.width / 3.5,
                    child: (_screenCachingProvider.productWishlist != null)
                        ? Image.network(
                            "${imagePath + _screenCachingProvider.productWishlist.products[i].productDetails.image}",
                            fit: BoxFit.fitHeight,
                          )
                        : Container(
                            //margin: EdgeInsets.only(left: 5),
                            padding: EdgeInsets.all(10),
                            height: MediaQuery.of(context).size.width / 3,
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: SkeletonAnimation(
                              child: Container(
                                color: Colors.grey[300],
                              ),
                            ),
                          ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Container(
                      margin: EdgeInsets.only(left: 10),
                      child: (_screenCachingProvider.productWishlist != null)
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: (_screenCachingProvider.productWishlist != null)
                                      ? AutoSizeText(
                                          "${_screenCachingProvider.productWishlist.products[i].productDetails.description.name} ",
                                          overflow: TextOverflow.ellipsis,
                                          presetFontSizes: [
                                            17,
                                            15,
                                            14,
                                            12,
                                          ],
                                          style: TextStyle(fontWeight: FontWeight.w600),
                                        )
                                      : Container(),
                                ),
                                Container(
                                  height: MediaQuery.of(context).size.width / 19,
                                  child: (_screenCachingProvider.productWishlist != null)
                                      ? AutoSizeText(
                                          "${_screenCachingProvider.productWishlist.products[i].productDetails.manufacturer.name}",
                                          overflow: TextOverflow.ellipsis,
                                          presetFontSizes: [14, 13, 12],
                                          style: TextStyle(color: Colors.black54),
                                        )
                                      : Container(),
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      height: MediaQuery.of(context).size.width / 18,
                                      child: (_screenCachingProvider.productWishlist != null)
                                          ? AutoSizeText(
                                              "$displayprice",
                                              overflow: TextOverflow.ellipsis,
                                              presetFontSizes: [16, 15, 14, 13, 12],
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontWeight: FontWeight.w600),
                                            )
                                          : Container(),
                                    ),
                                    (_screenCachingProvider.productWishlist != null) && specialprice != null
                                        ? Container(
                                            margin: EdgeInsets.only(left: 18),
                                            height: MediaQuery.of(context).size.width / 19,
                                            width: MediaQuery.of(context).size.width / 9,
                                            child: AutoSizeText(
                                              "$orginalprice",
                                              overflow: TextOverflow.ellipsis,
                                              presetFontSizes: [14, 12, 10],
                                              style: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.black54),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                                (_screenCachingProvider.productWishlist != null) && specialprice != null
                                    ? Container(
                                        height: MediaQuery.of(context).size.width / 19.9,
                                        child: AutoSizeText(
                                          "${discountPercentage(orginalprice, specialprice)}% OFF TODAY",
                                          style: TextStyle(color: Colors.green[300], fontWeight: FontWeight.w800),
                                          presetFontSizes: [18, 17, 14, 13, 12, 10, 8],
                                        ),
                                      )
                                    : Container(),
                              ],
                            )
                          : Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SkeletonAnimation(
                                    child: Container(
                                      height: MediaQuery.of(context).size.width / 30,
                                      width: MediaQuery.of(context).size.width / 3,
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                  SkeletonAnimation(
                                    child: Container(
                                      height: MediaQuery.of(context).size.width / 30,
                                      width: MediaQuery.of(context).size.width / 6,
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                  SkeletonAnimation(
                                    child: Container(
                                      height: MediaQuery.of(context).size.width / 30,
                                      width: MediaQuery.of(context).size.width / 3,
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.width / 12.9,
                          margin: EdgeInsets.only(top: MediaQuery.of(context).size.width / 19, left: 25, right: 4),
                          child: (_screenCachingProvider.productWishlist != null)
                              ? AutoSizeText(
                                  "Out of Stock",
                                  style: TextStyle(color: Colors.red[600], fontWeight: FontWeight.w500),
                                  presetFontSizes: [18, 17, 14, 13, 12, 10, 8],
                                  textAlign: TextAlign.start,
                                  maxLines: 2,
                                )
                              : Container(),
                        ),
                        (_screenCachingProvider.productWishlist != null)
                            ? InkWell(
                                onTap: () async {
                                  // setState(() {
                                  //   loaded = false;
                                  //   print(removingFromWishlist);
                                  // });
                                  removeFromWishList(_screenCachingProvider.productWishlist.products[i].product_id);

                                  // for(int j =i;j<_productWishlist.products.length-1;i++){
                                  //   _productWishlist.products[j]=_productWishlist.products[j+1];
                                  // }

                                  // build(context);
                                  // body();
                                },
                                child: removingFromWishlist
                                    ? CircularProgressIndicator()
                                    : Container(
                                        margin: const EdgeInsets.only(
                                          right: 10,
                                          bottom: 10,
                                        ),
                                        alignment: Alignment.bottomRight,
                                        child: Icon(
                                          Icons.cancel,
                                          size: MediaQuery.of(context).size.width / 15,
                                          color: Colors.grey[400],
                                        ),
                                      ),
                              )
                            : Container()
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void productDetailNavigator(id) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductDetailScreen(
          productId: id,
        ),
      ),
    ).whenComplete(() {
      initWishListProducts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Wish list"),
        bottom: PreferredSize(
          child: Container(
              height: MediaQuery.of(context).size.width / 9,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width / 24,
                    margin: EdgeInsets.only(left: 8),
                    child: AutoSizeText(
                      "Last updated on Aug 14 02:26 PM",
                      presetFontSizes: [16, 14, 10, 8],
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.width / 18,
                    margin: EdgeInsets.only(right: 8),
                    child: emptyProducts
                        ? AutoSizeText(
                            "0 Items",
                            presetFontSizes: [18, 16, 14, 10, 8],
                            style: TextStyle(color: Colors.black54),
                          )
                        : _screenCachingProvider.productWishlist != null
                            ? AutoSizeText(
                                "${_screenCachingProvider.productWishlist.products.length} Items",
                                presetFontSizes: [18, 16, 14, 10, 8],
                                style: TextStyle(color: Colors.black54),
                              )
                            : SizedBox(),
                  ),
                ],
              )),
          preferredSize: Size(0, 40),
        ),
      ),
      body: (_screenCachingProvider.productWishlist != null)
          ? (emptyProducts
              ? Container(
                  //margin: EdgeInsets.only(top:20),
                  child: Center(
                    child: Text("NO products"),
                  ),
                )
              : body())
          : Center(
              child: CircularProgressIndicator(),
            ),

      bottomNavigationBar: Container(
          // alignment: Alignment.center,
          height: MediaQuery.of(context).size.width / 8,
          color: Colors.transparent,
          child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width / 10,
            child: RaisedButton(
              // shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(15)),
              color: Colors.blue,
              onPressed: () {
                for (int i = 0; i < _screenCachingProvider.productWishlist.products.length; i++) {
                  removeFromWishList(_screenCachingProvider.productWishlist.products[i].product_id);
                }
                setState(() {
                  emptyProducts = true;
                });
              },
              child: AutoSizeText(
                "Clear Wish List",
                presetFontSizes: [20, 18, 16, 14, 12, 10],
                style: TextStyle(color: Colors.white),
              ),
            ),
          )),
      // floatingActionButton: Container(
      //   width: MediaQuery.of(context).size.width,
      //   height: MediaQuery.of(context).size.width/6,
      //   child: RaisedButton(
      //     onPressed: (){},
      //     child: Text("data"),
      //   ),
      // ),
    );
  }
}
