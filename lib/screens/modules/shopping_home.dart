import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gocomartapp/app_main.dart' as main;
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/controllers/local_storage.dart';
//import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';
//import 'package:gocomartapp/models/shopping_home/get_offered_products.dart';
//import 'package:gocomartapp/models/shopping_home/get_recent_product.dart';
//import 'package:gocomartapp/models/shopping_home/get_trending_products.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/providers/shopping_home_provider.dart';
import 'package:gocomartapp/screens/modules/product_search.dart';
import 'package:gocomartapp/screens/product-list/product_list_screen.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/src/screens/widgets/TrendingBrandsVerticalScroll.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skeleton_text/skeleton_text.dart';

import '../../controllers/oc_cache_img.dart';
import '../../services/shopping_services.dart';
import '../product/product_detail_screen.dart';

class ShoppingHome extends StatefulWidget {
  @override
  _ShoppingHomeState createState() => _ShoppingHomeState();
}

class _ShoppingHomeState extends State<ShoppingHome> {
  ScrollController _controller = ScrollController();
  //GlobalProvider _globalProvider;

  ShoppingHomeModel shoppingHomeModel;

  List colors = [
    Colors.blue[600],
    Colors.pink[400],
    Colors.cyan[700],
    Colors.orange[700],
    Colors.purple,
    Colors.green,
    Colors.indigo[400],
  ];
  Random random = new Random();
  int index = 0;

  //models.GetOfferProductsResponse getOfferProductsResponse;

  ShoppingServices _shoppingServices;
  final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
  OCImageCache ocImageCache = OCImageCache();

  List _currentTrendList = <Widget>[];
  bool shoppingProductLoaded = false;
  // bool offerProductLoad = false;
  // bool appBannerLoad = false;
  // bool recentProductLoader = false;
  int recentViewLength = 0;
  bool showSearchOnAppBar = false;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  ShoppingHomeProvider _shoppingHomeProvider;

  @override
  void didChangeDependencies() {
    //_cartProvider = Provider.of<CartProvider>(context);
    _shoppingHomeProvider = Provider.of<ShoppingHomeProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();
    this._scrollControlHandler();
  }

  loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    //_globalProvider = Provider.of<GlobalProvider>(context);
    setState(() {
      shoppingHomeModel = _shoppingHomeProvider.shoppingHomeModel;
    });

    if (shoppingHomeModel != null) {
      setState(() {
        shoppingProductLoaded = true;
        recentViewLength = shoppingHomeModel.getRecentProduct.products.length;
        latestOffer();
        //buildCurrentTrendDetail();
      });
    }
    initShoppingHomeDetails();
    testFunction();
    LocalStorage _localStorage = LocalStorage(context: context);
    _localStorage.updateAppConst();
  }

  initShoppingHomeDetails() async {
    QueryResult shoppingHomeProductResult = await _shoppingServices.getMixedShoppingHomeProducts();
    if (!shoppingHomeProductResult.hasException) {
      _shoppingHomeProvider.shoppingHomeModel = ShoppingHomeModel.fromJSON(shoppingHomeProductResult.data);

      setState(() {
        shoppingHomeModel = _shoppingHomeProvider.shoppingHomeModel;
        shoppingProductLoaded = true;
        recentViewLength = shoppingHomeModel.getRecentProduct.products.length;
        carousalImages(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.width);
        latestOffer();
        buildCurrentTrendDetail();
      });
    }

    print(shoppingHomeProductResult.data);
  }

  testFunction() async {
    final SharedPreferences prefs = await _prefs;
    //  prefs.setString('auth_token', dummyToken);

    print("\n \n \n \n ");
    this._shoppingServices.newAPICAll();
    print("\n \n \n \n ");
  }

  void _scrollControlHandler() {
    _controller.addListener(() {
      print(_controller.position.pixels);
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
      if (_controller.position.pixels > 245) {
        setState(() {
          showSearchOnAppBar = true;
        });
      } else {
        setState(() {
          showSearchOnAppBar = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              //primary: true,
              floating: true,
              pinned: true,
              elevation: 0,
              backgroundColor: Colors.white,
              title: !showSearchOnAppBar
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          'assets/images/icons/icons8-document.svg',
                          height: 50,
                        ),
                        Text(
                          'GOCOMART',
                          style: TextStyle(
                            fontFamily: 'Product Sans',
                            fontSize: 18,
                            color: const Color(0xff3b5999),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        )
                      ],
                    )
                  : searchWidget(width),
            ),
            // SliverFlip(
            //   child: showSearchOnAppBar
            //       ? Column(
            //           crossAxisAlignment: CrossAxisAlignment.start,
            //           children: <Widget>[
            //             Hero(
            //               tag: 'appBar',
            //               child: Text(
            //                 "Total Rewards333333333333",
            //                 style: TextStyle(fontSize: 14),
            //               ),
            //             ),
            //           ],
            //         )
            //       : SizedBox(),
            // ),
            SliverFillRemaining(
              child: SingleChildScrollView(
                controller: _controller,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    //carousal
                    Container(
                      color: Colors.transparent,
                      height: MediaQuery.of(context).size.width / 1.9,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(left: 5, right: 5),
                      child: Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        elevation: 4,
                        child: shoppingProductLoaded == true
                            ? carousalImages(width: width, height: width)
                            : Container(
                                child: SkeletonAnimation(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ),

                    //search
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: searchWidget(width),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 12),
                      child: Center(
                        child: Container(
                          padding: const EdgeInsets.only(top: 15.0),
                          height: width / 3.3,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            color: const Color(0xffe9e7e7),
                          ),
                          child: ListView.builder(
                            itemCount: 10,
                            scrollDirection: Axis.horizontal,
                            // padding: EdgeInsets,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  // Adobe XD layer: 'shoe-icon' (shape)
                                  Container(
                                    // padding: EdgeInsets.all(10),
                                    width: width / 6.3,
                                    height: width / 6.3,

                                    margin: EdgeInsets.symmetric(horizontal: 10),
                                    padding: const EdgeInsets.all(2),

                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30.0),
                                      color: Colors.white,
                                      // image: DecorationImage(
                                      //   image: const NetworkImage(
                                      //       'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRol53fBPBxRPAtFMG8VwBBBWBDx0H7vQRmOw&usqp=CAU'),
                                      //   fit: BoxFit.fill,
                                      // ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(30.0),
                                        child: Image.network(
                                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRol53fBPBxRPAtFMG8VwBBBWBDx0H7vQRmOw&usqp=CAU',
                                        ),
                                      ),
                                    ),
                                    // child: Image.network(
                                    //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRol53fBPBxRPAtFMG8VwBBBWBDx0H7vQRmOw&usqp=CAU',
                                    //     ),
                                  ),
                                  Container(
                                    // color: Colors.red,
                                    width: width / 7,
                                    margin: const EdgeInsets.only(top: 8),
                                    child: AutoSizeText(
                                      "Foot Wear",
                                      textAlign: TextAlign.center,
                                      maxFontSize: 18,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      minFontSize: 10,
                                      style: TextStyle(
                                        fontSize: width / 33,
                                        color: const Color(0xff757171),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  )
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                    ),

                    //latest product
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        //crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.width / 12,
                            //height: 20,
                            //color: Colors.blue,
                            padding: EdgeInsets.only(left: 10, top: 5),
                            child: AutoSizeText(
                              "Latest Offer",
                              style: TextStyle(color: Colors.grey[700], fontWeight: FontWeight.w700),
                              presetFontSizes: [22, 20, 16, 14, 12, 10],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(right: 15),
                            child: InkWell(
                              onTap: () {
                                navigate("Latest Offer");
                              },
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //end of latest product

                    //latest offer card
                    Container(
                      //  padding: EdgeInsets.only(bottom: 20),
                      color: Colors.transparent,
                      height: MediaQuery.of(context).size.width / 1.75,
                      child: shoppingProductLoaded == false
                          ? Container(
                              child: offerProductShimmer(),
                            )
                          : latestOffer(),
                    ),
                    //end of latest offer card

                    //trending brands
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 8.0,
                      ),
                      child: TrendingBrandsVerticalScroll(width: MediaQuery.of(context).size.width),
                    ),

                    Container(
                      color: Colors.transparent,
                      height: width / 3.9,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(left: 5, right: 5, top: 18),
                      child: Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        elevation: 4,
                        child: shoppingProductLoaded == true
                            ? carousalImages(width: width, height: width / 3.9)
                            : Container(
                                child: SkeletonAnimation(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ),

                    //current trend card
                    Container(
                      margin: const EdgeInsets.only(top: 18),
                      // height: 100,
                      child: Container(child: currentTrend()),
                    ),
                    //end of current trend card

                    //Recent View
                    recentViewLength == 0
                        ? Container()
                        : Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: Row(
                              //crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  height: MediaQuery.of(context).size.width / 12,
                                  //height: 20,
                                  //color: Colors.blue,
                                  padding: EdgeInsets.only(left: 10, top: 5),
                                  child: AutoSizeText(
                                    "Recent Views",
                                    style: TextStyle(color: Colors.grey[700], fontWeight: FontWeight.w700),
                                    presetFontSizes: [22, 20, 16, 14, 12, 10],
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(right: 15),
                                  child: InkWell(
                                    onTap: () {
                                      navigate("Recent Views");
                                    },
                                    child: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                    //end of Recent view

                    recentViewLength == 0
                        ? Container()
                        : Container(
                            //  padding: EdgeInsets.only(bottom: 20),
                            margin: EdgeInsets.only(top: 18),
                            color: Colors.transparent,
                            height: MediaQuery.of(context).size.width / 2.532,
                            child: shoppingProductLoaded ? recentView() : Container(),
                          ),
                    //refer image

                    Container(
                      width: width,
                      height: width / 2,
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      // decoration: BoxDecoration(
                      //   image: DecorationImage(
                      //     image: const AssetImage(''),
                      //     fit: BoxFit.fill,
                      //   ),
                      // ),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.network(
                            "https://brightonballetschool.co.uk/wp-content/uploads/2019/10/gift-vouchers.jpg",
                            fit: BoxFit.fill,
                          )),
                    ),

                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //search tab under app bar

  InkWell searchWidget(width) {
    return InkWell(
      onTap: () {
        showSearch(context: context, delegate: ProductSearch());
      },
      child: Container(
        width: width / 1.05,
        height: width / 9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(22.0),
          color: const Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              color: const Color(0x29000000),
              offset: Offset(0, 0),
              blurRadius: 2,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              color: Colors.grey[600],
            ),
            Text(
              'Search products, brands and more',
              style: TextStyle(
                fontFamily: 'Product Sans',
                fontSize: 14,
                color: const Color(0xff9e9a9a),
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    );
  }

  Widget leftDrawer() {
    return Drawer(
      elevation: 20,
      child: ListView(
        children: <Widget>[],
      ),
    );
  }

  Widget carousalImages({@required double width, @required height}) {
    List _appBannerImages = <CachedNetworkImage>[];
    for (int i = 0; i < shoppingHomeModel.getAppBanners.banners.images.length; i++) {
      _appBannerImages.add(
        CachedNetworkImage(
          imageUrl: "${imagePath + shoppingHomeModel.getAppBanners.banners.images[i].image}",
          width: width / 2.2,
          height: height / 3.2,
          placeholder: (context, url) => ImagePreLoader(),
          errorWidget: (context, url, error) => ImageError(),
          fit: BoxFit.fill,
        ),
      );
    }
    return Carousel(
      onImageTap: (index) {},
      boxFit: BoxFit.fill,
      indicatorBgPadding: 3.0,
      noRadiusForIndicator: true,
      overlayShadowColors: Colors.transparent,
      showIndicator: true,
      animationCurve: Curves.easeInOut,
      animationDuration: Duration(seconds: 1),
      autoplay: true,
      dotIncreaseSize: 1.5,
      dotVerticalPadding: 5,
      overlayShadow: false,
      dotBgColor: Colors.transparent,
      images: _appBannerImages,
    );
  }

  Widget latestOffer() {
    return ListView.builder(
      itemCount: shoppingHomeModel.getOfferedProducts.products.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) {
        var discountPercent =
            main.discountPercentage(shoppingHomeModel.getOfferedProducts.products[i].details.price, shoppingHomeModel.getOfferedProducts.products[i].price);
        //        var imageCache = ocImageCache.getCachedImage(
        //          path: getOfferProducts.products[i].details.image,
        //          imageQuality: ImageQuality.high,
        //        );
        return InkWell(
          onTap: () {
            productDetailNavigator(shoppingHomeModel.getOfferedProducts.products[i].product_id);
          },
          child: Stack(
            children: <Widget>[
              Card(
                color: Colors.white,
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                elevation: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width / 2.2,
                      height: MediaQuery.of(context).size.width / 3.2,
                      child: Image.network(
                        "${imagePath + shoppingHomeModel.getOfferedProducts.products[i].details.image}",
                        // "${imagePath + "cache/" + imageCache}",

                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    // Container(
                    //   height: 10,
                    //   child: Text("data"))
                    Expanded(
                        child: Container(
                      padding: const EdgeInsets.only(
                        left: 10,
                        top: 12,
                      ),
                      width: MediaQuery.of(context).size.width / 2.2,
                      color: Colors.grey[700],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: MediaQuery.of(context).size.width / 21,
                                width: MediaQuery.of(context).size.width / 3.3,
                                //color: Colors.red,
                                child: Container(
                                  child: Text(
                                    "${shoppingHomeModel.getOfferedProducts.products[i].details.manufacturer.name}",
                                    //textWidthBasis: TextWidthBasis.parent,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width / 25),
                                    maxLines: 1,

                                    //presetFontSizes: [13, 10, 9, 8, 7],
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height: MediaQuery.of(context).size.width / 11.5,
                                //color: Colors.red,
                                child: AutoSizeText(
                                  "${shoppingHomeModel.getOfferedProducts.products[i].details.description.name}",
                                  style: TextStyle(
                                    //height: 1.1,
                                    color: Colors.white70,
                                  ),
                                  presetFontSizes: [
                                    15,
                                    12,
                                  ],
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.bottomCenter,
                                  //color: Colors.red,
                                  height: MediaQuery.of(context).size.width / 21,
                                  child: AutoSizeText(
                                    "₹",
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                    presetFontSizes: [11, 10, 9, 8, 7],
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.bottomCenter,
                                  //color: Colors.red,
                                  height: MediaQuery.of(context).size.width / 21,
                                  child: AutoSizeText(
                                    "${shoppingHomeModel.getOfferedProducts.products[i].price}",
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                    presetFontSizes: [
                                      19,
                                      18,
                                      17,
                                      16,
                                      15,
                                      14,
                                      13,
                                      11,
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 8, top: 3),
                                  //alignment: Alignment.bottomCenter,
                                  height: MediaQuery.of(context).size.width / 27,
                                  child: AutoSizeText(
                                    "₹${shoppingHomeModel.getOfferedProducts.products[i].details.price}",
                                    style: TextStyle(
                                      color: Colors.white60,
                                      //fontWeight: FontWeight.w800,
                                      decoration: TextDecoration.lineThrough,
                                      decorationColor: Colors.white70,
                                    ),
                                    maxLines: 1,
                                    presetFontSizes: [12, 11, 10, 9, 8, 7],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.width / 3.8,
                left: MediaQuery.of(context).size.width / 3,
                //height: MediaQuery.of(context).size.width/25,
                //width: 30,
                child: Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.width / 8.5,
                  width: MediaQuery.of(context).size.width / 8.5,
                  decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                  child: Container(
                    // padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(color: Colors.red[800], shape: BoxShape.circle),
                    //width:MediaQuery.of(context).size.width/10,
                    //padding: EdgeInsets.only(left: 6, right: 5),
                    // height: MediaQuery.of(context).size.width / 14,
                    child: Container(
                      height: MediaQuery.of(context).size.width / 8,
                      padding: const EdgeInsets.all(9.0),
                      child: AutoSizeText(
                        "$discountPercent% \n OFF",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                        presetFontSizes: [16, 11, 9, 8, 7],
                        maxLines: 2,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Widget currentTrend() {
    return Card(
      borderOnForeground: true,
      elevation: 0.5,
      child: Column(
        children: <Widget>[
          //current trend text
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Row(
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width / 12,
                  //height: 20,
                  //color: Colors.blue,
                  padding: EdgeInsets.only(left: 10, top: 5),
                  child: AutoSizeText(
                    "Current Trend",
                    style: TextStyle(color: Colors.grey[700], fontWeight: FontWeight.w700),
                    presetFontSizes: [22, 20, 16, 14, 12, 10],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(right: 15),
                  child: InkWell(
                    onTap: () {
                      navigate("Current Trend");
                    },
                    child: Icon(
                      Icons.arrow_forward,
                      color: Colors.grey[700],
                    ),
                  ),
                ),
              ],
            ),
          ),
          //end of current trend text

          Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                height: MediaQuery.of(context).size.width / 12,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (context, i) {
                    return Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                      // width: 76.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        color: const Color(0xffeaee86),
                      ),
                      child: Center(
                        child: Text(
                          'Bag',
                          style: TextStyle(
                            fontFamily: 'Product Sans',
                            fontSize: 15,
                            color: const Color(0xff3d3c3c),
                            height: 0.4666666666666667,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    );
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 25),
                child: Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.end,
                  // spacing: 5,
                  children: buildCurrentTrendDetail() != null ? buildCurrentTrendDetail() : <Widget>[],
                  ///////////////////////////////
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  List<Widget> buildCurrentTrendDetail() {
    final length = shoppingHomeModel?.getTrendingProducts?.products?.length ?? 0;

    _currentTrendList = <Widget>[];
    for (var i = 0; i < length; i++) {
      var imageCache = ocImageCache.getCachedImage(
        path: shoppingHomeModel.getTrendingProducts.products[i].productDetails.image,
        imageQuality: ImageQuality.high,
      );
      setState(() {
        _currentTrendList.add(Container(
          width: MediaQuery.of(context).size.width / 2.1,
          // height: MediaQuery.of(context).size.width / 3.5,

          //text and image
          child: InkWell(
            onTap: () {
              productDetailNavigator(shoppingHomeModel.getTrendingProducts.products[i].product_id);
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              width: MediaQuery.of(context).size.width / 2,

              //color: Colors.red,
              child: Card(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.0),
                    gradient: LinearGradient(
                      begin: Alignment(0.0, 0.11),
                      end: Alignment(0.0, 1.0),
                      colors: [Colors.white, Colors.grey[400], const Color(0xff000000)],
                      stops: [0.0, 0.288, 1.0],
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0x29000000),
                        offset: Offset(0, 1),
                        blurRadius: 1,
                      ),
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //text + aling
                      Container(
                        padding: const EdgeInsets.all(15),
                        width: MediaQuery.of(context).size.width / 3,
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Image.network(
                            "${imagePath + "cache/" + imageCache}",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10),
                        width: MediaQuery.of(context).size.width / 2.3,
                        child: Container(
                          height: MediaQuery.of(context).size.width / 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: MediaQuery.of(context).size.width / 20,
                                child: AutoSizeText(
                                  "${shoppingHomeModel.getTrendingProducts.products[i].productDetails.manufacturer.name}",
                                  presetFontSizes: [16, 14, 13],
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                                ),
                              ),
                              Container(
                                // color: Colors.red,
                                width: MediaQuery.of(context).size.width / 2.4,
                                // height:
                                //     MediaQuery.of(context).size.width / 13.5,
                                child: AutoSizeText(
                                  "${shoppingHomeModel.getTrendingProducts.products[i].name}",
                                  presetFontSizes: [
                                    16,
                                    14,
                                    12,
                                    11,
                                  ],
                                  textAlign: TextAlign.start,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: -0.5),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 8),

                                    height: MediaQuery.of(context).size.width / 20,
                                    //height: MediaQuery.of(context).size.width / 20,
                                    child: AutoSizeText(
                                      "₹ 1,099",
                                      presetFontSizes: [16, 14, 13],
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 8, left: 10),

                                    height: MediaQuery.of(context).size.width / 24,
                                    //height: MediaQuery.of(context).size.width / 20,
                                    child: AutoSizeText(
                                      "₹ 1,099",
                                      presetFontSizes: [13, 11, 10],
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(fontWeight: FontWeight.w500, decoration: TextDecoration.lineThrough, color: Colors.grey[300]),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
      });
    }
    return _currentTrendList;
  }

  Widget recentView() {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: shoppingHomeModel.getRecentProduct.products.length,
      itemBuilder: (context, i) {
        var imageCache = ocImageCache.getCachedImage(
          path: shoppingHomeModel.getRecentProduct.products[i].productDetails.image,
          imageQuality: ImageQuality.high,
        );
        return Container(
          color: Colors.transparent,
          margin: EdgeInsets.only(
            left: 1,
          ),
          width: MediaQuery.of(context).size.width / 2.8,
          //height: MediaQuery.of(context).size.width / 3,
          child: InkWell(
            onTap: () {
              productDetailNavigator(shoppingHomeModel.getRecentProduct.products[i].product_id);
            },
            child: Card(
              color: Colors.white,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              //elevation: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    //color: Colors.indigoAccent,
                    height: MediaQuery.of(context).size.width / 5,
                    child: Image.network(
                      "${imagePath + "cache/" + imageCache}",
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  ClipPath(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    clipper: ClipRecentView(),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          color: colors[i % colors.length],
                          height: MediaQuery.of(context).size.width / 5.89,
                          // child: Text("data"),
                        ),
                        Positioned(
                            top: MediaQuery.of(context).size.width / 23.2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.only(left: 8),
                                    width: MediaQuery.of(context).size.width / 4,
                                    height: MediaQuery.of(context).size.width / 22,
                                    child: AutoSizeText("${shoppingHomeModel.getRecentProduct.products[i].productDetails.manufacturer.name}",
                                        maxLines: 1,
                                        presetFontSizes: [17, 16, 14, 13, 12, 11],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(color: Colors.white))),
                                Container(
                                    width: MediaQuery.of(context).size.width / 3.1,
                                    height: MediaQuery.of(context).size.width / 15,
                                    padding: EdgeInsets.only(left: 8),
                                    child: AutoSizeText("${shoppingHomeModel.getRecentProduct.products[i].productDetails.description.name}",
                                        maxLines: 2,
                                        textAlign: TextAlign.start,
                                        presetFontSizes: [12, 11, 10],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(color: Colors.white))),
                              ],
                            ))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void navigate(String title) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductListScreen(
          title: title,
        ),
      ),
    );
  }

  void productDetailNavigator(id) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductDetailScreen(
          productId: id,
        ),
      ),
    );
  }

//offer product shimmer effect

  offerProductShimmer() {
    return ListView.builder(
      itemCount: 4,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) {
        return Container(
          width: MediaQuery.of(context).size.width / 2.2,
          margin: const EdgeInsets.only(left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2.2,
                height: MediaQuery.of(context).size.width / 3.2,
                child: SkeletonAnimation(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
              ),
              Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.width / 3.9,
                  width: MediaQuery.of(context).size.width / 2.2,
                  child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.start, children: skeltonLines(3)))
            ],
          ),
        );
      },
    );
  }

  skeltonLines(int n) {
    List skeltons = <Widget>[];
    for (int i = 0; i < n; i++) {
      skeltons.add(SkeletonAnimation(
        child: Container(
          height: 10,
          width: MediaQuery.of(context).size.width / 2.8,
          decoration: BoxDecoration(
            color: Colors.grey[300],
          ),
        ),
      ));
    }
    return skeltons;
  }
}

class ClipRecentView extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();

    path.lineTo(0.0, size.height / 10);

    var secondControlPoint = Offset((size.width / 2.4), size.height / 150);
    var secondEndPoint = Offset(size.width, size.height / 3.2);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    return path;
  }

  @override
  bool shouldReclip(ClipRecentView oldClipper) => true;
}
