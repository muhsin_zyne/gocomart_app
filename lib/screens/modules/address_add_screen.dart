// import 'package:flutter/material.dart';
// import 'package:auto_size_text/auto_size_text.dart';
// import 'package:gocomart/services/shopping_services.dart';
// import 'package:graphql/client.dart';
// import 'package:gocomart/models/address/address.dart';
// import 'package:gocomart/providers/address_provider.dart';
// import 'package:gocomart/screens/address_screen.dart';
// import 'package:provider/provider.dart';
// import 'package:gocomart/models/validate_pincode/validate_pin_code.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

// bool _edit = false;

// Address _address;
// AddressProvider _addressProvider;
// //GlobalProvider _globalProvider;

// class AddressAddScreen extends StatelessWidget {
//   AddressAddScreen({edit, address}) {
//     _edit = edit;
//     _address = address;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         FocusScope.of(context).unfocus();
//       },
//       child: Scaffold(
//         resizeToAvoidBottomPadding: false,
//         resizeToAvoidBottomInset: true,
//         extendBody: true,
//         appBar: AppBar(
//           title: Text("New Address"),
//         ),
//         body: AddressAdd(),
//       ),
//     );
//   }
// }

// class AddressAdd extends StatefulWidget {
//   AddressAdd({Key key}) : super(key: key);

//   @override
//   _AddressAddState createState() => _AddressAddState();
// }

// class _AddressAddState extends State<AddressAdd> {
//   bool isChecked = true;
//   final _formKey = GlobalKey<FormState>();
//   int radioGroup = 0;
//   TextEditingController _pinCodeController = TextEditingController();
//   TextEditingController _address1 = TextEditingController();
//   TextEditingController _city = TextEditingController();
//   TextEditingController _mobile = TextEditingController();
//   TextEditingController _name = TextEditingController();
//   // ignore: non_constant_identifier_names
//   TextEditingController _alt_contact = TextEditingController();
//   TextEditingController _landmark = TextEditingController();
//   TextEditingController _company = TextEditingController();
//   TextEditingController _type = TextEditingController();

//   var _selectedValue = "other";
//   FocusNode pinFocusNode;
//   List<String> address = [];
//   ShoppingServices _shoppingServices;
//   Validate_pin_code pinCodeData;
//   bool validPinCode = false;
//   bool pincodeAddressLoading;
//   bool saving = false;
//   String selectedArea;
//   int selectedPostalCodeId;
//   @override
//   void initState() {
//     super.initState();
//     _shoppingServices = ShoppingServices(context: context);

//     pinFocusNode = new FocusNode();
//     // listen to focus changes
//     listenFocusNodes();
//     _landmark.text = "";
//     _company.text = "";
//     _alt_contact.text = "";
//     _type.text = "";
//     loadProviders();

//     if (_edit == true) {
//       initAddress();
//       callValidatepincode();
//     }
//   }

//   listenFocusNodes() {
//     pinFocusNode.addListener(() async {
//       print('focusNode updated: hasFocus: ${pinFocusNode.hasFocus}');
//       if (!pinFocusNode.hasFocus) {
//         callValidatepincode();
//       }
//     });
//   }

//   callValidatepincode() async {
//     await Future.delayed(Duration(microseconds: 5));
//     setState(() {
//       address.clear();
//       if (_address?.postcode.toString() != _pinCodeController.text)
//         selectedArea = null;
//       _city.clear();
//       pincodeAddressLoading = true;
//     });
//     print(_pinCodeController.text);

//     QueryResult result = await _shoppingServices.validatePincode(
//         pincode: _pinCodeController.text.length > 0
//             ? int.parse(_pinCodeController.text)
//             : null);
//     if (!result.hasErrors) {
//       pinCodeData = Validate_pin_code.fromJson(result.data["validatePinCode"]);
//       print(pinCodeData.success);
//       setState(() {
//         if (pinCodeData.postals.length > 0) {
//           validPinCode = true;

//           _city.text = pinCodeData.postals[0].admin_name3;

//           //suggetions part
//           for (int i = 0; i < pinCodeData?.postals?.length ?? 0; i++) {
//             // print(pinCodeData.postals[i]);
//             address.add(pinCodeData.postals[i].place_name);
//           }
//           // print(address);
//         } else {
//           validPinCode = false;
//         }
//         pincodeAddressLoading = false;
//       });
//     }
//     _formKey.currentState.validate();
//   }

//   loadProviders() async {
//     await Future.delayed(Duration(microseconds: 10));
//     //_globalProvider = Provider.of<GlobalProvider>(context);
//     _addressProvider = Provider.of<AddressProvider>(context);
//   }

//   initAddress() async {
//     setState(() {
//       _pinCodeController.text = _address.postcode.toString();
//       _address1.text = _address.address_1;
//       selectedArea = _address.address_2;
//       _city.text = _address.city;
//       _mobile.text = _address.mobile_number;
//       _name.text = _address.name;
//       _alt_contact.text = _address.alt_contact;
//       _landmark.text = _address.landmark;
//       _company.text = _address.company;
//       _type.text = _address.type;
//       selectedPostalCodeId = _address.postal_code_id;
//       if (_address.type == "home") {
//         _selectedValue = "home";
//         isChecked = false;
//       } else if (_address.type == "work") {
//         _selectedValue = "work";
//         isChecked = false;
//       } else
//         _selectedValue = "other";
//     });
//   }

//   _setSelectedValue(val) {
//     setState(() {
//       _selectedValue = val;
//       if (_selectedValue != "other") {
//         _type.text = _selectedValue;
//       } else {
//         _type.text = "";
//       }
//     });
//   }

//   //textStyle
//   TextStyle optionTextStyle() {
//     return TextStyle(
//       color: Colors.grey[600],
//     );
//   }

//   callAddress() async {
//     await addAddress();
//   }

//   ediAddress() async {
//     QueryResult result = await _shoppingServices.editAddress(
//         address_id: _address.address_id,
//         address_1: _address1.text,
//         address_2: selectedArea,
//         city: _city.text,
//         postcode: int.parse(_pinCodeController.text),
//         postal_code_id: selectedPostalCodeId,
//         type: _type.text,
//         name: _name.text,
//         mobile_number: _mobile.text,
//         company: "",
//         landmark: _landmark.text,
//         alt_contact: _alt_contact.text);
//     if (!result.hasErrors) {
//       _address = Address.fromJson(result.data["editAddress"]["address"]);
//       print(_address.name);
//       setState(() {
//         saving = false;
//       });
//       showDialog(
//           context: context,
//           barrierDismissible: false,
//           builder: (context) {
//             return AlertDialog(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(10.0),
//                 ),
//               ),
//               title: Text(
//                 'Saved',
//                 style: TextStyle(color: Color(0xff3a3a3a)),
//               ),
//               actions: <Widget>[
//                 MaterialButton(
//                   elevation: 5.0,
//                   child: Text("ok"),
//                   onPressed: () {
//                     navigate();
//                   },
//                 ),
//               ],
//             );
//           });
//     }
//   }

//   addAddress() async {
//     QueryResult result = await _shoppingServices.addAddress(
//         address_1: _address1.text,
//         address_2: selectedArea,
//         city: _city.text,
//         postcode: int.parse(_pinCodeController.text),
//         type: _type.text,
//         name: _name.text,
//         postal_code_id: selectedPostalCodeId,
//         mobile_number: _mobile.text,
//         company: "",
//         landmark: _landmark.text,
//         alt_contact: _alt_contact.text);
//     if (!result.hasErrors) {
//       Address address;

//       address = Address.fromJson(result.data["addAddress"]["address"]);
//       _addressProvider.currentAddress = address;
//       _addressProvider.nullAddress = false;
//       print(address.address_1);
//       showDialog(
//           context: context,
//           barrierDismissible: false,
//           builder: (context) {
//             return AlertDialog(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(10.0),
//                 ),
//               ),
//               title: Text(
//                 'Saved',
//                 style: TextStyle(color: Color(0xff3a3a3a)),
//               ),
//               actions: <Widget>[
//                 MaterialButton(
//                   elevation: 5.0,
//                   child: Text("ok"),
//                   onPressed: () {
//                     navigate();
//                   },
//                 ),
//               ],
//             );
//           });
//     }
//   }

//   navigate() {
//     Navigator.pop(context);
//     if (_edit != true) Navigator.pop(context);
//   }

//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.of(context).size.width;
//     return addressForm(width);
//   }

//   addressForm(width) {
//     return Form(
//       key: _formKey,
//       child: ListView(
//         reverse: false,
//         children: <Widget>[
//           Container(
//             margin: const EdgeInsets.only(left: 8, right: 8, top: 9),
//             child: Container(
//               padding: const EdgeInsets.only(left: 15, right: 18, bottom: 25, top: 25),
//               color: Colors.transparent,
//               // height: MediaQuery.of(context).size.width * 1.2,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   // Text(
//                   //   "Address:",
//                   //   textAlign: TextAlign.left,
//                   //   style: TextStyle(
//                   //       fontWeight: FontWeight.w600,
//                   //       color: Colors.grey[700],

//                   //       fontSize: 23),
//                   // ),
//                   Container(
//                     child: Stack(
//                       alignment: Alignment.centerRight,
//                       children: <Widget>[
//                         TextFormField(
//                           focusNode: pinFocusNode,
//                           controller: _pinCodeController,
//                           enableSuggestions: true,
//                           keyboardType: TextInputType.number,
//                           decoration: InputDecoration(
//                             labelText: "PIN Code*",
//                             contentPadding:
//                                 EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                             border: OutlineInputBorder(),
//                           ),
//                           validator: (value) {
//                             validation(value);
//                             if (!validPinCode) return "Enter valid pin";
//                             return null;
//                           },
//                         ),
//                         pincodeAddressLoading == true
//                             ? Positioned(
//                                 right: 10,
//                                 //  margin: EdgeInsets.only(right: width/30),
//                                 child: SpinKitWave(
//                                   size: 20,
//                                   color: Colors.green,
//                                   type: SpinKitWaveType.start,
//                                 ),
//                               )
//                             : SizedBox(),
//                       ],
//                     ),
//                   ),

//                   Padding(
//                     padding: const EdgeInsets.only(top:10.0),
//                     child: TextFormField(
//                       controller: _address1,
//                       decoration: InputDecoration(
//                         labelText: "Home No., Building Number*",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                       validator: (value) => validation(value),
//                     ),
//                   ),
//                   // TextFormField(
//                   //   focusNode: address2FocusNode,
//                   //   controller: _address2,
//                   //   validator: (value) => validation(value),
//                   //   decoration:
//                   //       InputDecoration(labelText: "Locality / Area / Colony"),
//                   // ),
//                   Padding(
//                      padding: const EdgeInsets.only(top:10.0),
//                     child: DropdownButtonFormField(
//                       decoration: InputDecoration(
//                         labelText: "Select Area*",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                       value: selectedArea,
//                       items: address.length > 0
//                           ? address.map((String value) {
//                               return DropdownMenuItem<String>(
//                                 value: value,
//                                 child: Text(value),
//                               );
//                             }).toList()
//                           : null,
//                       onChanged: (value) {
//                         setState(() {
//                           selectedArea = value;
//                           for (int i = 0; i < pinCodeData.postals.length; i++) {
//                             if (selectedArea ==
//                                 pinCodeData.postals[i].place_name)
//                               selectedPostalCodeId = pinCodeData.postals[i].id;
//                           }
//                         });
//                       },
//                       validator: (value) => validation(value),
//                     ),
//                   ),

//                   // SimpleAutocompleteFormField(
//                   //   itemBuilder: (context, i) {
//                   //     return Text("$i add");
//                   //   },
//                   //   onSearch: (String search) async => address,
//                   //   onChanged: (value) =>
//                   //       setState(() => _address2.text = value),
//                   //   onSaved: (value) => setState(() => _address2.text = value),
//                   // ),
//                   Padding(

//                      padding: const EdgeInsets.only(top:10.0),

//                     child: TextFormField(
//                       controller: _city,
//                       validator: (value) => validation(value),
//                       decoration: InputDecoration(
//                         labelText: "City*",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),
//                   Padding(
//                      padding: const EdgeInsets.only(top:10.0),
//                     child: TextFormField(
//                       controller: _landmark,
//                       decoration: InputDecoration(
//                         labelText: "Landmark(Optional)",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),

//                   Padding(
//                      padding: const EdgeInsets.only(top:10.0),
//                     child: TextFormField(
//                       controller: _name,
//                       validator: (value) => validation(value),
//                       decoration: InputDecoration(
//                         labelText: "Name*",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),
//                   Padding(
//                      padding: const EdgeInsets.only(top:10.0),
//                     child: TextFormField(
//                       controller: _mobile,
//                       keyboardType: TextInputType.number,
//                       validator: (value) => validation(value),
//                       decoration: InputDecoration(
//                         labelText: "Mobiile Number*",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),
//                   Padding(
//                      padding: const EdgeInsets.only(top:10.0),
//                     child: TextFormField(
//                       controller: _alt_contact,
//                       decoration: InputDecoration(
//                         labelText: "Alternate Contact Number(Optional)",
//                         contentPadding:
//                             EdgeInsets.only(bottom: -1, top: -5, left: 10),
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),

//                   //radio
//                   Container(
//                     margin: const EdgeInsets.only(top: 20),
//                     height: width / 17,
//                     child: AutoSizeText(
//                       "Type :",
//                       presetFontSizes: [
//                         22,
//                         20,
//                         18,
//                       ],
//                       style: optionTextStyle(),
//                     ),
//                   ),
//                   Row(
//                     children: <Widget>[
//                       Row(
//                         children: <Widget>[
//                           Radio(
//                             value: "home",
//                             onChanged: (val) {
//                               isChecked = false;

//                               _setSelectedValue(val);
//                             },
//                             groupValue: _selectedValue,
//                           ),
//                           Container(
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "Home",
//                               presetFontSizes: [22, 20, 18, 16],
//                               style: optionTextStyle(),
//                             ),
//                           )
//                         ],
//                       ),
//                       Row(
//                         children: <Widget>[
//                           Radio(
//                             value: "work",
//                             onChanged: (val) {
//                               isChecked = false;

//                               _setSelectedValue(val);
//                             },
//                             groupValue: _selectedValue,
//                           ),
//                           Container(
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "Work",
//                               presetFontSizes: [22, 20, 18, 16],
//                               style: optionTextStyle(),
//                             ),
//                           )
//                         ],
//                       ),
//                       Row(
//                         children: <Widget>[
//                           Radio(
//                             value: "other",
//                             onChanged: (val) {
//                               isChecked = true;

//                               _setSelectedValue(val);
//                             },
//                             groupValue: _selectedValue,
//                           ),
//                           Container(
//                             height: width / 17,
//                             child: AutoSizeText(
//                               "Other",
//                               presetFontSizes: [22, 20, 18, 16],
//                               style: optionTextStyle(),
//                             ),
//                           )
//                         ],
//                       ),
//                     ],
//                   ),

//                   isChecked == true
//                       ? TextFormField(
//                           controller: _type,
//                           validator: (value) => validation(value),
//                           decoration: InputDecoration(labelText: "Other*"),
//                         )
//                       : Container(),

//                   //save
//                   Container(
//                     alignment: Alignment.center,
//                     color: !saving ? Colors.blue[900] : null,
//                     margin: const EdgeInsets.only(left: 0, top: 10, right: 0),
//                     padding: const EdgeInsets.symmetric(vertical: 15),
//                     child: InkWell(
//                       onTap: () {
//                         if (_formKey.currentState.validate()) {
//                           FocusScope.of(context).unfocus();
//                           setState(() {
//                             saving = true;
//                           });

//                           _edit == true ? ediAddress() : callAddress();
//                         } else {
//                           showDialog(
//                             context: context,
//                             builder: (context) {
//                               return AlertDialog(
//                                 title: Text("Enter valid Details"),
//                                 content: Text(
//                                     "Please check the details you have entered"),
//                                 actions: <Widget>[
//                                   FlatButton(
//                                     child: new Text("Ok"),
//                                     onPressed: () {
//                                       Navigator.of(context).pop();
//                                     },
//                                   )
//                                 ],
//                               );
//                             },
//                           );
//                         }
//                       },
//                       child: saving
//                           ? SpinKitWave(
//                               size: 40,
//                               color: Theme.of(context).primaryColor,
//                               type: SpinKitWaveType.start,
//                             )
//                           : AutoSizeText(
//                               "Save",
//                               style: TextStyle(color: Colors.white),
//                             ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           SizedBox(
//             height: 30,
//           )
//         ],
//       ),
//     );
//   }

// //validation
//   String validation(value) {
//     if (value.isEmpty) return "Fill this Field";
//     return null;
//   }
// }
