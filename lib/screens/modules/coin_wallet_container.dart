import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/theme/theme_const.dart';

class CoinWallet extends StatefulWidget {
  @override
  _CoinWalletState createState() => _CoinWalletState();
}

class _CoinWalletState extends State<CoinWallet> with TickerProviderStateMixin {
  int cashWalletAmountSize = 50;
  bool cashWalletReduced = false;
  int cashWalletHeight = 180;
  int rupeeSignSize = 30;
  int minCashLabelSize = 50;
  int maxCashLabelSize = 70;
  AnimationController _animationController;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels > 60) {
        if (cashWalletReduced == false) {
          _reduceHeader();
        }
      } else {
        if (cashWalletReduced == true) {
          _increaseHeader();
        }
      }
      //print(_scrollController.position.pixels);
//      if (_scrollController.position.pixels ==
//          _scrollController.position.maxScrollExtent) {
//        print('maximum scrolled trigger');
//      }
    });
  }

  void _increaseHeader() {
    cashWalletReduced = false;
    _animationController = AnimationController(
      duration: Duration(milliseconds: 100),
      vsync: this,
      upperBound: 200,
      lowerBound: 73,
    );
    _animationController.forward();
    _animationController.addListener(() {
      setState(() {
        var height = _animationController.value.toInt();
        cashWalletHeight = height;
        if ((height - 130) > 25) {
          rupeeSignSize = height.abs() > 30 ? 30 : height.abs();
          cashWalletAmountSize = (height - 130) > 50 ? 50 : height - 130;
        } else {
          rupeeSignSize = 10;
          cashWalletAmountSize = 25;
        }
      });
    });
  }

  void _reduceHeader() {
    cashWalletReduced = true;
    _animationController = AnimationController(
      duration: Duration(milliseconds: 100),
      vsync: this,
      upperBound: 200,
      lowerBound: 73,
    );

    _animationController.reverse(from: 200);
    _animationController.addListener(() {
      setState(() {
        var height = _animationController.value.toInt();
        cashWalletHeight = height;
        if ((height - 130) > 25) {
          rupeeSignSize = height.abs() > 30 ? 30 : height.abs();
          cashWalletAmountSize = (height - 130) > 50 ? 50 : height - 130;
        } else {
          rupeeSignSize = 10;
          cashWalletAmountSize = 25;
        }
      });
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    if (_animationController != null) {
      _animationController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Stack(
            children: <Widget>[
              // Max Size
              Container(
                decoration: BoxDecoration(
                  color: kThemePrimaryColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  ),
                ),
                width: double.infinity,
                height: cashWalletHeight.toDouble(),
                //color: Colors.pink,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.coins,
                      color: Color.fromRGBO(10, 10, 10, .1),
                      size: (cashWalletHeight - 100).toDouble() < 40 ? 0.0 : (cashWalletHeight - 100).toDouble(),
                    ),
                  ],
                ),
              ),
              Container(
                //color: kThemePrimaryColor,
                height: cashWalletHeight.toDouble(),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            FontAwesomeIcons.rupeeSign,
                            color: Colors.white,
                            size: rupeeSignSize.toDouble(),
                          ),
                          Text(
                            '98,493',
                            style: kCashWalletLabel.copyWith(
                              fontSize: cashWalletAmountSize.toDouble(),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        //crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            'Total Goco Coins',
                            style: kCashWalletSmallLabel,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(
              child: GridView.count(
                controller: _scrollController,
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('He\'d have you all unravel at the'),
                    color: Colors.teal[100],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Heed not the rabble'),
                    color: Colors.teal[200],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Sound of screams but the'),
                    color: Colors.teal[300],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Who scream'),
                    color: Colors.teal[400],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution, they...'),
                    color: Colors.teal[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: const Text('Revolution is coming...'),
                    color: Colors.teal[500],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
