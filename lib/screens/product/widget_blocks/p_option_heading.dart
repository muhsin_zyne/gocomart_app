import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';


class POptionHeading extends StatelessWidget {
  final double width;
  final String head1;
  final String stock;
  final Color color;
  POptionHeading({this.width, this.head1, this.stock, this.color});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomLeft,
          //color: Colors.red,
          margin: EdgeInsets.only(bottom: 10),
          height: width / 13,
          width: width / 2.0,
          child: AutoSizeText(
            head1,
            presetFontSizes: [
              26,
              24,
              22,
              20,
              18,
              16,
              14,
            ],
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: Colors.black54,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 15),
          alignment: Alignment.bottomRight,
          height: width / 18,
          width: width / 3,
          child: AutoSizeText(
            stock,
            textAlign: TextAlign.center,
            maxLines: 1,
            presetFontSizes: [22, 20, 18, 16, 14, 12, 10, 8],
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: color,
            ),
          ),
        )
      ],
    );
  }
}
