import 'package:auto_size_text/auto_size_text.dart';
import 'package:color/color.dart' as customColor;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/product_detail/customer_offer/offeredOptions.dart';
import 'package:gocomartapp/models/product_detail/customer_offer/optionValue.dart';
import 'package:gocomartapp/models/product_detail/p_option_values.dart';
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/providers/product_provider.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/theme/hexcolor.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

import 'p_option_heading.dart';
import 'product_option_child_list.dart';

class ProductOptionBlock extends StatefulWidget {
  final Function() notifyParent;

  final ProductDetail productDetail;
  final GlobalProvider globalProvider;
  final ShoppingServices shoppingServices;
  ProductOptionBlock({@required this.productDetail, this.globalProvider, this.shoppingServices, @required this.notifyParent});
  @override
  _ProductOptionBlockState createState() => _ProductOptionBlockState();
}

class _ProductOptionBlockState extends State<ProductOptionBlock> {
  List<Widget> pOptionWidgets = [];
  List<Widget> currentBuildingItems = [];
  List<Widget> currentChildOptionList = [];
  List<dynamic> currentChildOptionListRaw = [];
  List<dynamic> currentChildOptionRawList = [];
  int currentOptionValueId = 0;
  int currentChildOptionValue = 0;
  int subSelectedOptionId = 0;
  OCImageCache ocImageCache = OCImageCache();

  POptionValues userSelectedOption;
  ProductProvider _productProvider;

  String _productOptionSubSelectionHeading = '';

  @override
  void initState() {
    super.initState();
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    _productProvider = Provider.of<ProductProvider>(context);

    /// blank set state to load the build after load Client triggered
    setState(() {});
  }

  void refresh() {
    setState(() {
      widget.notifyParent();
    });
  }

  bool tapped = false;
  @override
  Widget build(BuildContext context) {
    buildOptionList();
//    return Container();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: pOptionWidgets,
          ),
          ProductOptionChildFinal(
            optionBuilderList: currentChildOptionListRaw,
            swipeOptionHeading: _productOptionSubSelectionHeading,
            notifyParent: refresh,
          ),
          // SwipeOptionBlock(
          //   actionItems: currentChildOptionList,
          //   swipeOptionHeading: _productOptionSubSelectionHeading,
          // ),
        ],
      ),
    );
  }

  void buildOptionList() {
    tapped = false;
    pOptionWidgets = [];
    currentBuildingItems = [];
    for (int i = 0; i < widget.productDetail.product_option.length; i++) {
      final cPOption = widget.productDetail.product_option[i];
      setState(
        () {
          pOptionWidgets.add(
            Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: headings(
                    MediaQuery.of(context).size.width,
                    cPOption.option.description.name,
                    "",
                    Colors.red,
                  ),
                ),
              ],
            ),
          );
        },
      );
      //dynamic currentSelectedOption = cPOption.optionValues[0].description.name;
      if (cPOption.option.type == 'select') {
        // color block
        for (int j = 0; j < cPOption.optionValues.length; j++) {
          final cOptionValue = cPOption.optionValues[j];
          //print(cOptionValue.toJson());

          var productOptionImage = '';
          if (cPOption.optionValues[j].optionImage.image != '') {
            productOptionImage = cPOption.optionValues[j].optionImage.image;
          } else {
            productOptionImage = widget.productDetail.image;
          }
          var productOptionCachedImg = ocImageCache.getCachedImage(
            path: productOptionImage,
            imageQuality: ImageQuality.high,
          );

          var productApplicablePrice = 0;
          setState(() {
            productApplicablePrice = widget.productDetail.price;
          });

          if (widget.productDetail.special.price != null && widget.productDetail.special.price < widget.productDetail.price) {
            setState(() {
              productApplicablePrice = widget.productDetail.special.price;
            });
          }

          //bool isFlatOffer = false;

          /// considering customer special offer for product options
          if (_productProvider?.customerOffer != null) {
            if (_productProvider.customerOffer?.customerPrice?.is_flexible_option == true) {
              if (_productProvider.customerOffer.customerPrice.offer_price < productApplicablePrice) {
                setState(() {
                  productApplicablePrice = _productProvider.customerOffer.customerPrice.offer_price;
                });
              }
            } else {
              if (_productProvider.customerOffer?.customerPrice?.option_length ?? 0 > 0) {
                cPOption.optionValues[j].isFlatOffer = this._findThisProductOptionHasCustomerOffer(cPOption.optionValues[j]);
                if (cPOption.optionValues[j].isFlatOffer == true) {
                  setState(() {
                    productApplicablePrice = _productProvider.customerOffer.customerPrice.offer_price;
                  });
                }
              }
            }
          }

          var priceOperator = cPOption.optionValues[j].price_prefix.toString();
          if (cPOption.optionValues[j].isFlatOffer != true) {
            setState(() {
              // price variation in product options
              if (cPOption.optionValues[j].price != 0) {
                if (priceOperator == '-') {
                  productApplicablePrice = productApplicablePrice - cPOption.optionValues[j].price;
                } else if (priceOperator == '+') {
                  productApplicablePrice = productApplicablePrice + cPOption.optionValues[j].price;
                }
              }
            });
          }

          currentBuildingItems.add(
            InkWell(
              child: Container(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: cOptionValue.quantity < 1
                          ? Colors.transparent
                          : (currentOptionValueId == cOptionValue.product_option_value_id
                              ? GocoMartAppTheme.buildLightTheme().primaryColor
                              : Colors.transparent),
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        //color: Colors.indigoAccent,
                        height: MediaQuery.of(context).size.width / 4,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width / 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.network(
                                    "${widget.globalProvider.cdnImagePoint}cache/$productOptionCachedImg",
                                    scale: 1.4,
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.only(top: 10),
                            ),
                            cOptionValue.quantity < 1
                                ? Positioned.fill(
                                    child: Center(
                                      child: Container(
                                        color: Colors.white,
                                        child: AutoSizeText(
                                          'Out of Stock',
                                          presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
                                          style: TextStyle(
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 4,
                        //margin: EdgeInsets.only(top: 10, left: 15),
                        child: AutoSizeText(
                          "₹ $productApplicablePrice",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 15, 14],
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ),
              onTap: cOptionValue.quantity > 0
                  ? () {
                      //print(cPOption.optionValues[j].toJson());
                      //print(cPOption.optionValues[j].optionImage.image);

                      actionTapOnProductOption(cOptionValue);
                    }
                  : actionOutOfStockMessage,
            ),
          );
        }

        Widget toAppend = SwipeOptionBlock(actionItems: currentBuildingItems);
        currentBuildingItems = [];
        setState(() {
          pOptionWidgets.add(toAppend);
        });
      } else {
        // general block
        for (int j = 0; j < cPOption.optionValues.length; j++) {
          final POptionValues cOptionValue = cPOption.optionValues[j];
          //print(cOptionValue.toJson());
          currentBuildingItems.add(
            InkWell(
              child: Container(
                alignment: Alignment.centerLeft,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Card(
                  color:
                      cOptionValue.quantity < 1 ? Colors.white : ((cOptionValue.product_option_value_id == currentOptionValueId) ? Colors.blue : Colors.white),
                  shape: StadiumBorder(
                    side: BorderSide(color: Colors.black54, width: 1.5),
                  ),
                  child: Container(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.width / 11,
                    child: Text(
                      cOptionValue.description.name,
                      style: TextStyle(
                          color: cOptionValue.quantity < 1
                              ? Colors.red
                              : ((cOptionValue.product_option_value_id == currentOptionValueId) ? Colors.white : Colors.black)),
                      textAlign: TextAlign.center,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
              onTap: cOptionValue.quantity > 0
                  ? () {
                      setState(() {
                        //currentSelectedOption = cPOption.optionValues[j].description.name;
                        _productProvider.currentOptionValueId = currentOptionValueId;
                      });
                      actionTapOnProductOption(cOptionValue);
                    }
                  : actionOutOfStockMessage,
            ),
          );
        }

        Widget toAppend = SwipeOptionBlock(actionItems: currentBuildingItems);
        currentBuildingItems = [];
        setState(() {
          pOptionWidgets.add(toAppend);
        });
      }
    }
  }

  bool _findThisProductOptionHasCustomerOffer(POptionValues pOption) {
    for (int i = 0; i < _productProvider.customerOffer.offeredOptions.length; i++) {
      OfferedOptions cOfferedOptions = _productProvider.customerOffer.offeredOptions[i];

      for (int j = 0; j < cOfferedOptions.optionValue.length; j++) {
        OptionValue cOptionValue = cOfferedOptions.optionValue[j];
        if ((pOption.product_option_value_id == cOptionValue.product_option_value_id) && (pOption.product_option_id == cOptionValue.product_option_id)) {
          return true;
        }
      }
    }
    return false;
  }

  actionOutOfStockMessage() {
    Fluttertoast.showToast(
      msg: "Sorry! Item out of stock",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  actionTapOnProductOption(POptionValues cOptionValues) async {
    loadNewImage(cOptionValues.product_option_value_id);
    //print(cOptionValues.toJson());
    _productProvider.pCartOptionData[0]['product_option_id'] = cOptionValues.product_option_id;
    _productProvider.pCartOptionData[0]['product_option_value_id'] = cOptionValues.product_option_value_id;
    setState(() {
      _productProvider.updatePriceInfo(cOptionValues);
    });
    setState(() {
      currentOptionValueId = cOptionValues.product_option_value_id;
      _productProvider.currentOptionValueId = currentOptionValueId;
    });

    if (cOptionValues.child == null) {
      setState(() {
        //currentChildOptionList = [];
        userSelectedOption = cOptionValues;
        _productProvider.setCurrentProductOptionSelected(userSelectedOption);
      });
    } else {
      userSelectedOption = null;
      buildOnSubSelection(cOptionValues);
    }

    widget.notifyParent();
  }

  loadNewImage(pOptionValueId) async {
    QueryResult pOptionAdditionalImg = await widget.shoppingServices.productOptionImages(pOptionValueId: pOptionValueId);
    if (!pOptionAdditionalImg.hasException) {
      final List<String> imageList = [];

      if (pOptionAdditionalImg.data['productOptionImages']['image'] != null) {
        imageList.add("${widget.globalProvider.cdnImagePoint}${pOptionAdditionalImg.data['productOptionImages']['image']}");
      }
      if (pOptionAdditionalImg.data['productOptionImages']['images'].length > 0) {
        for (int i = 0; i < pOptionAdditionalImg.data['productOptionImages']['images'].length; i++) {
          var currentImage = pOptionAdditionalImg.data['productOptionImages']['images'][i];
          imageList.add("${widget.globalProvider.cdnImagePoint}$currentImage");
        }
      }
      if (imageList.length > 0) {
        await _productProvider.reloadImage(imageList);
      }
    } else {}
    widget.notifyParent();
  }

  buildOnSubSelection(POptionValues cOptionValues) {
    //print(cOptionValues.toJson());
    _productProvider.currentOptionValueId = 0;
    if (cOptionValues.child.option?.description?.name != '') {
      setState(() {
        _productOptionSubSelectionHeading = cOptionValues.child.option.description.name;
      });
    } else {
      setState(() {
        _productOptionSubSelectionHeading = '';
      });
    }
    //print(cOptionValues.child.option.description.name);
    if (cOptionValues.child.option.type == 'select') {
      // color block  generate script
      // not using as of now as color is the main object
      setState(() {
        currentChildOptionList = [];
      });

      for (int i = 0; i < cOptionValues.child.childOptionItems.length; i++) {
        final cChildOptionItem = cOptionValues.child.childOptionItems[i];
        setState(() {
          currentChildOptionList.add(
            InkWell(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                width: 50,
                height: 50,
                child: Text(
                  cChildOptionItem.description.name,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              onTap: () {
                actionTapOnProductOption(cChildOptionItem);
              },
            ),
          );
        });
      }
    } else {
      // general selection generate script
      setState(() {
        currentChildOptionList = [];
        currentChildOptionListRaw = [];
      });
      for (int i = 0; i < cOptionValues.child.childOptionItems.length; i++) {
        final cChildOptionItem = cOptionValues.child.childOptionItems[i];
        setState(() {
          currentChildOptionListRaw.add(cChildOptionItem);
        });
      }
    }
  }

  getColor(String colorName) {
    customColor.RgbColor colorData = customColor.RgbColor.name(colorName.toLowerCase()); //directly reference the const RgbColor without the factory
    return HexColor("#${colorData.toHexColor()}");
  }

  Widget headings(double width, String head1, String stock, Color color) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomLeft,
          //color: Colors.red,
          margin: const EdgeInsets.only(left: 15, bottom: 10),
          height: width / 13,
          width: width / 2.0,
          child: AutoSizeText(
            head1,
            presetFontSizes: [
              26,
              24,
              22,
              20,
              18,
              16,
              14,
            ],
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: Colors.black54,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 15),
          alignment: Alignment.bottomRight,
          height: width / 18,
          width: width / 3,
          child: AutoSizeText(
            stock,
            textAlign: TextAlign.center,
            maxLines: 1,
            presetFontSizes: [22, 20, 18, 16, 14, 12, 10, 8],
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: color,
            ),
          ),
        )
      ],
    );
  }
}

class SwipeOptionColorBlock extends StatelessWidget {
  final List<Widget> actionItems;
  final String swipeOptionHeading;
  const SwipeOptionColorBlock({Key key, @required this.actionItems, this.swipeOptionHeading = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 13, bottom: 10),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          children: <Widget>[
            swipeOptionHeading != ''
                ? POptionHeading(
                    head1: swipeOptionHeading,
                    width: MediaQuery.of(context).size.width,
                    //color: Colors.red,
                    stock: "",
                  )
                : Container(),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: actionItems),
          ],
        ),
      ),
    );
  }
}

class SwipeOptionBlock extends StatelessWidget {
  final List<Widget> actionItems;
  final String swipeOptionHeading;
  const SwipeOptionBlock({Key key, @required this.actionItems, this.swipeOptionHeading = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 13, bottom: 8),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          children: <Widget>[
            swipeOptionHeading != ''
                ? POptionHeading(
                    head1: swipeOptionHeading,
                    width: MediaQuery.of(context).size.width,
                    //color: Colors.red,
                    stock: "",
                  )
                : Container(),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: actionItems),
          ],
        ),
      ),
    );
  }
}

class ClipRecentView extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();

    path.lineTo(0.0, size.height / 10);

    var secondControlPoint = Offset((size.width / 2.4), size.height / 150);
    var secondEndPoint = Offset(size.width, size.height / 3.2);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    return path;
  }

  @override
  bool shouldReclip(ClipRecentView oldClipper) => true;
}
