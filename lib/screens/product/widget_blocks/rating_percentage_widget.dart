import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';

class RatingPercentageWidget extends StatelessWidget {
  const RatingPercentageWidget({Key key, @required this.width, @required this.starNumber, @required this.largestRating, @required this.countOfRating}) : super(key: key);

  final double width;
  final double largestRating;
  final int starNumber;
  final dynamic countOfRating;

  Color progressColor(rating) {
    if (rating == 5)
      return Colors.green[600];
    else if (rating == 4)
      return Colors.lightGreen[700];
    else if (rating == 3)
      return Colors.lightGreen;
    else if (rating == 2)
      return Colors.orange[700];
    else if (rating == 1)
      return Colors.red;
    else {
      return Colors.transparent;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          "$starNumber",
          style: TextStyle(color: Colors.grey[600], fontSize: width / 28),
        ),
        Icon(
          Icons.star,
          size: width / 23,
          color: Colors.grey[600],
        ),
        LinearPercentIndicator(width: width / 3.5, lineHeight: width / 55, percent: countOfRating / largestRating, progressColor: progressColor(starNumber)),
        Text(
          "$countOfRating",
          style: TextStyle(color: Colors.grey[600], fontSize: width / 28),
        ),
      ],
    );
  }
}
