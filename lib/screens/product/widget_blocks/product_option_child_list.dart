import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gocomartapp/models/product_detail/p_option_values.dart';
import 'package:gocomartapp/providers/product_provider.dart';
import 'package:provider/provider.dart';

import 'p_option_heading.dart';

class ProductOptionChildFinal extends StatefulWidget {
  final Function() notifyParent;

  final List<dynamic> optionBuilderList;
  final String swipeOptionHeading;

  ProductOptionChildFinal({this.optionBuilderList, this.swipeOptionHeading = '', @required this.notifyParent});
  @override
  _ProductOptionChildFinalState createState() => _ProductOptionChildFinalState();
}

class _ProductOptionChildFinalState extends State<ProductOptionChildFinal> {
  int currentOptionValueId;
  POptionValues userSelectedOption;
  //GlobalProvider _globalProvider;
  ProductProvider _productProvider;

  @override
  void initState() {
    super.initState();
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    //_globalProvider = Provider.of<GlobalProvider>(context);
    _productProvider = Provider.of<ProductProvider>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: widget.swipeOptionHeading != ''
              ? POptionHeading(
                  head1: widget.swipeOptionHeading,
                  width: MediaQuery.of(context).size.width,
                  //color: Colors.red,
                  stock: "",
                )
              : Container(),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          height: 50,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: this.widget.optionBuilderList.length,
            itemBuilder: (BuildContext context, int index) {
              final POptionValues cChildOptionItem = this.widget.optionBuilderList[index];
              return InkWell(
                child: Container(
                  alignment: Alignment.centerLeft,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Card(
                    color: cChildOptionItem.quantity < 1 ? Colors.white : ((cChildOptionItem.product_option_value_id == _productProvider.currentOptionValueId) ? Colors.blue : Colors.white),
                    shape: StadiumBorder(
                      side: BorderSide(color: Colors.black54, width: 1.5),
                    ),
                    child: Container(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.width / 11,
                      child: Text(
                        cChildOptionItem.description.name,
                        style: TextStyle(
                            color: cChildOptionItem.quantity < 1 ? Colors.red : ((cChildOptionItem.product_option_value_id == _productProvider.currentOptionValueId) ? Colors.white : Colors.black)),
                        textAlign: TextAlign.center,
                        maxLines: 1,
                      ),
                    ),
                  ),
                ),
                onTap: cChildOptionItem.quantity > 0
                    ? () {
                        if (cChildOptionItem.quantity > 0) {
                          actionTapOnProductOption(cChildOptionItem);
                        }
                      }
                    : actionOutOfStockMessage,
              );
            },
          ),
        ),
      ],
    );
  }

  actionTapOnProductOption(POptionValues cOptionValues) {
    _productProvider.pCartOptionData[1]['product_option_id'] = cOptionValues.product_option_id;
    _productProvider.pCartOptionData[1]['product_option_value_id'] = cOptionValues.product_option_value_id;
    setState(() {
      _productProvider.updatePriceInfoForChild(cOptionValues);
    });
    setState(() {
      _productProvider.currentOptionValueId = cOptionValues.product_option_value_id;
    });
    if (cOptionValues.child == null) {
      _productProvider.setCurrentProductOptionSelected(cOptionValues);
    }
    widget.notifyParent();
  }

  actionOutOfStockMessage() {
    Fluttertoast.showToast(
      msg: "Sorry! Item out of stock",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }
}
