import 'package:flutter/material.dart';
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/shopping_services.dart';

class ProductOptionV2 extends StatefulWidget {
  final ProductDetail productDetail;
  final GlobalProvider globalProvider;
  final ShoppingServices shoppingServices;
  ProductOptionV2({
    @required this.productDetail,
    this.globalProvider,
    this.shoppingServices,
  });
  @override
  _ProductOptionV2State createState() => _ProductOptionV2State();
}

class _ProductOptionV2State extends State<ProductOptionV2> {
  //ProductProvider _productProvider;
  @override
  void initState() {
    super.initState();
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    //_productProvider = Provider.of<ProductProvider>(context);

    /// blank set state to load the build after load Client triggered
    setState(() {});
  }

  triggerFormatter() {}

  @override
  Widget build(BuildContext context) {
    triggerFormatter();
    return Container();
  }
}
