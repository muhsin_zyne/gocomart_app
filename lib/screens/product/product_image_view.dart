import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:photo_view/photo_view.dart';
import 'package:extended_image/extended_image.dart';

var _images;

class ProductImageView extends StatefulWidget {
  int currentIndex;
  ProductImageView(ig, int currentImageIndex) {
    _images = ig;
    currentIndex = currentImageIndex;
  }
  @override
  _ProductImageViewState createState() => _ProductImageViewState();
}

class _ProductImageViewState extends State<ProductImageView> {
  int currentIndex;
  bool initialImage = true;
  @override
  void initState() {
    currentIndex = widget.currentIndex != null ? widget.currentIndex : 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 9.0, top: 12),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: new BoxDecoration(
                      color: Colors.black54,
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                      ),
                    )),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                height: MediaQuery.of(context).size.height / 1.3,
                //  padding: const EdgeInsets.only(top:10),
                // color: Colors.red,
                child: PhotoViewGallery.builder(
                  //   // scrollPhysics: const BouncingScrollPhysics(),
                  //   //  onPageChanged: (int i){},
                  itemCount: _images.length,

                  backgroundDecoration: BoxDecoration(color: Colors.white),
                  onPageChanged: (index) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                  pageController: PageController(initialPage: currentIndex),
                  builder: (context, index) {
                    return PhotoViewGalleryPageOptions(
                      imageProvider: NetworkImage(
                        "${_images[index]}",
                      ),
                      minScale: PhotoViewComputedScale.contained,
                      maxScale: PhotoViewComputedScale.contained * 3,
                      // heroAttributes: PhotoViewHeroAttributes(tag: index),
                    );
                  },
                ),
                // child: ExtendedImageGesturePageView.builder(
                //     itemCount: _images.length,
                //     onPageChanged: (int index) {
                //       setState(() {
                //         currentIndex = index;
                //       });
                //     },
                //     itemBuilder: (BuildContext context, int index) {
                //       Widget img = ExtendedImage.network(
                //         _images[index],
                //         fit: BoxFit.contain,
                //         mode: ExtendedImageMode.gesture,
                //       );
                //       img = Container(
                //         child: img,
                //       );
                //       if (index == currentIndex) {
                //         return Hero(
                //           tag: _images[index] + index.toString(),
                //           child: img,
                //         );
                //       } else {
                //         return img;
                //       }
                //     }),
              ),
              Container(
                  margin: EdgeInsets.only(top: 20, right: 20),
                  child: Text(
                      "${currentIndex != null ? currentIndex + 1 : 1}/${_images.length}"))
            ],
          ),
        ],
      ),
    );
  }
}
