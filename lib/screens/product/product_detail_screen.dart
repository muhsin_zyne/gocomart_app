import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/address/address.dart';
import 'package:gocomartapp/models/cart/cartResponse.dart';
import 'package:gocomartapp/models/cart/productBuyNowPreview.dart';
import 'package:gocomartapp/models/product_detail/delivery_estimation/delivery_estimation.dart';
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'package:gocomartapp/models/wishlist_status/wishlist_status.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/providers/product_provider.dart';
import 'package:gocomartapp/screens/cart/cart_screen.dart';
import 'package:gocomartapp/screens/cart/summary_screen.dart';
import 'package:gocomartapp/screens/product/product_image_view.dart';
import 'package:gocomartapp/screens/product/product_review_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/util/product_helper.dart';
import 'package:graphql/client.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skeleton_text/skeleton_text.dart';

import './widget_blocks/product_option_block.dart';
import './widget_blocks/rating_percentage_widget.dart';
import '../../theme/theme_const.dart';

class ProductDetailScreen extends StatefulWidget {
  final productId;

  ProductDetailScreen({@required this.productId});

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  ShoppingServices _shoppingServices;
  GlobalProvider _globalProvider;
  ProductProvider _productProvider;
  CartProvider _cartProvider;
  ProductDetail _productDetail;
  AddressProvider _addressProvider;
  WishlistStatus _wishlistStatus;
  DeliveryEstimation _deliveryEstimation;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final TextEditingController _controller = TextEditingController();
  ScrollController _scrollController;
  List<String> images = <String>[];
  final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
  bool isLoded = false;
  double largestRating = 0.0;

  double specialPrice = 0.0;
  double productPrice = 0.0;
  double displayPrice = 0.0;
  int percentage;
  bool _globalLoaded = false;
  //bool _buyNowEnabled = false;
  bool wishList = false;
  bool estimationLoaded = false;
  bool estimationLoading = false;
  bool invalidPin = false;
  bool _addingToCart = false;
  bool _buyNowItem = false;
  bool _outOfStockItem = false;
  int pin;
  String manufacturerName;

  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();

    _scrollController = ScrollController(
      initialScrollOffset: 0.0,
      keepScrollOffset: true,
    );
  }

  void loadClient() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {
      pin = prefs.getInt('pincode');
      _controller.text = pin.toString();
      if (pin == null) _controller.text = "";
    });
    await Future.delayed(Duration(microseconds: 10));
    _globalProvider = Provider.of<GlobalProvider>(context);
    _productProvider = Provider.of<ProductProvider>(context);
    _cartProvider = Provider.of<CartProvider>(context);
    _productProvider.markProductOpenedAsNew();
    _addressProvider = Provider.of<AddressProvider>(context);
    init();
  }

  void init() async {
    setState(() {
      _globalLoaded = true;
    });

    //Wish list  status
    // load customer offer by product in to product provider
    QueryResult _wishListResult = await _shoppingServices.wishListStatus(product: widget.productId);
    if (!_wishListResult.hasException) {
      _wishlistStatus = WishlistStatus.fromJson(_wishListResult.data['wishlistStatus']);
      setState(() {
        print(_wishListResult.data);
        if (_wishlistStatus.isTrue == true)
          wishList = true;
        else
          wishList = false;
      });
    }
    this._loadCustomerOfferByProduct();

    specialPrice = 0.0;
    displayPrice = 0.0;
    ProductHelper _productHelper = ProductHelper();
    QueryResult productDtlResponse = await _shoppingServices.getProductDtl(productId: widget.productId);
    if (!productDtlResponse.hasException) {
      var productData = productDtlResponse.data['product'];
      manufacturerName = productDtlResponse.data['product']['manufacturer']['name'];
      dynamic formattedOption = await _productHelper.formatProductOption(productData['product_option']);
      dynamic reviewSummary = await _productHelper.formatReviewSummary(productData['ratingSummary']);
      productData['ratingSummary'] = reviewSummary;
      productData['product_option'] = formattedOption;
      _productDetail = ProductDetail.fromJson(productData);
      if (_productDetail.product_option.length == 0) {
        _productProvider.buyNowPermission = true;
      }
      if (_productDetail.quantity <= 0) {
        setState(() {
          _productProvider.buyNowPermission = false;
          _outOfStockItem = true;
        });
      } else {
        _outOfStockItem = false;
      }
      setDefaultProductImages();
      setState(() {
        isLoded = true;
        _productProvider.productStdPrice = _productDetail.price.toDouble();
        _productProvider.productBaseStdPrice = _productDetail.price.toDouble();
        _productProvider.productDisplayPrice = _productDetail.price.toDouble();
      });

      if (_productDetail.special.price != null && _productDetail.special.price < _productDetail.price) {
        _productProvider.productSpPrice = _productDetail.special.price.toDouble();
        _productProvider.productBaseSpecialPrice = _productDetail.special.price.toDouble();
      }

      largestRating = _productDetail.ratingSummary[0].count_of_rating.toDouble();
      setState(() {
        for (int i = 0; i < _productDetail.ratingSummary.length; i++) {
          if (_productDetail.ratingSummary[i].count_of_rating.toDouble() > largestRating)
            largestRating = _productDetail.ratingSummary[i].count_of_rating.toDouble();
        }
      });
      this._injectCustomerPriceInToScreen();
    } else {
      //print(productDtlResponse.errors);
    }
  }

  void _loadCustomerOfferByProduct() async {
    QueryResult customerOfferData = await _shoppingServices.customerOfferByProduct(productId: widget.productId);
    if (customerOfferData.data['customerOfferByProduct']['customerOffer'] != null) {
      _productProvider.customerOffer = customerOfferData.data['customerOfferByProduct']['customerOffer'];
      setState(() {});
    } else {
      _productProvider.customerOffer = null;
    }
  }

  void _injectCustomerPriceInToScreen() {
    _productProvider.notify();
    if (_productProvider.customerOffer != null && _productProvider?.customerOffer?.customerPrice != null) {
      if (_productProvider.productDisplayPrice > _productProvider.customerOffer.customerPrice.offer_price) {
        _productProvider.productSpPrice = _productProvider.customerOffer.customerPrice.offer_price.toDouble();
      }
    } else {}
//    if (_productProvider?.customerOffer != null) {
//      print(_productProvider.customerOffer.toJson());
//      if (_productProvider.productDisplayPrice >
//          _productProvider.customerOffer.customerPrice.offer_price) {
//        _productProvider.productSpPrice =
//            _productProvider.customerOffer.customerPrice.offer_price.toDouble();
//      }
//    }
  }

  void setDefaultProductImages() {
    _productProvider.pImages.add("${imagePath + _productDetail.image}");
    //images.add("${imagePath + _productDetail.image}");
    for (int i = 0; i < _productDetail.images.length; i++) {
      setState(() {
        //images.add("${imagePath + _productDetail.images[i].image}");
        _productProvider.pImages.add("${imagePath + _productDetail.images[i].image}");
      });
    }
  }

  addRemoveWishList() async {
    if (wishList == false) {
      setState(() {
        wishList = true;
      });
      await _shoppingServices.addToWishList(productId: widget.productId);
    } else if (wishList == true) {
      setState(() {
        wishList = false;
      });
      await _shoppingServices.removeFromWishList(productId: widget.productId);
    }
  }

  //delivery estimation time
  checkEstimationTime() async {
    QueryResult result = await _shoppingServices.deliveryEstimation(postalCode: pin);
    print(result.data);
    if (!result.hasException) {
      _deliveryEstimation = DeliveryEstimation.fromJson(result.data["deliveryEstimation"]);
      setState(() {
        estimationLoaded = true;
      });
      if (_deliveryEstimation.error != null) {
        setState(() {
          invalidPin = true;
        });
      } else {
        setState(() {
          invalidPin = false;
        });
        final SharedPreferences prefs = await _prefs;
        prefs.setInt('pincode', pin);
      }
    }
  }

  // add to cart action
  addToCart() async {
    setState(() {
      this._addingToCart = true;
    });
    QueryResult queryResult = await _shoppingServices.addToCart(
      productId: widget.productId,
      cartOptionInput: _productProvider.pCartOptionDataApi,
    );
    if (!queryResult.hasException) {
      this._cartProvider.cartResponse = CartResponse.fromJson(queryResult.data['addToCart']);
//      CartResponse cartResponse =
//          CartResponse.fromJson(queryResult.data['addToCart']);
      if (this._cartProvider.cartResponse.error == false) {
        showSimpleNotification(
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    AutoSizeText(
                      this._cartProvider.cartResponse.message,
                      presetFontSizes: [15, 14, 13, 12, 11, 10, 9, 8],
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CartScreen()),
                        );
                      },
                      child: Container(
                        child: Text(
                          'View Cart',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          background: Colors.green,
        );
      } else {
        showSimpleNotification(
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    AutoSizeText(
                      this._cartProvider.cartResponse.message,
                      presetFontSizes: [15, 14, 13, 12, 11, 10, 9, 8],
                    ),
                  ],
                )
              ],
            ),
          ),
          background: Colors.red,
        );
      }
    }
    setState(() {
      this._addingToCart = false;
    });
  }

  void _buyNowProductTrigger() async {
    setState(() {
      this._buyNowItem = true;
    });
    QueryResult queryResult = await _shoppingServices.singleProductBuyNow(
      productId: widget.productId,
      cartOptionInput: _productProvider.pCartOptionDataApi,
    );
    if (!queryResult.hasException) {
      QueryResult dDeliveryAddress = await _shoppingServices.defaultDeliveryAddress();
      _cartProvider.productBuyNowPreview = ProductBuyNowPreview.fromJson(queryResult.data['singleProductBuyNow']);
      if (!dDeliveryAddress.hasException) {
        if (dDeliveryAddress.data['defaultDeliveryAddress']['address'] != null) {
          _addressProvider.currentAddress = Address.fromJson(dDeliveryAddress.data['defaultDeliveryAddress']['address']);
        } else {
          _addressProvider.currentAddress = Address.fromJson({});
        }
      }
      setState(() {
        this._buyNowItem = false;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SummaryScreen(),
        ),
      ).whenComplete(() {
        this.pageReloaded();
      });
    } else {
      setState(() {
        this._buyNowItem = false;
      });
    }
  }

  void pageReloaded() {
    setState(() {
      _cartProvider.notify();
    });
  }

  refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    int currentImageIndex;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: InkWell(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              _productProvider?.ready == true
                  ? Stack(
                      children: <Widget>[
                        InkWell(
                          onTap: _productProvider.pImages.length >= 1
                              ? () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ProductImageView(_productProvider.pImages, currentImageIndex),
                                    ),
                                  );
                                }
                              : null,
                          child: Container(
                            margin: const EdgeInsets.only(top: 25),
                            height: width / 1.3,
                            child: SizedBox.expand(
                              child: isLoded == true
                                  ? Stack(
                                      children: <Widget>[
                                        Swiper(
                                          itemBuilder: (BuildContext context, int index) {
                                            return Image.network(
                                              "${_productProvider.pImages[index]}",
                                              fit: BoxFit.contain,
                                            );
                                          },
                                          itemCount: _productProvider.pImages.length,
                                          pagination: new SwiperPagination(),
                                          //control: new SwiperControl(),
                                          index: 0,
                                          onIndexChanged: (value) {
                                            currentImageIndex = value;
                                          },
                                        ),
                                        _productProvider.imageLoading == true
                                            ? Positioned.fill(
                                                child: Center(
                                                child: CircularProgressIndicator(),
                                              ))
                                            : Container(),
                                      ],
                                    )
                                  : Container(
                                      child: SkeletonAnimation(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.grey[300],
                                          ),
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                        ),
                        Container(
                          constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height / 1.75),
                          margin: EdgeInsets.only(top: width / 1.21, right: 0),
                          decoration: BoxDecoration(boxShadow: []),
                          child: Center(
                            child: Card(
                              elevation: 10,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25),
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(top: 28.0, left: 18, right: 16),
                                        child: Container(
                                          width: width / 1.6,
                                          height: width / 7,
                                          child: Container(
                                            width: width / 10,
                                            child: isLoded
                                                ? AutoSizeText(
                                                    '${_productDetail.description.name}',
                                                    presetFontSizes: [
                                                      20,
                                                      18,
                                                      16,
                                                      14,
                                                    ],
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      letterSpacing: 0.27,
                                                      color: Colors.black54,
                                                    ),
                                                  )
                                                : Container(
                                                    width: width / 15,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 1,
                                                          child: SkeletonAnimation(
                                                            child: Container(
                                                              height: 10,
                                                              decoration: BoxDecoration(
                                                                color: Colors.grey[300],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: SkeletonAnimation(
                                                            child: Container(
                                                              height: 10,
                                                              decoration: BoxDecoration(
                                                                color: Colors.grey[300],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                          ),
                                        ),
                                      ),

                                      //rating

                                      // TODO rating API integration
                                      Container(
                                        padding: const EdgeInsets.only(
                                          top: 50,
                                        ),
                                        margin: EdgeInsets.only(right: width / 13),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              height: width / 14,
                                              width: width / 12,
                                              child: AutoSizeText(
                                                '${(_productDetail?.avgRating?.avg_rating ?? 0.0).toStringAsFixed(1)}',
                                                textAlign: TextAlign.left,
                                                presetFontSizes: [22, 20, 18, 16, 14, 12],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w200,
                                                  letterSpacing: 0.27,
                                                  color: Colors.green,
                                                ),
                                              ),
                                            ),
                                            Icon(
                                              Icons.star,
                                              color: Colors.green,
                                              size: width / 18,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: const EdgeInsets.only(left: 15, bottom: 8, top: 16),
                                    width: width / 1.2,
                                    child: isLoded
                                        ? Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment.center,
                                                    height: width / 13,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          padding: EdgeInsets.only(top: 2),
                                                          height: width / 17,
                                                          child: AutoSizeText(
                                                            '₹',
                                                            maxLines: 1,
                                                            //textAlign: TextAlign.end,
                                                            presetFontSizes: [23, 21, 19, 17, 16],
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.w700,
                                                              color: Colors.green,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: width / 14,
                                                          child: AutoSizeText(
                                                            '${_productProvider.productDisplayPrice.round()}',
                                                            maxLines: 1,
                                                            textAlign: TextAlign.center,
                                                            presetFontSizes: [24, 22, 20, 18],
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.w700,
                                                              letterSpacing: 0.27,
                                                              color: Colors.green,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: const EdgeInsets.only(left: 12, top: 3),
                                                    alignment: Alignment.center,
                                                    // color: Colors.blue,
                                                    height: width / 17,
                                                    child: _productProvider.productSpPrice == 0.0
                                                        ? Container()
                                                        : AutoSizeText(
                                                            '₹${_productProvider.productStdPriceDis.round()}',
                                                            textAlign: TextAlign.center,
                                                            presetFontSizes: [22, 20, 18, 16, 14],
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.w400,
                                                                letterSpacing: 0.27,
                                                                color: Colors.grey[600],
                                                                decoration: TextDecoration.lineThrough,
                                                                decorationColor: Colors.black54),
                                                          ),
                                                  ),
                                                  Container(
                                                    margin: const EdgeInsets.only(left: 12),
                                                    alignment: Alignment.center,
                                                    // color: Colors.blue,
                                                    height: width / 20,
                                                    child: _productProvider.productSpPrice == 0.0
                                                        ? Container()
                                                        : Container(
                                                            width: width / 2.6,
                                                            child: AutoSizeText(
                                                              '${_productProvider.offerPercentage.round()}% Special Offer Today',
                                                              textAlign: TextAlign.center,
                                                              presetFontSizes: [22, 20, 18, 16, 14],
                                                              style: TextStyle(
                                                                fontWeight: FontWeight.w400,
                                                                letterSpacing: 0.27,
                                                                color: Colors.yellow[800],
                                                              ),
                                                            ),
                                                          ),
                                                  ),
                                                ],
                                              ),
                                              _outOfStockItem
                                                  ? Row(
                                                      children: [
                                                        Expanded(
                                                          child: Container(
                                                            child: AutoSizeText(
                                                              _productDetail.stock.name.toString(),
                                                              minFontSize: 18,
                                                              maxFontSize: 22,
                                                              style: kOutOfStockLabelText,
                                                            ),
                                                            margin: EdgeInsets.only(top: 10),
                                                          ),
                                                        )
                                                      ],
                                                    )
                                                  : Container(),
                                            ],
                                          )
                                        : SkeletonAnimation(
                                            child: Container(
                                              color: Colors.grey,
                                            ),
                                          ),
                                  ),
                                  Divider(
                                    thickness: 2,
                                  ),

                                  //Option Values
                                  isLoded == true
                                      ? Column(
                                          children: <Widget>[
//                                            ProductOptionV2(
//                                              productDetail: _productDetail,
//                                              globalProvider: _globalProvider,
//                                              shoppingServices: _shoppingServices,
//                                            ),
                                            ProductOptionBlock(
                                              productDetail: _productDetail,
                                              globalProvider: _globalProvider,
                                              shoppingServices: _shoppingServices,
                                              notifyParent: refresh,
                                            ),
                                          ],
                                        )
                                      : Container(
                                          height: 50,
                                          margin: const EdgeInsets.only(top: 30, right: 20, left: 20, bottom: 20),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              SkeletonAnimation(
                                                child: Container(
                                                  height: 10,
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[300],
                                                  ),
                                                ),
                                              ),
                                              SkeletonAnimation(
                                                child: Container(
                                                  height: 10,
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[300],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),

                                  Divider(
                                    thickness: 2,
                                  ),
                                  Container(
                                    alignment: Alignment.bottomCenter,
                                    margin: const EdgeInsets.only(left: 15, bottom: 18, top: 18),
                                    height: width / 12,
                                    width: width / 5,
                                    child: AutoSizeText(
                                      "Details",
                                      presetFontSizes: [
                                        26,
                                        24,
                                        22,
                                        20,
                                        18,
                                        17,
                                        16,
                                        15,
                                        14,
                                      ],
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        letterSpacing: 0.27,
                                        color: Colors.black54,
                                      ),
                                    ),
                                  ),

                                  Container(
                                    margin: const EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: isLoded ? details(width) : skeltonLines(n: 5, wid: width / 1.5),
                                    ),
                                  ),

                                  Divider(
                                    height: 60,
                                    thickness: 2,
                                  ),

                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.bottomLeft,
                                        //color: Colors.red,
                                        margin: const EdgeInsets.only(left: 15, bottom: 10),
                                        height: width / 13,
                                        width: width / 2.0,
                                        child: AutoSizeText(
                                          "Rating & Reviews",
                                          presetFontSizes: [
                                            26,
                                            24,
                                            22,
                                            20,
                                            18,
                                            16,
                                            14,
                                          ],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            letterSpacing: 0.27,
                                            color: Colors.black54,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(right: 15),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          //crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => ProductReviewScreeen(widget.productId,
                                                          manufacturerName: manufacturerName, productDtl: _productDetail, ratingIndicator: indicators(width))),
                                                );
                                              },
                                              child: Container(
                                                  //color: Colors.black,
                                                  //s margin: const EdgeInsets.only(right: 15),
                                                  alignment: Alignment.topCenter,
                                                  height: width / 18,
                                                  width: width / 6.1,
                                                  child: AutoSizeText(
                                                    "Reviews",
                                                    textAlign: TextAlign.center,
                                                    maxLines: 1,
                                                    presetFontSizes: [22, 20, 18, 16, 14, 12, 10, 8],
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 0.27,
                                                      color: Colors.grey,
                                                    ),
                                                  )),
                                            ),
                                            Icon(
                                              Icons.chevron_right,
                                              color: Colors.grey,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),

                                  isLoded
                                      ? Container(
                                          width: width,
                                          height: width / 2.5,
                                          child: Row(
                                            children: <Widget>[
                                              //rating
                                              Expanded(
                                                flex: 4,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                      height: width / 12,
                                                      width: width / 6.5,
                                                      decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(30)),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: <Widget>[
                                                          Container(
                                                              height: width / 19,
                                                              child: Container(
                                                                  alignment: Alignment.bottomRight,
                                                                  child: AutoSizeText(
                                                                    "${(_productDetail?.avgRating?.avg_rating ?? 0.0).toStringAsFixed(1)}",
                                                                    presetFontSizes: [22, 20, 18, 16, 14, 12, 10],
                                                                    style: TextStyle(color: Colors.white),
                                                                  ))),
                                                          Icon(
                                                            Icons.star,
                                                            size: width / 20,
                                                            color: Colors.white,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      alignment: Alignment.bottomCenter,
                                                      margin: EdgeInsets.only(top: 5),
                                                      width: width / 5,
                                                      height: width / 12,
                                                      child: AutoSizeText(
                                                        "${_productDetail?.productReviewRatingCount?.rating ?? 0} Ratings and ${_productDetail?.productReviewRatingCount?.review_count ?? 0} reviews",
                                                        presetFontSizes: [18, 16, 14, 12, 10, 8],
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.grey[600], letterSpacing: .05),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              //divider
                                              VerticalDivider(
                                                color: Colors.grey[400],
                                                thickness: 1,
                                              )
                                              //reviews
                                              ,
                                              Expanded(
                                                flex: 6,
                                                child: Container(
                                                  padding: const EdgeInsets.only(left: 20),
                                                  alignment: Alignment.center,
                                                  height: width / 3.5,
                                                  child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: indicators(width)),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      : Container(),

                                  Divider(
                                    height: 40,
                                    thickness: 2,
                                  ),

                                  Container(
                                    // height: width / 6,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 4,
                                          child: Container(
                                            alignment: Alignment.topLeft,
                                            margin: const EdgeInsets.only(left: 15, bottom: 10),
                                            height: width / 15,
                                            width: width / 2.0,
                                            child: Container(
                                              alignment: Alignment.topLeft,
                                              child: AutoSizeText(
                                                "Delivery",
                                                presetFontSizes: [
                                                  26,
                                                  24,
                                                  22,
                                                  20,
                                                  18,
                                                  16,
                                                  14,
                                                ],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  letterSpacing: 0.27,
                                                  color: Colors.black54,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: Container(
                                            // color: Colors.green,
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      // alignment: Alignment.centerRight,
                                                      width: width / 3,
                                                      child: TextFormField(
                                                        maxLength: 6,
                                                        controller: _controller,
                                                        // initialValue:"$pin",
                                                        onChanged: (value) {
                                                          pin = int.parse(value);
                                                          setState(() {
                                                            estimationLoading = false;
                                                          });
                                                        },
                                                        autofocus: false,
                                                        keyboardType: TextInputType.number,
                                                        decoration: InputDecoration(
                                                            counterText: "",
                                                            labelText: "Enter Pincode",
                                                            //focusedBorder: InputBorder.none,
                                                            //enabledBorder: InputBorder.none,
                                                            disabledBorder: InputBorder.none,
                                                            fillColor: null,
                                                            contentPadding: EdgeInsets.only(top: 1),
                                                            labelStyle: TextStyle(fontSize: width / 28)),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(top: 5),
                                                      alignment: Alignment.bottomCenter,
                                                      height: width / 16,
                                                      width: width / 5.5,
                                                      child: RaisedButton(
                                                        color: estimationLoading ? Colors.grey : Colors.green,
                                                        child: Text(
                                                          "Check",
                                                          style: TextStyle(fontSize: width / 33, color: Colors.white),
                                                        ),
                                                        onPressed: estimationLoading
                                                            ? null
                                                            : () {
                                                                setState(() {
                                                                  estimationLoading = true;
                                                                });
                                                                checkEstimationTime();
                                                              },
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Container(
                                                  alignment: Alignment.bottomLeft,
                                                  margin: const EdgeInsets.only(bottom: 20),
                                                  height: width / 11,
                                                  width: width / 2.6,
                                                  child: estimationLoaded
                                                      ? Container(
                                                          child: _deliveryEstimation.error != null
                                                              ? AutoSizeText(
                                                                  "${_deliveryEstimation.error}",
                                                                  textAlign: TextAlign.left,
                                                                  style: TextStyle(color: Colors.red, letterSpacing: -0.5),
                                                                  presetFontSizes: [
                                                                    14,
                                                                    12,
                                                                  ],
                                                                )
                                                              : AutoSizeText(
                                                                  "Estimated delivery within ${_deliveryEstimation.bestDeliveryEstimate.max_days} days",
                                                                  textAlign: TextAlign.left,
                                                                  style: TextStyle(color: Colors.yellow[800], letterSpacing: -0.5),
                                                                  presetFontSizes: [
                                                                    16,
                                                                    14,
                                                                    12,
                                                                  ],
                                                                ),
                                                        )
                                                      : null,
                                                ),
                                                // Container(
                                                //     // color: Colors.green,
                                                //     height: width / 15,
                                                //     width: width / 2.2,
                                                //     //alignment: Alignment.centerRight,
                                                //     child: AutoSizeText(
                                                //       "If the order is placed before 4:00 PM",
                                                //       textAlign: TextAlign.left,
                                                //       style: TextStyle(
                                                //           color: Colors.yellow[800],
                                                //           letterSpacing: -0.5),
                                                //       presetFontSizes: [
                                                //         14,
                                                //         12,
                                                //         10,
                                                //         8
                                                //       ],
                                                //     )),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),

                                  Divider(
                                    thickness: 2,
                                  ),

                                  SizedBox(
                                    height: 80,
                                  )
                                ], //add widget
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          top: (width / 1.34),
                          right: 35,
                          child: isLoded
                              ? Card(
                                  // color: DesignCourseAppTheme.nearlyBlue,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                                  elevation: 10.0,
                                  child: Container(
                                    width: width / 7.2,
                                    height: width / 7.2,
                                    child: Center(
                                      child: InkWell(
                                        onTap: () {
                                          addRemoveWishList();
                                        },
                                        child: Icon(
                                          Icons.favorite,
                                          color: wishList ? Colors.red : Colors.grey[350],
                                          size: width / 12.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
                        ),
                        Positioned(
                            top: 30,
                            left: 20,
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                  padding: const EdgeInsets.all(8),
                                  decoration: new BoxDecoration(
                                    color: Colors.black54,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 5),
                                    child: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                    ),
                                  )),
                            ))
                      ],
                    )
                  : Container()
            ],
          ),
        ),
      ),
      bottomSheet: (_globalLoaded && isLoded)
          ? Row(
              //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: _productProvider.buyNowPermission ? addToCart : null,
                    child: Container(
                      height: width / 8,
                      width: width / 2,
                      //color: Colors.yellow[900],
                      child: Center(
                        child: _addingToCart
                            ? SpinKitWave(
                                color: GocoMartAppTheme.buildLightTheme().primaryColor,
                                type: SpinKitWaveType.start,
                                size: 30,
                              )
                            : AutoSizeText(
                                "ADD TO CART",
                                style: TextStyle(
                                  color: _productProvider.buyNowPermission ? Theme.of(context).primaryColor : Colors.grey,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                ),
                                maxLines: 1,
                              ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (_productProvider.buyNowPermission)
                        ? () {
                            this._buyNowProductTrigger();
//                            var product = [];
//                            product.add(widget.productId);
//                            Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                builder: (context) => SummaryScreen(),
//                              ),
//                            );
                          }
                        : null,
                    child: Container(
                      height: width / 8,
                      width: width / 2,
                      color: (_productProvider.buyNowPermission) ? Theme.of(context).primaryColor : Colors.grey,
                      child: Center(
                        child: _buyNowItem
                            ? SpinKitWave(
                                color: Colors.white,
                                type: SpinKitWaveType.start,
                                size: 30,
                              )
                            : AutoSizeText(
                                "BUY NOW",
                                style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.w700),
                                maxLines: 1,
                              ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[],
            ),
    );
  }

  //-------------------------------------------------------------------------------------------//

  indicators(width) {
    if (largestRating == 0) largestRating = 1;
    List<Widget> list = [];
    for (int i = 0; i < 5; i++) {
      list.add(RatingPercentageWidget(
        countOfRating: _productDetail?.ratingSummary[i]?.count_of_rating,
        largestRating: largestRating,
        starNumber: i + 1,
        width: width,
      ));
    }
    return list;
  }

  skeltonLines({int n, double wid}) {
    List skeltons = <Widget>[];
    for (int i = 0; i < n; i++) {
      skeltons.add(Container(
        margin: const EdgeInsets.only(top: 15),
        child: SkeletonAnimation(
          child: Container(
            height: 10,
            width: wid,
            decoration: BoxDecoration(
              color: Colors.grey[300],
            ),
          ),
        ),
      ));
    }
    return skeltons;
  }

  swiper() {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return Image.network(
          "${_productProvider.pImages[index]}",
          fit: BoxFit.fitHeight,
        );
      },
      itemCount: _productProvider.pImages.length,
      pagination: new SwiperPagination(),
      //control: new SwiperControl(),
    );
  }

//heading
  Widget headings(double width, String head1, String stock, Color color) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomLeft,
          margin: const EdgeInsets.only(left: 15, bottom: 10),
          height: width / 13,
          width: width / 2.0,
          child: AutoSizeText(
            head1,
            presetFontSizes: [
              26,
              24,
              22,
              20,
              18,
              16,
              14,
            ],
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: Colors.black54,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 15),
          alignment: Alignment.bottomRight,
          height: width / 18,
          width: width / 3,
          child: AutoSizeText(
            stock,
            textAlign: TextAlign.center,
            maxLines: 1,
            presetFontSizes: [22, 20, 18, 16, 14, 12, 10, 8],
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: 0.27,
              color: color,
            ),
          ),
        )
      ],
    );
  }

//details
  details(width) {
    var det = <Widget>[];
    for (int i = _productDetail.attributes.length - 1; i >= 0; i--) {
      det.add(Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    alignment: Alignment.centerLeft,
                    height: width / 20,
                    child: AutoSizeText(
                      "${_productDetail.attributes[i].getAttributeName}",
                      textAlign: TextAlign.left,
                      presetFontSizes: [18, 16, 14, 12, 10, 8],
                      style: TextStyle(color: Colors.grey[600], letterSpacing: -0.5),
                    )),
                Container(
                    height: width / 20,
                    alignment: Alignment.centerRight,
                    child: AutoSizeText(
                      "${_productDetail.attributes[i].text}",
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey, letterSpacing: -0.5),
                      presetFontSizes: [18, 16, 14, 12, 10, 8],
                    )),
              ],
            ),
            Divider(
              thickness: 1,
              height: 1,
            )
          ],
        ),
      ));
    }
    return det;
  }

  //end of class
}

//size raio

class CustomRadioButton extends StatefulWidget {
  CustomRadioButton({
    this.buttonLables,
    this.buttonValues,
    this.radioButtonValue,
    this.buttonColor,
    this.selectedColor,
    this.hight = 35,
    this.width = 100,
    this.horizontal = false,
    this.enableShape = false,
    this.elevation = 10,
    this.customShape,
  })  : assert(buttonLables.length == buttonValues.length),
        assert(buttonColor != null),
        assert(selectedColor != null);

  final bool horizontal;

  final List buttonValues;

  final double hight;
  final double width;

  final List buttonLables;

  final Function(dynamic) radioButtonValue;

  final Color selectedColor;

  final Color buttonColor;
  final ShapeBorder customShape;
  final bool enableShape;
  final double elevation;

  _CustomRadioButtonState createState() => _CustomRadioButtonState();
}

class _CustomRadioButtonState extends State<CustomRadioButton> {
  int currentSelected = 0;
  String currentSelectedLabel;

  @override
  void initState() {
    super.initState();
    currentSelectedLabel = widget.buttonLables[0];
  }

  List<Widget> buildButtonsRow(width) {
    List<Widget> buttons = [];
    for (int index = 0; index < widget.buttonLables.length; index++) {
      var button = Container(
        //   width: width/10,
        //margin: EdgeInsets.only(left: 10),
        decoration: new BoxDecoration(
          // color: Colors.black,
          shape: BoxShape.circle,
        ),
        // flex: 1,
        child: Card(
          color: currentSelectedLabel == widget.buttonLables[index] ? widget.selectedColor : widget.buttonColor,
          elevation: 1,
          shape: StadiumBorder(
            side: BorderSide(color: Colors.black54, width: 1),
          ),
          child: InkWell(
            onTap: () {
              widget.radioButtonValue(widget.buttonValues[index]);
              setState(() {
                currentSelected = index;
                currentSelectedLabel = widget.buttonLables[index];
              });
            },
            child: Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              alignment: Alignment.center,
              height: width / 11,
              // width: width/20,
              child: Text(
                widget.buttonLables[index],
                textAlign: TextAlign.center,
                style: TextStyle(color: currentSelectedLabel == widget.buttonLables[index] ? Colors.white : Colors.black45, fontSize: width / 34),
                //presetFontSizes: [22,20,19,18,17,16,15,14,12,10],
                maxLines: 1,
              ),
            ),
          ),
        ),
      );
      buttons.add(button);
    }
    return buttons;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: const EdgeInsets.only(left: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: buildButtonsRow(MediaQuery.of(context).size.width),
        ),
      ),
    );
  }
}

class RadioColor extends StatefulWidget {
  RadioColor({
    this.buttonLables,
    this.buttonValues,
    this.radioButtonValue,
    this.color,
    this.buttonColor,
    this.selectedColor,
    this.hight = 35,
    this.width = 100,
    this.horizontal = false,
    this.enableShape = false,
    this.elevation = 10,
    this.customShape,
  });

  final bool horizontal;

  final List buttonValues;

  final double hight;
  final double width;
  final List<Color> color;
  final List<String> buttonLables;

  final Function(dynamic) radioButtonValue;

  final Color selectedColor;

  final Color buttonColor;
  final ShapeBorder customShape;
  final bool enableShape;
  final double elevation;
  _RadioColorState createState() => _RadioColorState();
}

class _RadioColorState extends State<RadioColor> {
  int currentSelected = 0;
  String currentSelectedLabel;

  @override
  void initState() {
    super.initState();
    currentSelectedLabel = widget.buttonValues[0];
  }

  List<Widget> buildButtonsRow(width) {
    List<Widget> buttons = [];
    for (int index = 0; index < widget.buttonValues.length; index++) {
      var button = Container(
        width: width / 10,
        //margin: EdgeInsets.only(left: 10),
        decoration: new BoxDecoration(
          // color: Colors.black,
          shape: BoxShape.circle,
        ),
        // flex: 1,
        child: Card(
          color: widget.color[index],
          elevation: 1,
          shape: StadiumBorder(
            side: BorderSide(color: Colors.transparent, width: 1),
          ),
          child: InkWell(
            onTap: () {
              widget.radioButtonValue(widget.buttonValues[index]);
              setState(() {
                currentSelected = index;
                currentSelectedLabel = widget.buttonValues[index];
              });
            },
            child: Container(
              // color: Colors.black,

              alignment: Alignment.center,
              height: width / 12,
              //color: widget.color[index],
              // width: width/20,
              child: currentSelectedLabel == widget.buttonValues[index]
                  ? Icon(
                      Icons.done,
                      color: currentSelectedLabel == "white" ? Colors.black : Colors.white,
                    )
                  : null,
            ),
          ),
        ),
      );
      buttons.add(button);
    }
    return buttons;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(right: 10),
      child: Container(
        margin: const EdgeInsets.only(left: 15, right: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: buildButtonsRow(MediaQuery.of(context).size.width),
        ),
      ),
    );
  }
}
