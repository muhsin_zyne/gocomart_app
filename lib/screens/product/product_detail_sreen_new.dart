import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/product_detail_new/new_product_detail.dart';
import 'package:gocomartapp/providers/product_provider.dart';
import 'package:gocomartapp/screens/product/product_image_view.dart';
import 'package:gocomartapp/screens/product/widget_blocks/p_option_heading.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:skeleton_text/skeleton_text.dart';

final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;

List<Widget> expandChild = [];
NewProductDetail newProductDetail;
ProductProvider _productProvider;

class ProductDetailScreenNew extends StatefulWidget {
  final product_id;
  ProductDetailScreenNew({Key key, @required this.product_id}) : super(key: key);

  @override
  _ProductDetailScreenNewState createState() => _ProductDetailScreenNewState();
}

class _ProductDetailScreenNewState extends State<ProductDetailScreenNew> with ChangeNotifier {
  ShoppingServices _shoppingServices;

  PersistentBottomSheetController _controller; // <------ Instance variable
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoded = false;

  @override
  void initState() {
    newProductDetail = null;
    expandChild.clear();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();
    super.initState();
  }

  loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    QueryResult _newProductDetail = await _shoppingServices.getProductDtlnew(productId: widget.product_id);
    if (!_newProductDetail.hasException) {
      setState(() {
        newProductDetail = NewProductDetail.fromJSON(_newProductDetail.data);
        setDefaultProductImages();
        isLoded = true;
      });
    }
    print(_newProductDetail.data);
  }

  void setDefaultProductImages() {
    _productProvider.pImages.add("${imagePath + newProductDetail.product.image}");
    //images.add("${imagePath + _productDetail.image}");
    for (int i = 0; i < newProductDetail.product.images.length; i++) {
      setState(() {
        //images.add("${imagePath + _productDetail.images[i].image}");
        _productProvider.pImages.add("${imagePath + newProductDetail.product.images[i].image}");
      });
    }
  }

  swiper() {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return Image.network(
          "${_productProvider.pImages[index]}",
          fit: BoxFit.fitHeight,
        );
      },
      itemCount: _productProvider.pImages.length,
      pagination: new SwiperPagination(),
      //control: new SwiperControl(),
    );
  }

  @override
  void didChangeDependencies() {
    _productProvider = Provider.of<ProductProvider>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    int currentImageIndex;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('new product detail'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _productProvider.ready
                ? Stack(
                    children: [
                      InkWell(
                        onTap: _productProvider.pImages.length >= 1
                            ? () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ProductImageView(_productProvider.pImages, currentImageIndex),
                                  ),
                                );
                              }
                            : null,
                        child: Container(
                          margin: const EdgeInsets.only(top: 25),
                          height: width / 1.3,
                          child: SizedBox.expand(
                            child: isLoded == true
                                ? Stack(
                                    children: <Widget>[
                                      Swiper(
                                        itemBuilder: (BuildContext context, int index) {
                                          return Image.network(
                                            "${_productProvider.pImages[index]}",
                                            fit: BoxFit.contain,
                                          );
                                        },
                                        itemCount: _productProvider.pImages.length,
                                        pagination: new SwiperPagination(),
                                        //control: new SwiperControl(),
                                        index: 0,
                                        onIndexChanged: (value) {
                                          currentImageIndex = value;
                                        },
                                      ),
                                      _productProvider.imageLoading == true
                                          ? Positioned.fill(
                                              child: Center(
                                              child: CircularProgressIndicator(),
                                            ))
                                          : Container(),
                                    ],
                                  )
                                : Container(
                                    child: SkeletonAnimation(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.grey[300],
                                        ),
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ],
                  )
                : Container,
            isLoded ? productOptions() : Container(),
            InkWell(
              onTap: () async {
                // _controller =
                //     await _scaffoldKey.currentState.showBottomSheet((context) {
                //   return productOptions(calledBottomSheet: true);
                // });

                Navigator.of(context).push(
                  PageRouteBuilder(
                    opaque: false, // set to false
                    pageBuilder: (_, __, ___) => NewBottomSheet(),
                  ),
                );

                // showModalBottomSheet(
                //   context: context,
                //   builder: (_) => MyBottomSheet(productOptions),
                // );
                // Navigator.push(context, MaterialPageRoute(builder: (context) {
                //   notifyListeners();
                //   return Scaffold(
                //     body: Column(
                //       mainAxisAlignment: MainAxisAlignment.end,
                //       children: [
                //         Container(
                //           color: Colors.red,
                //           height: MediaQuery.of(context).size.height / 2,
                //           width: MediaQuery.of(context).size.width,
                //           child: productOptions(),
                //         ),
                //       ],
                //     ),
                //   );
                // }));
              },
              child: Container(
                child: Text("Bottom"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Column productOptions({bool calledBottomSheet}) {
  return Column(
    children: [
      // POptionHeading(head1: ,),
      ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: newProductDetail.product.productOptionNew.length,
        itemBuilder: (BuildContext context, int optionIndex) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              POptionHeading(
                head1: newProductDetail.product.productOptionNew[optionIndex].name,
                width: MediaQuery.of(context).size.width,
                //color: Colors.red,
                stock: "",
              ),
              Container(
                height: MediaQuery.of(context).size.width / 3.2,
                //color: Colors.green,
                child: ListView.builder(
                  // physics: NeverScrollableScrollPhysics(),
                  // shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: newProductDetail.product.productOptionNew[optionIndex].optionValuesNew.length,
                  itemBuilder: (BuildContext context, int optionValueIndex) {
                    return InkWell(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 7),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].quantity < 1
                                ? Colors.transparent
                                : (_productProvider.currentOptionValueId ==
                                        newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].productOptionValueId
                                    ? GocoMartAppTheme.buildLightTheme().primaryColor
                                    : Colors.transparent),
                            width: 2,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(15.0),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 14.0),
                          child: Stack(
                            children: [
                              Container(
                                width: 50,
                                height: MediaQuery.of(context).size.width / 4,
                                child: Image.network(FlavorConfig.instance.flavorValues.cdnImagePoint +
                                    newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].image),
                              ),
                              newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].quantity < 1
                                  ? Positioned.fill(
                                      child: Center(
                                        child: Container(
                                          color: Colors.white,
                                          child: AutoSizeText(
                                            'Out of Stock',
                                            presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
                                            style: TextStyle(
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        expandChild.clear();
                        _productProvider.currentOptionValueId =
                            newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].productOptionValueId;
                        // _productProvider.childOptions = newProductDetail
                        //     .product
                        //     .productOptionNew[optionIndex]
                        //     .optionValuesNew[optionValueIndex]
                        //     .childOptions;
                        newExpandChild(context: context, optionIndex: optionIndex, optionValueIndex: optionValueIndex);

                        print(newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions.length);
                      },
                    );
                  },
                ),
              ),
              WrapWidget()
            ],
          );
        },
      ),
    ],
  );
}

newExpandChild({@required optionIndex, @required optionValueIndex, @required context}) {
  expandChild.clear();
  for (int i = 0; i < newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions.length; i++) {
    // print(i);
    // setState(() {
    expandChild.add(
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          POptionHeading(
            head1: "${newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions[i].name}",
            width: MediaQuery.of(context).size.width,
            //color: Colors.red,
            stock: "",
          ),
          Container(
            height: 50,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions[i].optionValuesNewChild.length,
              itemBuilder: (context, optionValuechildIndex) {
                return InkWell(
                  onTap: () async {
                    _productProvider.currentChildOptionValueId = newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex]
                        .childOptions[i].optionValuesNewChild[optionValuechildIndex].productOptionValueId;
                    newExpandChild(context: context, optionIndex: optionIndex, optionValueIndex: optionValueIndex);
                  },
                  child: Card(
                    color: newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].quantity < 1
                        ? Colors.white
                        : ((_productProvider.currentChildOptionValueId ==
                                newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions[i]
                                    .optionValuesNewChild[optionValuechildIndex].productOptionValueId)
                            ? Colors.blue
                            : Colors.white),
                    // color: Colors.white,
                    shape: StadiumBorder(
                      side: BorderSide(color: Colors.black54, width: 1.5),
                    ),
                    child: Container(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.width / 11,
                      child: Text(
                        newProductDetail.product.productOptionNew[optionIndex].optionValuesNew[optionValueIndex].childOptions[i]
                            .optionValuesNewChild[optionValuechildIndex].name,
                        // style: TextStyle(
                        //     color: cOptionValue.quantity < 1
                        //         ? Colors.red
                        //         : ((cOptionValue
                        //                     .product_option_value_id ==
                        //                 currentOptionValueId)
                        //             ? Colors.white
                        //             : Colors.black)),
                        style: TextStyle(color: Colors.black),
                        textAlign: TextAlign.center,
                        maxLines: 1,
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
    // });
  }
}

class WrapWidget extends StatelessWidget {
  const WrapWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: expandChild,
    );
  }
}

class NewBottomSheet extends StatefulWidget {
  NewProductDetail newProductDetail;

  NewBottomSheet({Key key}) : super(key: key);

  @override
  _NewBottomSheetState createState() => _NewBottomSheetState();
}

class _NewBottomSheetState extends State<NewBottomSheet> {
  ProductProvider _productProvider;

  @override
  void didChangeDependencies() {
    _productProvider = Provider.of<ProductProvider>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400].withOpacity(.7),
      body: InkWell(
        splashColor: Colors.transparent,
        onTap: () {
          Navigator.pop(context);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              splashColor: Colors.transparent,
              onTap: () {},
              child: Container(
                color: Colors.grey,
                // height: 50,
                width: double.infinity,
                child: productOptions(),
                // child: child,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
