import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'package:gocomartapp/models/product_review/get_product_reviews.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

//import 'package:gocomart/components/ext_libraries/expandable_card.dart';
//import 'package:expandable/expandable.dart';
int productId;
String manufacturer;
ProductDetail _productDetail;
final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
List<Widget> ratingIndicatorWidget;

class ProductReviewScreeen extends StatelessWidget {
  ProductReviewScreeen(
    productid, {
    @required manufacturerName,
    @required productDtl,
    @required ratingIndicator,
  }) {
    productId = productid;
    manufacturer = manufacturerName;
    _productDetail = productDtl;
    ratingIndicatorWidget = ratingIndicator;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Ratings & reviews")),
      body: ReviewBody(),
    );
  }
}

class ReviewBody extends StatefulWidget {
  @override
  _ReviewBodyState createState() => _ReviewBodyState();
}

class _ReviewBodyState extends State<ReviewBody> {
  ScrollController _scrollController = ScrollController();
  bool fetchMore = false;
  bool loadingReview = true;
  bool pageReady;
  int currentPagination = 1;
  int dataLoadLimit = 4;
  dynamic totalProductReviews = [];
  bool callApi = true;
  var dateAdded;

  ShoppingServices _shopppingServices;
  GetProductReviews productReviews;

  @override
  void initState() {
    super.initState();
    _shopppingServices = ShoppingServices(context: context);
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 10));
    //pageReady = true;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        callApi ? pagination() : voidFunction();
      }
    });

    getProductReview();
  }

  pagination() async {
    currentPagination++;
    setState(() {
      fetchMore = true;
    });
    getProductReview();
  }

  voidFunction() {}
  getProductReview() async {
    setState(() {
      loadingReview = false;
    });

    QueryResult productReviewResult =
        await _shopppingServices.getProductReview(product_id: productId, currentPage: currentPagination, dataLimit: dataLoadLimit);
    if (!productReviewResult.hasException) {
      productReviews = GetProductReviews.fromJson(productReviewResult.data['getProductReviews']);
      setState(() {
        fetchMore = false;
      });
      // productReviews.reviews[0].author

      if (productReviews.reviews.length > 0) {
        setState(() {
          totalProductReviews = [totalProductReviews, productReviews.reviews].expand((x) => x).toList();
        });
      } else {
        callApi = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(left: 5),
                height: MediaQuery.of(context).size.width / 5,
                width: MediaQuery.of(context).size.width / 3.5,
                child: Image.network(
                  "${imagePath + _productDetail.image}",
                  fit: BoxFit.fitHeight,
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        height: MediaQuery.of(context).size.width / 16,
                        width: MediaQuery.of(context).size.width / 1.7,
                        child: AutoSizeText(
                          "$manufacturer",
                          overflow: TextOverflow.ellipsis,
                          presetFontSizes: [
                            20,
                            18,
                            15,
                            14,
                            12,
                          ],
                          textAlign: TextAlign.left,
                          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.width / 19,
                        width: MediaQuery.of(context).size.width / 1.7,
                        child: AutoSizeText(
                          "${_productDetail.description.name}",
                          overflow: TextOverflow.ellipsis,
                          presetFontSizes: [14, 13, 12],
                          style: TextStyle(color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Divider(
            thickness: 1,
          ),

          //Ratings

          Container(
            width: width,
            // color: Colors.red,
            height: width / 3.5,
            child: Row(
              children: <Widget>[
                //rating
                Expanded(
                  flex: 4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: width / 12,
                        width: width / 6.5,
                        decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(30)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: width / 19,
                                child: Container(
                                    alignment: Alignment.bottomRight,
                                    child: AutoSizeText(
                                      "${(_productDetail?.avgRating?.avg_rating ?? 0.0).toStringAsFixed(1)}",
                                      presetFontSizes: [22, 20, 18, 16, 14, 12, 10],
                                      style: TextStyle(color: Colors.white),
                                    ))),
                            Icon(
                              Icons.star,
                              size: width / 20,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.bottomCenter,
                        margin: EdgeInsets.only(top: 5),
                        //color: Colors.redAccent,
                        width: width / 5,
                        height: width / 12,
                        child: AutoSizeText(
                          "${_productDetail?.productReviewRatingCount?.rating ?? 0} Ratings and ${_productDetail?.productReviewRatingCount?.review_count ?? 0} reviews",
                          presetFontSizes: [18, 16, 14, 12, 10, 8],
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.grey[600], letterSpacing: .05),
                        ),
                      )
                    ],
                  ),
                ),
                //divider
                VerticalDivider(
                  color: Colors.grey[400],
                  thickness: 1,
                )
                //reviews
                ,
                Expanded(
                  flex: 6,
                  child: Container(
                    padding: const EdgeInsets.only(left: 20),
                    alignment: Alignment.center,
                    height: width / 3.5,
                    child:
                        Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: ratingIndicatorWidget
                            //5 star

                            ),
                  ),
                )
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),

          // reviews()
          loadingReview == false
              ? totalProductReviews.length > 0
                  ? Column(
                      children: reviews(width),
                    )
                  : Center(
                      heightFactor: width / 20,
                      child: Text("No reviews yet"),
                    )
              : Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
          fetchMore == true ? loaderSpinner() : SizedBox(),
        ],
      ),
    );
  }

  List<Widget> reviews(width) {
    List<Widget> revBody = [];

    for (int i = 0; i < totalProductReviews.length; i++) {
      revBody.add(
        Container(
          margin: const EdgeInsets.only(left: 15, right: 15, top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: new BoxDecoration(
                          border: Border.all(color: Color(0xff3b5999), width: 2.5),
                          shape: BoxShape.circle,
                        ),
                        child: Image.network("https://cdn1.iconfinder.com/data/icons/user-pictures/100/boy-512.png"),

                        height: width / 8,
                        //color: Colors.red,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 18),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: width / 18,
                              width: width / 2.2,
                              child: AutoSizeText(
                                "${totalProductReviews[i].author}",
                                presetFontSizes: [18, 16, 14, 12],
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              ),
                            ),
                            Container(
                              height: width / 25,
                              width: width / 2.2,
                              child: AutoSizeText(
                                "Reviewed on january 21",
                                presetFontSizes: [17, 15, 13, 11],
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),

                  //rating
                  Container(
                    height: width / 15,
                    width: width / 9,
                    decoration: BoxDecoration(color: ratingColor(totalProductReviews[i].rating), borderRadius: BorderRadius.circular(30)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            height: width / 18,
                            child: Container(
                                alignment: Alignment.bottomRight,
                                child: AutoSizeText(
                                  "${totalProductReviews[i].rating}",
                                  presetFontSizes: [22, 20, 18, 16, 14, 12, 10],
                                  style: TextStyle(color: Colors.white),
                                ))),
                        Icon(
                          Icons.star,
                          size: width / 25,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                ],
              ),
              (totalProductReviews[i].title).toString().length > 0 && totalProductReviews[i].title != null
                  ? Container(
                      margin: const EdgeInsets.only(left: 8, top: 4),
                      child: AutoSizeText(
                        "${totalProductReviews[i].title}",
                        style: TextStyle(color: Colors.grey[600], fontSize: width / 21),
                      ),
                    )
                  : SizedBox(
                      height: 3,
                    ),
              (totalProductReviews[i].text).toString().length > 0 && totalProductReviews[i].text != null
                  ? Container(
                      margin: const EdgeInsets.only(left: 8, top: 2),
                      width: width / 1.2,
                      child: AutoSizeText(
                        "${totalProductReviews[i].text}",
                        style: TextStyle(color: Colors.grey),
                        maxLines: null,
                      ),
                    )
                  : SizedBox(
                      height: 3,
                    ),
              Divider(
                thickness: 1,
              ),
            ],
          ),
        ),
      );
    }

    return revBody;
  }

  Color ratingColor(int rating) {
    if (rating == 5)
      return Colors.green[600];
    else if (rating == 4)
      return Colors.lightGreen[700];
    else if (rating == 3)
      return Colors.lightGreen;
    else if (rating == 2)
      return Colors.orange[700];
    else if (rating == 1)
      return Colors.red;
    else {
      return Colors.transparent;
    }
  }

  Widget loaderSpinner() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
