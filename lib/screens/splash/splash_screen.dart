import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gocomartapp/controllers/base_controller.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/locator.dart';
import 'package:gocomartapp/src/screens/auth/login_screen.dart';
import 'package:gocomartapp/src/services/dynamic_link.dart';
import 'package:graphql/client.dart';

class SplashScreen extends StatefulWidget {
  static const String id = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final DynamicLinkService _dynamicLinkService = locator<DynamicLinkService>();

  AccountServices accountServices;
  bool minimumReached = false;
  dynamic type = '';
  dynamic link = 'not alter';
  @override
  void initState() {
    print('SplashScreen starts now | ${DateTime.now()} ');
    this.accountServices = AccountServices(context: context);
    this._init();
    super.initState();
  }

  void _init() async {
    await Future.delayed(Duration(microseconds: 100));
    await _dynamicLinkService.handleDynamicLinks(context);
    this._isLoggedIn();
  }

  void _isLoggedIn() async {
    bool loggedIn = await BaseController().isLoggedIn();
    if (loggedIn) {
      this._tokenValidate();
    } else {
      this._actionLogin();
    }
  }

  void _tokenValidate() async {
    QueryResult result = await this.accountServices.userDetails();
    if (result == null || result.hasException) {
      this._actionLogin();
    } else {
      this._actionHome();
    }
  }

  void _actionLogin() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  void _actionHome() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

  void initDynamicLinks() async {}

  triggerAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("$type"),
          content: Text("$link"),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Image.asset('assets/images/logo-invert.png')],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SpinKitDoubleBounce(
                        color: Colors.white,
                        size: 50,
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
