import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/auth_manager.dart';
import 'package:gocomartapp/screens/cart/cart_screen.dart';
import 'package:gocomartapp/screens/category/category_screen.dart';
import 'package:gocomartapp/screens/profile/profile_home/profile_screen.dart';
import 'package:gocomartapp/src/screens/home/shopping_home_v2.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';

import '../src/screens/wallet/wallet_home_screen.dart';
import 'profile/profile_home/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  static const String id = 'home_screen';

  static void setPreviousTabAsSelected(BuildContext context) {
    _HomeScreenState state = context.findAncestorStateOfType<_HomeScreenState>();
    state.setPreviousTab();
  }

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with AuthManager {
  String appTitle = getTranslate('appTitle');
  double elevationValue = 3.0;
  @override
  void initState() {
    print('HomeScreen starts now | ${DateTime.now()} ');
    super.initState();
    //appState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  void appState() {
    super.canAccess(context, true);
  }

  void setPreviousTab() {
    _selectedIndex = _cachedIndexes[_cachedIndexes.length - 2];
    _onChangeBottomNavigation(_selectedIndex);
  }

  int _selectedIndex = 0;
  List<int> _cachedIndexes = [0];
  void _onChangeBottomNavigation(int index) {
    _cachedIndexes.add(index);
    if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => CartScreen()),
      );
    } else {
      setState(() {
        _selectedIndex = index;
        formatTitle();
      });
    }
  }

  void formatTitle() {
    switch (_selectedIndex) {
      case 0:
        {
          elevationValue = 3.0;
          appTitle = getTranslate('appTitle');
        }
        break;
      case 1:
        {}
        break;
      case 2:
        {}
        break;
      case 4:
        {
          elevationValue = 0.0;
          appTitle = 'PROFILE';
        }
        break;
    }
  }

  loadNavigationContent() {
    print(_selectedIndex);
    switch (_selectedIndex) {
      case 0:
        {
          return ShoppingHomeV2();
        }
        break;
      case 2:
        {
          return CategoryScreen();
        }
        break;
      case 3:
        {
          return WalletHomeScreen();
        }
        break;
      case 4:
        {
          return ProfileScreen();
        }
        break;
      default:
        return Container();
    }
  }

  Future<bool> onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: Center(child: new Text('Are you sure?')),

            // titlePadding: ,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
            content: Container(
                height: MediaQuery.of(context).size.height / 13,
                child: Column(
                  children: [
                    Center(
                        child: Text(
                      'Do you want to exit the App',
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.of(context).pop(false),
                            child: Text(
                              "NO",
                              style: TextStyle(fontWeight: FontWeight.w600, color: Colors.grey[600]),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => Navigator.of(context).pop(true),
                            child: Text(
                              "YES",
                              style: TextStyle(fontWeight: FontWeight.w600, color: Colors.grey[600]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
            // clipBehavior: ClipRecentView,
            //buttonPadding: EdgeInsets.symmetric(horizontal: 10),
            // actions: <Widget>[

            //   // SizedBox(height: 16),
            // ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onBackPressed,
      child: Scaffold(
        body: loadNavigationContent(),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.home, size: 20),
              title: Text(
                'HOME',
                style: TextStyle(fontSize: 12),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.shoppingCart,
                size: 20,
              ),
              title: Text(
                'CART',
                style: TextStyle(fontSize: 12),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.shoppingBag,
                size: 20,
              ),
              title: Text(
                'CATEGORY',
                style: TextStyle(fontSize: 12),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.wallet,
                size: 20,
              ),
              title: Text(
                'WALLET',
                style: TextStyle(fontSize: 12),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.userAlt,
                size: 20,
              ),
              title: Text(
                'PROFILE',
                style: TextStyle(fontSize: 12),
              ),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: GocoMartAppTheme.buildLightTheme().primaryColor,
          onTap: _onChangeBottomNavigation,
        ),
      ),
    );
  }
}
