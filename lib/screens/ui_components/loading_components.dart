import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImagePreLoader extends StatelessWidget {
  final String image;
  final double width;
  const ImagePreLoader({
    Key key,
    this.width = 40,
    this.image = 'assets/images/icons/picture.svg',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        image,
        width: width,
      ),
    );
  }
}

class ImageErrorDummy extends StatelessWidget {
  final double size;
  const ImageErrorDummy({
    Key key,
    this.size = 40,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Image.asset(
        'assets/images/g_logo_100.png',
        fit: BoxFit.fitHeight,
        color: Colors.grey.withOpacity(.4),
      ),
    );
  }
}

class ImageError extends StatelessWidget {
  final double size;
  const ImageError({
    Key key,
    this.size = 40,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Center(
        child: Icon(
          Icons.error,
          size: size,
        ),
      ),
    );
  }
}

class PageProgressSpinner extends StatelessWidget {
  const PageProgressSpinner({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
