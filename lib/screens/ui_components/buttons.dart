import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class GoCoBottomButton extends StatelessWidget {
  const GoCoBottomButton({
    Key key,
    this.label = '',
    this.minFontSize = 16,
    this.maxFontSize = 18,
    this.textColor = Colors.white,
    this.fontWeight = FontWeight.bold,
    @required this.onTap,
    this.buttonColor = Colors.blue,
  }) : super(key: key);
  final String label;
  final double minFontSize;
  final double maxFontSize;
  final Color textColor;
  final FontWeight fontWeight;
  final Function onTap;
  final Color buttonColor;

  void dummyFunction() {}
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            color: buttonColor,
          ),
          Positioned.fill(
              child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: onTap,
              child: Center(
                child: AutoSizeText(
                  label,
                  minFontSize: minFontSize,
                  maxFontSize: maxFontSize,
                  style: TextStyle(
                    color: textColor,
                    fontWeight: fontWeight,
                  ),
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }
}
