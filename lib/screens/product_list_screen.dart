import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'product-list/filter.dart';

String title;

class ProductListScreen extends StatefulWidget {
  ProductListScreen(String tit) {
    title = tit;
  }

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  ScrollController _scrollViewController = new ScrollController();

  var heart = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              title: Text(title),
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: PreferredSize(
                child: Container(
                  height: MediaQuery.of(context).size.width / 8,
                  color: Colors.white,
                  child: Row(
                    //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.width / 7.3,
                          margin: EdgeInsets.only(top: 1),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(width: 1.0, color: Colors.black26),
                              right: BorderSide(width: 1.0, color: Colors.black26),
                            ),
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FilterScreen(),
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  FontAwesomeIcons.filter,
                                  color: Colors.grey,
                                  size: MediaQuery.of(context).size.width / 25,
                                ),
                                Container(
                                  padding: const EdgeInsets.only(left: 7, top: 3),
                                  height: 25,
                                  width: MediaQuery.of(context).size.width / 4.8,
                                  //color: Colors.green,
                                  child: AutoSizeText(
                                    "Filter",
                                    presetFontSizes: [22, 19, 18, 16, 14, 10, 8],
                                    style: TextStyle(color: Colors.black54),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.width / 7.3,
                          margin: EdgeInsets.only(top: 1),
                          decoration: BoxDecoration(
                              border: Border(
                            bottom: BorderSide(width: 1.0, color: Colors.black26),
                          )),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                FontAwesomeIcons.sort,
                                color: Colors.grey,
                                size: MediaQuery.of(context).size.width / 25,
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 7, top: 1),
                                height: 25,
                                width: MediaQuery.of(context).size.width / 4.8,
                                //color: Colors.green,
                                child: AutoSizeText(
                                  "Sort",
                                  presetFontSizes: [22, 19, 18, 16, 14, 10, 8],
                                  style: TextStyle(color: Colors.black54),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                preferredSize: Size(0, 52),
              ),
            )
          ];
        },
        body: productLists(),
      ),
    );
  }

  Widget productLists() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, crossAxisSpacing: .1, childAspectRatio: 0.65),
      itemCount: 20,
      itemBuilder: (BuildContext context, int i) {
        return Card(
          elevation: 2,
          child: Container(
            height: MediaQuery.of(context).size.width / 2,

            //
            child: Stack(
              children: <Widget>[
                // Image
                Container(
                  height: MediaQuery.of(context).size.width / 2.3,
                  child: Image. asset(
                    "assets/images/home/bag1.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                //heart
                Positioned(
                  top: MediaQuery.of(context).size.width / 35,
                  right: MediaQuery.of(context).size.width / 20,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        if (heart == 0)
                          heart = 1;
                        else
                          heart = 0;
                      });
                    },
                    child: Icon(
                      Icons.favorite,
                      size: MediaQuery.of(context).size.width / 15,
                      color: heart == 1 ? Colors.red : Colors.grey[600],
                    ),
                  ),
                ),

                //Bottom part
                Positioned(
                  top: MediaQuery.of(context).size.width / 2.23,
                  left: 10,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AutoSizeText(
                        "Calvin Kellin",
                        overflow: TextOverflow.ellipsis,
                        presetFontSizes: [
                          18,
                          15,
                          14,
                          12,
                        ],
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.width / 19,
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: AutoSizeText(
                          "Ladies fashion Shoulder bag",
                          overflow: TextOverflow.ellipsis,
                          presetFontSizes: [14, 13, 12],
                          style: TextStyle(color: Colors.black54),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 3),
                        height: MediaQuery.of(context).size.width / 19,
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.width / 18,
                              width: MediaQuery.of(context).size.width / 9,
                              child: AutoSizeText(
                                "₹1,229",
                                overflow: TextOverflow.ellipsis,
                                presetFontSizes: [16, 15, 14, 13, 12],
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.width / 19,
                              width: MediaQuery.of(context).size.width / 9,
                              child: AutoSizeText(
                                "₹1,999",
                                overflow: TextOverflow.ellipsis,
                                presetFontSizes: [14, 12, 10],
                                style: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.black54),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(color: Colors.grey[350], borderRadius: BorderRadius.circular(8)),
                              height: MediaQuery.of(context).size.width / 19,
                              width: MediaQuery.of(context).size.width / 6.5,
                              child: AutoSizeText(
                                "6 colors",
                                overflow: TextOverflow.ellipsis,
                                presetFontSizes: [14, 12, 10],
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 3),
                        height: MediaQuery.of(context).size.width / 17,
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 1),
                              height: MediaQuery.of(context).size.width / 18,
                              width: MediaQuery.of(context).size.width / 4.1,
                              child: AutoSizeText(
                                "35% OFF TODAY",
                                style: TextStyle(color: Colors.green[400], fontWeight: FontWeight.w500),
                                presetFontSizes: [18, 14, 13, 12, 10, 8],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 6, right: 1.3, top: 1.5),
                              decoration: BoxDecoration(color: Colors.grey[350], borderRadius: BorderRadius.circular(8)),
                              height: MediaQuery.of(context).size.width / 21,
                              width: MediaQuery.of(context).size.width / 6.5,
                              child: AutoSizeText(
                                "1 in stock",
                                overflow: TextOverflow.ellipsis,
                                presetFontSizes: [14, 12, 10, 9, 8],
                                style: TextStyle(color: Colors.red),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
