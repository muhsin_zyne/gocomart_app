import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/providers/category_provider.dart';

class SortWidgetBlock extends StatefulWidget {
  final CategoryProvider categoryProvider;
  final Function refreshProductList;
  const SortWidgetBlock({Key key, this.categoryProvider, this.refreshProductList}) : super(key: key);
  @override
  _SortWidgetBlockState createState() => _SortWidgetBlockState();
}

class _SortWidgetBlockState extends State<SortWidgetBlock> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        for (int i = 0; i < AppSortList.ALL.length; i++)
          _myRadioButton(
            title: AppSortList.ALL[i].name,
            value: AppSortList.ALL[i].id,
            onChanged: (newValue) async {
              setState(() {
                this.widget.categoryProvider.selectedSort = AppSortList.ALL[i];
              });
              this.widget.refreshProductList();
              await Future.delayed(Duration(milliseconds: 500));
              Navigator.pop(context);
            },
          ),
      ],
    );
  }

  Widget _myRadioButton({String title, int value, Function onChanged}) {
    return Container(
      //height: MediaQuery.of(context).size.height / 20,
      child: RadioListTile(
        value: value,
        groupValue: this.widget.categoryProvider.selectedSort.id,
        onChanged: onChanged,
        title: Text(title),
        activeColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
