import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/models/filter-new/filter.dart';
import 'package:gocomartapp/models/filter-new/filterGroup.dart';
import 'package:gocomartapp/models/filter-new/filterGroupProvider.dart';
import 'package:gocomartapp/models/filter/getFiltersByGroup.dart';
import 'package:gocomartapp/providers/category_provider.dart';
import 'package:gocomartapp/screens/ui_components/buttons.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  CategoryProvider _categoryProvider;

  @override
  void didChangeDependencies() {
    _categoryProvider = Provider.of<CategoryProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  void actionClearFilter() async {
    await _categoryProvider.clearAllFilterItems();
    Navigator.pop(context);
  }

  void actionApplyFilter() async {
    await _categoryProvider.applyFilterAction();
    //await Future.delayed(Duration(seconds: 3));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        //elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Filter',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: FilterScreenContent(),
      bottomSheet: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: Colors.black12,
            ),
          ),
        ),
        child: SafeArea(
          child: Container(
            height: 60,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: GoCoBottomButton(
                    buttonColor: Colors.black54,
                    label: "Clear",
                    onTap: actionClearFilter,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: GoCoBottomButton(
                    buttonColor: Theme.of(context).primaryColor,
                    label: "Apply",
                    onTap: actionApplyFilter,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class FilterScreenContent extends StatefulWidget {
  @override
  _FilterScreenContentState createState() => _FilterScreenContentState();
}

class _FilterScreenContentState extends State<FilterScreenContent> {
  ShoppingServices _shoppingServices;
  CategoryProvider _categoryProvider;
  GetFiltersByGroup _getFiltersByGroup;
  FilterGroupProvider _filterGroupProvider;
  bool pageReady = false;
  bool filterItemsLoading = false;
  @override
  void initState() {
    _shoppingServices = ShoppingServices(context: context);
    super.initState();
    _loadClient();
  }

  @override
  void didChangeDependencies() {
    _categoryProvider = Provider.of<CategoryProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    this._getCachedFilters();
    this._networkFetchFilters();
  }

  void _getCachedFilters() async {
    this._categoryProvider.saveLastFilterState();
    if (_categoryProvider.filterGroupProvider?.filterGroups != null) {
      setState(() {
        this._filterGroupProvider = _categoryProvider.filterGroupProvider;
        final FilterGroup initialFilterGroup = _filterGroupProvider.filterGroups[0];
        initialFilterGroup.filters = _filterGroupProvider.filters;
        _categoryProvider.selectedFilterGroup = initialFilterGroup;
        pageReady = true;
      });
    } else {
      print("else triggered");
    }
  }

  void _networkFetchFilters() async {
    if (_categoryProvider.productListSelectedCategoryId != 0) {
      QueryResult filtersMenu = await _shoppingServices.getFilterGroupByCategory(categoryId: _categoryProvider.productListSelectedCategoryId);
      if (!filtersMenu.hasException) {
        print("network result came");
        _categoryProvider.filterGroupProvider = FilterGroupProvider.fromJson(filtersMenu.data['getFilterGroupByCategory']);
        setState(() {
          this._filterGroupProvider = _categoryProvider.filterGroupProvider;
          if (_categoryProvider.selectedFilterGroup == null || _selectedFilterGroupIsAvailable(this._categoryProvider.selectedFilterGroup) == false) {
            final FilterGroup initialFilterGroup = _filterGroupProvider.filterGroups[0];
            initialFilterGroup.filters = _filterGroupProvider.filters;
            _categoryProvider.selectedFilterGroup = initialFilterGroup;
          }
          pageReady = true;
        });
      } else {
        print(filtersMenu.exception);
      }
    } else {
      // other nettwork filter gose here
    }
  }

  bool _selectedFilterGroupIsAvailable(FilterGroup selectedFilterGroup) {
    var index = this._filterGroupProvider.filterGroups.indexWhere((element) => element.filterGroupId == selectedFilterGroup.filterGroupId);
    if (index == -1) {
      return false;
    }

    if (_categoryProvider.lastLoadedCategory == _categoryProvider.productListSelectedCategoryId) {
      return true;
    } else {
      return false;
    }
  }

  _filterGroupSelected(FilterGroup selectedGroup) {
    setState(() {
      _categoryProvider.selectedFilterGroup = selectedGroup;
      this.filterItemsLoading = true;
    });
    this._filterGroupSelectedCachedFetch(selectedGroup);
    this._filterGroupSelectedNetworkFetch(selectedGroup);

    // setState(() {
    //   _categoryProvider.selectedFilterGroup = selectedGroup;
    // });
  }

  _updateSelectedFilterGroup(FilterGroup selectedGroup) {
    // setting for cache
    final FilterGroup filterGroupItem = _categoryProvider.filterGroupProvider.filterGroups.firstWhere((element) => element.filterGroupId == selectedGroup.filterGroupId);
    filterGroupItem.filters = selectedGroup.filters;
    // setting for view
    setState(() {
      _categoryProvider.selectedFilterGroup = selectedGroup;
      filterItemsLoading = false;
    });
  }

  // cached filter fetch if already available in provider array
  _filterGroupSelectedCachedFetch(FilterGroup selectedGroup) {
    final FilterGroup filterGroupItem = _categoryProvider.filterGroupProvider.filterGroups.firstWhere((element) => element.filterGroupId == selectedGroup.filterGroupId);
    if ((filterGroupItem?.filters?.length ?? 0) > 0) {
      setState(() {
        _categoryProvider.selectedFilterGroup = filterGroupItem;
        filterItemsLoading = false;
      });
    }
  }

  _filterGroupSelectedNetworkFetch(FilterGroup selectedGroup) async {
    QueryResult filtersQuery = await this._shoppingServices.getFilterGroupFilters(categoryId: _categoryProvider.productListSelectedCategoryId, filterGroupId: selectedGroup.filterGroupId);
    if (!filtersQuery.hasException) {
      final FilterGroupProvider tempFilterGroup = FilterGroupProvider.fromJson(filtersQuery.data['getFilterGroupFilters']);
      selectedGroup.filters = tempFilterGroup.filters;
      this._updateSelectedFilterGroup(selectedGroup);
    } else {
      print(filtersQuery.exception);
    }
  }

  @override
  Widget build(BuildContext context) {
    return pageReady
        ? Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    color: Color(0xfff6f6f6),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 60),
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Color(0xffDBE3F1),
                      child: ListView.builder(
                        itemBuilder: (BuildContext context, int index) {
                          FilterGroup cFilterGroup = _filterGroupProvider.filterGroups[index];
                          return Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    left: BorderSide(
                                      width: ((_categoryProvider?.selectedFilterGroup?.filterGroupId ?? 0) == cFilterGroup.filterGroupId) ? 6 : 0,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  color: ((_categoryProvider?.selectedFilterGroup?.filterGroupId ?? 0) == cFilterGroup.filterGroupId) ? Color(0xfff6f6f6) : Color(0xffDBE3F1),
                                ),
                                margin: EdgeInsets.only(top: 2),
                                child: Container(
                                  margin: EdgeInsets.only(left: 5, top: 5, right: 2),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          child: Center(
                                            child: AutoSizeText(
                                              cFilterGroup.description.name ?? '',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                              ),
                                            ),
                                          ),
                                          height: 50,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Positioned.fill(
                                child: Material(
                                  child: InkWell(
                                    onTap: () {
                                      this._filterGroupSelected(cFilterGroup);
                                    },
                                  ),
                                  color: Colors.transparent,
                                ),
                              )
                            ],
                          );
                        },
                        itemCount: _filterGroupProvider?.filterGroups?.length ?? 0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: filterItemsLoading == false
                      ? Container(
                          color: Color(0xfff6f6f6),
                          child: ListView.builder(
                            itemBuilder: (BuildContext context, int index) {
                              Filter cFilter = _categoryProvider.selectedFilterGroup.filters[index];
                              return Column(
                                children: <Widget>[
                                  ListTile(
                                    onTap: () {
                                      setState(() {
                                        cFilter.value = !cFilter.value;
                                        print(cFilter.toJson());
                                        _categoryProvider.filterItemChanged(cFilter.value, cFilter);
                                      });
                                    },
                                    leading: Checkbox(
                                      value: (_categoryProvider.findFilterAppliedByFilterId(cFilter.filterId)),
                                      onChanged: (value) {
                                        setState(() {
                                          _categoryProvider.filterItemChanged(value, cFilter);
                                          cFilter.value = value;
                                        });
                                      },
                                    ),
                                    title: Text(cFilter.name),
                                  ),
                                ],
                              );
                            },
                            itemCount: _categoryProvider?.selectedFilterGroup?.filters?.length ?? 0,
                          ),
                        )
                      : PageProgressSpinner(),
                ),
              ],
            ),
          )
        : PageProgressSpinner();
  }
}
