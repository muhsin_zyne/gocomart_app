import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/product-list/product.dart';
import 'package:gocomartapp/models/product-list/product_list.dart';
import 'package:gocomartapp/providers/category_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/modules/skeleton_animations.dart';
import 'package:gocomartapp/screens/product-list/filter.dart';
import 'package:gocomartapp/screens/product-list/sort_widget.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';

class ProductListScreen extends StatefulWidget {
  final String title;
  final int categoryId;

  const ProductListScreen({Key key, this.title = '', this.categoryId = 0}) : super(key: key);
  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  // providers
  CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  HtmlUnescape _htmlUnescape = HtmlUnescape();

  CategoryProvider _categoryProvider;
  GlobalProvider _globalProvider;
  OCImageCache ocImageCache = new OCImageCache();
  ShoppingServices _shoppingServices;
  ScrollController _scrollController = new ScrollController();
  List<Product> rawProductList = [];

  List<Widget> appSortList = [];
  var heart = 0;

  int totalResult = 0;
  int page = 1;
  int limit = 10;
  bool productLoading = false;

  @override
  void didChangeDependencies() {
    _categoryProvider = Provider.of<CategoryProvider>(context, listen: true);
    _globalProvider = Provider.of<GlobalProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    this._init();
  }

  void _init() async {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        this._paginationRequest();
      }
    });
    _shoppingServices = ShoppingServices(context: context);
    this._loadClient();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    this._categoryProvider.productListSelectedCategoryId = this.widget.categoryId;
    this._clearOldFilterElements();
    this._getProducts();
    //(_categoryProvider.productListSelectedCategoryId);
  }

  void _clearOldFilterElements() {
    this._categoryProvider.clearCachedCheckListSelected();
  }

  void _paginationRequest() {
    if (rawProductList.length < totalResult) {
      if (rawProductList.length > 0) {
        page++;
        this._getProducts();
      }
    } else {
      //print("loading end");
      // Fluttertoast.showToast(
      //   msg: "No more Products to show",
      //   toastLength: Toast.LENGTH_SHORT,
      // );
    }
  }

  _getProducts({bool forceReload = false}) async {
    setState(() {
      productLoading = true;
    });
    if (forceReload == true) {
      this.page = 1;
    }
    dynamic filters = {"filter_ids": _categoryProvider.selectFilterList};

    dynamic categories = [];
    if (_categoryProvider.productListSelectedCategoryId != 0) {
      categories.add(_categoryProvider.productListSelectedCategoryId);
    }
    QueryResult result = await _shoppingServices.productList(
        categories: categories,
        filters: filters,
        page: page,
        limit: limit,
        sort: _categoryProvider.selectedSort.value,
        isNetwork: (forceReload ? true : false));
    if (!result.hasException) {
      ProductList productList = ProductList.fromJson(result.data['productList']);
      if (productList.products.length > 0) {
        setState(() {
          totalResult = productList.total;
          if (forceReload == true) {
            rawProductList = [];
          }
          rawProductList = [rawProductList, productList.products].expand((x) => x).toList();
        });
      }
    } else {
      print("having error in product fetch");
    }
    setState(() {
      productLoading = false;
    });
  }

  void _applyFilterAction() {
    /// print("apply filter action");
    setState(() {
      this.rawProductList = [];
    });
    this._getProducts();
  }

  void _applySort() {
    setState(() {
      this.rawProductList = [];
    });
    this._getProducts(forceReload: true);
  }

  void _showSortingScreen() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
            height: 300,
            child: SortWidgetBlock(
              categoryProvider: this._categoryProvider,
              refreshProductList: _applySort,
            ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              title: Text(this.widget.title),
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: PreferredSize(
                child: Container(
                  height: MediaQuery.of(context).size.width / 8,
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.width / 7.3,
                          margin: EdgeInsets.only(top: 1),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(width: 1.0, color: Colors.black26),
                              right: BorderSide(width: 1.0, color: Colors.black26),
                            ),
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FilterScreen(),
                                ),
                              ).whenComplete(() {
                                this._applyFilterAction();
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  FontAwesomeIcons.filter,
                                  color: Colors.grey,
                                  size: MediaQuery.of(context).size.width / 25,
                                ),
                                Container(
                                  padding: const EdgeInsets.only(left: 7, top: 3),
                                  height: 25,
                                  width: MediaQuery.of(context).size.width / 4.8,
                                  //color: Colors.green,
                                  child: AutoSizeText(
                                    "Filter",
                                    presetFontSizes: [22, 19, 18, 16, 14, 10, 8],
                                    style: TextStyle(color: Colors.black54),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.width / 7.3,
                          margin: EdgeInsets.only(top: 1),
                          decoration: BoxDecoration(
                              border: Border(
                            bottom: BorderSide(width: 1.0, color: Colors.black26),
                          )),
                          child: Container(
                            child: InkWell(
                              onTap: _showSortingScreen,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    FontAwesomeIcons.sort,
                                    color: Colors.grey,
                                    size: MediaQuery.of(context).size.width / 25,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(left: 7, top: 1),
                                    height: 25,
                                    width: MediaQuery.of(context).size.width / 4.8,
                                    //color: Colors.green,
                                    child: AutoSizeText(
                                      "Sort",
                                      presetFontSizes: [22, 19, 18, 16, 14, 10, 8],
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                preferredSize: Size(0, 52),
              ),
            )
          ];
        },
        body: RefreshIndicator(
          onRefresh: () async {
            await this._getProducts(forceReload: true);
          },
          child: (productLoading == false && rawProductList.length == 0)
              ? noProducts()
              : Container(
                  child: GridView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 2, vertical: 0),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      childAspectRatio: 0.63,
                      mainAxisSpacing: 10,
                    ),
                    itemCount: this.rawProductList.length + (productLoading == true ? 4 : 0),
                    itemBuilder: (BuildContext context, int index) {
                      if (index >= this.rawProductList.length) {
                        return ProductListSkeletonAnimation();
                      }
                      Product cProduct = rawProductList[index];
                      double applicablePrice = cProduct.price.toDouble();

                      // print(cProduct.price);

                      if (cProduct?.special?.price != null) {
                        if ((cProduct.special.price > 0) && (cProduct.special.price < cProduct.price)) {
                          applicablePrice = cProduct.special.price.toDouble();
                        }
                      }
                      if (cProduct.customerPrice?.offer_price != null) {
                        if (cProduct.customerPrice.offer_price > 0) {
                          if (cProduct.customerPrice.offer_price < applicablePrice) {
                            applicablePrice = cProduct.customerPrice.offer_price.toDouble();
                          }
                        }
                      }

                      int calculatedPercentage = discountPercentage(cProduct.price, applicablePrice);

                      return Stack(
                        children: [
                          Card(
                            elevation: 2,
                            child: Container(
                              color: Colors.white,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context).size.width / 2.2,
                                              height: MediaQuery.of(context).size.width / 2,
                                              child: Container(
                                                child: CachedNetworkImage(
                                                  imageUrl: "${_globalProvider.cdnImagePoint + cProduct.image}",
                                                  fit: BoxFit.cover,
                                                  placeholder: (context, url) => Center(
                                                    child: ImagePreLoader(),
                                                  ),
                                                  errorWidget: (context, url, error) => Container(
                                                    child: ImageError(),
                                                  ),
                                                ),
                                                padding: EdgeInsets.all(10),
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    top: 2,
                                    child: Container(
                                        width: 50,
                                        height: 50,
                                        //color: Colors.red,
                                        child: InkWell(
                                          onTap: () {},
                                          child: Icon(
                                            Icons.favorite,
                                            size: MediaQuery.of(context).size.width / 12,
                                            color: heart == 1 ? Colors.red : Colors.grey[600],
                                          ),
                                        )),
                                  ),
                                  cProduct.avlColorCount > 0
                                      ? Positioned(
                                          bottom: 50,
                                          right: 10,
                                          child: Container(
                                            width: 100,
                                            height: 30,
                                            //color: Colors.red,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[350],
                                                    borderRadius: BorderRadius.circular(8),
                                                  ),
                                                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                                  child: AutoSizeText(
                                                    "${cProduct.avlColorCount} Color${cProduct.avlColorCount > 1 ? 's' : ''}",
                                                    overflow: TextOverflow.ellipsis,
                                                    presetFontSizes: [14, 12, 10],
                                                    textAlign: TextAlign.center,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      : Container(),
                                  Positioned.fill(
                                    child: Container(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(
                                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                                    child: AutoSizeText(
                                                      "${_htmlUnescape.convert(cProduct.description.name)}",
                                                      overflow: TextOverflow.ellipsis,
                                                      minFontSize: 14,
                                                      maxFontSize: 14,
                                                      maxLines: 2,
                                                      style: TextStyle(fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(
                                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                                    child: AutoSizeText(
                                                      "${cProduct?.manufacturer?.name ?? ''}",
                                                      overflow: TextOverflow.ellipsis,
                                                      minFontSize: 15,
                                                      maxFontSize: 19,
                                                      style: TextStyle(color: Colors.black54),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 2,
                                                  child: Container(
                                                    padding: EdgeInsets.only(left: 10),
                                                    child: AutoSizeText(
                                                      "${inr.formatCurrency(applicablePrice)}",
                                                      //overflow: TextOverflow.ellipsis,
                                                      textAlign: TextAlign.left,
                                                      minFontSize: 16,
                                                      maxFontSize: 22,
                                                      style: TextStyle(fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                                (applicablePrice < cProduct.price)
                                                    ? Expanded(
                                                        flex: 2,
                                                        child: Container(
                                                          child: AutoSizeText(
                                                            "${inr.formatCurrency(cProduct.price.toDouble())}",
                                                            overflow: TextOverflow.ellipsis,
                                                            presetFontSizes: [14, 12, 10],
                                                            style: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.black54),
                                                          ),
                                                        ),
                                                      )
                                                    : Container(),
                                              ],
                                            ),
                                          ),
                                          calculatedPercentage > 0
                                              ? Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 1,
                                                        child: AutoSizeText(
                                                          "$calculatedPercentage% OFF TODAY",
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.green, fontWeight: FontWeight.w600),
                                                          minFontSize: 14,
                                                          maxFontSize: 20,
                                                          maxLines: 1,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                )
                                              : Container(),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Material(
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ProductDetailScreen(
                                      productId: cProduct.productId,
                                    ),
                                  ),
                                ).whenComplete(() {});
                              },
                            ),
                            color: Colors.transparent,
                          ),
                        ],
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }

  Widget noProducts() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('No Product'),
            ],
          ),
        ],
      ),
    );
  }
}
