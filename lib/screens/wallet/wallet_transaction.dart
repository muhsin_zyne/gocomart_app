import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/models/wallet/coinTransaction.dart';
import 'package:gocomartapp/models/wallet/coinTransactionHistory.dart';
import 'package:gocomartapp/models/wallet_screen/transaction.dart';
import 'package:gocomartapp/models/wallet_screen/transaction_history.dart';
import 'package:gocomartapp/screens/wallet/widgets/transactionTile.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/util/enum.dart';
import 'package:graphql/client.dart';
import 'package:skeleton_text/skeleton_text.dart';

class WalletTransaction extends StatefulWidget {
  WalletTransaction({
    Key key,
    this.walletType = GOCOWallet.cashWallet,
  }) : super(key: key);
  final GOCOWallet walletType;

  @override
  _WalletTransactionState createState() => _WalletTransactionState();
}

class _WalletTransactionState extends State<WalletTransaction> {
  CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  ScrollController _scrollController = ScrollController();
  AccountServices _accountServices;
  bool pageReady = false;
  int paginationLimit = 20;
  int page = 1;
  int totalLength = 0;
  List<Transaction> rawTransactionList = [];
  List<CoinTransaction> rawCoinTransactionList = [];
  bool dataAppending = false;
  @override
  void initState() {
    super.initState();
    _accountServices = AccountServices(context: context);
    _loadClient();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _init();
  }

  _init() {
    _transactionRefresh(clear: true);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _paginationRequest();
      }
    });
  }

  _paginationRequest() async {
    print(totalLength);
    if (totalLength > rawTransactionList.length) {
      this.page++;
      setState(() {
        this.dataAppending = true;
      });
      await _transactionRefresh(clear: false);
      setState(() {
        this.dataAppending = false;
      });
    } else {}
  }

  _transactionRefresh({clear = false}) async {
    if (this.widget.walletType == GOCOWallet.cashWallet) {
      QueryResult result = await _accountServices.cashWalletTransactions(transPage: this.page, transLimit: this.paginationLimit);
      if (!result.hasException) {
        TransactionHistory transactionHistory = TransactionHistory.fromJson(result.data['transactionHistory']);
        setState(() {
          this.totalLength = transactionHistory.total;
        });
        if (clear == true) {
          setState(() {
            rawTransactionList = [];
          });
        }
        if (transactionHistory.data.length > 0) {
          setState(() {
            rawTransactionList = [rawTransactionList, transactionHistory.data].expand((x) => x).toList();
          });
        }
      }
    }
    // coin wallet transaction fetch
    else if (this.widget.walletType == GOCOWallet.coinWallet) {
      print(this.page);
      print(this.paginationLimit);
      QueryResult result = await _accountServices.coinWalletTransactions(transPage: this.page, transLimit: this.paginationLimit);
      if (!result.hasException) {
        CoinTransactionHistory transactionHistory = CoinTransactionHistory.fromJson(result.data['coinTransactionHistory']);
        setState(() {
          this.totalLength = transactionHistory.total;
        });
        if (clear == true) {
          setState(() {
            rawCoinTransactionList = [];
          });
        }
        if (transactionHistory.transactions.length > 0) {
          setState(() {
            rawCoinTransactionList = [rawCoinTransactionList, transactionHistory.transactions].expand((x) => x).toList();
          });
        }
      } else {
        print(result.exception);
      }
    }
    setState(() {
      this.pageReady = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Transactions'),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          this.page = 1;
          await this._transactionRefresh(clear: true);
        },
        child: pageReady
            ? ((this.widget.walletType == GOCOWallet.cashWallet && rawTransactionList.length == 0) ||
                    (this.widget.walletType == GOCOWallet.coinWallet && rawCoinTransactionList.length == 0))
                ? Container(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset(
                            'assets/images/icons/icons8-document.svg',
                            width: 120,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                                  child: AutoSizeText(
                                    'Look like you don\'t have any transactions to show',
                                    style: TextStyle(color: Colors.black54),
                                    textAlign: TextAlign.center,
                                    presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 80,
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(
                    padding: EdgeInsets.all(15),
                    child: this.widget.walletType == GOCOWallet.cashWallet
                        ? ListView.builder(
                            controller: _scrollController,
                            itemBuilder: (BuildContext context, int index) {
                              if (index >= this.rawTransactionList.length) {
                                return skeletonAnimationBlock(3);
                              }
                              Transaction currentItem = rawTransactionList[index];
                              return Column(
                                children: <Widget>[
                                  TransactionTile(
                                    transactionType: currentItem.type == 'cr' ? TransactionType.cr : TransactionType.dr,
                                    transDescription: currentItem.description ?? '',
                                    transDate: formatTime(currentItem.created_at),
                                    transAmount: '${inr.formatCurrency(currentItem.amount.toDouble())}',
                                  ),
                                  Divider(),
                                ],
                              );
                            },
                            itemCount: this.rawTransactionList.length + (dataAppending == true ? 1 : 0),
                          )
                        : ListView.builder(
                            controller: _scrollController,
                            itemBuilder: (BuildContext context, int index) {
                              if (index >= this.rawCoinTransactionList.length) {
                                return skeletonAnimationBlock(3);
                              }
                              CoinTransaction currentItem = rawCoinTransactionList[index];
                              return Column(
                                children: <Widget>[
                                  TransactionTile(
                                    transactionType: currentItem.type == 'cr' ? TransactionType.cr : TransactionType.dr,
                                    transDescription: currentItem.description ?? '',
                                    transDate: formatTime(currentItem.created_at),
                                    transAmount: '${inr.formatToNumber(currentItem.coin_value.toDouble())}',
                                  ),
                                  Divider(),
                                ],
                              );
                            },
                            itemCount: this.rawCoinTransactionList.length + (dataAppending == true ? 1 : 0),
                          ),
                  )
            : skeletonAnimationBlock(6),
      ),
    );
  }

  Widget skeletonAnimationBlock(length) {
    List animation = <Widget>[];
    for (int i = 0; i < length; i++) {
      animation.add(skeltonWidget());
      animation.add(Divider());
    }
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: animation,
      ),
    );
  }

  Widget skeltonWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: SkeletonAnimation(
              child: Container(
                height: 20,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Expanded(
          flex: 9,
          child: Container(
            height: 60,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 5,
                ),
                SkeletonAnimation(
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                SkeletonAnimation(
                  child: Container(
                    height: 10,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
