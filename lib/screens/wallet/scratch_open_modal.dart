import 'package:flutter/material.dart';
import 'package:gocomartapp/components/ui_animations/fireworks.dart';

Color intToColor(int col) {
  col = col % 10;
  if (col == 1) return Colors.red;
  if (col == 2) return Colors.green;
  if (col == 3) return Colors.orange;
  if (col == 4) return Colors.blue;
  if (col == 5) return Colors.pink;
  if (col == 6) return Colors.brown;
  if (col == 7) return Colors.deepOrange;
  if (col == 8) return Colors.tealAccent;
  if (col == 8) return Colors.black45;
  if (col == 10) return Color(0xff00aff0);
  return Colors.black;
}

class ListTileDemoParticle extends Particle {
  @override
  void paint(Canvas canvas, Size size, progress, seed) {
    CompositeParticle(
      children: [
        Firework(),
        Firework(),
        Firework(),
        RectangleMirror.builder(
          numberOfParticles: 10,
          particleBuilder: (int) {
            return AnimatedPositionedParticle(
              begin: Offset(0.0, -30.0),
              end: Offset(0.0, -120.0),
              child: FadingRect(width: 5.0, height: 15.0, color: intToColor(int)),
            );
          },
          initialDistance: 0.0,
        ),
        RectangleMirror.builder(
          numberOfParticles: 10,
          particleBuilder: (int) {
            return AnimatedPositionedParticle(
              begin: Offset(0.0, -25.0),
              end: Offset(0.0, -50.0),
              child: FadingRect(width: 5.0, height: 15.0, color: intToColor(int)),
            );
          },
          initialDistance: 30.0,
        ),
        RectangleMirror.builder(
          numberOfParticles: 10,
          particleBuilder: (int) {
            return AnimatedPositionedParticle(
              begin: Offset(0.0, -40.0),
              end: Offset(0.0, -100.0),
              child: FadingRect(width: 5.0, height: 15.0, color: intToColor(int)),
            );
          },
          initialDistance: 10.0,
        ),
        Firework(),
        Firework(),
        Firework(),
        Firework(),
      ],
    ).paint(canvas, size, progress, seed);
  }
}

class FreeOrderParticles extends Particle {
  @override
  void paint(Canvas canvas, Size size, progress, seed) {
    CompositeParticle(children: [
      Firework(),
      Firework(),
      Firework(),
      Firework(),
      Firework(),
      Firework(),
      Firework(),
      RectangleMirror.builder(
          numberOfParticles: 8,
          particleBuilder: (int) {
            return AnimatedPositionedParticle(
              begin: Offset(0.0, -30.0),
              end: Offset(0.0, -250.0),
              child: FadingRect(width: 5.0, height: 25.0, color: intToColor(int)),
            );
          },
          initialDistance: 0.0),
      RectangleMirror.builder(
          numberOfParticles: 8,
          particleBuilder: (int) {
            return AnimatedPositionedParticle(
              begin: Offset(0.0, 0.0),
              end: Offset(0.0, -75.0),
              child: FadingRect(width: 7.0, height: 15.0, color: intToColor(int)),
            );
          },
          initialDistance: 30.0),
      RectangleMirror.builder(
        numberOfParticles: 8,
        particleBuilder: (int) {
          return AnimatedPositionedParticle(
            begin: Offset(0.0, -40.0),
            end: Offset(0.0, -190.0),
            child: FadingRect(width: 5.0, height: 15.0, color: intToColor(int)),
          );
        },
        initialDistance: 60.0,
      ),
    ]).paint(canvas, size, progress, seed);
  }
}
