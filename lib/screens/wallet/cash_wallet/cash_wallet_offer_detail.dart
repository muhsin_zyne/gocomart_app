import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:gocomartapp/controllers/action_manager.dart';
import 'package:gocomartapp/main.dart';
import 'package:gocomartapp/models/wallet/walletOffer.dart';

class CashWalletOfferDetailScreen extends StatefulWidget {
  final WalletOffer walletOffer;
  const CashWalletOfferDetailScreen({
    Key key,
    @required this.walletOffer,
  }) : super(key: key);
  @override
  _CashWalletOfferDetailScreenState createState() => _CashWalletOfferDetailScreenState();
}

class _CashWalletOfferDetailScreenState extends State<CashWalletOfferDetailScreen> {
  ScrollController _scrollController = ScrollController();
  double cardPostion = 0.0;
  bool popQue = false;

  _scrollControllerInt() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels <= _scrollController.position.minScrollExtent) {
        setState(() {
          cardPostion = 0;
        });
      }
      if (_scrollController.position.pixels > 0 && _scrollController.position.pixels <= 120) {
        setState(() {
          cardPostion = _scrollController.position.pixels.roundToDouble();
        });
      }
//      if (cardPostion == 0 && _scrollController.position.pixels <= _scrollController.position.minScrollExtent) {
//        popContext();
//      }
    });
  }

  void popContext() {
    if (popQue != true) {
      popQue = true;
      Navigator.pop(context);
    } else {
      print("pop having a que");
    }
  }

  @override
  void initState() {
    super.initState();
    this._scrollControllerInt();
  }

  void doAction() {
    ActionManager actionManager = ActionManager(context);
    actionManager.runAction(this.widget.walletOffer.action);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Colors.grey.withOpacity(.5),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    //color: Colors.red,
                  ),
                  headerBlock(),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      margin: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          topLeft: Radius.circular(30.0),
                        ),
                        color: Colors.white,
                      ),
                      height: MediaQuery.of(context).size.height * .72 + (cardPostion),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                        child: SingleChildScrollView(
                          controller: _scrollController,
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: AutoSizeText(
                                      'FROM ${this.widget.walletOffer.offer_from}',
                                      presetFontSizes: [14, 13, 12, 11, 10],
                                      style: TextStyle(
                                        color: Color(0xff535353),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: AutoSizeText(
                                        '${this.widget.walletOffer.title}',
                                        presetFontSizes: [22, 21, 20, 19],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff353535),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: AutoSizeText(
                                        '${this.widget.walletOffer.description}',
                                        presetFontSizes: [14, 13, 12, 11, 10],
                                        textAlign: TextAlign.left,
                                        maxLines: 5,
                                        style: TextStyle(
                                          color: Color(0xff353535),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Html(
                                data: this.widget.walletOffer.content,
                                onLinkTap: (url) {
                                  print("Opening $url...");
                                },
                                onImageTap: (src) {
                                  print(src);
                                },
                                onImageError: (exception, stackTrace) {
                                  print(exception);
                                },
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                      //color: Colors.white,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Container(
        height: 70,
        color: Colors.transparent,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    //color: Colors.blue,
                    child: FlatButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
                      onPressed: doAction,
                      color: Theme.of(context).primaryColor,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: AutoSizeText(
                          this.widget.walletOffer.action_label ?? '',
                          presetFontSizes: [15, 14, 13],
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget headerBlock() {
    return Container(
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: InkWell(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 20,
                      child: IconButton(
                        //padding: EdgeInsets.zero,
                        icon: Icon(Icons.close),
                        color: Colors.white,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
            Hero(
              tag: "walletOfferHero${this.widget.walletOffer.id}",
              child: Container(
                  margin: EdgeInsets.only(top: 20),
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: HexColor(this.widget.walletOffer.card_color),
                  ),
                  child: Image.asset('assets/images/texture/${this.widget.walletOffer.offer_card_image}')),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: InkWell(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 20,
                      child: IconButton(
                        //padding: EdgeInsets.zero,
                        icon: Icon(Icons.share),
                        color: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                    onTap: () {},
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
