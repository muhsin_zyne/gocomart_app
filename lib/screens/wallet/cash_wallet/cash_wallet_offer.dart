import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/hero_route.dart';
import 'package:gocomartapp/main.dart';
import 'package:gocomartapp/models/wallet/walletOffer.dart';
import 'package:gocomartapp/models/wallet/walletOffers.dart';
import 'package:gocomartapp/screens/wallet/cash_wallet/cash_wallet_offer_detail.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:graphql/client.dart';

class CashWalletOfferScreen extends StatefulWidget {
  static const String id = 'cash_wallet_offer_screen';
  @override
  _CashWalletOfferScreenState createState() => _CashWalletOfferScreenState();
}

class _CashWalletOfferScreenState extends State<CashWalletOfferScreen> {
  int currentItem = 1;
  AppServices _appServices;
  WalletOffers _walletOffers;
  bool pageReady = false;
  @override
  void initState() {
    super.initState();
    _appServices = AppServices(context: context);

    this._loadClient();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    this._init();
  }

  void _init() async {
    this._getWalletOffers();
  }

  void _getWalletOffers() async {
    QueryResult result = await _appServices.getWalletOffers(walletType: 'cash_wallet');
    if (!result.hasException) {
      _walletOffers = WalletOffers.fromJson(result.data['getWalletOffers']);
    }
    setState(() {
      this.pageReady = true;
    });
  }

  void _navigateToOfferDetail() {
    var selectedIndex = currentItem - 1;
    WalletOffer selectedOffer = this._walletOffers.offers[selectedIndex];
    Navigator.push(
      context,
      HeroDialogRoute(
        builder: (context) => CashWalletOfferDetailScreen(
          walletOffer: selectedOffer,
        ),
      ),
    ).whenComplete(() async {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        backgroundColor: Colors.white,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Discover Offers',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16,
              ),
            ),
            Text(
              '$currentItem of ${_walletOffers?.offers?.length ?? 0}',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 12,
              ),
            )
          ],
        ),
        elevation: 0,
      ),
      body: pageReady
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _walletOffers.offers.length > 1
                    ? Container(
                        height: MediaQuery.of(context).size.height * .7,
                        child: PageView.builder(
                          controller: PageController(viewportFraction: 0.8),
                          onPageChanged: (value) {
                            print(value);
                            setState(() {
                              currentItem = value + 1;
                            });
                          },
                          allowImplicitScrolling: false,
                          reverse: false,
                          itemCount: _walletOffers.offers.length,
                          itemBuilder: (BuildContext context, int index) {
                            WalletOffer cOffer = _walletOffers.offers[index];
                            return WalletOfferSwipe(
                              heroTag: "walletOfferHero${cOffer.id}",
                              walletOffer: cOffer,
                            );
                          },
                        ),
                      )
                    : Container(),
                SafeArea(
                  child: FlatButton(
                    onPressed: _navigateToOfferDetail,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 13),
                      child: Text(
                        'See Offer Details',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                    ),
                  ),
                )
              ],
            )
          : pageLoader(),
    );
  }

  Widget pageLoader() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

class WalletOfferSwipe extends StatelessWidget {
  final String heroTag;
  final WalletOffer walletOffer;
  const WalletOfferSwipe({
    Key key,
    this.heroTag = '',
    @required this.walletOffer,
  }) : super(key: key);

  void _navigateToOfferDetail(context) {
    WalletOffer selectedOffer = this.walletOffer;
    Navigator.push(
      context,
      HeroDialogRoute(
        builder: (context) => CashWalletOfferDetailScreen(
          walletOffer: selectedOffer,
        ),
      ),
    ).whenComplete(() async {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _navigateToOfferDetail(context);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Center(
                          child: Stack(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 140,
                                backgroundColor: Colors.grey.withOpacity(.3),
                              ),
                              Positioned.fill(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  child: Hero(
                                    tag: heroTag,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: HexColor(this.walletOffer.card_color),
                                      ),
                                      child: Image.asset('assets/images/texture/${this.walletOffer.offer_card_image}'),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: AutoSizeText(
                          this.walletOffer.title,
                          presetFontSizes: [22, 21, 20, 19],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xff353535),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        child: AutoSizeText(
                          this.walletOffer.description,
                          presetFontSizes: [14, 13, 12, 11, 10],
                          textAlign: TextAlign.center,
                          maxLines: 5,
                          style: TextStyle(
                            color: Color(0xff353535),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: AutoSizeText(
                          this.walletOffer.offer_from,
                          textAlign: TextAlign.center,
                          presetFontSizes: [16, 15, 14, 13],
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff353535),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                        child: AutoSizeText(
                          'Offer Expires ${dateFormat(dateTime: this.walletOffer.valid_to)}',
                          textAlign: TextAlign.center,
                          presetFontSizes: [14, 13, 12],
                          style: TextStyle(
                            color: Color(0xff353535),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
