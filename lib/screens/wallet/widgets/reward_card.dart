import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RewardCard extends StatelessWidget {
  final String heroTag;
  final Function onTap;
  final bool isScratched;
  final Color unScratchedColor;
  final String texturePng;
  final String brandLogoNetworkLink;
  final String scratchedValue;
  final String offerText;
  final String offerProvider;

  const RewardCard({
    Key key,
    this.heroTag,
    this.onTap,
    this.isScratched = false,
    this.unScratchedColor = Colors.blue,
    this.texturePng = 'texture_01.png',
    this.brandLogoNetworkLink = '',
    this.scratchedValue = '',
    this.offerText = '',
    this.offerProvider = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Container(
          child: Hero(
            tag: heroTag,
            child: Material(
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Container(),
                    isScratched
                        ? Positioned(
                            width: MediaQuery.of(context).size.width * 0.40,
                            bottom: 30,
                            child: Container(
                              margin: EdgeInsets.all(5.0),
                              padding: EdgeInsets.all(20.0),
                              // child: image,
                              child: CachedNetworkImage(
                                imageUrl: this.brandLogoNetworkLink,
                                placeholder: (context, url) => Container(
                                  child: SvgPicture.asset('assets/images/icons/icons8-image-file.svg'),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                              ),
                            ),
                          )
                        : Container(
                            color: unScratchedColor,
                            child: Image.asset('assets/images/texture/$texturePng'),
                          ),
                    isScratched
                        ? Positioned(
                            bottom: 10.0,
                            width: MediaQuery.of(context).size.width * 0.40,
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      AutoSizeText(
                                        scratchedValue,
                                        presetFontSizes: [22, 21, 20, 19, 18, 17],
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 1,
                                  ),
                                  AutoSizeText(
                                    offerText,
                                    presetFontSizes: [10, 12, 14, 16, 18],
                                    maxLines: 1,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 5),
                                    child: AutoSizeText(
                                      offerProvider,
                                      presetFontSizes: [10, 12, 14, 16, 18],
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: onTap,
                        child: Container(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
