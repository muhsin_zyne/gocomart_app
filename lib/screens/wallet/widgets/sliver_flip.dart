import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SliverFlip extends SingleChildRenderObjectWidget {
  SliverFlip({Widget child, Key key}) : super(child: child, key: key);
  @override
  RenderObject createRenderObject(BuildContext context) {
    // TODO: implement createRenderObject
    return RenderSilverFlip();
    //throw UnimplementedError();
  }
}

class SliverFlipWithFixed extends SingleChildRenderObjectWidget {
  SliverFlipWithFixed({Widget child, Key key}) : super(child: child, key: key);
  @override
  RenderObject createRenderObject(BuildContext context) {
    // TODO: implement createRenderObject
    return RenderSilverFlipFixed();
    //throw UnimplementedError();
  }
}

class RenderSilverFlip extends RenderSliverSingleBoxAdapter {
  RenderSilverFlip({
    RenderBox child,
  }) : super(child: child);
  @override
  void performLayout() {
    if (child == null) {
      geometry = SliverGeometry.zero;
      return;
    }
    final SliverConstraints constraints = this.constraints;
    child.layout(constraints.asBoxConstraints(), parentUsesSize: true);
    double childExtent;
    switch (constraints.axis) {
      case Axis.horizontal:
        childExtent = child.size.width;
        break;
      case Axis.vertical:
        childExtent = child.size.height;
        break;
    }
    assert(childExtent != null);
    final double paintedChildSize = calculatePaintOffset(constraints, from: 0.0, to: childExtent);
    final double cacheExtent = calculateCacheOffset(constraints, from: 0.0, to: childExtent);

    assert(paintedChildSize.isFinite);
    assert(paintedChildSize >= 0.0);
    geometry = SliverGeometry(
      scrollExtent: childExtent,
      paintExtent: paintedChildSize,
      cacheExtent: cacheExtent,
      maxPaintExtent: childExtent,
      hitTestExtent: paintedChildSize,
      hasVisualOverflow: childExtent > constraints.remainingPaintExtent || constraints.scrollOffset > 0.0,
    );
    setChildParentData(child, constraints, geometry);
  }
}

class RenderSilverFlipFixed extends RenderSliverSingleBoxAdapter {
  RenderSilverFlipFixed({
    RenderBox child,
  }) : super(child: child);
  @override
  void performLayout() {
    if (child == null) {
      geometry = SliverGeometry.zero;
      return;
    }
    final SliverConstraints constraints = this.constraints;
    child.layout(constraints.asBoxConstraints(), parentUsesSize: true);
    double childExtent;
    switch (constraints.axis) {
      case Axis.horizontal:
        childExtent = child.size.width;
        break;
      case Axis.vertical:
        childExtent = child.size.height;
        break;
    }
    assert(childExtent != null);
    //double paintedChildSize = calculatePaintOffset(constraints, from: 0.0, to: childExtent);
    calculatePaintOffset(constraints, from: 0.0, to: childExtent);
    calculateCacheOffset(constraints, from: 0.0, to: childExtent);
    //double cacheExtent = calculateCacheOffset(constraints, from: 0.0, to: childExtent);

    //assert(paintedChildSize.isFinite);
    //assert(paintedChildSize >= 0.0);
    geometry = SliverGeometry(
      scrollExtent: 20,
      paintExtent: 20,
      paintOrigin: constraints.scrollOffset,
      maxPaintExtent: 20,
    );
    setChildParentData(child, constraints, geometry);
  }
}
