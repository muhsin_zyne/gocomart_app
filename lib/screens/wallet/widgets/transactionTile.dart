import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/util/enum.dart';

class TransactionTile extends StatelessWidget {
  final TransactionType transactionType;
  final String transDescription;
  final String transDate;
  final String transAmount;

  TransactionTile({
    @required this.transactionType,
    this.transDescription = '',
    this.transDate = '',
    this.transAmount = '',
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RotationTransition(
                  turns: AlwaysStoppedAnimation(315 / 360),
                  child: Icon(
                    transactionType == TransactionType.cr ? Icons.arrow_back : Icons.arrow_forward,
                    color: transactionType == TransactionType.cr ? Colors.green : Colors.red,
                    size: 30,
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: AutoSizeText(
                    transDescription,
                    maxLines: 2,
                    textAlign: TextAlign.left,
                    presetFontSizes: [16, 15, 14, 13, 12],
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                    ),
                  ),
                ),
                Container(
                  child: AutoSizeText(
                    transDate,
                    maxLines: 1,
                    textAlign: TextAlign.left,
                    presetFontSizes: [13, 12, 11, 10, 9],
                    style: TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  transAmount,
                  textAlign: TextAlign.right,
                  maxLines: 1,
                  presetFontSizes: [18, 17, 16, 15],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: transactionType == TransactionType.cr ? Colors.black54 : Colors.red,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
