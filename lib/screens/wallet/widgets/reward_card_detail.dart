import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/controllers/curency_format.dart';

// ignore: must_be_immutable
class RewardCardDetail extends StatelessWidget {
  final String heroTag;
  final Function onTap;
  final bool isScratched;
  final Color unScratchedColor;
  final String texturePng;
  final String brandLogoNetworkLink;
  final String scratchedValue;
  final String offerText;
  final String offerProvider;
  final bool isDetails;
  final bool locationDisabled;
  final String cardDisabledText;

  RewardCardDetail(
      {Key key,
      this.heroTag,
      this.onTap,
      this.isScratched = false,
      this.unScratchedColor = Colors.blue,
      this.texturePng = 'texture_01.png',
      this.brandLogoNetworkLink = '',
      this.scratchedValue = '',
      this.offerText = '',
      this.offerProvider = '',
      this.isDetails = true,
      this.locationDisabled = false,
      this.cardDisabledText = ''})
      : super(key: key);

  final CurrencyFormat inr = CurrencyConst.inrFormat1;

  @override
  Widget build(BuildContext context) {
    //print(this.isScratched);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      margin: EdgeInsets.all(0),
      padding: EdgeInsets.all(0),
      height: MediaQuery.of(context).size.width * .76,
      width: MediaQuery.of(context).size.width * .76,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 35,
            width: MediaQuery.of(context).size.width * .76,
            child: Container(
              margin: EdgeInsets.all(30),
              padding: EdgeInsets.all(30),
              child: CachedNetworkImage(
                imageUrl: this.brandLogoNetworkLink,
                placeholder: (context, url) => Container(
                  child: SvgPicture.asset('assets/images/icons/icons8-image-file.svg'),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            width: MediaQuery.of(context).size.width * .76,
            child: Container(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      AutoSizeText(
                        'You hav \'e won ${inr.formatCurrency(int.parse(scratchedValue).toDouble())}',
                        presetFontSizes: [26, 28, 30],
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  AutoSizeText(
                    offerText,
                    presetFontSizes: [12, 14, 16, 18],
                    maxLines: 1,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: AutoSizeText(
                      offerProvider,
                      presetFontSizes: [12, 14, 16, 18],
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                ],
              ),
            ),
          ),
          (isScratched == false && locationDisabled == true)
              ? Positioned.fill(
                  child: Container(
                    color: unScratchedColor,
                    child: Image.asset('assets/images/texture/$texturePng'),
                  ),
                )
              : Container(),
          (isScratched == false && locationDisabled == true)
              ? Positioned(
                  bottom: 20,
                  child: Container(
                    width: MediaQuery.of(context).size.width * .76,
                    height: 40,
                    color: Colors.black.withOpacity(0.4),
                    child: AutoSizeText(
                      cardDisabledText,
                      textAlign: TextAlign.center,
                      presetFontSizes: [16, 15, 14, 13, 12],
                      style: TextStyle(color: Colors.white.withOpacity(0.7)),
                    ),
                  ),
                )
              : Container(),
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: onTap,
              child: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
