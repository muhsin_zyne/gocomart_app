import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:provider/provider.dart';

class TestPage extends StatelessWidget {
  static const String id = 'test_page_01';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test Page'),
      ),
      body: TestPageContent(),
    );
  }
}

class TestPageContent extends StatefulWidget {
  @override
  _TestPageContentState createState() => _TestPageContentState();
}

class _TestPageContentState extends State<TestPageContent> {
  GlobalProvider _globalProvider;

  @override
  void initState() {
    super.initState();
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    _globalProvider = Provider.of<GlobalProvider>(context);
    await _globalProvider.initGpsRequest(context);
    if ((_globalProvider.gpsServiceStatus != ServiceStatus.enabled) && (_globalProvider.gpsPermissionStatus != PermissionStatus.granted)) {
    } else {}
  }

  getPosition() async {}

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
            onPressed: getPosition,
            child: Text("test"),
          ),
        ],
      ),
    );
  }
}
