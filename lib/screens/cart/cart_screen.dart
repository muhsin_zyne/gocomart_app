import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/cart/cartResponse.dart';
import 'package:gocomartapp/models/cart/item.dart';
import 'package:gocomartapp/models/cart/productOptionValue.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/cart/summary_screen.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:graphql/client.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  ShoppingServices _shoppingServices;
  CartProvider _cartProvider;
  bool _globalLoaded = false;
  CartResponse cartResponse;
  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _cartProvider = Provider.of<CartProvider>(context);
    init();
  }

  void init() async {
    setState(() {
      _globalLoaded = true;
    });
    this.getCartItems();
  }

  getCartItems() async {
    QueryResult result = await _shoppingServices.getCartItems();
    _cartProvider.cartResponse = CartResponse.fromJson(result.data['getCartItems']);
    _cartProvider.firstLoad = true;
  }

  _cartProductByNow() {
    _cartProvider.buyNowPreviewShouldLoad = true;
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SummaryScreen(),
      ),
    ).whenComplete(() {
      _cartProvider.buyNowPreviewShouldLoad = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (_globalLoaded == true) {
      //getCartItems();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      bottomSheet: (_cartProvider?.cartResponse?.items?.length ?? 0) > 0
          ? Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.black12,
                  ),
                ),
              ),
              height: width / 6,
              child: Row(
                //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _cartProvider != null
                      ? Expanded(
                          flex: 6,
                          child: Container(
                              padding: EdgeInsets.only(left: 20, top: 5),
                              height: width / 6,
                              width: width / 2,
                              //color: Colors.yellow[900],
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  _cartProvider != null
                                      ? Container(
                                          decoration: BoxDecoration(),
                                          height: width / 18,
                                          margin: EdgeInsets.only(right: 8),
                                          child: AutoSizeText(
                                            "${_cartProvider.cartLength}  " + ((_cartProvider.cartLength > 1) ? "Items" : "Item"),
                                            presetFontSizes: [18, 16, 14, 10, 8],
                                            style: TextStyle(color: Colors.black54),
                                          ),
                                        )
                                      : Container(),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      AutoSizeText(
                                        "₹${_cartProvider.cartTotal} ",
                                        style: TextStyle(color: Colors.green, fontSize: width / 20, fontWeight: FontWeight.w600),
                                        maxLines: 2,
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                        )
                      : Container(),
                  Expanded(
                    flex: 5,
                    child: InkWell(
                      onTap: (_cartProvider?.firstLoad == true) && (_cartProvider.cartResponse.items.length > 0)
                          ? () {
                              this._cartProductByNow();
                            }
                          : null,
                      child: Center(
                        child: Container(
                          margin: const EdgeInsets.only(right: 8),
                          alignment: Alignment.center,
                          height: width / 10,
                          width: width / 2.5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Theme.of(context).primaryColor,
                          ),
                          child: AutoSizeText(
                            "CHECK OUT",
                            style: TextStyle(letterSpacing: .2, color: Colors.white, fontSize: width / 25, fontWeight: FontWeight.w600),
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          : SizedBox(),
      body: RefreshIndicator(
        onRefresh: () async {
          await this.getCartItems();
        },
        child: CartScreenContent(),
      ),
    );
  }
}

// ignore: must_be_immutable
class CartScreenContent extends StatefulWidget {
  @override
  _CartScreenContentState createState() => _CartScreenContentState();
}

class _CartScreenContentState extends State<CartScreenContent> {
  OCImageCache ocImageCache = OCImageCache();
  ShoppingServices _shoppingServices;
  GlobalProvider _globalProvider;
  CartProvider _cartProvider;
  bool _globalLoaded = false;
  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    loadClient();
  }

  void loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    init();
  }

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(context);
    _cartProvider = Provider.of<CartProvider>(context);
    super.didChangeDependencies();
  }

  void init() async {
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      _globalLoaded = true;
    });
  }

  double discountPercentage(double orgPrice, double offerPrice) {
    double percentage = ((orgPrice - offerPrice) / orgPrice) * 100;
    return percentage;
  }

  void removeItem(Item cartItem) async {
    setState(() {
      cartItem.isLoading = true;
      this._cartProvider.notify();
    });

    var cartOptionInput = [];
    QueryResult result = await _shoppingServices.removeFromCart(
      productId: cartItem.product_id,
      cartId: cartItem.cart_id,
      cartOptionInput: cartOptionInput,
    );

    setState(() {
      _cartProvider.cartResponse = CartResponse.fromJson(result.data['removeFromCart']);
    });
  }

  void incrementCartItem(Item cartItem, key) async {
    setState(() {
      _cartProvider.cartResponse.items[key].isLoading = true;
      cartItem.quantity++;
      this._cartProvider.notify();
    });
    var cartOptionInput = [];
    if (cartItem.productOptionsValues != null) {
      for (int i = 0; i < cartItem.productOptionsValues.length; i++) {
        cartOptionInput.add({
          "product_option_id": cartItem.productOptionsValues[i].productOption.product_option_id,
          "product_option_value_id": cartItem.productOptionsValues[i].productOptionValue.product_option_value_id
        });
      }
    }

    QueryResult result = await _shoppingServices.addToCart(
      productId: cartItem.product_id,
      cartOptionInput: cartOptionInput,
    );

    setState(() {
      _cartProvider.cartResponse = CartResponse.fromJson(result.data['addToCart']);
    });

    this.showInAppNotification(
      message: _cartProvider.cartResponse.message,
      color: _cartProvider.cartResponse.error ? Colors.red : Colors.green,
    );
  }

  void decrementCartItem(Item cartItem, key) async {
    setState(() {
      _cartProvider.cartResponse.items[key].isLoading = true;
      _cartProvider.cartResponse.items[key].quantity--;
      this._cartProvider.notify();
    });

    var cartOptionInput = [];
    if (cartItem.productOptionsValues != null) {
      for (int i = 0; i < cartItem.productOptionsValues.length; i++) {
        cartOptionInput.add({
          "product_option_id": cartItem.productOptionsValues[i].productOption.product_option_id,
          "product_option_value_id": cartItem.productOptionsValues[i].productOptionValue.product_option_value_id
        });
      }
    }
    QueryResult result = await _shoppingServices.removeFromCart(
      productId: cartItem.product_id,
      cartOptionInput: cartOptionInput,
    );
    setState(() {
      _cartProvider.cartResponse = CartResponse.fromJson(result.data['removeFromCart']);
    });

    this.showInAppNotification(
      message: _cartProvider.cartResponse.message,
      color: _cartProvider.cartResponse.error ? Colors.red : Colors.green,
    );
  }

  void showInAppNotification({message = '', color = Colors.green}) {
    showSimpleNotification(
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                AutoSizeText(
                  message,
                  presetFontSizes: [15, 14, 13, 12, 11, 10, 9, 8],
                ),
              ],
            )
          ],
        ),
      ),
      background: color,
    );
  }

  void removeOnTap(Item cartItem, key) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Remove?"),
          content: Text("Do you want to remove this item?"),
          actions: <Widget>[
            FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                setState(() {
                  _cartProvider.cartResponse.items[key].isLoading = true;
                  _cartProvider.notify();
                });

                this.removeItem(cartItem);
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.only(bottom: 60),
      child: _globalLoaded
          ? Container(
              child: (_cartProvider.cartResponse?.items?.length ?? 0) > 0
                  ? ListView.builder(
                      itemCount: _cartProvider.cartResponse?.items?.length ?? 0,
                      itemBuilder: (context, int i) {
                        // Builder start here
                        //
                        Item cartItem = _cartProvider.cartResponse.items[i];
                        //double discountPercentage;
                        List<Widget> cartOptionList = [];
                        if (cartItem.productOptionsValues != null) {
                          for (int optionLength = 0; optionLength < cartItem.productOptionsValues?.length; optionLength++) {
                            ProductOptionValue currentOption = cartItem.productOptionsValues[optionLength];
                            var optionValue = currentOption.productOptionValue.description.name;
                            // cartOptionList
                            //     .add(Text('$optionTitle : $optionValue'));
                            cartOptionList.add(
                              AutoSizeText(
                                "$optionValue ${(optionLength == cartItem.productOptionsValues.length - 1 ? "" : ", ")}",
                                overflow: TextOverflow.ellipsis,
                                minFontSize: 8,
                                maxFontSize: 18,
                                style: TextStyle(fontSize: width / 32, color: Colors.black54),
                              ),
                            );
                          }
                        }
                        var imageCache = ocImageCache.getCachedImage(
                          path: cartItem.cartImage,
                          imageQuality: ImageQuality.high,
                        );
                        return Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          child: Container(
                            color: Colors.transparent,
                            // margin: const EdgeInsets.only(
                            //   top: 8,
                            // ),
                            // height: width / 2,
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10, top: 10),
                                  margin: const EdgeInsets.only(top: 12),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                        color: Colors.black12,
                                      ),
                                      top: BorderSide(
                                        color: Colors.black12,
                                      ),
                                    ),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () {
                                          productDetailNavigator(cartItem.product_id);
                                        },
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    // padding: EdgeInsets.all(10),
                                                    margin: const EdgeInsets.only(left: 5),
                                                    height: MediaQuery.of(context).size.width / 6,
                                                    child: Image.network(
                                                      "${_globalProvider.cdnImagePoint + "cache/" + imageCache}",
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 8.0),
                                                    child: Wrap(
                                                      children: cartOptionList,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              flex: 12,
                                              child: Container(
                                                //color: Colors.red,
                                                decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black12))),
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.only(left: 10),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    AutoSizeText(
                                                      cartItem.product.description.name,
                                                      overflow: TextOverflow.ellipsis,
                                                      maxFontSize: 20,
                                                      minFontSize: 9,
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 30, color: Colors.black54),
                                                    ),
                                                    AutoSizeText(
                                                      cartItem.product.manufacturer.name,
                                                      overflow: TextOverflow.ellipsis,
                                                      minFontSize: 8,
                                                      maxFontSize: 18,
                                                      style: TextStyle(fontSize: width / 32, color: Colors.black54),
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        AutoSizeText(
                                                          "₹ ${cartItem.itemTotal}",
                                                          //"₹ ${(specialPrice * cartItem.quantity)}",
                                                          overflow: TextOverflow.ellipsis,
                                                          textAlign: TextAlign.left,
                                                          minFontSize: 9,
                                                          maxFontSize: 20,
                                                          style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 28, color: Colors.green),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(left: 10),
                                                          height: MediaQuery.of(context).size.width / 23,
                                                          child: cartItem.discountPercentage > 0
                                                              ? AutoSizeText(
                                                                  "₹ ${(cartItem.basePrice * cartItem.quantity).round()}",
                                                                  //"₹ ${(specialPrice * cartItem.quantity)}",
                                                                  overflow: TextOverflow.ellipsis,
                                                                  textAlign: TextAlign.left,
                                                                  minFontSize: 9,
                                                                  maxFontSize: 20,
                                                                  style: TextStyle(
                                                                      decoration: TextDecoration.lineThrough,
                                                                      decorationColor: Colors.red,
                                                                      fontWeight: FontWeight.w600,
                                                                      fontSize: width / 28,
                                                                      color: Colors.grey),
                                                                )
                                                              : SizedBox(),
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                      height: MediaQuery.of(context).size.width / 23,
                                                      child: cartItem.discountPercentage > 0
                                                          ? AutoSizeText(
                                                              '${cartItem.discountPercentage.round()}% Special Offer Today',
                                                              style: TextStyle(
                                                                color: Colors.yellow[800],
                                                                fontWeight: FontWeight.w600,
                                                              ),
                                                              presetFontSizes: [18, 17, 14, 13, 12, 10, 8],
                                                            )
                                                          : Container(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
//                                          Expanded(
//                                            flex: 3,
//                                            child: Container(
//                                              margin: const EdgeInsets.only(left: 5),
//                                              height: MediaQuery.of(context).size.width / 5,
//                                              //color: Colors.black12,
//                                              child: Row(
//                                                children: <Widget>[
//                                                  Column(
//                                                    mainAxisAlignment: MainAxisAlignment.center,
//                                                    crossAxisAlignment: CrossAxisAlignment.start,
//                                                    children: <Widget>[
//                                                      Container(
//                                                        alignment: Alignment.center,
//                                                        width: width / 18,
//                                                        height: width / 20,
//                                                        decoration: BoxDecoration(
//                                                          color: Colors.black38,
//                                                          borderRadius: BorderRadius.only(
//                                                            topLeft: Radius.circular(10),
//                                                            topRight: Radius.circular(10),
//                                                          ),
//                                                        ),
//                                                        child: InkWell(
//                                                          onTap: () {
//                                                            this.incrementCartItem(cartItem, i);
//                                                          },
//                                                          child: Icon(
//                                                            Icons.keyboard_arrow_up,
//                                                            color: Colors.white,
//                                                            size: MediaQuery.of(context).size.width / 20,
//                                                          ),
//                                                        ),
//                                                      ),
//
//                                                      //qty
//                                                      Container(
//                                                        alignment: Alignment.center,
//                                                        width: width / 18,
//                                                        height: width / 20,
//                                                        decoration:
//                                                            BoxDecoration(color: Colors.white, border: Border(left: BorderSide(color: Colors.black38), right: BorderSide(color: Colors.black38))),
//                                                        child: Text(
//                                                          "${cartItem.quantity}",
//                                                          style: TextStyle(fontSize: MediaQuery.of(context).size.width / 30, color: Colors.black38),
//                                                          textAlign: TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      Container(
//                                                          width: width / 18,
//                                                          height: width / 20,
//                                                          decoration: BoxDecoration(
//                                                            // border: Border.all(
//                                                            //     color:Colors.black38,
//                                                            //     width: 1),
//                                                            color: Colors.black38,
//                                                            borderRadius: BorderRadius.only(
//                                                              bottomLeft: Radius.circular(10),
//                                                              bottomRight: Radius.circular(10),
//                                                            ),
//                                                          ),
//                                                          child: InkWell(
//                                                            onTap: () {
//                                                              if (cartItem.quantity > 1) {
//                                                                this.decrementCartItem(cartItem, i);
//                                                              } else {
//                                                                this.removeOnTap(cartItem, i);
//                                                              }
//                                                            },
//                                                            child: Icon(
//                                                              Icons.keyboard_arrow_down,
//                                                              color: Colors.white,
//                                                              size: width / 20,
//                                                            ),
//                                                          ))
//                                                    ],
//                                                  ),
////                                                  Container(
////                                                    margin:
////                                                        const EdgeInsets.only(
////                                                            left: 5),
////                                                    child: InkWell(
////                                                      onTap: () {
////                                                        removeOnTap(cartItem);
////                                                      },
////                                                      child: Icon(
////                                                        Icons.delete,
////                                                        size: width / 18,
////                                                        color: Colors.grey,
////                                                      ),
////                                                    ),
////                                                  ),
//                                                ],
//                                              ),
//                                            ),
//                                          )
                                          ],
                                        ),
                                      ),
                                      Divider(),
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 2,
                                              child: Container(
                                                height: 40,
                                                child: Center(
                                                  child: AutoSizeText(
                                                    "Quantity",
                                                    overflow: TextOverflow.ellipsis,
                                                    maxFontSize: 26,
                                                    minFontSize: 9,
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 25, color: Colors.black54),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 2,
                                              child: Container(),
                                            ),
                                            Expanded(
                                              flex: 3,
                                              child: Container(
                                                //color: Colors.blue,
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: InkWell(
                                                        onTap: () {
                                                          if (cartItem.quantity > 1) {
                                                            this.decrementCartItem(cartItem, i);
                                                          } else {
                                                            this.removeOnTap(cartItem, i);
                                                          }
                                                        },
                                                        child: Icon(
                                                          FontAwesomeIcons.minus,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: AutoSizeText(
                                                        "${cartItem.quantity}",
                                                        presetFontSizes: [25, 24, 23, 22, 21, 20],
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(color: Theme.of(context).primaryColor),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: InkWell(
                                                        onTap: () {
                                                          this.incrementCartItem(cartItem, i);
                                                        },
                                                        child: Icon(
                                                          FontAwesomeIcons.plus,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                cartItem.isLoading
                                    ? Positioned.fill(
                                        child: Container(
                                          color: Colors.grey.withOpacity(0.1),
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 0),
                                            child: Center(
                                              child: SpinKitWave(
                                                color: GocoMartAppTheme.buildLightTheme().primaryColor,
                                                type: SpinKitWaveType.start,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                          ),
                          secondaryActions: <Widget>[
                            IconSlideAction(
                              caption: 'Delete',
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () {
                                removeOnTap(cartItem, i);
                              },
                            ),
                          ],
                        );
                      },
                    )
                  : Container(
                      child: Center(
                        child: _cartProvider.firstLoad == true
                            ? Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 100,
                                  ),
                                  Container(
                                    child: SvgPicture.asset(
                                      'assets/images/icons/shopping-cart.svg',
                                      width: 200,
                                    ),
                                  ),
                                  Container(
                                    child: AutoSizeText(
                                      "Your cart is empty",
                                      presetFontSizes: [26, 25, 24, 23, 22, 21, 20, 19],
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  RaisedButton(
                                    child: Text("SHOP NOW"),
                                    onPressed: () {
                                      Navigator.of(context)
                                          .pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (Route<dynamic> route) => false);
                                      // Navigator.push(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) => HomeScreen()),
                                      // );
                                    },
                                    color: GocoMartAppTheme.buildLightTheme().primaryColor,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 10,
                                    ),
                                    splashColor: Colors.grey,
                                  ),
                                ],
                              )
                            : CircularProgressIndicator(),
                      ),
                    ),
            )
          : Container(),
    );
  }

  void productDetailNavigator(id) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductDetailScreen(
          productId: id,
        ),
      ),
    );
  }
}
