import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/controllers/ui_notifications.dart';
import 'package:gocomartapp/models/address/address.dart';
import 'package:gocomartapp/models/cart/productBuyNowPreview.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/cart/widgets/delivery_address.dart';
import 'package:gocomartapp/screens/cart/widgets/order_review_block.dart';
import 'package:gocomartapp/screens/checkout/checkout_screen.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class SummaryScreen extends StatefulWidget {
  @override
  _SummaryScreenState createState() => _SummaryScreenState();
}

class _SummaryScreenState extends State<SummaryScreen> {
  //CartProvider _cartProvider;
  AddressProvider _addressProvider;
  @override
  void initState() {
    super.initState();
    this._loadClient();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    //_cartProvider = Provider.of<CartProvider>(context);
    _addressProvider = Provider.of<AddressProvider>(context);
  }

  _proceedToPayment() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CheckOutScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Summary'),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(86.0),
          child: Container(
            color: Colors.white,
            height: 86.0,
            child: DeliveryAddressBlock(),
          ),
        ),
      ),
      body: SummaryScreenContent(),
      bottomSheet: Row(
        //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _addressProvider != null
              ? Expanded(
                  child: InkWell(
                    onTap: _addressProvider.continuePaymentPage
                        ? () {
                            this._proceedToPayment();
                          }
                        : null,
                    child: Container(
                      height: MediaQuery.of(context).size.width / 7,
                      width: MediaQuery.of(context).size.width,
                      color: _addressProvider.continuePaymentPage ? Theme.of(context).primaryColor : Colors.grey,
                      child: Center(
                        child: AutoSizeText(
                          "PROCEED TO PAYMENT",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ),
                )
              : Container(
                  height: MediaQuery.of(context).size.width / 7,
                  width: MediaQuery.of(context).size.width,
                ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class SummaryScreenContent extends StatefulWidget {
  List products = [];
  @override
  _SummaryScreenContentState createState() => _SummaryScreenContentState();
}

class _SummaryScreenContentState extends State<SummaryScreenContent> {
  OCImageCache ocImageCache = OCImageCache();
  ShoppingServices _shoppingServices;
  //GetAddress _address;
  AddressProvider _addressProvider;
  GlobalProvider _globalProvider;
  CartProvider _cartProvider;
  bool firstLoading = true;
  bool addressLoading = true;
  bool _isReady = false;

  @override
  void initState() {
    super.initState();
    this._loadClient();
    this.widget.products = [];
  }

  _loadClient() async {
    _shoppingServices = ShoppingServices(context: context);
    await Future.delayed(Duration(microseconds: 10));
    _globalProvider = Provider.of<GlobalProvider>(context);
    _addressProvider = Provider.of<AddressProvider>(context);
    _cartProvider = Provider.of<CartProvider>(context);
    //this._addressLoading();
    await Future.delayed(Duration(microseconds: 10));
    if (_cartProvider.buyNowPreviewShouldLoad == false) {
      setState(() {
        _cartProvider.buyNowPreviewShouldLoad = false;
        this._isReady = true;
      });
    } else {
      // api call
      QueryResult queryResult = await _shoppingServices.cartProductByNow();
      if (!queryResult.hasException) {
        _cartProvider.productBuyNowPreview = ProductBuyNowPreview.fromJson(queryResult.data['cartProductByNow']);

        if (queryResult.data['defaultDeliveryAddress']['address'] == null) {
          _addressProvider.currentAddress = Address.fromJson({});
        } else {
          _addressProvider.currentAddress = Address.fromJson(queryResult.data['defaultDeliveryAddress']['address']);
        }

        _cartProvider.buyNowPreviewShouldLoad = false;
        if (_cartProvider.productBuyNowPreview.error == true) {
          this._handleBuyNowError(_cartProvider.productBuyNowPreview.message);
        } else {
          if (_cartProvider.productBuyNowPreview.message != '') {
            _showRequestMessage(_cartProvider.productBuyNowPreview.message);
          }
        }
        setState(() {
          _cartProvider.buyNowPreviewShouldLoad = false;
          this._isReady = true;
        });
      }
    }
  }

  _handleBuyNowError(String message) {
    Navigator.pop(context);
    UINotification.showInAppNotification(color: Colors.red, message: message);
  }

  _showRequestMessage(String message) {
    UINotification.showInAppNotification(color: Colors.orange, message: message, duration: Duration(seconds: 3));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return _isReady == true
        ? _cartProvider?.productBuyNowPreview?.items != null
            ? Container(
                color: Color(0xfffdfcccc).withOpacity(.1),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ReviewCartItemBlock(
                        cartProvider: _cartProvider,
                        globalProvider: _globalProvider,
                        width: MediaQuery.of(context).size.width,
                      ),
                      Container(
                        //color: Color(0xffF1F4FD),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 10, left: 10),
                              child: Text(
                                "Price details ",
                                style: TextStyle(
                                  fontSize: width / 20,
                                  letterSpacing: .03,
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            //address padding
                            Container(
                              padding: EdgeInsets.only(left: 10, bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 8),
                                        height: width / 17,
                                        width: width / 1.8,
                                        child: AutoSizeText(
                                          "Price",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(right: 8, top: 8),
                                        height: width / 17,
                                        child: AutoSizeText(
                                          "₹${_cartProvider?.productBuyNowPreview?.totalAmount ?? 0}",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 8),
                                        height: width / 17,
                                        width: width / 1.8,
                                        child: AutoSizeText(
                                          "Delivery Fee",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(right: 8, top: 8),
                                        height: width / 17,
                                        child: AutoSizeText(
                                          "₹${_cartProvider.productBuyNowPreview.deliveryCharges}",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 8),
                                        height: width / 17,
                                        width: width / 1.8,
                                        child: AutoSizeText(
                                          "Tax and Charges",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(right: 8, top: 8),
                                        height: width / 17,
                                        child: AutoSizeText(
                                          "₹${_cartProvider.productBuyNowPreview.additionalTax}",
                                          presetFontSizes: [16, 15, 14, 13, 12, 11],
                                          style: TextStyle(color: Colors.grey[600]),
                                        ),
                                      ),
                                    ],
                                  ),
                                  _cartProvider.productBuyNowPreview.freeDeliveryAdjustment != 0
                                      ? Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(top: 8),
                                              height: width / 17,
                                              width: width / 1.8,
                                              child: AutoSizeText(
                                                "Delivery Free Discound",
                                                presetFontSizes: [16, 15, 14, 13, 12, 11],
                                                style: TextStyle(color: Colors.grey[600]),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(right: 8, top: 8),
                                              height: width / 17,
                                              child: AutoSizeText(
                                                "₹${_cartProvider.productBuyNowPreview.freeDeliveryAdjustment}",
                                                presetFontSizes: [18, 16, 14],
                                                style: TextStyle(color: Colors.grey[600]),
                                              ),
                                            ),
                                          ],
                                        )
                                      : Row(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 8),
                                        height: width / 17,
                                        width: width / 1.8,
                                        child: AutoSizeText(
                                          "Total Payable Amount",
                                          presetFontSizes: [18, 16, 14],
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF4c4d52),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(right: 8, top: 8),
                                        height: width / 17,
                                        child: AutoSizeText(
                                          "₹${_cartProvider.productBuyNowPreview.totalPayableAmount}",
                                          presetFontSizes: [18, 16, 14],
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF4c4d52),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 80,
                      )
                    ],
                  ),
                ),
              )
            : Container()
        : Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
