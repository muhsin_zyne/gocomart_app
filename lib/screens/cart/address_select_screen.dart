import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/models/address/address.dart';
import 'package:gocomartapp/models/address/get_address.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/screens/profile/address/add_address.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

// class _AddressSelectScreenState extends State<AddressSelectScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(

//       body: AddressSelectContent(),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                 builder: (context) => AddAddress(),
//               )).whenComplete(() {
//            // initAddress();
//           });
//         },
//         child: Icon(Icons.add),
//       ),
//     );
//   }
// }

class AddressSelectScreen extends StatefulWidget {
  @override
  _AddressSelectScreenState createState() => _AddressSelectScreenState();
}

class _AddressSelectScreenState extends State<AddressSelectScreen> {
  bool isReady = false;
  AccountServices _accountServices;
  //GlobalProvider _globalProvider;
  AddressProvider _addressProvider;
  GetAddress _getAddress;
  @override
  void initState() {
    super.initState();
    _accountServices = AccountServices(context: context);
    this._loadClient();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    // _globalProvider = Provider.of<GlobalProvider>(context);
    _addressProvider = Provider.of<AddressProvider>(context);
    this._loadAddressList();
    this._cachedAddressList();
  }

  void _cachedAddressList() {
    if (_addressProvider.getAddress != null) {
      if (_addressProvider.getAddress.address.length > 0) {
        setState(() {
          _getAddress = _addressProvider.getAddress;
          this.isReady = true;
        });
      }
    }
  }

  void _loadAddressList() async {
    QueryResult addressList = await _accountServices.getAddress();
    if (!addressList.hasException) {
      _getAddress = GetAddress.fromJson(addressList.data['getAddress']);
      _addressProvider.getAddress = _getAddress;
    }
    setState(() {
      this.isReady = true;
    });
  }

  _updateDeliveryAddress(Address currentAddress) {
    setState(() {
      _addressProvider.currentAddress = currentAddress;
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Delivery Address'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddAddress(),
              )).whenComplete(() {
            Navigator.pop(context);
            _loadClient();
          });
        },
        child: Icon(Icons.add),
      ),
      body: isReady
          ? Container(
              child: ListView.builder(
                itemCount: _getAddress?.address?.length,
                itemBuilder: (context, int i) {
                  Address currentAddress = _getAddress.address[i];
                  return Card(
                    child: InkWell(
                      onTap: () {
                        this._updateDeliveryAddress(currentAddress);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Card(
                                  child: Container(
                                    child: Icon(
                                      Icons.location_on,
                                      size: 34,
                                      color: Colors.white,
                                    ),
                                    padding: EdgeInsets.all(3),
                                  ),
                                  color: Theme.of(context).primaryColor,
                                ),
                                AutoSizeText(
                                  currentAddress.type.toUpperCase(),
                                  presetFontSizes: [14, 13, 12, 11, 10],
                                  style: TextStyle(
                                    color: Colors.black.withOpacity(.5),
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.7,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                    currentAddress.name,
                                    presetFontSizes: [
                                      16,
                                      15,
                                      14,
                                      13,
                                      12,
                                      11,
                                      10,
                                      9
                                    ],
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.5),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  currentAddress.company != ''
                                      ? AutoSizeText(
                                          currentAddress.company,
                                          presetFontSizes: [
                                            16,
                                            15,
                                            14,
                                            13,
                                            12,
                                            11,
                                            10,
                                            9
                                          ],
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Color(0xff73767a),
                                          ),
                                        )
                                      : Container(),
                                  AutoSizeText(
                                    "${currentAddress.address_1}${currentAddress.address_2 != '' ? ',' : ''} ${currentAddress.address_2} ${currentAddress.landmark != '' ? ',' : ''} ${currentAddress.landmark}",
                                    presetFontSizes: [
                                      16,
                                      15,
                                      14,
                                      13,
                                      12,
                                      11,
                                      10,
                                      9
                                    ],
                                    maxLines: 4,
                                    style: TextStyle(
                                      color: Color(0xff73767a),
                                    ),
                                  ),
                                  AutoSizeText(
                                    "${currentAddress.city}${currentAddress.city != '' ? ',' : ''} ${currentAddress.postalZone.adminName1} ${currentAddress.postalZone.adminName1 != '' ? ',' : ''} ${currentAddress.postalZone.country.countryName}",
                                    presetFontSizes: [
                                      16,
                                      15,
                                      14,
                                      13,
                                      12,
                                      11,
                                      10,
                                      9
                                    ],
                                    maxLines: 3,
                                    style: TextStyle(
                                      color: Color(0xff73767a),
                                    ),
                                  ),
                                  AutoSizeText(
                                    '${currentAddress.mobileNumber} ${currentAddress.mobileNumber != '' ? ',' : ''} ${currentAddress.altContact}',
                                    presetFontSizes: [
                                      16,
                                      15,
                                      14,
                                      13,
                                      12,
                                      11,
                                      10,
                                      9
                                    ],
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Color(0xff73767a),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
