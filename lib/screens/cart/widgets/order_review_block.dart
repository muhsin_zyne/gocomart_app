import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/cart/productOptionValue.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';

// ignore: must_be_immutable
class ReviewCartItemBlock extends StatefulWidget {
  ReviewCartItemBlock({
    @required this.cartProvider,
    @required this.globalProvider,
    @required this.width,
  });
  CartProvider cartProvider;
  double width;
  GlobalProvider globalProvider;
  @override
  _ReviewCartItemBlockState createState() => _ReviewCartItemBlockState();
}

class _ReviewCartItemBlockState extends State<ReviewCartItemBlock> {
  OCImageCache ocImageCache = OCImageCache();

  @override
  Widget build(BuildContext context) {
    if (this.widget?.cartProvider != null) {
      if (this.widget.cartProvider.productBuyNowPreview.items.length > 0) {
        List<Widget> productList = [];
        this.widget.cartProvider.productBuyNowPreview.items.forEach((cartProductItem) {
          var imageCache = ocImageCache.getCachedImage(
            path: cartProductItem.cartImage,
            imageQuality: ImageQuality.high,
          );
          List<Widget> cartOptionList = [];
          if (cartProductItem.productOptionsValues != null) {
            for (int optionLength = 0; optionLength < cartProductItem.productOptionsValues?.length; optionLength++) {
              ProductOptionValue currentOption = cartProductItem.productOptionsValues[optionLength];
              var optionValue = currentOption.productOptionValue.description.name;
              cartOptionList.add(
                AutoSizeText(
                  "$optionValue ${(optionLength == cartProductItem.productOptionsValues.length - 1 ? "" : ", ")}",
                  overflow: TextOverflow.ellipsis,
                  minFontSize: 8,
                  maxFontSize: 18,
                  style: TextStyle(fontSize: widget.width / 32, color: Colors.black54),
                ),
              );
            }
          }
          productList.add(Card(
            elevation: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AutoSizeText(
                                '${cartProductItem.product.manufacturer.name}',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Color(0xff969798),
                                ),
                                presetFontSizes: [
                                  17,
                                  14,
                                  12,
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              AutoSizeText(
                                '${cartProductItem.product.description.name} ',
                                style: TextStyle(
                                  color: Color(0xff73767a),
                                ),
                                overflow: TextOverflow.ellipsis,
                                presetFontSizes: [19, 18, 17, 16],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: <Widget>[
                                  AutoSizeText(
                                    "₹${cartProductItem.priceInfo.itemTotal}",
                                    overflow: TextOverflow.ellipsis,
                                    presetFontSizes: [20, 19, 18, 17, 16, 15, 14, 13, 12],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.green,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  cartProductItem.priceInfo.itemTotal < cartProductItem.basePrice
                                      ? AutoSizeText(
                                          "₹${cartProductItem.basePrice}",
                                          overflow: TextOverflow.ellipsis,
                                          presetFontSizes: [17, 16, 15, 14, 13, 12],
                                          style: TextStyle(
                                            decoration: TextDecoration.lineThrough,
                                            color: Colors.black54,
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                              cartProductItem.offerAmount > 0
                                  ? Container(
                                      height: widget.width / 18,
                                      width: widget.width / 1.6,
                                      child: AutoSizeText(
                                        'You Saved ₹${cartProductItem.offerAmount} on this order',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.yellow[800],
                                        ),
                                        presetFontSizes: [
                                          17,
                                          14,
                                          12,
                                        ],
                                      ),
                                    )
                                  : Container(),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: <Widget>[
                                  AutoSizeText(
                                    'QTY ${cartProductItem.quantity}',
                                    presetFontSizes: [14, 13, 12, 11, 10],
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Wrap(
                                    children: cartOptionList,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                        flex: 3,
                      ),
                      Expanded(
                        child: Container(
                          child: Container(
                            //  color:Colors.red,
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(5),
                            child: Image.network(
                              "${widget.globalProvider.cdnImagePoint + "cache/" + imageCache}",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ));
        });

        return Container(
          margin: EdgeInsets.all(0),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      'REVIEW YOUR ORDER',
                      presetFontSizes: [16, 15, 14],
                      style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
              Column(children: productList),
            ],
          ),
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }
}
