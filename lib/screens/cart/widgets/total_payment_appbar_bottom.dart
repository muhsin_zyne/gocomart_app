import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class TotalPaymentOptionAppBarBottom extends StatefulWidget {
  @override
  _TotalPaymentOptionAppBarBottomState createState() =>
      _TotalPaymentOptionAppBarBottomState();
}

class _TotalPaymentOptionAppBarBottomState
    extends State<TotalPaymentOptionAppBarBottom> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * .5,
            child: AutoSizeText(
              'Total Payable Amount',
              presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
              style: TextStyle(
                color: Color(0xff73767a),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * .3,
            child: AutoSizeText(
              '₹1974',
              presetFontSizes: [22, 21, 20, 19],
              style: TextStyle(
                color: Color(0xff73767a),
              ),
              textAlign: TextAlign.right,
            ),
          )
        ],
      ),
    );
  }
}
