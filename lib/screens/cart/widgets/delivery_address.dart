import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/screens/cart/address_select_screen.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:provider/provider.dart';

class DeliveryAddressBlock extends StatefulWidget {
  @override
  _DeliveryAddressBlockState createState() => _DeliveryAddressBlockState();
}

class _DeliveryAddressBlockState extends State<DeliveryAddressBlock> {
  AddressProvider _addressProvider;

  _changeAddress() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddressSelectScreen(),
      ),
    ).whenComplete(() {
      initState();
    });
  }

  @override
  void initState() {
    super.initState();
    this._loadClient();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    _addressProvider = Provider.of<AddressProvider>(context);
  }

  @override
  Widget build(BuildContext context) {
    if (_addressProvider?.currentAddress?.address_id == null &&
        _addressProvider?.currentAddressLoaded == true) {
      return Container(
        //color: Colors.grey.withOpacity(0.2),
        color: Color(0xffFEFCF7),
        //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(0),
              elevation: 4,
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            AutoSizeText(
                              'Deliver at',
                              presetFontSizes: [15, 14, 13, 12, 11],
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * .7,
                              child: AutoSizeText(
                                "Your address book is empty!",
                                presetFontSizes: [
                                  18,
                                  17,
                                  16,
                                  15,
                                  14,
                                  13,
                                  12,
                                  11
                                ],
                                style: TextStyle(
                                  color: Colors.red,
                                ),
                              ),
                            ),
                            FlatButton(
                              onPressed: _changeAddress,
                              child: Container(
                                child: AutoSizeText(
                                  'ADD',
                                  presetFontSizes: [16, 15, 14, 13, 12, 11, 10],
                                  style: TextStyle(
                                    color: GocoMartAppTheme.buildLightTheme()
                                        .primaryColor,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }
    return _addressProvider?.currentAddress != null
        ? Container(
            //color: Colors.grey.withOpacity(0.2),
            color: Color(0xffFEFCF7),
            //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(0),
                  elevation: 4,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                AutoSizeText(
                                  'Deliver at',
                                  presetFontSizes: [15, 14, 13, 12, 11],
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Card(
                                      child: Container(
                                        child: Icon(
                                          Icons.location_on,
                                          size: 34,
                                          color: Colors.white,
                                        ),
                                        padding: EdgeInsets.all(3),
                                      ),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        AutoSizeText(
                                          '${_addressProvider.currentAddress.type.toUpperCase()}',
                                          presetFontSizes: [
                                            14,
                                            13,
                                            12,
                                            11,
                                            10,
                                            9
                                          ],
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Color(0xff73767a),
                                          ),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .50,
                                          child: AutoSizeText(
                                            '${_addressProvider.currentAddress.address_1}, ${_addressProvider.currentAddress.address_2}',
                                            presetFontSizes: [
                                              15,
                                              14,
                                              13,
                                              12,
                                              11,
                                              10,
                                              9,
                                              8
                                            ],
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Color(0xff73767a),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                FlatButton(
                                  onPressed: _changeAddress,
                                  child: Container(
                                    child: AutoSizeText(
                                      'CHANGE',
                                      presetFontSizes: [
                                        16,
                                        15,
                                        14,
                                        13,
                                        12,
                                        11,
                                        10
                                      ],
                                      style: TextStyle(
                                        color:
                                            GocoMartAppTheme.buildLightTheme()
                                                .primaryColor,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();
  }
}
