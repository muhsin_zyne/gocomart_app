import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:share/share.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/screens/profile/invited_users.dart';

class InviteScreen extends StatelessWidget {
  static const String id = 'invite_screen';

  static const String viewUsers = "View Users";

  static const List<String> menu = <String>[viewUsers];

  @override
  Widget build(BuildContext context) {
    void navigate(i) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => InvitedUsers(),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          PopupMenuButton(
            // padding: EdgeInsets.only(top:10),
            onSelected: navigate,
            color: Colors.white,
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  height: 30,
                  value: 1,
                  child: Text(
                    "View Users",
                    style: TextStyle(color: Colors.black),
                  ),
                )
              ];
            },

            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: InviteScreenContent(),
    );
  }
}

class InviteScreenContent extends StatefulWidget {
  @override
  _InviteScreenContentState createState() => _InviteScreenContentState();
}

class _InviteScreenContentState extends State<InviteScreenContent> {
  OCImageCache ocImageCache = OCImageCache();

  runTest() {
//    var data = ocImageCache.getCachedImage(
//        path: '/catalog/Anas%20Images/56y65.jpeg',
//        imageQuality: ImageQuality.high);
//    print(data);
  }

  @override
  Widget build(BuildContext context) {
    runTest();
    //var imagePrefix = 'http://webadmin.gocomart.com/image';
    //var imageSuffix = '/catalog/Anas%20Images/56y65.jpeg';
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .4 + 60,
                //color: Colors.red.withOpacity(0.4),
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            color: GocoMartAppTheme.buildLightTheme().primaryColor,
                            child: Image.asset(
                              'assets/images/world-vector.png',
                              color: Colors.white.withOpacity(0.1),
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * .4,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.5,
                                  child: AutoSizeText(
                                    'Invite your fried and Earn Money',
                                    textAlign: TextAlign.center,
                                    presetFontSizes: [25, 24, 23, 22, 21, 20, 18, 17, 16, 15],
                                    maxLines: 2,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.5,
                                  child: Image.asset(
                                    'assets/images/invite-earn-feture.png',
                                    scale: 2,
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 10,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Card(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Container(
                                  child: Image.asset('assets/images/icons/wallet-02.png'),
                                ),
                                title: Text('Invite by your refral link via SMS / Email / Whatsapp'),
                                subtitle: Text('You can Earn upto 130  on each firends purchase'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Divider(
                      color: GocoMartAppTheme.buildLightTheme().primaryColor,
                      height: 110,
                      endIndent: 25,
                      thickness: 2,
                      indent: 25,
                    ),
                    Row(
                      children: <Widget>[
                        InviteSteps(
                          flex: 1,
                          description: 'Invite your friend to Gocomart',
                          image: SvgPicture.asset(
                            'assets/images/icons/icons8-user-account.svg',
                            width: 60,
                          ),
                        ),
                        InviteSteps(
                          flex: 2,
                          highlighter: true,
                          description: 'Your friend placing first order',
                          image: SvgPicture.asset(
                            'assets/images/icons/icons8-shopping-bag.svg',
                            width: 60,
                          ),
                        ),
                        InviteSteps(
                          flex: 1,
                          description: 'You get an assured reward',
                          image: SvgPicture.asset(
                            'assets/images/icons/icons8-cash-in-hand.svg',
                            width: 60,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),

          Container(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText(
                      'YOUR REFERAL CODE',
                      textAlign: TextAlign.center,
                      presetFontSizes: [20, 19, 18, 17],
                      style: TextStyle(
                        color: Color(0xff606060),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DottedBorder(
                      borderType: BorderType.RRect,
                      radius: Radius.circular(12),
                      padding: EdgeInsets.all(6),
                      dashPattern: [6, 3, 2, 3],
                      color: Colors.red,
                      strokeWidth: 2,
                      child: Container(
                        width: MediaQuery.of(context).size.width * .5,
                        height: 40,
                        child: Center(
                          child: AutoSizeText(
                            'GO128384848',
                            textAlign: TextAlign.center,
                            presetFontSizes: [20, 19, 18, 17, 16],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * .3,
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: AutoSizeText(
                        'TAP TO COPY',
                        textAlign: TextAlign.center,
                        presetFontSizes: [14, 13, 12, 11, 10],
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: FlatButton(
                        onPressed: () {
                          share(context);
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: GocoMartAppTheme.buildLightTheme().primaryColor,
                          ),
                        ),
                        color: GocoMartAppTheme.buildLightTheme().primaryColor,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                          child: AutoSizeText(
                            'Invite Now',
                            presetFontSizes: [20, 19, 18],
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          // InkWell(
          //   onTap: (){
          //     Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //               builder: (context) => InvitedUsers(),
          //               ),
          //             );
          //   },
          //   child: Card(
          //     margin: EdgeInsets.only(top:15,left: 5,right: 5),
          //     elevation: 2,
          //     color: Colors.blue[50],
          //     child: Container(
          //       padding: const EdgeInsets.symmetric(vertical: 15),
          //       child: Row(
          //         //crossAxisAlignment: CrossAxisAlignment.stretch,
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: <Widget>[
          //           Container(
          //             height: MediaQuery.of(context).size.width / 13,
          //             //height: 20,
          //             //color: Colors.blue,
          //             padding: EdgeInsets.only(left: 10, top: 5),
          //             child: AutoSizeText(
          //               "Invited Users Status",
          //               style: TextStyle(
          //                   color: Colors.grey[700], fontWeight: FontWeight.w500),
          //               presetFontSizes: [22, 20, 16, 14, 12, 10],
          //             ),
          //           ),
          //           Row(
          //             mainAxisAlignment: MainAxisAlignment.end,
          //             children: <Widget>[
          //               Container(
          //                 height: MediaQuery.of(context).size.width / 13,
          //                 //height: 20,
          //                 //color: Colors.blue,
          //                 padding: EdgeInsets.only( top: 5,right: 5),
          //                 child: AutoSizeText(
          //                   "view",
          //                   style: TextStyle(
          //                       color: Colors.grey[700], fontWeight: FontWeight.w400),
          //                   presetFontSizes: [22, 20, 16, 14, 12, 10],
          //                 ),
          //               ),
          //               Icon(Icons.arrow_forward_ios, color: Colors.grey[700], size: MediaQuery.of(context).size.width/20,)
          //             ],
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          // ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }

  void share(BuildContext context) {
    final RenderBox box = context.findRenderObject();

    Share.share(
      "Hi, Checkout Indias first E-commerce and it's valet money!. You can avail UPTO 1000 daily by scratching and inviting. \n \n Donwload App now @ https://m.gocomart.com?GO12757f55",
      subject: 'share daata subject',
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
}

class InviteSteps extends StatelessWidget {
  final int flex;
  final String description;
  final Widget image;
  final bool highlighter;
  const InviteSteps({
    Key key,
    this.flex = 1,
    this.description = '',
    this.image,
    this.highlighter = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                border: Border.all(color: Color(0xff3b5999), width: 2.5),
                shape: BoxShape.circle,
              ),
              child: CircleAvatar(
                radius: highlighter == true ? 52 : 40,
                backgroundColor: Colors.white,
                child: image,
              ),
            ),
            Container(
              color: Colors.transparent,
              height: 100,
              width: 70,
              child: AutoSizeText(
                description,
                presetFontSizes: [16, 15, 14, 13],
                maxLines: 3,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black87),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
