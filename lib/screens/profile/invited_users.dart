import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';




class InvitedUsers extends StatefulWidget {
  InvitedUsers({Key key}) : super(key: key);

  @override
  _InvitedUsersState createState() => _InvitedUsersState();
}

class _InvitedUsersState extends State<InvitedUsers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Invited Users'), backgroundColor: Color(0xff3b5999),) ,
      floatingActionButton: SpeedDial(
        overlayColor: Colors.black12,
        overlayOpacity: .6,
        backgroundColor: Color(0xff3b5999),
        //closeManually: true,
        animatedIcon: AnimatedIcons.menu_close,
        children: [
          SpeedDialChild(
            child: Icon(Icons.file_download),
            label: "App Installed",
            backgroundColor: Color(0xffc0a90b),
            labelBackgroundColor: Color(0xffc0a90b),
            labelStyle: TextStyle(
              color: Colors.white
            ),
            onTap: ()=> print("App Installed Pressed"),
          ),
          SpeedDialChild(
            child: Icon(Icons.shopping_basket),
            label: "Order Placed",
            backgroundColor: Color(0xffff9307),
            labelBackgroundColor: Color(0xffff9307),
            labelStyle: TextStyle(
              color: Colors.white
            ),
            onTap: ()=> print("Order Placed Pressed"),
          ),
          SpeedDialChild(
            child: Icon(Icons.card_giftcard),
            label: "Gift Rewarded",
            backgroundColor: Color(0xff37af0d),
            labelBackgroundColor: Color(0xff37af0d),
            labelStyle: TextStyle(
            color: Colors.white
            ),
            onTap: ()=> print("Gift Rewarded Pressed"),
          ),SpeedDialChild(
            child: Icon(Icons.supervised_user_circle,color: Colors.white,),
            label: "All Users",
            backgroundColor: Colors.blue,
            labelBackgroundColor: Colors.blue,
            labelStyle: TextStyle(
              color: Colors.white
            ),
            onTap: ()=> print("App Installed Pressed"),
          )
        ],
      ),

      body: ListView.builder(
      padding: EdgeInsets.only(top: 5,bottom: 5),
      itemCount: 10,
      itemBuilder: (context,int i){
        return   Card(

          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.all(15),
                  child: Container(
                      decoration: new BoxDecoration(
                        border: Border.all(color:Color(0xff3b5999),width: 2.5),
                        shape: BoxShape.circle,
                      ),
                      child: Image.network("https://cdn1.iconfinder.com/data/icons/user-pictures/100/boy-512.png")
                  ),

                  height: 100,
                  //color: Colors.red,
                ),
              ),

              Expanded(
                flex: 4,
                child: Container(
                  //padding: EdgeInsets.only(left: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text('Registered Mobile :',style: TextStyle(
                              fontSize: 15,
                              color: Color(0xff73767a)
                          ),),
                          Text('86xxxxx278',style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff4e4f50)
                          ),),
                        ],
                      ),
                      Text('Installed on 12 December 2019', style: TextStyle(
                          color: Color(0xff969798),
                          fontSize: 13
                      ),),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.circular(8),
                          //color: Color(0xffc0a90b), // Installed Color
                          //color: Color(0xffff9307), //Order Placed Color
                          color: Color(0xff37af0d), // Rewarded Color
                        ),

                        child: Container(
                          child: Text('App Installed',style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                          ),),
                        ),
                      )
                    ],
                  ),
                  height: 100,

                  // color: Colors.blue,
                ),
              )
            ],
          ),

        );
      },
    ),


      
    );
}
}