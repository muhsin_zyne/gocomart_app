import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker_fork/flutter_cupertino_date_picker_fork.dart';
import 'package:gocomartapp/models/profile/account_details.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/providers/user_profile_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditProfile extends StatefulWidget {
  EditProfile({Key key, @required accDetails}) : super(key: key) {
    accountDetails = accDetails;
  }

  AccountDetails accountDetails;

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  String authToken;
  String dob;
  final picker = ImagePicker();
  var response;
  GlobalProvider _globalProvider;

  TextEditingController _addressController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _dobController = TextEditingController();
  File _selectedImage;
  UserProfileProvider _userProfileProvider;
  AccountServices _accountServices;
  bool saving = false;

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(
      context,
    );
    super.didChangeDependencies();
  }

  @override
  void initState() {
    _accountServices = AccountServices(context: context);
    initProfile();
    // setState(() {
    //   _nameController.text = widget.accountDetails.firstname +
    //       " " +
    //       widget.accountDetails.lastname;
    //   _mobileController.text = widget.accountDetails.telephone;
    //   _emailController.text = widget.accountDetails.email;
    //   _addressController.text = "Amaldev assress";
    //   dob = widget.accountDetails.dob != null
    //       ? widget.accountDetails.dob
    //       : "Select Address";
    // });

    initializeProfileData();

    super.initState();
  }

  editProfile() {
    _userProfileProvider.userDetails.accountDetails.name = _nameController.text;
    _userProfileProvider.userDetails.mobile_no =
        int.parse(_mobileController.text);
    _userProfileProvider.userDetails.accountDetails.email =
        _emailController.text;
  }

  initializeProfileData() {
    print(widget.accountDetails.dob);
    setState(() {
      _nameController.text = widget.accountDetails.name != null
          ? widget.accountDetails.name
          : "Gocomart User";
      _mobileController.text = widget.accountDetails.telephone;
      _emailController.text = widget.accountDetails.email;
      _addressController.text = "Amaldev address";
      dob = widget.accountDetails.dob != null
          ? widget.accountDetails.dob
          : "Select date of birth";
    });
  }

  initProfile() async {
    await Future.delayed(Duration(microseconds: 100));
    _userProfileProvider = Provider.of<UserProfileProvider>(
      context,
    );
    authToken = _globalProvider.authToken;
    print(authToken);
    await Future.delayed(Duration(microseconds: 100));
  }

  updateProfile() async {
    QueryResult updatedResponse = await _accountServices.updateProfile(
        email: _emailController.text,
        name: _nameController.text,
        dob: dob.compareTo("Select date of birth") == 0 ? null : dob);
    print(updatedResponse.exception);
    if (updatedResponse.hasException)
      return false;
    else
      return true;
  }

  getImage(ImageSource src) async {
    //getting image
    final image = await picker.getImage(source: src);
    //cropping image
    if (image != null) {
      Navigator.pop(context);
      final cropped = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality: 50,
        maxWidth: 700,
        maxHeight: 700,
        compressFormat: ImageCompressFormat.jpg,
      );
      print("------------------------------------------");
      //setting up multi part
      Uri uri = Uri.parse("https://api.gocomart.com/file/image_upload");
      var fileStream = new http.ByteStream(Stream.castFrom(cropped.openRead()));
      var length = await cropped.length();
      var request = new http.MultipartRequest("POST", uri);
      var multipartFile = new http.MultipartFile(
          'profile_image', fileStream, length,
          filename: "profile_image");
      request.files.add(multipartFile);
      request.headers.addAll({"Authorization": "Bearer: $authToken"});
      var r = await request.send();
      print("2------------------------------------------");

      //getting response
      var rp = await http.Response.fromStream(r);

      print(rp.statusCode);
      print("3s------------------------------------------");
      response = jsonDecode(rp.body);
      // request.fields.add("gender","male");
      // print(response['data']['profile_image']);
      this.setState(() {
        _selectedImage = cropped;
      });
      if (rp.statusCode != 200) {
        this.setState(() {
          _selectedImage = null;
        });

        return showDialog(
            context: context,
            barrierDismissible: true,
            builder: (context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                title: Text(
                  'Error',
                  style: TextStyle(color: Color(0xff3a3a3a)),
                ),
                content: Text("Upload Failed"),
                actions: <Widget>[
                  MaterialButton(
                    elevation: 5.0,
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            });
      } else {
        _userProfileProvider.userDetails.accountDetails.profileImage =
            response['data']['profile_image'];
      }
    }
  }

  ///functions

  Container imageChanger(double width, BuildContext context) {
    return Container(
      height: width / 2,
      //  width: width,
      // color: Colors.red,
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: width / 2.5,
              height: width / 2.5,

              //orginal image
              child: Container(
                // padding: EdgeInsets.all(20),
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: _selectedImage == null
                        ? widget.accountDetails.profileImage == null
                            ? AssetImage('assets/images/avatar.png')
                            : NetworkImage(_globalProvider.cdnPoint +
                                widget.accountDetails.profileImage)
                        : FileImage(_selectedImage),
                  ),
                ),
              ),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[400],
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                image: new DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(
                    "assets/images/home/imageLoaderIcon.png",
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: width / 3,
            right: width / 3.2,
            child: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).primaryColor,
              ),
              child: InkWell(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        child: Container(
                          height: MediaQuery.of(context).size.height / 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //camera
                              GetImageOption(
                                icon: Icons.camera_alt,
                                description: "Camera",
                                width: width,
                                backgroundColor: Theme.of(context).primaryColor,
                                onPressed: () {
                                  print("camera");
                                  getImage(ImageSource.camera);
                                },
                              ),
                              //gallery
                              GetImageOption(
                                icon: Icons.filter,
                                description: "Gallery",
                                width: width,
                                backgroundColor: Colors.blueGrey,
                                onPressed: () {
                                  print("gallery");
                                  getImage(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
                child: Icon(
                  Icons.camera_alt,
                  size: width / 15,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Column details(double width) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            showBottomSheet(
              width: width,
              controller: _nameController,
              hint: "Name",
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AutoSizeText(
                "Name",
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(fontSize: width / 24, color: Colors.grey[600]),
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    width: width / 1.4,
                    child: AutoSizeText(
                      "${_nameController.text}",
                      minFontSize: 15,
                      maxFontSize: 18,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: width / 25),
                    ),
                  ),
                  Icon(
                    Icons.arrow_right,
                    color: Colors.grey[600],
                  )
                ],
              )
            ],
          ),
        ),
        Divider(
          height: 40,
        ),
        InkWell(
          onTap: () {
            showBottomSheet(
              width: width,
              controller: _mobileController,
              hint: "Mobile",
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AutoSizeText(
                "Mobile",
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(fontSize: width / 24, color: Colors.grey[600]),
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    width: width / 1.4,
                    child: AutoSizeText(
                      "${_mobileController.text}",
                      minFontSize: 15,
                      maxFontSize: 18,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: width / 25),
                    ),
                  ),
                  Icon(
                    Icons.arrow_right,
                    color: Colors.grey[600],
                  )
                ],
              )
            ],
          ),
        ),
        Divider(
          height: 40,
        ),
        InkWell(
          onTap: () {
            showBottomSheet(
              width: width,
              controller: _emailController,
              hint: "Email",
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AutoSizeText(
                "Email",
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(fontSize: width / 24, color: Colors.grey[600]),
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    width: width / 1.4,
                    child: AutoSizeText(
                      "${_emailController.text}",
                      minFontSize: 15,
                      maxFontSize: 18,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: width / 25),
                    ),
                  ),
                  Icon(
                    Icons.arrow_right,
                    color: Colors.grey[600],
                  )
                ],
              )
            ],
          ),
        ),
        Divider(
          height: 40,
        ),
        InkWell(
          onTap: () {
            // showBottomSheet(
            //   width: width,
            //   controller: _dobController,
            //   hint: "Name",
            // );
            DatePicker.showDatePicker(
              context,
              initialDateTime: dob.compareTo("Select date of birth") == 0
                  ? DateTime.now()
                  : DateTime.parse(dob),
              minDateTime: DateTime.now().subtract(
                Duration(days: 100 * 365),
              ),
              maxDateTime: DateTime.now(),
              pickerMode: DateTimePickerMode.date,
              onConfirm: (dateTime, i) async {
                setState(() {
                  dob = DateFormat('yyyy-MM-dd').format(dateTime);
                });
                bool res = await updateProfile();
                print(res);
                editProfile();
              },
              dateFormat: "yyyy-MM-dd",
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AutoSizeText(
                "Date of Birth",
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(fontSize: width / 24, color: Colors.grey[600]),
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    width: width / 1.8,
                    child: AutoSizeText(
                      "$dob",
                      minFontSize: 15,
                      maxFontSize: 18,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: width / 25),
                    ),
                  ),
                  Icon(
                    Icons.arrow_right,
                    color: Colors.grey[600],
                  )
                ],
              )
            ],
          ),
        ),
        Divider(
          height: 40,
        ),
        InkWell(
          onTap: () {
            showBottomSheet(
              width: width,
              controller: _addressController,
              hint: "Address",
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AutoSizeText(
                "Address",
                textAlign: TextAlign.right,
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(fontSize: width / 24, color: Colors.grey[600]),
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    width: width / 1.4,
                    child: AutoSizeText(
                      "${_addressController.text}",
                      minFontSize: 15,
                      maxFontSize: 18,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: width / 25),
                    ),
                  ),
                  Icon(
                    Icons.arrow_right,
                    color: Colors.grey[600],
                  )
                ],
              )
            ],
          ),
        ),
        Divider(
          height: 40,
        )
      ],
    );
  }

  showBottomSheet(
      {@required width,
      @required TextEditingController controller,
      @required hint}) {
    final _formKey = GlobalKey<FormState>();
    String temp;
    return showModalBottomSheet<void>(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            padding: MediaQuery.of(context).viewInsets,
            width: MediaQuery.of(context).size.width / 2,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 1.6,
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        initialValue: controller.text,
                        keyboardType: TextInputType.text,
                        // autovalidate: true,
                        autofocus: true,
                        onChanged: (value) {
                          setState(() {
                            temp = value;
                          });
                        },
                        // controller: price,
                        decoration: InputDecoration(
                          labelText: "$hint",
                        ),
                        validator: (String value) {
                          if (value.length < 1) {
                            return "Required field";
                          }
                        },
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    child: InkWell(
                      onTap: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            controller.text = temp;
                          });
                          bool res = await updateProfile();
                          print(res);
                          if (res)
                            editProfile();
                          else {
                            initializeProfileData();
                          }

                          Navigator.pop(context);
                        }
                      },
                      child: Text(
                        "Save",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.grey[700], size: 10),
      ),
      body: ListView(
        children: <Widget>[
          imageChanger(width, context),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20),
            child: details(width),
          )
        ],
      ),
    );
  }
}

class GetImageOption extends StatelessWidget {
  const GetImageOption(
      {Key key,
      @required this.description,
      @required this.width,
      this.backgroundColor,
      this.onPressed,
      @required this.icon});

  final Color backgroundColor;
  final String description;
  final IconData icon;
  final VoidCallback onPressed;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: backgroundColor, gradient: null),
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
        ),
        InkWell(
          onTap: onPressed,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: AutoSizeText(
              "$description",
              maxFontSize: 15,
              minFontSize: 8,
              style: TextStyle(
                  fontSize: width / 28,
                  fontWeight: FontWeight.w500,
                  color: Colors.grey[600]),
            ),
          ),
        ),
      ],
    );
  }
}

class RowItems extends StatelessWidget {
  const RowItems({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        AutoSizeText("data"),
        Container(
          child: AutoSizeText("rjefhrbd,snaskcedm fz,sdkl>SJm cxdzakls"),
        )
      ],
    );
  }
}
