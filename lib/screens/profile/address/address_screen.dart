import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/components/listTileSkeleton.dart';
import 'package:gocomartapp/models/address/get_address.dart';
import 'package:gocomartapp/screens/profile/address/add_address.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

import './editAddress.dart';

class AddressScreen extends StatefulWidget {
  static const String id = "address_screen";
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  ShoppingServices _shoppingServices;
  GetAddress _address;

  bool loaded;
  bool deleted;
  @override
  void initState() {
    print("called address");
    super.initState();
    loaded = false;
    deleted = false;
    initAddress();
  }

  deleteAddress(id) async {
    setState(() {
      loaded = false;
      deleted = false;
    });
    QueryResult result = await _shoppingServices.deleteAddress(address_id: id);
    if (!result.hasException) {
      deleted = true;
    }
    if (deleted == true) initAddress();
  }

  initAddress() async {
    await new Future.delayed(const Duration(microseconds: 10));
    print("called");
    setState(() {
      loaded = false;
    });
    _shoppingServices = ShoppingServices(context: context);
    await Future.delayed(Duration(microseconds: 10));
    QueryResult addressResult = await _shoppingServices.getAddress();
    print(addressResult.exception);
    if (!addressResult.hasException) {
      _address = GetAddress.fromJson(addressResult.data['getAddress']);
      setState(() {
        loaded = true;
        print("loaded");
        deleted = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddAddress(),
              )).whenComplete(() {
            initAddress();
          });
        },
        child: Icon(Icons.add),
      ),
      body: loaded
          ? _address.address.length > 0
              ? ListView.builder(
                  padding: EdgeInsets.only(
                    left: 7,
                    right: 7,
                    top: 7,
                  ),
                  itemCount: _address.address.length,
                  itemBuilder: (context, int i) {
                    return Container(
                      color: Colors.transparent,
                      // margin: EdgeInsets.only(bottom: 10),
                      child: Container(
                        //height: MediaQuery.of(context).size.width / 2,
                        // padding: EdgeInsets.only(bottom: 30),

                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  width: width / 13,
                                  child: Icon(
                                    Icons.library_books,
                                    color: Colors.lime[800],
                                    size: width / 15,
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  width: width / 1.7,
                                  //color: Colors.red,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('${_address.address[i].type}', style: TextStyle(fontSize: width / 20)),
                                      Text(
                                        '${_address.address[i].name}, ${_address.address[i].address_1}, ${_address.address[i].address_2}, ${_address.address[i].city},${_address.address[i].landmark},${_address.address[i].postcode},${_address.address[i].mobileNumber}, ${_address.address[i].altContact}',
                                        style: TextStyle(fontSize: width / 30),
                                      ),
                                    ],
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => EditAddress(
                                                  initAddress: _address.address[i],
                                                ))).whenComplete(() {
                                      initAddress();
                                    });
                                  },
                                  child: Container(
                                      // padding: EdgeInsets.all(10),
                                      height: width / 16,
                                      width: width / 17,
                                      color: Colors.black12,
                                      margin: EdgeInsets.only(left: width / 15, top: 10),
                                      child: Icon(
                                        Icons.edit,
                                        size: width / 21,
                                        color: Theme.of(context).primaryColor,
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    deleteAddress(_address.address[i].address_id);
                                  },
                                  child: Container(
                                      // padding: EdgeInsets.all(10),
                                      height: width / 16,
                                      width: width / 17,
                                      color: Colors.black12,
                                      margin: EdgeInsets.only(left: 15, top: 10),
                                      child: Icon(
                                        Icons.delete,
                                        size: width / 21,
                                        color: Colors.pink[600],
                                      )),
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                )
              : Container(
                  child: Center(
                  child: AutoSizeText(
                    "Address is empty",
                    style: TextStyle(fontSize: width / 20, color: Colors.grey),
                  ),
                ))
          : Container(
              margin: const EdgeInsets.only(left: 10, top: 10),
              child: ListView.builder(
                itemCount: 8,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: <Widget>[
                      ListTileSkeleton(
                        width: width,
                        boxImage: false,
                      ),
                      Divider()
                    ],
                  );
                },
              ),
            ),
    );
  }
}
