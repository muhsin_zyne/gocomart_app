import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gocomartapp/models/address/address.dart';
import 'package:gocomartapp/models/validate_pincode/validate_pin_code.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

Address _initialAddress;

class EditAddress extends StatelessWidget {
  EditAddress({@required initAddress}) {
    _initialAddress = initAddress;
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        extendBody: true,
        appBar: AppBar(
          title: Text("Edit Address"),
        ),
        body: EditAddressBody(),
      ),
    );
  }
}

class EditAddressBody extends StatefulWidget {
  EditAddressBody({Key key}) : super(key: key);

  @override
  _EditAddressBodyState createState() => _EditAddressBodyState();
}

class _EditAddressBodyState extends State<EditAddressBody> {
  final _formKey = GlobalKey<FormState>();
  ShoppingServices _shoppingServices;
  FocusNode pinFocusNode;
  ValidatePinCode pinCodeData;
  bool validPincode = true;
  bool changeSelectedAddress = false;
  bool pincodeAddressLoading = false;
  bool isChecked = true;
  bool saving = false;
  String selectedArea;

  //values
  TextEditingController _pinCodeController = TextEditingController();
  TextEditingController _address1 = TextEditingController();
  List<DropdownMenuItem> areas = [];
  String selectedAreaValue;
  TextEditingController _name = TextEditingController();
  TextEditingController _city = TextEditingController();
  TextEditingController _mobile = TextEditingController();
  TextEditingController _altContact = TextEditingController();
  TextEditingController _landmark = TextEditingController();
  TextEditingController _company = TextEditingController();
  TextEditingController _type = TextEditingController();
  var _selectedValue;

  @override
  void initState() {
    pinFocusNode = new FocusNode();
    _shoppingServices = ShoppingServices(context: context);
    // listen to focus changes
    listenFocusNodes();
    initAddress();
    super.initState();
  }

  //focus node of pin
  listenFocusNodes() {
    pinFocusNode.addListener(() async {
      print('focusNode updated: hasFocus: ${pinFocusNode.hasFocus}');
      if (!pinFocusNode.hasFocus) {
        callValidatePin();
      } else {
        setState(() {
          selectedAreaValue = null;
          areas.clear();
          _city.text = null;
        });
      }
    });
  }

  //initializing address
  initAddress() async {
    setState(() {
      _pinCodeController.text = (_initialAddress.postcode).toString();
      _address1.text = _initialAddress.address_1;
      selectedAreaValue = (_initialAddress.postalCodeId).toString();
      selectedArea = _initialAddress.address_2;
      _city.text = _initialAddress.city;
      _mobile.text = _initialAddress.mobileNumber;
      _name.text = _initialAddress.name;
      _altContact.text = _initialAddress.altContact;
      _landmark.text = _initialAddress.landmark;
      _company.text = _initialAddress.company;
      _type.text = _initialAddress.type;
      if (_initialAddress.type == "home") {
        _selectedValue = "home";
        isChecked = false;
      } else if (_initialAddress.type == "work") {
        _selectedValue = "work";
        isChecked = false;
      } else
        _selectedValue = "other";
    });
    callValidatePin();
  }

  //validating pin and loading
  callValidatePin() async {
    setState(() {
      pincodeAddressLoading = true;
    });
    areas.clear();
    await Future.delayed(Duration(microseconds: 10));
    QueryResult result = await _shoppingServices.validatePincode(pincode: _pinCodeController.text.length > 0 ? int.parse(_pinCodeController.text) : null);
    print(result.data);
    if (!result.hasException) {
      pinCodeData = ValidatePinCode.fromJson(result.data["validatePinCode"]);

      //validating pincode
      setState(() {
        if (pinCodeData.postals.length > 0) {
          List<DropdownMenuItem> temp = [];
          for (int i = 0; i < pinCodeData?.postals?.length ?? 0; i++) {
            temp.add(
              DropdownMenuItem(
                child: Text(pinCodeData.postals[i].placeName),
                value: "${pinCodeData.postals[i].id}",
              ),
            );
          }
          setState(() {
            areas = temp;
            validPincode = true;
            changeSelectedAddress == true ? selectedAreaValue = (pinCodeData?.postals[0]?.id).toString() : (_initialAddress.postalCodeId).toString();
            _city.text = pinCodeData?.postals[0]?.adminName3 ?? null;
          });
        } else {
          setState(() {
            validPincode = false;
          });
        }
        setState(() {
          pincodeAddressLoading = false;
        });
      });
    }
    setState(() {
      changeSelectedAddress = true;
    });
  }

  //edit address api call
  ediAddress() async {
    QueryResult result = await _shoppingServices.editAddress(
        addressId: _initialAddress.address_id,
        address_1: _address1.text,
        address_2: selectedArea,
        city: _city.text,
        postcode: int.parse(_pinCodeController.text),
        postalCodeId: selectedAreaValue,
        type: _type.text,
        name: _name.text,
        mobileNumber: _mobile.text,
        company: "",
        landmark: _landmark.text,
        altContact: _altContact.text);
    print(result.exception);
    if (!result.hasException) {
      setState(() {
        saving = false;
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              title: Text(
                'Saved',
                style: TextStyle(color: Color(0xff3a3a3a)),
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text("ok"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(left: 8, right: 8, top: 9),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //pinCode form
              Container(
                child: Stack(
                  alignment: Alignment.centerRight,
                  children: <Widget>[
                    Form(
                      autovalidate: true,
                      child: TextFormField(
                        focusNode: pinFocusNode,
                        controller: _pinCodeController,
                        enableSuggestions: true,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "PIN Code*",
                          contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          validation(value);
                          if (!validPincode) return "Enter valid pin";
                          return null;
                        },
                      ),
                    ),

                    //loader for loading post offices
                  ],
                ),
              ),

              //Home address
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: TextFormField(
                  controller: _address1,
                  decoration: InputDecoration(
                    labelText: "Home No., Building Number*",
                    contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) => validation(value),
                ),
              ),

              //area
              Stack(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    margin: const EdgeInsets.only(top: 10.0),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                    child: Stack(
                      children: <Widget>[
                        DropdownButtonHideUnderline(
                          child: DropdownButton(
                            isExpanded: true,
                            underline: null,
                            focusColor: null,
                            iconDisabledColor: pincodeAddressLoading == true ? Colors.white : Colors.grey,
                            items: areas ?? [],
                            hint: Text("Select Area"),
                            value: selectedAreaValue,
                            onChanged: (value) {
                              setState(() {
                                selectedAreaValue = value;
                                for (int i = 0; i < pinCodeData.postals.length; i++) {
                                  if (selectedAreaValue == pinCodeData.postals[i].id.toString()) selectedArea = pinCodeData.postals[i].placeName;
                                }
                              });
                            },
                          ),
                        ),
                        pincodeAddressLoading == true
                            ? Positioned(
                                right: 8,
                                top: 12,
                                //  margin: EdgeInsets.only(right: width/30),
                                child: SpinKitWave(
                                  size: 20,
                                  color: Colors.green,
                                  type: SpinKitWaveType.start,
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                  selectedAreaValue != null
                      ? Container(
                          margin: const EdgeInsets.only(top: 3, left: 8),
                          child: Text(
                            "  Select Area  ",
                            style: TextStyle(backgroundColor: Colors.white, color: Colors.grey, fontSize: 12),
                          ),
                        )
                      : SizedBox(),
                ],
              ),

              //city
              Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      enabled: false,
                      controller: _city,
                      // validator: (value) => validation(value),
                      decoration: InputDecoration(
                        labelText: "City*",
                        contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),

                  //loader for loading post offices
                  pincodeAddressLoading == true && changeSelectedAddress
                      ? Positioned(
                          right: 20,
                          top: 25,
                          //  margin: EdgeInsets.only(right: width/30),
                          child: SpinKitWave(
                            size: 20,
                            color: Colors.green,
                            type: SpinKitWaveType.start,
                          ),
                        )
                      : SizedBox(),
                ],
              ),

              //landmark
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: TextFormField(
                  controller: _landmark,
                  decoration: InputDecoration(
                    labelText: "Landmark(Optional)",
                    contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              //name
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: TextFormField(
                  controller: _name,
                  validator: (value) => validation(value),
                  decoration: InputDecoration(
                    labelText: "Name*",
                    contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              //mobile number
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: TextFormField(
                  controller: _mobile,
                  keyboardType: TextInputType.number,
                  validator: (value) => validation(value),
                  decoration: InputDecoration(
                    labelText: "Mobiile Number*",
                    contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              //alternate contact
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: TextFormField(
                  controller: _altContact,
                  decoration: InputDecoration(
                    labelText: "Alternate Contact Number(Optional)",
                    contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              //radio
              Container(
                margin: const EdgeInsets.only(top: 20, left: 5),
                height: width / 17,
                child: AutoSizeText(
                  "Type :",
                  presetFontSizes: [
                    22,
                    20,
                    18,
                  ],
                  style: optionTextStyle(),
                ),
              ),
              Row(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Radio(
                        value: "home",
                        onChanged: (val) {
                          isChecked = false;

                          _setSelectedValue(val);
                        },
                        groupValue: _selectedValue,
                      ),
                      Container(
                        height: width / 17,
                        child: AutoSizeText(
                          "Home",
                          presetFontSizes: [22, 20, 18, 16],
                          style: optionTextStyle(),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: "work",
                        onChanged: (val) {
                          isChecked = false;

                          _setSelectedValue(val);
                        },
                        groupValue: _selectedValue,
                      ),
                      Container(
                        height: width / 17,
                        child: AutoSizeText(
                          "Work",
                          presetFontSizes: [22, 20, 18, 16],
                          style: optionTextStyle(),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: "other",
                        onChanged: (val) {
                          isChecked = true;

                          _setSelectedValue(val);
                        },
                        groupValue: _selectedValue,
                      ),
                      Container(
                        height: width / 17,
                        child: AutoSizeText(
                          "Other",
                          presetFontSizes: [22, 20, 18, 16],
                          style: optionTextStyle(),
                        ),
                      )
                    ],
                  ),
                ],
              ),

              isChecked == true
                  ? TextFormField(
                      controller: _type,
                      validator: (value) => validation(value),
                      decoration: InputDecoration(
                        labelText: "Other*",
                        contentPadding: EdgeInsets.only(bottom: -1, top: -5, left: 10),
                        border: OutlineInputBorder(),
                      ),
                    )
                  : Container(),

              Container(
                alignment: Alignment.center,
                color: !saving ? Colors.blue[900] : null,
                margin: const EdgeInsets.only(left: 0, top: 10, right: 0),
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: InkWell(
                  onTap: () {
                    if (_formKey.currentState.validate()) {
                      print(_selectedValue);
                      FocusScope.of(context).unfocus();
                      setState(() {
                        saving = true;
                      });

                      ediAddress();
                    } else {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Enter valid Details"),
                            content: Text("Please check the details you have entered"),
                            actions: <Widget>[
                              FlatButton(
                                child: new Text("Ok"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        },
                      );
                    }
                  },
                  child: saving
                      ? SpinKitWave(
                          size: 40,
                          color: Theme.of(context).primaryColor,
                          type: SpinKitWaveType.start,
                        )
                      : AutoSizeText(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                ),
              ),
              SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }

  //validation
  String validation(value) {
    if (value == null)
      return "Fill this Field";
    else if (value.isEmpty) return "Fill this Field";
    return null;
  }

  //style
  TextStyle optionTextStyle() {
    return TextStyle(
      color: Colors.grey[600],
    );
  }

  //change radio button
  _setSelectedValue(val) {
    setState(() {
      _selectedValue = val;
      if (_selectedValue != "other") {
        _type.text = _selectedValue;
      } else {
        _type.text = "";
      }
    });
  }
}
