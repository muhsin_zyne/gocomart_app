import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/order_review/orders.dart';
import 'package:gocomartapp/models/view_review/view_review.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

Orders order;
var _rating = 0.0;
bool updated = false;
bool loaded = false;
final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
ViewReview review;
ViewReview _createReviewResult;
TextEditingController _description = TextEditingController();
TextEditingController _title = TextEditingController();

class Review extends StatefulWidget {
  Review({@required Orders ord}) {
    order = ord;
  }

  @override
  _ReviewState createState() => _ReviewState();
}

class _ReviewState extends State<Review> {
  ShoppingServices _shoppingServices;

  @override
  void initState() {
    loaded = false;
    _shoppingServices = ShoppingServices(context: context);
    super.initState();

    if (order.review_id == null) {
      loaded = true;
    } else {
      initReview();
    }
  }

  initReview() async {
    await Future.delayed(Duration(microseconds: 10));
    QueryResult reviewResult = await _shoppingServices.viewReview(reviewId: order.review_id);
    if (!reviewResult.hasException) {
      review = ViewReview.fromJson(reviewResult.data['viewReview']);
      setState(() {
        if (review.review.rating != null) {
          _rating = num.tryParse(review.review.rating.toString())?.toDouble();
        } else {
          _rating = 0.0;
        }
        _title.text = review.review.title;
        _description.text = review.review.text;
        loaded = true;
      });
      //print(review.review.order_id);
    }
  }

  createReview() async {
    await Future.delayed(Duration(microseconds: 10));
    QueryResult reviewResult = await _shoppingServices.createReview(
        product_id: order.product_id,
        rating: _rating,
        order_id: order.order_id,
        order_product_id: order.order_product_id,
        message: _description.text,
        title: _title.text);
    if (!reviewResult.hasException) {
      _createReviewResult = ViewReview.fromJson(reviewResult.data['createReview']);
      setState(() {
        order.review_id = _createReviewResult.review.review_id;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        extendBody: false,
        appBar: AppBar(
          title: Text("Review Product"),
        ),
        bottomSheet: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(width: 1.0, color: Colors.black38),
            ),
            color: Colors.white,
          ),
          //color: Colors.red,
          alignment: Alignment.centerRight,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width / 7,
          child: InkWell(
            onTap: updated
                ? () async {
                    setState(() {
                      updated = false;
                    });
                    if (order.review_id == null) {
                      await createReview();
                      // flushbar removed
                    } else {
                      await _shoppingServices.updateReview(reviewId: order.review_id, rating: _rating, message: _description.text, title: _title.text);
                      // flushbar removed
                    }
                  }
                : null,
            child: Container(
              margin: const EdgeInsets.only(right: 30),
              child: order.review_id == null
                  ? Text(
                      "Save",
                      style: TextStyle(fontSize: MediaQuery.of(context).size.width / 16, color: updated ? Theme.of(context).primaryColor : Colors.grey),
                    )
                  : Text(
                      "Update",
                      style: TextStyle(fontSize: MediaQuery.of(context).size.width / 16, color: updated ? Theme.of(context).primaryColor : Colors.grey),
                    ),
            ),
          ),
        ),
        body: loaded
            ? SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Card(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(5),
                              height: width / 4,
                              child: Image.network(
                                "${imagePath + order.image}",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Container(
                              //padding: EdgeInsets.only(left: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    // color: Colors.red,
                                    height: width / 16,
                                    width: width / 1.6,
                                    child: AutoSizeText(
                                      '${order.name}',
                                      style: TextStyle(
                                        color: Color(0xff73767a),
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                      presetFontSizes: [19, 18, 17, 16],
                                    ),
                                  ),
                                  Container(
                                    height: width / 18,
                                    width: width / 1.6,
                                    child: AutoSizeText(
                                      'Fasttrack',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Color(0xff969798),
                                      ),
                                      presetFontSizes: [
                                        17,
                                        14,
                                        12,
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: width / 17.2,
                                    width: width / 1.6,
                                    child: AutoSizeText(
                                      'Deliverd on 02 January 2020',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.green,
                                      ),
                                      presetFontSizes: [17, 15, 12, 11],
                                    ),
                                  ),

                                  //Rating Bar
                                ],
                              ),
                              height: width / 3,

                              // color: Colors.blue,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      //color: Colors.green,
                      height: width / 15,
                      margin: const EdgeInsets.only(left: 15, top: 20),
                      child: AutoSizeText(
                        "Give Your Rating",
                        style: TextStyle(color: Colors.grey[600]),
                        presetFontSizes: [23, 21, 20, 18],
                      ),
                    ),
                    Container(
                      //width: width/1.8,
                      //alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.only(left: 10, top: 8, bottom: 15),
                      child: RatingBar(
                        initialRating: _rating,
                        onRatingChanged: (rating) {
                          setState(() {
                            updated = true;
                            _rating = rating;
                          });
                        },
                        filledIcon: Icons.star,
                        emptyIcon: Icons.star_border,
                        // halfFilledIcon: Icons.star_half,
                        isHalfAllowed: false,
                        filledColor: Colors.yellow[700],
                        emptyColor: Colors.grey,
                        //halfFilledColor: Colors.amberAccent,
                        size: 48,
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15, top: 5, bottom: 10),
                      // constraints: BoxConstraints(minHeight: 100),
                      child: TextField(
                        controller: _title,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        onChanged: (d) {
                          setState(() {
                            updated = true;
                          });
                        },
                        textInputAction: TextInputAction.newline,
                        decoration: InputDecoration(
                          disabledBorder: InputBorder.none,
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          hintText: "Short title",
                          hintStyle: TextStyle(fontSize: 18),
                        ),
                        showCursor: true,
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15, top: 5, bottom: 60),
                      constraints: BoxConstraints(minHeight: 100),
                      child: TextField(
                        controller: _description,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        onChanged: (d) {
                          setState(() {
                            updated = true;
                          });
                        },
                        textInputAction: TextInputAction.newline,
                        decoration: InputDecoration(
                          disabledBorder: InputBorder.none,
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          hintText: "Type Your review",
                          hintStyle: TextStyle(fontSize: 18),
                        ),
                        showCursor: true,
                      ),
                    )
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }
}
