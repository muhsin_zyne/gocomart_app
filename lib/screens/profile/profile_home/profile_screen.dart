import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/components/iconGenerator.dart';
import 'package:gocomartapp/components/listTileSkeleton.dart';
import 'package:gocomartapp/components/logout_modal.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/main.dart' as main;
import 'package:gocomartapp/models/profile/user.dart';
import 'package:gocomartapp/models/profile_screen_model/get_profile_app_links.dart';
import 'package:gocomartapp/providers/app_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/providers/user_profile_provider.dart';
import 'package:gocomartapp/screens/profile/edit_profile_screen.dart';
import 'package:gocomartapp/screens/profile/profile_home/profile_presenter.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/onesignal/OneSignalExternalUserIDResponse.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/theme/theme_const.dart';
import 'package:graphql/client.dart';
import 'package:load/load.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ProfileScreen extends StatefulWidget {
  static const String id = 'profile_screen';
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        //backgroundColor: GocoMartAppTheme.buildLightTheme().primaryColor,
        appBar: AppBar(
            //elevation: 0.0,
            ),
        body: ProfileScreenContent(),
      ),
    );
  }
}

class ProfileScreenContent extends StatefulWidget {
  @override
  _ProfileScreenContentState createState() => _ProfileScreenContentState();
}

class _ProfileScreenContentState extends State<ProfileScreenContent> with BaseStatefulScreen implements IProfileScreen {
  //bool _refralRequest = false;
  bool _isNotReferred = true;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  LogoutModal logOutModal = LogoutModal();
  List<Widget> profileLinks = [];
  AppServices _appServices;

  AppProvider _appProvider;
  UserProfileProvider _userProfileProvider;
  User _user;
  GetProfileAppLinks _profileAppLinks;

  GlobalProvider _globalProvider;
  OCImageCache ocImageCache = OCImageCache();
  var imageCache;

  AccountServices _accountServices;
  bool userDetailsLoaded = false;
  ProfilePresenter _presenter;

  Future actionRequestRef() async {}

  @override
  void initState() {
    super.initState();
    _appServices = AppServices(context: context);
    _accountServices = AccountServices(context: context);
    this._loadClient();
    userDetails();
    initProfilePageLinks();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    _presenter = new ProfilePresenter(context: context, view: this);
  }

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(
      context,
    );
    _userProfileProvider = Provider.of<UserProfileProvider>(
      context,
    );
    _appProvider = Provider.of<AppProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  userDetails() async {
    await Future.delayed(Duration(microseconds: 100));
    //await Future.delayed(Duration(microseconds: 100));
    setState(() {
      _user = _userProfileProvider.userDetails;
    });
    if (_user != null) {
      userDetailsLoaded = true;
    }
    QueryResult getUserDetailsResult = await _accountServices.userDetails();

    print(getUserDetailsResult.data);
    if (!getUserDetailsResult.hasException) {
      setState(() {
        _userProfileProvider.userDetails = User.fromJson(getUserDetailsResult.data['me']['user']);
      });
    }
    //print(_userProfileProvider.userDetails.mobile_no);
    setState(() {
      _user = _userProfileProvider.userDetails;
      userDetailsLoaded = true;
      // imageCache = ocImageCache.getCachedImage(
      //   path: _user.accountDetails.profileImage,
      //   imageQuality: ImageQuality.sup,
      // );
    });
    print(_user.accountDetails.dob);
    // print(_user.);
  }

  initProfilePageLinks() async {
    await Future.delayed(Duration(microseconds: 100));
    // print(_globalProvider.getProfileAppLinks.links[0].toJson());
    // await Future.delayed(Duration(microseconds: 100));
    // print(_appProvider.getProfileAppLinks.message);
    // await Future.delayed(Duration(microseconds: 5));
    QueryResult profileLinkResult = await _appServices.getProfileAppLinks();

    if (!profileLinkResult.hasException) {
      _profileAppLinks = GetProfileAppLinks.fromJson(profileLinkResult.data["getProfileAppLinks"]);

      _appProvider.getProfileAppLinks = GetProfileAppLinks.fromJson(profileLinkResult.data["getProfileAppLinks"]);
    }
  }

  _requestLogout() {
    createAlertDialog(context);
  }

  createAlertDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          title: Text(
            'Are you sure to logout',
            style: TextStyle(color: Color(0xff3a3a3a)),
          ),
          actions: <Widget>[
            MaterialButton(
              elevation: 5.0,
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            MaterialButton(
              elevation: 5.0,
              child: Text("Logout"),
              onPressed: () {
                Navigator.pop(context);
                _presenter.actionLogoutHandler();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Container(
        //color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 3.8,
              decoration: BoxDecoration(
                color: GocoMartAppTheme.buildLightTheme().primaryColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: const Radius.circular(20.0),
                  bottomRight: const Radius.circular(20.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        _userProfileProvider.userDetails != null
                            ? Container(
                                width: width / 4.5,
                                height: width / 4.5,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: _userProfileProvider.userDetails.accountDetails.profileImage == null
                                            ? AssetImage('assets/images/avatar.png')
                                            : NetworkImage("${_globalProvider.cdnPoint + _userProfileProvider.userDetails.accountDetails.profileImage}"))),
                              )
                            : CircleAvatar(
                                radius: 40,
                                backgroundColor: Colors.white12,
                                child: SkeletonAnimation(
                                  child: CircleAvatar(backgroundColor: Colors.white12, radius: 40),
                                ),
                              ),
                        //  CircleAvatar(
                        //     radius: 40,
                        //     backgroundColor: Colors.white12,
                        //     // foregroundColor: Colors.white12,
                        //     child: SkeletonAnimation(
                        //       child: Container(
                        //         color: Colors.white10,
                        //       ),
                        //     ),
                        //   ),
                        Positioned(
                            top: 0,
                            right: 0,
                            child: _userProfileProvider.userDetails != null
                                ? AutoSizeText(
                                    "${_userProfileProvider.userDetails?.goco_id ?? null}",
                                    maxLines: 2,
                                    minFontSize: 20,
                                    textAlign: TextAlign.left,
                                    style: kGeneralText,
                                  )
                                : getSingleSkeleton(width)),
                      ],
                    ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    Row(
                      children: <Widget>[
                        _userProfileProvider.userDetails != null
                            ? Container(
                                height: width / 13,
                                constraints: BoxConstraints(
                                  maxWidth: width / 1.5,
                                ),
                                //height: MediaQuery.of(context).size.width/13.71,
                                child: AutoSizeText(
                                  "${_userProfileProvider.userDetails?.accountDetails?.name ?? "Gocomart User"}",
                                  maxLines: 1,
                                  presetFontSizes: [
                                    28,
                                    26,
                                    24,
                                    22,
                                    20,
                                    18,
                                  ],
                                  //textAlign: TextAlign.left,
                                  style: kGeneralText,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )
                            : getSingleSkeleton(width),
                        SizedBox(
                          width: 2,
                        ),
                        _userProfileProvider.userDetails != null
                            ? Image.asset(
                                'assets/images/medal.png',
                                width: width / 14,
                              )
                            : SizedBox(),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              InkWell(
                                onTap: _userProfileProvider.userDetails != null
                                    ? () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => EditProfile(
                                                    accDetails: _userProfileProvider.userDetails.accountDetails,
                                                  ),
                                              maintainState: false),
                                        ).whenComplete(() {
                                          print(_userProfileProvider.userDetails.accountDetails.name);
                                          setState(() {
                                            _user = _userProfileProvider.userDetails;
                                          });
                                        });
                                      }
                                    : () {},
                                highlightColor: Colors.orange,
                                splashColor: Colors.white,
                                focusColor: Colors.red,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xff4F69A3),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                  child: Container(
                                    height: width / 8,
                                    width: width / 9,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 8,
                                      ),
                                      child: Icon(
                                        Icons.edit,
                                        size: width / 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _userProfileProvider.userDetails != null
                            ? Container(
                                alignment: Alignment.topCenter,
                                height: width / 18,
                                child: AutoSizeText(
                                  " ${_userProfileProvider.userDetails?.mobile_no ?? "error"}",
                                  maxLines: 1,
                                  presetFontSizes: [22, 20, 18, 16, 14],
                                  textAlign: TextAlign.left,
                                  style: kGeneralText,
                                ),
                              )
                            : getSingleSkeleton(width),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: Column(
                  children: <Widget>[
                    _isNotReferred == true ? getRefBlock() : Container(),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(),
                    // Column(
                    //   children: profileLinks,
                    // ),

                    (_appProvider?.getProfileAppLinks?.links?.length ?? 0) > 0
                        ? Container(
                            child: ListView.builder(
                                itemCount: _appProvider?.getProfileAppLinks?.links?.length ?? 0,
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (context, i) {
                                  return Column(
                                    children: <Widget>[
                                      ListTile(
                                        onTap: () {
                                          Navigator.pushNamed(context, _appProvider.getProfileAppLinks.links[i].link);
                                        },
                                        leading: getIcon(
                                            string: _appProvider.getProfileAppLinks.links[i].icon,
                                            hexColor: _appProvider.getProfileAppLinks.links[i].primaryColor),
                                        title: Text(_appProvider.getProfileAppLinks.links[i].title),
                                        subtitle: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(_appProvider.getProfileAppLinks.links[i].description),
                                          ],
                                        ),
                                        enabled: true,
                                        trailing: Icon(FontAwesomeIcons.chevronRight),
                                      ),
                                      Divider(),
                                    ],
                                  );
                                }),
                          )
                        : Container(
                            margin: const EdgeInsets.only(left: 8),
                            child: Column(
                              children: <Widget>[
                                ListTileSkeleton(
                                  width: width,
                                  boxImage: false,
                                ),
                                Divider(),
                                ListTileSkeleton(
                                  width: width,
                                  boxImage: false,
                                ),
                              ],
                            ),
                          ),
                    (_appProvider?.getProfileAppLinks?.links?.length ?? 0) > 0
                        ? ListTile(
                            onTap: _requestLogout,
                            leading: Icon(
                              FontAwesomeIcons.powerOff,
                              color: Colors.red,
                            ),
                            title: Text('Logout'),
                            subtitle: Text("Yes! you may have a short break"),
                            enabled: true,
                            trailing: Icon(FontAwesomeIcons.chevronRight),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getRefBlock() {
    return Container(
      child: Card(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.exclamation,
                size: 35,
                color: Color(0xffFCC209),
              ),
              title: Text("You are not registed by any referals"),
              subtitle: Text('Still you can gift your friend the benifit of referal'),
            ),
            // Container(
            //   child: Padding(
            //     padding: const EdgeInsets.symmetric(horizontal: 70),
            //     child: PinCodeTextField(
            //       length: 9,
            //       obsecureText: false,
            //       animationType: AnimationType.scale,
            //       shape: PinCodeFieldShape.underline,
            //       fieldHeight: 30,
            //       fieldWidth: 20,
            //       textStyle: TextStyle(fontSize: 20),
            //       animationDuration: Duration(milliseconds: 300),
            //       onChanged: (String value) {
            //         if (value.length < 9) {
            //           _refralRequest = false;
            //         }
            //       },
            //       onCompleted: (String value) {
            //         setState(() {
            //           _refralRequest = true;
            //         });
            //       },
            //     ),
            //   ),
            // ),

            Container(
              height: MediaQuery.of(context).size.width / 6,
              padding: const EdgeInsets.only(left: 30, right: 50, top: 20),
              child: TextField(
                decoration: InputDecoration(contentPadding: EdgeInsets.all(10), border: OutlineInputBorder()),
              ),
            ),
            // ignore: deprecated_member_use
            ButtonTheme.bar(
              height: 2,
              // make buttons use the appropriate styles for cards
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      child: const Text('SUBMIT'),
                      onPressed:
                          //_refralRequest == true ?
                          actionRequestRef
                      //: null,
                      ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getSingleSkeleton(width) {
    return SkeletonAnimation(
      child: Container(
        height: 10,
        width: width / 5,
        decoration: BoxDecoration(
          color: Colors.black45,
        ),
      ),
    );
  }

  @override
  void onLogoutDone(OneSignalExternalUserIDResponse oneSignalExternalUserIDResponse) {
    hideLoadingDialog();
    main.main();
  }
}
