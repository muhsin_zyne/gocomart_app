import 'package:flutter/material.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/onesignal/OneSignalExternalUserIDResponse.dart';
import 'package:gocomartapp/src/services/one_signal.dart';
import 'package:load/load.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePresenter {
  BuildContext context;
  IProfileScreen view;
  OneSignalService oneSignalService;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  ProfilePresenter({this.context, this.view}) {
    _init();
  }

  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    oneSignalService = OneSignalService(context: context);
  }

  actionLogoutHandler() async {
    showLoadingDialog();
    final SharedPreferences prefs = await _prefs;
    prefs.remove('auth_token');
    prefs.remove('userData');
    OneSignalExternalUserIDResponse oneSignalExternalUserIDResponse = await oneSignalService.removeExternalUserId();
    view.onLogoutDone(oneSignalExternalUserIDResponse);
  }
}
