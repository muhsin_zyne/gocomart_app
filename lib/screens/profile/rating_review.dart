import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/controllers/local_storage.dart';
import 'package:gocomartapp/models/order_review/get_customer_order_review.dart';
import 'package:gocomartapp/models/order_review/orders.dart';
import 'package:gocomartapp/screens/profile/review.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';

class RatingReview extends StatelessWidget {
  const RatingReview({Key key}) : super(key: key);
  static const String id = "rating_screen";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ratings & Reviews"),
      ),
      body: RatingReviewBody(),
    );
  }
}

class RatingReviewBody extends StatefulWidget {
  RatingReviewBody({Key key}) : super(key: key);

  @override
  _RatingReviewBodyState createState() => _RatingReviewBodyState();
}

class _RatingReviewBodyState extends State<RatingReviewBody> {
  ScrollController _scrollController = ScrollController();
  ShoppingServices _shoppingServices;
  GetCustomerOrderReview getCustomerOrderReview;
  LocalStorage _localStorage;
  final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
  bool fetchMore = false;
  double _rating = 0;
  bool callApi = true;
  int currentPagination = 1;
  bool loadingOrders = true;
  int dataLoadLimit = 7;
  List<Orders> totalOrders = [];
  bool onRated = false;

  @override
  void initState() {
    super.initState();
    _shoppingServices = ShoppingServices(context: context);
    _localStorage = LocalStorage(context: context);
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 10));
    await _localStorage.getOrderStatus();
    //pageReady = true;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        callApi ? pagination() : nullCall();
      }
    });
    getOrderReview();
  }

  void nullCall() {}

  pagination() async {
    currentPagination++;
    setState(() {
      fetchMore = true;
    });
    getOrderReview();
  }

  getOrderReview() async {
    QueryResult orderReviewResult = await _shoppingServices.getCustomerOrderReview(currentPage: currentPagination, dataLimit: dataLoadLimit);

    print(orderReviewResult.data);
    if (!orderReviewResult.hasException) {
      getCustomerOrderReview = GetCustomerOrderReview.fromJson(orderReviewResult.data["getCustomerOrderReview"]);
      setState(() {
        fetchMore = false;
      });

      if (getCustomerOrderReview.orders.length > 0) {
        setState(() {
          totalOrders = [totalOrders, getCustomerOrderReview.orders].expand((x) => x).toList();
        });
      } else {
        setState(() {
          callApi = false;
        });
      }
    }
    setState(() {
      loadingOrders = false;
    });
  }

  showToast() {
    return Fluttertoast.showToast(
        msg: "Rating Updated",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.grey[800],
        textColor: Colors.white,
        timeInSecForIosWeb: 1,
        fontSize: 12.0);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: <Widget>[
            loadingOrders == false
                ? totalOrders.length > 0
                    ? Column(
                        children: orders(width),
                      )
                    : Container(
                        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 2 - 50),
                        child: Center(
                          child: Text("empty"),
                        ),
                      )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
            fetchMore == true ? loaderSpinner() : SizedBox(),
          ],
        ));
  }

  Widget loaderSpinner() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  orders(width) {
    List<Widget> order = [];
    for (int i = 0; i < totalOrders.length; i++) {
      if (totalOrders[i].rating != null) {
        _rating = num.tryParse(totalOrders[i].rating.toString())?.toDouble();
      }
      // _rating=0;
      order.add(Card(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                //  color:Colors.red,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(5),
                height: width / 4,
                child: Image.network(
                  "${imagePath + totalOrders[i].image}",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                //padding: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      // color: Colors.red,
                      height: width / 16,
                      width: width / 1.6,
                      child: AutoSizeText(
                        '${totalOrders[i].name}',
                        style: TextStyle(
                          color: Color(0xff73767a),
                        ),
                        overflow: TextOverflow.ellipsis,
                        presetFontSizes: [19, 18, 17, 16],
                      ),
                    ),
                    Container(
                      height: width / 18,
                      width: width / 1.6,
                      child: AutoSizeText(
                        '${totalOrders[i].order_status_id.toString()}',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Color(0xff969798),
                        ),
                        presetFontSizes: [
                          17,
                          14,
                          12,
                        ],
                      ),
                    ),
                    Container(
                      height: width / 18,
                      width: width / 1.6,
                      child: AutoSizeText(
                        '${_localStorage.orderStatusConst['DELIVERED'].toString()}',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.green,
                        ),
                        presetFontSizes: [
                          17,
                          14,
                          12,
                        ],
                      ),
                    ),

                    //Rating Bar
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Container(
                              width: width / 2,
                              alignment: Alignment.centerLeft,
                              child: totalOrders[i].order_status_id.toString() == _localStorage.orderStatusConst['DELIVERED'].toString()
                                  ? Container(
                                      child: totalOrders[i].review_id != null
                                          ? RatingBar(
                                              initialRating: _rating,
                                              onRatingChanged: (rating) async {
                                                showToast();
                                                setState(() {
                                                  print(rating);
                                                  if (rating > 0.0) {
                                                    _rating = rating;
                                                  } else {
                                                    rating = 1;
                                                    _rating = 1;
                                                  }
                                                });

                                                await _shoppingServices.updateReview(reviewId: totalOrders[i].review_id, rating: _rating);
                                              },
                                              filledIcon: Icons.star,
                                              emptyIcon: Icons.star_border,
                                              // halfFilledIcon: Icons.star_half,
                                              isHalfAllowed: false,
                                              filledColor: Colors.yellow[700],
                                              emptyColor: Colors.grey,
                                              maxRating: 5,

                                              //halfFilledColor: Colors.amberAccent,
                                              size: 30,
                                            )
                                          : InkWell(
                                              onTap: () {
                                                navigate(totalOrders[i]);
                                              },
                                              child: RatingBar.readOnly(
                                                initialRating: 0,
                                                filledIcon: Icons.star,
                                                emptyIcon: Icons.star_border,
                                                // halfFilledIcon: Icons.star_half,
                                                isHalfAllowed: false,
                                                filledColor: Colors.yellow[700],
                                                emptyColor: Colors.grey,
                                                maxRating: 5,

                                                //halfFilledColor: Colors.amberAccent,
                                                size: 30,
                                              ),
                                            ),
                                    )
                                  : Container(
                                      height: width / 22,
                                      width: width / 1.6,
                                      child: AutoSizeText(
                                        'Product not delivered',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.red[300],
                                        ),
                                        presetFontSizes: [
                                          17,
                                          14,
                                          13,
                                          12,
                                        ],
                                      ),
                                    ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: totalOrders[i].order_status_id.toString() == _localStorage.orderStatusConst['DELIVERED'].toString()
                                ? InkWell(
                                    onTap: () {
                                      navigate(totalOrders[i]);
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.topRight,
                                          height: width / 19,
                                          child: AutoSizeText(
                                            "Review",
                                            style: TextStyle(color: Theme.of(context).primaryColor),
                                            presetFontSizes: [
                                              14,
                                              12,
                                              11,
                                            ],
                                          ),
                                        ),
                                        Icon(Icons.chevron_right, size: width / 17, color: Theme.of(context).primaryColor)
                                      ],
                                    ),
                                  )
                                : SizedBox(),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                height: width / 3,

                // color: Colors.blue,
              ),
            )
          ],
        ),
      ));
    }

    return order;
  }

  navigate(order) {
    return Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Review(ord: order),
      ),
    ).whenComplete(() {
      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(builder: (context) => RatingReview()));
    });
  }
}
