import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:gocomartapp/screens/profile/order/widgets/timeline_node.dart';

class ShippingStatus extends StatefulWidget {
  ShippingStatus({Key key}) : super(key: key);

  @override
  _ShippingStatusState createState() => _ShippingStatusState();
}

class _ShippingStatusState extends State<ShippingStatus> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("Shipping Status"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: .1),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: header(width, context),
              ),
            ),
            ShippingStatusWidget(width: width)
          ],
        ),
      ),
    );
  }

  Widget header(double width, BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //order number
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AutoSizeText(
                  "Order Number : ",
                  minFontSize: 8,
                  maxFontSize: 18,
                  maxLines: 1,
                  style: TextStyle(
                      letterSpacing: .01,
                      fontSize: width / 30,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                AutoSizeText(
                  "GOCO123WYFNX",
                  minFontSize: 8,
                  maxFontSize: 18,
                  style: TextStyle(
                      fontSize: width / 30,
                      letterSpacing: -.1,
                      color: Theme.of(context).primaryColor),
                ),
              ],
            ),
            //shipment number
            Container(
              margin: EdgeInsets.only(top: 8),
              width: width / 1.5,
              child: AutoSizeText(
                "Shipment 1 : GODADAGDFKJSNCMFSKDFJ1",
                minFontSize: 8,
                maxFontSize: 18,
                maxLines: 1,
                style: TextStyle(
                    fontSize: width / 30,
                    letterSpacing: -.1,
                    color: Colors.black,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.green[600],
              ),
              margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
              width: 10,
              height: 10,
            ),
            AutoSizeText(
              "Deliverd with Smile",
              minFontSize: 8,
              maxFontSize: 18,
              maxLines: 1,
              style: TextStyle(
                fontSize: width / 38,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class ShippingStatusWidget extends StatelessWidget {
  const ShippingStatusWidget({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  timeline() {
    List<Widget> tl = [];
    TimelineNodeLineType linetype(i) {
      if (i == 0)
        return TimelineNodeLineType.BottomHalf;
      else if (i == 4) return TimelineNodeLineType.TopHalf;

      return TimelineNodeLineType.Full;
    }

    for (int i = 0; i < 5; i++) {
      tl.add(TimelineNode(
        style: TimelineNodeStyle(
            lineType: linetype(i),
            pointType: TimelineNodePointType.Circle,
            lineColor: Colors.green,
            pointColor: Colors.green),
        child: Container(
          margin: const EdgeInsets.only(top: 20, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Ordered And Approved"),
              Text(
                "17 March 202,sunday",
                style: TextStyle(
                  color: Color(0xff969798),
                  fontSize: width / 30,
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return tl;
  }

  @override
  Widget build(BuildContext context) {
    // return Column(
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: <Widget>[
    //     Padding(
    //       padding: const EdgeInsets.only(left: 15.0, bottom: 5),
    //       child: AutoSizeText(
    //         'Shipping status',
    //         minFontSize: 11,
    //         maxFontSize: 20,
    //         style: TextStyle(
    //             fontSize: width / 22,
    //             color: Colors.black54,
    //             fontWeight: FontWeight.w300),
    //       ),
    //     ),
    //     Divider(),
    //     //timeline
    //     Column(children: timeline())
    //   ],
    // );

    return Column(
      children: timeline(),
    );
  }
}
