import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/components/listTileSkeleton.dart';
import 'package:gocomartapp/controllers/local_storage.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/get_orders/get_orders.dart';
import 'package:gocomartapp/models/get_orders/orders.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/profile/order/order_status.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:graphql/client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:page_transition/page_transition.dart';
//part
//part 'orders.g.dart';

class OrdersScreen extends StatefulWidget {
  static const String id = "orders_screen";
  const OrdersScreen({Key key}) : super(key: key);

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<OrdersScreen> {
  ShoppingServices _shoppingServices;
  ScrollController _scrollController = ScrollController();
  OCImageCache ocImageCache = OCImageCache();
  GlobalProvider _globalProvider;
  bool callApi = true;
  int currentPagination = 1;
  bool fetchMore = false;
  Get_orders _getOrders;
  List<Orders> totalOrders = [];
  bool loadingOrders = true;
  LocalStorage localStorage;

  @override
  void initState() {
    localStorage = LocalStorage(context: context);
    super.initState();

    _shoppingServices = ShoppingServices(context: context);
    init();
  }

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(context);
    super.didChangeDependencies();
  }

  init() async {
    print("initw1");

    await Future.delayed(Duration(microseconds: 10));
    await localStorage.getOrderStatus();
    print(callApi);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print(_scrollController.position.pixels);
        callApi ? pagination() : nullCall();
      }
    });
    getOrders();
  }

  void nullCall() {}

  pagination() async {
    print("initw2");
    currentPagination++;
    setState(() {
      fetchMore = true;
    });
    getOrders();
  }

  getOrders() async {
    print("initw3");
    QueryResult orderResult = await _shoppingServices.getOrders(
        currentPage: currentPagination, dataLimit: 5);
    // print("object");
    // print(orderResult.exception);
    // print(orderResult.data);
    if (!orderResult.hasException) {
      _getOrders = Get_orders.fromJson(orderResult.data['getOrders']);
      setState(() {
        fetchMore = false;
      });

      if (_getOrders.orders.length > 0) {
        setState(() {
          totalOrders =
              [totalOrders, _getOrders.orders].expand((x) => x).toList();
        });
        // print(totalOrders[0].products.length);
      } else {
        setState(() {
          callApi = false;
        });
      }
    }
    setState(() {
      loadingOrders = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("My orders"),
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: <Widget>[
            !loadingOrders
                ? totalOrders.length > 0
                    ? bodyWidget(width)
                    : Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 2 - 50),
                        child: Center(
                          child: Text("empty"),
                        ),
                      )
                : ListView.builder(
                    itemCount: 5,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, count) {
                      return ListTileSkeleton(
                        width: width,
                        boxImage: true,
                      );
                    },
                  ),
            fetchMore == true ? loaderSpinner() : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget loaderSpinner() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  ListView bodyWidget(double width) {
    return ListView.builder(
        itemCount: totalOrders.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, i) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderStatus(
                        orderRequestGUID: totalOrders[i].order_request_guid,
                      ),
                    ),
                  );
                },
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AutoSizeText(
                                "Order:${totalOrders[i]?.invoice_prefix}${totalOrders[i]?.invoice_no}",
                                minFontSize: 8,
                                maxFontSize: 18,
                                style: TextStyle(
                                    fontSize: width / 28,
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              AutoSizeText(
                                "Placed on ${totalOrders[i]?.date_added != null ? DateFormat("MMM d, yyyy").format(DateTime.parse(totalOrders[i]?.date_added)) : " "}",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                maxFontSize: 18,
                                minFontSize: 8,
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontSize: width / 30),
                              )
                            ],
                          ),
                          AutoSizeText(
                            "View More >",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            maxFontSize: 18,
                            minFontSize: 8,
                            style: TextStyle(
                                color: Colors.black54, fontSize: width / 27),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      //   color: Colors.red,
                      height: width / 2.8,
                      padding: const EdgeInsets.only(bottom: 10),
                      width: width,
                      child: ListView.builder(
                        shrinkWrap: true,
                        // physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: totalOrders[i].products.length,
                        itemBuilder: (BuildContext context, int index) {
                          print(totalOrders[i].products[index].dispatchedAt);
                          var imageCache = ocImageCache.getCachedImage(
                            path: totalOrders[i].products[index].image,
                            imageQuality: ImageQuality.high,
                          );
                          return Container(
                            //   color: Colors.blue,
                            width: width / 1.4,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  // padding: EdgeInsets.all(10),
                                  // margin: EdgeInsets.only(left: 5),
                                  height: MediaQuery.of(context).size.width / 6,
                                  width: MediaQuery.of(context).size.width / 5,
                                  child: Image.network(
                                    "${_globalProvider.cdnImagePoint + "cache/" + imageCache}",
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.only(left: 4),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          // color: Colors.red,
                                          margin: const EdgeInsets.only(top: 8),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.7,
                                          child: AutoSizeText(
                                            "${totalOrders[i].products[index].productDetails.manufacturer.name}",
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 8,
                                            maxFontSize: 20,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: width / 30,
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Container(
                                          //color: Colors.red,
                                          // margin: const EdgeInsets.only(top: 3),

                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.7,
                                          child: AutoSizeText(
                                            "${totalOrders[i].products[index].productDetails.description.name}",
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 8,
                                            maxFontSize: 20,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: width / 30,
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Container(
                                            margin: const EdgeInsets.only(
                                                top: 5, right: 10),
                                            child: shipmentStatusTitle(
                                                orderStatusId: totalOrders[i]
                                                    .products[index]
                                                    .currentProductStatus
                                                    .order_status_id,
                                                actDelivery: totalOrders[i]
                                                            ?.products[index]
                                                            ?.actDelivery !=
                                                        null
                                                    ? "${DateFormat("EEE, MMM d, yyyy").format(DateTime.parse(totalOrders[i].products[index].actDelivery))}"
                                                    : "",
                                                dispatchedAt: totalOrders[i]
                                                            ?.products[index]
                                                            ?.dispatchedAt !=
                                                        null
                                                    ? "${DateFormat("EEE, MMM d, yyyy").format(DateTime.parse(totalOrders[i].products[index].dispatchedAt))}"
                                                    : "",
                                                estDelivery: totalOrders[i]
                                                            ?.products[index]
                                                            ?.estDelivery !=
                                                        null
                                                    ? "${DateFormat("EEE, MMM d, yyyy").format(DateTime.parse(totalOrders[i].products[index].estDelivery))}"
                                                    : "",
                                                message: "message",
                                                width: width)),
                                        Container(
                                            margin:
                                                const EdgeInsets.only(top: 3),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.8,
                                            child: shipmentStatusSubtitle(
                                                estDelivery: totalOrders[i]
                                                            ?.products[index]
                                                            ?.estDelivery !=
                                                        null
                                                    ? "${DateFormat("EEE, MMM d, yyyy").format(DateTime.parse(totalOrders[i].products[index].estDelivery))}"
                                                    : "",
                                                orderStatusId: totalOrders[i]
                                                    .products[index]
                                                    .currentProductStatus
                                                    .order_status_id,
                                                width: width)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  AutoSizeText shipmentStatusTitle(
      {@required width,
      @required message,
      @required orderStatusId,
      @required dispatchedAt,
      @required actDelivery,
      @required estDelivery}) {
    print(dispatchedAt);
    String str = 'const';
    Color color;
    if (orderStatusId ==
        localStorage.orderStatusConst['DELIVERED'].toString()) {
      str = "${getTranslate('delivered_on')} $actDelivery";
      color = Colors.green;
    } else if (orderStatusId ==
        localStorage.orderStatusConst['SHIPPED'].toString()) {
      str = "${getTranslate('item_shipped')}";
      color = Colors.grey[700];
    } else if (orderStatusId ==
        localStorage.orderStatusConst['DISPATCHED'].toString()) {
      str = "${getTranslate('dispatched_title')} $dispatchedAt";
      color = Colors.grey[700];
    } else if (orderStatusId ==
        localStorage.orderStatusConst['ORDER_PROCESSING'].toString()) {
      str = "${getTranslate('order_processing')}";
      color = Colors.yellow[700];
    } else {
      str = "$message";
      color = Colors.grey[700];
    }

    return AutoSizeText(
      str,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      maxFontSize: 18,
      minFontSize: 8,
      style: TextStyle(color: color, fontSize: width / 30),
    );
  }

  shipmentStatusSubtitle(
      {@required width,
      @required estDelivery,
      actDelivery,
      @required orderStatusId}) {
    String str = "";
    Color color = Colors.grey[700];
    if (localStorage.orderStatusConst['DISPATCHED'].toString() ==
        orderStatusId) {
      str = "${getTranslate('delivery_expected_by')} $estDelivery";
      color = Colors.yellow[700];
    }

    return AutoSizeText(
      "$str",
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      maxFontSize: 18,
      minFontSize: 8,
      style: TextStyle(
          color: color, fontSize: width / 30, fontWeight: FontWeight.w500),
    );
  }
}
