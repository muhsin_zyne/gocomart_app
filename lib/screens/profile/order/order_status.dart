import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/components/listTileSkeleton.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/controllers/oc_cache_img.dart';
import 'package:gocomartapp/models/get_order_details/OrderDetails.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/profile/order/package_delivery_tracking_page.dart';
import 'package:gocomartapp/screens/profile/order/widgets/timeline_node.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/src/controllers/page_dynamic_route_controller.dart';
import 'package:graphql/client.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:skeleton_text/skeleton_text.dart';

class OrderStatus extends StatefulWidget {
  static const String id = 'order_status_screen';
  final String orderRequestGUID;
  const OrderStatus({Key key, this.orderRequestGUID}) : super(key: key);

  @override
  _OrderStatusState createState() => _OrderStatusState();
}

class _OrderStatusState extends State<OrderStatus> {
  //declaration
  ShoppingServices _shoppingServices;
  OrderDetails _orderDetails;
  bool dataLoaded = false;
  OCImageCache ocImageCache = OCImageCache();
  GlobalProvider _globalProvider;
  CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  String orderRequestGUID;
  @override
  void initState() {
    print(widget.orderRequestGUID);
    _shoppingServices = ShoppingServices(context: context);
    super.initState();
    this._isPageReadyForApiCall();
  }

  void _isPageReadyForApiCall() async {
    await Future.delayed(Duration(microseconds: 10));
    this.orderRequestGUID == null ? _isPageReadyForApiCall() : this.getOrderDetails();
  }

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(context);
    final OrderStatusScreenArguments args = ModalRoute.of(context).settings.arguments;
    if (this.widget.orderRequestGUID == null) {
      this.orderRequestGUID = args.orderRequestGUID;
    } else {
      this.orderRequestGUID = this.widget.orderRequestGUID;
    }
    super.didChangeDependencies();
  }

  getOrderDetails() async {
    await Future.delayed(Duration(microseconds: 10));
    QueryResult orderResult = await _shoppingServices.getOrderDetails(orderRequestGuid: "${this.orderRequestGUID}");
    if (!orderResult.hasException) {}
    _orderDetails = OrderDetails.fromJSON(orderResult.data['getOrderDetails']);
    setState(() {
      dataLoaded = true;
    });
    print(_orderDetails.order.invoiceNo);
  }

  BoxDecoration containerBoxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 5, // soften the shadow
          spreadRadius: .1, //extend the shadow
          offset: Offset(
            0.0, // Move to right 10  horizontally
            0.0, // Move to bottom 10 Vertically
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey[350],
        appBar: AppBar(
          title: Text("Order Status"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              dataLoaded
                  ? Container(
                      margin: const EdgeInsets.only(bottom: 8.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                      decoration: containerBoxDecoration(),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            "Order Number : ",
                            minFontSize: 8,
                            maxFontSize: 18,
                            style: TextStyle(fontSize: width / 25),
                          ),
                          AutoSizeText(
                            "${_orderDetails.order.invoicePrefix}${_orderDetails.order.invoiceNo}",
                            minFontSize: 8,
                            maxFontSize: 18,
                            style: TextStyle(fontSize: width / 25, color: Colors.green),
                          ),
                        ],
                      ))
                  : Container(
                      width: width,
                      margin: const EdgeInsets.only(bottom: 8.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                      decoration: containerBoxDecoration(),
                      child: SkeletonAnimation(
                          child: Container(
                        width: width / 2,
                        color: Colors.grey[350],
                        height: 8,
                      )),
                    ),
              //detail of shipment products
              dataLoaded
                  ? Container(
                      margin: const EdgeInsets.only(bottom: 0.0),
                      padding: const EdgeInsets.symmetric(vertical: 0),
                      //decoration: containerBoxDecoration(),
                      child: productListing(width: width, isProcessingProduct: false),
                    )
                  : Container(
                      decoration: containerBoxDecoration(),
                      child: ListTileSkeleton(
                        width: width,
                        boxImage: true,
                      ),
                    ),

              //detail of processing products
              dataLoaded
                  ? _orderDetails.order.processingProducts.length > 0
                      ? Container(
                          margin: const EdgeInsets.only(bottom: 8.0),
                          padding: const EdgeInsets.symmetric(vertical: 0),
                          //decoration: containerBoxDecoration(),
                          child: productListing(width: width, isProcessingProduct: true),
                        )
                      : SizedBox()
                  : Container(
                      decoration: containerBoxDecoration(),
                      child: ListTileSkeleton(
                        width: width,
                        boxImage: true,
                      ),
                    ),
              //shipping status
              // Container(
              //   decoration: containerBoxDecoration(),
              //   margin: const EdgeInsets.only(top: 5, bottom: 5),
              //   padding: const EdgeInsets.symmetric(vertical: 10),
              //   child: ShippingStatusWidget(width: width),
              // ),

              //invoice download

              //shipping details
              dataLoaded
                  ? Container(
                      margin: const EdgeInsets.only(bottom: 8.0),
                      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                      decoration: containerBoxDecoration(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            'Shipping Details',
                            minFontSize: 11,
                            maxFontSize: 20,
                            style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w300),
                          ),
                          Divider(),
                          AutoSizeText(
                            '${_orderDetails.order.orderAddress.shippingName}',
                            minFontSize: 11,
                            maxFontSize: 20,
                            style: TextStyle(fontSize: width / 25, color: Colors.black87, fontWeight: FontWeight.bold),
                          ),
                          AutoSizeText(
                            '${_orderDetails.order.orderAddress.shippingAddress_1} ${_orderDetails.order.orderAddress.shippingAddress_2} \n${_orderDetails.order.orderAddress.shippingCity} \nErnakulam \nkerala-${_orderDetails.order.orderAddress.shippingPostcode} \n${_orderDetails.order.orderAddress.shippingMobileNumber}',
                            minFontSize: 11,
                            maxFontSize: 20,
                            style: TextStyle(fontSize: width / 29, color: Colors.black54, fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      height: width / 3,
                      width: width,
                      margin: const EdgeInsets.only(bottom: 8.0),
                      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                      decoration: containerBoxDecoration(),
                      child: ListView.builder(
                          itemCount: 4,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, i) {
                            return Padding(
                              padding: const EdgeInsets.all(7.0),
                              child: SkeletonAnimation(
                                  child: Container(
                                width: i == 0 ? width / 2.5 : width / 2,
                                color: Colors.grey[350],
                                height: 10,
                              )),
                            );
                          }),
                    ),

              //price details
              dataLoaded
                  ? Container(
                      margin: const EdgeInsets.only(bottom: 8.0),
                      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                      decoration: containerBoxDecoration(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: AutoSizeText(
                              'Price Details',
                              minFontSize: 11,
                              maxFontSize: 20,
                              style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Divider(),
                          dataLoaded
                              ? ListView.builder(
                                  itemCount: _orderDetails.order.invoiceOrderTotal.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, i) {
                                    return _orderDetails.order.invoiceOrderTotal[i].code != "total"
                                        ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              AutoSizeText(
                                                '${_orderDetails.order.invoiceOrderTotal[i].title}',
                                                minFontSize: 11,
                                                maxFontSize: 20,
                                                style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                                              ),
                                              AutoSizeText(
                                                '${inr.formatCurrency(_orderDetails.order.invoiceOrderTotal[i].value.toDouble())}',
                                                minFontSize: 11,
                                                maxFontSize: 20,
                                                style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                                              ),
                                            ],
                                          )
                                        : Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              AutoSizeText(
                                                '${_orderDetails.order.invoiceOrderTotal[i].title}',
                                                minFontSize: 11,
                                                maxFontSize: 20,
                                                style: TextStyle(fontSize: width / 27, color: Colors.black87, fontWeight: FontWeight.bold),
                                              ),
                                              AutoSizeText(
                                                '${inr.formatCurrency(_orderDetails.order.invoiceOrderTotal[i].value.toDouble())}',
                                                minFontSize: 11,
                                                maxFontSize: 20,
                                                style: TextStyle(fontSize: width / 27, color: Colors.black87, fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          );
                                  },
                                )
                              : Container(),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   children: <Widget>[
                          //     AutoSizeText(
                          //       'Product Listed Price',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //     AutoSizeText(
                          //       '₹ 1250',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   children: <Widget>[
                          //     AutoSizeText(
                          //       'Selling Price',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //     AutoSizeText(
                          //       '₹ 1250',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   children: <Widget>[
                          //     AutoSizeText(
                          //       'Shipping Fee',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //     AutoSizeText(
                          //       '₹00',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   children: <Widget>[
                          //     AutoSizeText(
                          //       'Tax',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //     AutoSizeText(
                          //       '₹50',
                          //       minFontSize: 11,
                          //       maxFontSize: 20,
                          //       style: TextStyle(
                          //           fontSize: width / 27,
                          //           color: Colors.black54,
                          //           fontWeight: FontWeight.w300),
                          //     ),
                          //   ],
                          // ),
                        ],
                      ),
                    )
                  : Container(),

              //Grabbed Offers
              Container(
                margin: const EdgeInsets.only(bottom: 8.0),
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                decoration: containerBoxDecoration(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: AutoSizeText(
                        'Grabbed Offers',
                        minFontSize: 11,
                        maxFontSize: 20,
                        style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w300),
                      ),
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        AutoSizeText(
                          'Flat Discount',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                        ),
                        AutoSizeText(
                          '₹ 1250',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        AutoSizeText(
                          'GoCo Wallet Usage',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                        ),
                        AutoSizeText(
                          '₹ 1250',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 27, color: Colors.black54, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              //Download Invoice
              Container(
                decoration: containerBoxDecoration(),
                margin: const EdgeInsets.only(top: 5, bottom: 5),
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: InkWell(
                  onTap: () async {
                    var status = await Permission.storage.status;
                    if (status.isGranted) {}
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      AutoSizeText(
                        'Download Invoice',
                        minFontSize: 11,
                        maxFontSize: 20,
                        style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w300),
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.teal,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.file_download,
                          color: Colors.white,
                          size: width / 17,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              //need support
              Container(
                decoration: containerBoxDecoration(),
                margin: const EdgeInsets.only(bottom: 15),
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        AutoSizeText(
                          'Need Support ?',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 24, color: Colors.green, fontWeight: FontWeight.w600),
                        ),
                        AutoSizeText(
                          'Tap to ask',
                          minFontSize: 11,
                          maxFontSize: 20,
                          style: TextStyle(fontSize: width / 28, color: Colors.black54, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.device_unknown,
                        color: Colors.white,
                        size: width / 17,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  ListView productListing({@required double width, @required bool isProcessingProduct}) {
    // print(_orderDetails.order.shipmentGroupProducts);
    return ListView.builder(
        itemCount: isProcessingProduct ? 1 : _orderDetails.order.shipmentGroupProducts.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, i) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 0),
            child: InkWell(
              onTap: !isProcessingProduct
                  ? () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PackageDeliveryTrackingPage(
                              shipmentGroupProduct: _orderDetails.order.shipmentGroupProducts[i],
                              orderNnumber: _orderDetails.order.invoicePrefix + _orderDetails.order.invoiceNo.toString(),
                            ),
                          ));
                    }
                  : null,
              child: Card(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              !isProcessingProduct
                                  ? Container(
                                      width: width / 1.5,
                                      child: AutoSizeText(
                                        // "Shipment 1 : GODADAGDFKJSNCMFSKDFJ1",
                                        "${_orderDetails.order.shipmentGroupProducts[i].gocoShipmentCode}",
                                        minFontSize: 8,
                                        maxFontSize: 18,
                                        maxLines: 1,
                                        style: TextStyle(fontSize: width / 30, color: Colors.black, fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  : SizedBox(),
                              Row(
                                children: <Widget>[
                                  !isProcessingProduct
                                      ? Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: Colors.green[600],
                                          ),
                                          margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
                                          width: 10,
                                          height: 10,
                                        )
                                      : SizedBox(),
                                  // AutoSizeText(
                                  //   isProcessingProduct
                                  //       ? "Processing"
                                  //       : "${_orderDetails.order.shipmentGroupProducts[i].orderShipmentHistory[_orderDetails.order.shipmentGroupProducts[i].orderShipmentHistory.length - 1].name}",
                                  //   minFontSize: 8,
                                  //   maxFontSize: 18,
                                  //   maxLines: 1,
                                  //   style: TextStyle(
                                  //       fontSize: width / 30,
                                  //       color: Colors.black,
                                  //       fontWeight: FontWeight.bold),
                                  // ),
                                ],
                              ),
                              AutoSizeText(
                                !isProcessingProduct
                                    ? "Placed On ${DateFormat("dd MMMM yyyy, EEE").format(DateTime.parse(_orderDetails.order.dateAdded))}"
                                    : "Shipment status updated soon",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                maxFontSize: 18,
                                minFontSize: 8,
                                style: TextStyle(color: Colors.black54, fontSize: width / 33),
                              )
                            ],
                          ),
                          !isProcessingProduct
                              ? AutoSizeText(
                                  "View More >",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  maxFontSize: 18,
                                  minFontSize: 8,
                                  style: TextStyle(color: Colors.black54, fontSize: width / 27),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      //color: Colors.red,
                      height: width / 3.5,
                      padding: const EdgeInsets.only(bottom: 10),
                      width: width,
                      child: ListView.builder(
                        shrinkWrap: true,
                        // physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: isProcessingProduct
                            ? _orderDetails.order.processingProducts.length
                            : _orderDetails.order.shipmentGroupProducts[i].shipmentProducts.length,
                        itemBuilder: (BuildContext context, int index) {
                          //image
                          var imageCache = ocImageCache.getCachedImage(
                            path: isProcessingProduct
                                ? _orderDetails.order.processingProducts[index].image
                                : _orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.image,
                            imageQuality: ImageQuality.high,
                          );
                          String options = "";
                          if (isProcessingProduct) {
                            for (int k = 0; k < _orderDetails.order.processingProducts[index].option.length; k++) {
                              options = options + _orderDetails.order.processingProducts[index].option[k].value.toString();
                            }
                          } else {
                            //options
                            for (int k = 0; k < _orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.option.length; k++) {
                              options = options + _orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.option[k].value.toString();
                            }
                          }

                          return Container(
                            //   color: Colors.blue,
                            width: width / 1.4,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  // padding: EdgeInsets.all(10),
                                  // margin: EdgeInsets.only(left: 5),
                                  height: MediaQuery.of(context).size.width / 6,
                                  width: MediaQuery.of(context).size.width / 5,
                                  child: Image.network(
                                    "${_globalProvider.cdnImagePoint + "cache/" + imageCache}",
                                    fit: BoxFit.contain,
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.only(left: 4),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          // color: Colors.red,
                                          margin: const EdgeInsets.only(top: 8),
                                          width: MediaQuery.of(context).size.width / 1.7,
                                          child: AutoSizeText(
                                            isProcessingProduct
                                                ? "${_orderDetails.order.processingProducts[index].name}"
                                                : "${_orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.name}",
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 8,
                                            maxFontSize: 20,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 30, color: Colors.grey[700]),
                                          ),
                                        ),
                                        Container(
                                          //color: Colors.red,
                                          // margin: const EdgeInsets.only(top: 3),

                                          width: MediaQuery.of(context).size.width / 1.7,
                                          child: AutoSizeText(
                                            isProcessingProduct
                                                ? "${_orderDetails.order.processingProducts[index].model}"
                                                : "${_orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.model}",
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 8,
                                            maxFontSize: 20,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 30, color: Colors.grey[700]),
                                          ),
                                        ),
                                        options.length > 1
                                            ? Container(
                                                margin: const EdgeInsets.only(top: 5, right: 10),
                                                child: AutoSizeText(
                                                  "$options",
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  maxFontSize: 18,
                                                  minFontSize: 8,
                                                  style: TextStyle(color: Colors.black54, fontSize: width / 30),
                                                ),
                                              )
                                            : SizedBox(),
                                        Container(
                                          margin: const EdgeInsets.only(top: 3),
                                          width: MediaQuery.of(context).size.width / 1.8,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              AutoSizeText(
                                                isProcessingProduct
                                                    ? "${inr.formatCurrency(_orderDetails.order.processingProducts[index].price.toDouble())}"
                                                    : "${inr.formatCurrency(_orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.price.toDouble())}",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                maxFontSize: 18,
                                                minFontSize: 8,
                                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: width / 30),
                                              ),
                                              AutoSizeText(
                                                isProcessingProduct
                                                    ? _orderDetails.order.processingProducts[index].quantity > 1
                                                        ? " x${_orderDetails.order.processingProducts[index].quantity}"
                                                        : ""
                                                    : _orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.quantity > 1
                                                        ? " x${_orderDetails.order.shipmentGroupProducts[i].shipmentProducts[index].orderProduct.quantity}"
                                                        : "",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                maxFontSize: 18,
                                                minFontSize: 8,
                                                style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w700, fontSize: width / 30),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

//------widgets-----//

class ProductDetailWidget extends StatelessWidget {
  const ProductDetailWidget({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(5),
            height: width / 4,
            child: Image.network(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSkWF_N313FR2ok3YVCYN_L8gQfEzKMnEe4Ia3QzAz_Hvnu7FQ3&usqp=CAU",
              fit: BoxFit.contain,
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: Container(
            //padding: EdgeInsets.only(left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //product name
                AutoSizeText(
                  'Adidas Comfort Running shoes',
                  minFontSize: 11,
                  maxFontSize: 20,
                  style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w700),
                ),
                //manufacturer
                AutoSizeText(
                  'Adidas',
                  minFontSize: 10,
                  maxFontSize: 18,
                  style: TextStyle(
                    color: Color(0xff969798),
                    fontSize: width / 25,
                  ),
                ),
                AutoSizeText(
                  'Color,Size',
                  minFontSize: 10,
                  maxFontSize: 18,
                  style: TextStyle(
                    color: Color(0xff969798),
                    fontSize: width / 25,
                  ),
                ),
                //seller details
                AutoSizeText(
                  'Seller Details',
                  minFontSize: 10,
                  maxFontSize: 18,
                  style: TextStyle(
                    color: Color(0xff969798),
                    fontSize: width / 25,
                  ),
                ),

                //Amount
                AutoSizeText(
                  '₹ 999',
                  minFontSize: 10,
                  maxFontSize: 18,
                  style: TextStyle(color: Colors.black54, fontSize: width / 25, fontWeight: FontWeight.w700),
                ),

                //Rating Bar
              ],
            ),
            height: width / 3,

            // color: Colors.blue,
          ),
        )
      ],
    );
  }
}

//shipping status widget
class ShippingStatusWidget extends StatelessWidget {
  const ShippingStatusWidget({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  timeline() {
    List<Widget> tl = [];
    TimelineNodeLineType linetype(i) {
      if (i == 0)
        return TimelineNodeLineType.BottomHalf;
      else if (i == 1) return TimelineNodeLineType.TopHalf;

      return TimelineNodeLineType.Full;
    }

    for (int i = 0; i < 2; i++) {
      tl.add(TimelineNode(
        style: TimelineNodeStyle(lineType: linetype(i), pointType: TimelineNodePointType.Circle, lineColor: Colors.green, pointColor: Colors.green),
        child: Container(
          margin: const EdgeInsets.only(top: 20, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Ordered And Approved"),
              Text(
                "17 March 202,sunday",
                style: TextStyle(
                  color: Color(0xff969798),
                  fontSize: width / 30,
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return tl;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 15.0, bottom: 5),
          child: AutoSizeText(
            'Shipping status',
            minFontSize: 11,
            maxFontSize: 20,
            style: TextStyle(fontSize: width / 22, color: Colors.black54, fontWeight: FontWeight.w300),
          ),
        ),
        Divider(),
        //timeline
        Column(children: timeline())
      ],
    );
  }
}
