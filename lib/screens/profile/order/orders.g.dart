// part of 'orders.dart';

// class OrdersContent extends StatefulWidget {
//   const OrdersContent({Key key}) : super(key: key);

//   @override
//   _OrdersContentState createState() => _OrdersContentState();
// }

// class _OrdersContentState extends State<OrdersContent> {
//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.of(context).size.width;
//     return Container(
//       child: ListView.builder(
//           itemCount: 10,
//           itemBuilder: (context, index) {
//             return Container(
//               margin: const EdgeInsets.all(8.0),
//               padding: const EdgeInsets.all(8),
//               decoration: new BoxDecoration(
//                 color: Colors.white,
//                 boxShadow: [
//                   BoxShadow(
//                     color: Colors.grey,
//                     blurRadius: 5, // soften the shadow
//                     spreadRadius: .2, //extend the shadow
//                     offset: Offset(
//                       0.0, // Move to right 10  horizontally
//                       0.0, // Move to bottom 10 Vertically
//                     ),
//                   )
//                 ],
//               ),
//               child: InkWell(
//                 onTap: (){
//                   Navigator.push(context, MaterialPageRoute(builder: (context)=>OrderStatus()));
//                 },
//                 child: Padding(
//                   padding: const EdgeInsets.only(top:10.0,left: 5.0),
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       //order details and status
//                       Expanded(
//                           flex: 4,
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               AutoSizeText(
//                                 "Adidas Comfort Running Shoe",
//                                 minFontSize: 10,
//                                 maxFontSize: 19,
//                                 style: TextStyle(
//                                     fontSize: width / 24,
//                                     fontWeight: FontWeight.bold),
//                               ),

//                               //order id
//                               Container(
//                                 margin: const EdgeInsets.only(top: 5),
//                                 child: OrderDetailTile(
//                                   width: width,
//                                   iconColor: Colors.orange[800],
//                                   iconData: Icons.assignment,
//                                   title: "Order id: GOFWAD5463",
//                                   titleColor: Colors.grey[700],
//                                 ),
//                               ),
//                               // order status
//                               OrderDetailTile(
//                                 width: width,
//                                 iconColor: Colors.yellow[700],
//                                 iconData: Icons.flag,
//                                 title:
//                                     "Dispatched from Goco Fullfillment Center on December 02 2019",
//                                 titleColor: Colors.grey[700],
//                               ),
//                               //status
//                               Container(
//                                 margin: const EdgeInsets.only(bottom: 10),
//                                 child: OrderDetailTile(
//                                   width: width,
//                                   iconColor: Colors.yellow[900],
//                                   iconData: Icons.shopping_cart,
//                                   title: "Expected Delivery on 20 December 2019",
//                                   titleColor: Colors.green[800],
//                                   titleWeight: FontWeight.w700,
//                                 ),
//                               )
//                             ],
//                           )),

//                       //image and status
//                       Expanded(
//                         flex: 2,
//                         child: Padding(
//                           padding: const EdgeInsets.only(right:5.0),
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.end,
//                             children: <Widget>[
//                               Container(
//                                 padding: const EdgeInsets.symmetric(
//                                     horizontal: 5.0, vertical: 1),
//                                 decoration: BoxDecoration(
//                                     color: Colors.green,
//                                     borderRadius: BorderRadius.circular(5)),
//                                 child: AutoSizeText(
//                                   "Delivered",
//                                   minFontSize: 10,
//                                   maxFontSize: 19,
//                                   style: TextStyle(
//                                       fontSize: width / 28,
//                                       color: Colors.white,
//                                       fontWeight: FontWeight.w700),
//                                 ),
//                               ),

//                               Container(
//                                 margin: const EdgeInsets.only(top: 5),
//                                 width: double.infinity,
//                                 height: width / 4,
//                                 child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSkWF_N313FR2ok3YVCYN_L8gQfEzKMnEe4Ia3QzAz_Hvnu7FQ3&usqp=CAU", fit: BoxFit.contain,),
//                               ),
//                               //footer of image
//                               Container(
//                                 margin: const EdgeInsets.only(top:10.0,bottom: 10),
//                                 child: AutoSizeText(
//                                   "Rate & review",
//                                   minFontSize: 10,
//                                   maxFontSize: 19,
//                                   style: TextStyle(
//                                       fontSize: width / 35,
//                                       color: Colors.green,
//                                       fontWeight: FontWeight.w700),
//                                 ),
//                               )
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             );
//           }),
//     );
//   }
// }

// class OrderDetailTile extends StatelessWidget {
//   const OrderDetailTile(
//       {Key key,
//       @required this.width,
//       @required this.iconData,
//       @required this.iconColor,
//       @required this.title,
//       this.titleWeight,
//       @required this.titleColor})
//       : super(key: key);

//   final double width;
//   final IconData iconData;
//   final Color iconColor;
//   final String title;
//   final Color titleColor;
//   final FontWeight titleWeight;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: 7),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           Icon(
//             iconData,
//             color: iconColor,
//             size: width / 17,
//           ),
//           Container(
//             alignment: Alignment.centerLeft,
//             // color: Colors.red,
//             width: width / 1.95,
//             margin: EdgeInsets.only(left: 5),
//             child: AutoSizeText(
//               "$title",
//               minFontSize: 10,
//               maxFontSize: 19,
//               style: TextStyle(
//                   fontSize: width / 30,
//                   color: titleColor,
//                   fontWeight: titleWeight),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
