import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/models/get_order_details/ShipmentGroupProduct.dart';
import 'package:gocomartapp/models/get_order_details/logs.dart';
import 'package:gocomartapp/models/get_order_details/order_shipment_history.dart';
import 'package:intl/intl.dart';
import 'package:timelines/timelines.dart';

const kTileHeight = 50.0;
ShipmentGroupProduct _shipmentGroupProduct;
String _order_number;

class PackageDeliveryTrackingPage extends StatelessWidget {
  PackageDeliveryTrackingPage({shipmentGroupProduct, @required orderNnumber}) {
    _shipmentGroupProduct = shipmentGroupProduct;
    _order_number = orderNnumber;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text("${getTranslate('shipment_status')}"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: .1),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: header(width, context),
                ),
              ),
              Center(
                child: Container(
                  // width: 360.0,
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _DeliveryProcesses(),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget header(double width, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //order number
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              AutoSizeText(
                "${getTranslate('order_number')} : ",
                minFontSize: 8,
                maxFontSize: 18,
                maxLines: 1,
                style: TextStyle(
                    letterSpacing: .01,
                    fontSize: width / 30,
                    color: Colors.black,
                    fontWeight: FontWeight.w500),
              ),
              AutoSizeText(
                "$_order_number",
                minFontSize: 8,
                maxFontSize: 18,
                style: TextStyle(
                    fontSize: width / 30,
                    letterSpacing: -.1,
                    color: Theme.of(context).primaryColor),
              ),
            ],
          ),
          //shipment number
          Container(
            margin: EdgeInsets.only(top: 8),
            width: width / 1.5,
            child: AutoSizeText(
              "${getTranslate('shipment')} : ${_shipmentGroupProduct.gocoShipmentCode}",
              minFontSize: 8,
              maxFontSize: 18,
              maxLines: 1,
              style: TextStyle(
                  fontSize: width / 30,
                  letterSpacing: -.1,
                  color: Colors.black,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
  }
}

class _InnerTimeline extends StatelessWidget {
  const _InnerTimeline({
    @required this.logs,
  });

  final List<Logs> logs;

  @override
  Widget build(BuildContext context) {
    bool isEdgeIndex(int index) {
      return index == 0 || index == logs.length + 1;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: FixedTimeline.tileBuilder(
        theme: TimelineTheme.of(context).copyWith(
          nodePosition: 0,
          connectorTheme: TimelineTheme.of(context).connectorTheme.copyWith(
                thickness: 1.0,
              ),
          indicatorTheme: TimelineTheme.of(context)
              .indicatorTheme
              .copyWith(size: 10.0, position: 0.5, color: Colors.green),
        ),
        builder: TimelineTileBuilder(
          indicatorBuilder: (_, index) =>
              !isEdgeIndex(index) ? Indicator.dot(size: 10.0) : null,
          startConnectorBuilder: (_, index) => Connector.solidLine(),
          endConnectorBuilder: (_, index) => Connector.solidLine(),
          contentsBuilder: (_, index) {
            if (isEdgeIndex(index)) {
              return null;
            }

            return Padding(
              padding: EdgeInsets.only(left: 8.0, top: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(logs[index - 1].log.toString(),
                      style: TextStyle(fontSize: 13.0, color: Colors.black54)),
                  Text(
                    "${logs[index - 1].time != null ? DateFormat("MMM d, yyyy h:m a").format(DateTime.parse(logs[index - 1].time)) : " "}",
                    style: TextStyle(
                      fontSize: 11.0,
                    ),
                  ),
                ],
              ),
            );
          },
          itemExtentBuilder: (_, index) => isEdgeIndex(index) ? 10.0 : 80.0,
          nodeItemOverlapBuilder: (_, index) =>
              isEdgeIndex(index) ? true : null,
          itemCount: logs.length + 2,
        ),
      ),
    );
  }
}

class _DeliveryProcesses extends StatelessWidget {
  // const _DeliveryProcesses({Key key, this.processes})
  //     : assert(processes != null),
  //       super(key: key);

  // final List<_DeliveryProcess> processes;
  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(
        color: Color(0xff9b9b9b),
        fontSize: 12.5,
      ),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: FixedTimeline.tileBuilder(
          theme: TimelineThemeData(
            nodePosition: 0,
            color: Color(0xff989898),
            indicatorTheme: IndicatorThemeData(
              position: 0.01,
              size: 20.0,
            ),
            connectorTheme: ConnectorThemeData(
              thickness: 2.5,
            ),
          ),
          builder: TimelineTileBuilder.connected(
            connectionDirection: ConnectionDirection.before,
            itemCount: _shipmentGroupProduct.orderShipmentHistory.length + 1,
            contentsBuilder: (_, index) {
              if (index < _shipmentGroupProduct.orderShipmentHistory.length) {
                if (_shipmentGroupProduct.orderShipmentHistory[index].code ==
                    "DELIVERED")
                  return Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${getTranslate('delivered')}",
                          style: DefaultTextStyle.of(context).style.copyWith(
                                fontSize: 18.0,
                              ),
                        ),
                        Text(
                          "${_shipmentGroupProduct.orderShipmentHistory[index].time != null ? DateFormat("MMM d, yyyy h:m a").format(DateTime.parse(_shipmentGroupProduct.orderShipmentHistory[index].time)) : " "}",
                          style: TextStyle(
                            fontSize: 11.0,
                          ),
                        ),
                      ],
                    ),
                  );

                return Padding(
                  padding: EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _shipmentGroupProduct
                                .orderShipmentHistory[index].name,
                            style: DefaultTextStyle.of(context).style.copyWith(
                                  fontSize: 16.0,
                                ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 3.0),
                            child: Text(
                              "${_shipmentGroupProduct.orderShipmentHistory[index].time != null ? DateFormat("MMM d, yyyy h:m a").format(DateTime.parse(_shipmentGroupProduct.orderShipmentHistory[index].time)) : " "}",
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                      _shipmentGroupProduct
                                  .orderShipmentHistory[index].logs.length >
                              1
                          ? _InnerTimeline(
                              logs: _shipmentGroupProduct
                                  .orderShipmentHistory[index].logs)
                          : SizedBox(
                              height: 40,
                            ),
                    ],
                  ),
                );
              } else if (_shipmentGroupProduct
                      .orderShipmentHistory[
                          _shipmentGroupProduct.orderShipmentHistory.length - 1]
                      .code !=
                  "DELIVERED") {
                return Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${getTranslate('delivered')}",
                        style: DefaultTextStyle.of(context)
                            .style
                            .copyWith(fontSize: 16.0, color: Colors.grey),
                      ),
                      Text(
                        getTranslate('est_delivery_on') +
                            " : ${_shipmentGroupProduct.shipmentProducts[0].orderProduct.estDelivery != null ? DateFormat("MMM d, yyyy h:m a").format(DateTime.parse(_shipmentGroupProduct.shipmentProducts[0].orderProduct.estDelivery)) : " "}",
                        style: TextStyle(
                          fontSize: 11.0,
                        ),
                      ),
                    ],
                  ),
                );
              }
            },
            indicatorBuilder: (_, index) {
              if (index < _shipmentGroupProduct.orderShipmentHistory.length) {
                if (_shipmentGroupProduct.orderShipmentHistory[index].code ==
                    "DELIVERED") {
                  return DotIndicator(
                    color: Color(0xff66c97f),
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                      size: 12.0,
                    ),
                  );
                } else {
                  return DotIndicator(
                    // size: 2.5,
                    size: 15.0,
                    color: Color(0xff66c97f),
                    // borderWidth: 2.5,
                  );
                }
              } else if (_shipmentGroupProduct
                      .orderShipmentHistory[
                          _shipmentGroupProduct.orderShipmentHistory.length - 1]
                      .code !=
                  "DELIVERED") {
                return DotIndicator(
                  color: Colors.grey[300],
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 12.0,
                  ),
                );
              }
            },
            connectorBuilder: (_, index, ___) {
              if (index < _shipmentGroupProduct.orderShipmentHistory.length) {
                return SolidLineConnector(
                  color:
                      _shipmentGroupProduct.orderShipmentHistory[index].code ==
                              "DELIVERED"
                          ? Color(0xff66c97f)
                          : Color(0xff66c97f),
                );
              } else if (_shipmentGroupProduct
                      .orderShipmentHistory[
                          _shipmentGroupProduct.orderShipmentHistory.length - 1]
                      .code !=
                  "DELIVERED") {
                return SolidLineConnector(
                  color: Colors.grey[300],
                );
              }
            },
          ),
        ),
      ),
    );
  }
}

class _OrderInfo {
  const _OrderInfo({
    @required this.id,
    @required this.date,
    @required this.deliveryProcesses,
  });

  final int id;
  final DateTime date;
  final List<_DeliveryProcess> deliveryProcesses;
}

class _DriverInfo {
  const _DriverInfo({
    @required this.name,
    this.thumbnailUrl,
  });

  final String name;
  final String thumbnailUrl;
}

class _DeliveryProcess {
  const _DeliveryProcess(
    this.name, {
    this.messages = const [],
  });

  const _DeliveryProcess.complete()
      : this.name = 'Done',
        this.messages = const [];

  final String name;
  final List<_DeliveryMessage> messages;

  bool get isCompleted => name == 'Done';
}

class _DeliveryMessage {
  const _DeliveryMessage(this.createdAt, this.message);

  final String createdAt; // final DateTime createdAt;
  final String message;

  @override
  String toString() {
    return '$createdAt $message';
  }
}
