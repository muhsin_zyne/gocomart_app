import 'package:flutter/material.dart';

import 'country_list.dart';

class CountryPickerCustom extends StatefulWidget {
  final Function onChanged;
  CountryPickerCustom({
    Key key,
    @required this.onChanged,
  }) : super(key: key);

  @override
  _CountryPickerCustomState createState() => _CountryPickerCustomState();
}

class _CountryPickerCustomState extends State<CountryPickerCustom> {
  List<PopupMenuItem<String>> countryList = [];
  List<dynamic> rawList = Country.ALL;
  Country selected = Country.IN;
  @override
  void didChangeDependencies() {
    this.loadList();
    super.didChangeDependencies();
  }

  void loadList() {
    rawList.forEach((element) {
      Country data = element;
      countryList.add(
        PopupMenuItem(
          value: data.code,
          child: Row(
            children: <Widget>[
              Container(
                child: Container(
                  width: 40,
                  child: Image.asset(
                    data.asset,
                    width: 40,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                height: 20,
                child: Text(
                  data.code,
                  style: TextStyle(fontSize: 16, color: Colors.black54),
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  updateSelected(String code) {
    setState(() {
      selected = rawList.firstWhere((element) {
        Country data = element;
        return data.code == code;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PopupMenuButton(
        onSelected: (selected) {
          updateSelected(selected);
          this.widget.onChanged(selected);
        },
        child: Container(
          //decoration: myBoxDecoration(),
          padding: EdgeInsets.all(5),
          child: Center(
            child: Container(
              width: 50,
              child: Image.asset(
                selected.asset,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        itemBuilder: (BuildContext context) => countryList,
      ),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        width: 1,
      ),
    );
  }
}
