import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:gocomartapp/theme/theme_const.dart';

class ResentCodeBlock extends StatefulWidget {
  ResentCodeBlock({Key key, this.otpVerificationProgress, this.loginScreenRequest}) : super(key: key);
  final bool otpVerificationProgress;
  final LoginScreenRequest loginScreenRequest;

  @override
  _ResentCodeBlockState createState() => _ResentCodeBlockState();
}

class _ResentCodeBlockState extends State<ResentCodeBlock> {
  Timer timer;
  int start = 30;
  int _otpAttemptCount = 0;
  AccountServices _accountServices;
  @override
  void initState() {
    super.initState();
    _accountServices = AccountServices(context: context);
    this._loadClient();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    this.startTimer();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (start < 1) {
          timer.cancel();
        } else {
          setState(
            () {
              start--;
            },
          );
        }
      },
    );
  }

  _resentCode() async {
    if (_otpAttemptCount >= 2) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          'Please check your mobile number!',
          style: kToastMessage,
        ),
        duration: Duration(seconds: 3),
      ));
      await new Future.delayed(const Duration(seconds: 2));
      Navigator.of(context).pop();
    }
    _otpAttemptCount++;
    // resent Query request gose here
    start = 30;
    startTimer();
    //QueryResult result = await _accountServices.actionLogin(this.widget.loginRequestData);
    //LoginResponse loginResponse = LoginResponse.fromJson(result.data['login']); // ignore: unused_local_variable
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          this.widget.otpVerificationProgress == false
              ? AutoSizeText(
                  "Did'nt Recive the OTP?",
                  maxLines: 3,
                  maxFontSize: 20,
                  minFontSize: 16,
                  textAlign: TextAlign.center,
                  style: kGeneralText.copyWith(color: Colors.black),
                )
              : AutoSizeText(
                  "Verifying OTP",
                  maxLines: 3,
                  maxFontSize: 20,
                  minFontSize: 16,
                  textAlign: TextAlign.center,
                  style: kGeneralText.copyWith(color: Colors.black),
                ),
          this.widget.otpVerificationProgress == false
              ? FlatButton(
                  onPressed: () {
                    _resentCode();
                  },
                  child: start < 1
                      ? AutoSizeText(
                          "Resend go-co code",
                          maxLines: 3,
                          maxFontSize: 20,
                          minFontSize: 16,
                          textAlign: TextAlign.center,
                          style: kGeneralText.copyWith(color: Colors.blue),
                        )
                      : AutoSizeText(
                          "$start",
                          maxLines: 3,
                          maxFontSize: 20,
                          minFontSize: 16,
                          textAlign: TextAlign.center,
                          style: kGeneralText.copyWith(color: Colors.blue),
                        ),
                )
              : Container(),
        ],
      ),
    );
  }
}
