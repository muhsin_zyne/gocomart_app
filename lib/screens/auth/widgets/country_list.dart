import 'package:flutter/material.dart';

class Country {
  final String asset;
  final String code;
  const Country({
    @required this.asset,
    @required this.code,
  });

  static const Country AE = Country(asset: "assets/images/flags/ae_flag.png", code: "+971");
  static const Country IN = Country(asset: "assets/images/flags/in_flag.png", code: "+91");

  static const ALL = <Country>[IN, AE, IN];
}
