import 'package:audioplayers/audio_cache.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/controllers/flare_slomotion_controller.dart';
import 'package:gocomartapp/models/order/OrderInfo.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/screens/profile/order/order_status.dart';
import 'package:gocomartapp/src/screens/wallet/cashwallet/cash_wallet_rewards.dart';
import 'package:page_transition/page_transition.dart';

class OrderConfirmationScreen extends StatefulWidget {
  final OrderInfo orderInfo;

  const OrderConfirmationScreen({Key key, this.orderInfo}) : super(key: key);
  @override
  _OrderConfirmationScreenState createState() => _OrderConfirmationScreenState();
}

class _OrderConfirmationScreenState extends State<OrderConfirmationScreen> {
  SlowMoController _controller = SlowMoController("order_success", speed: 3);
  bool animationCompleted = false;
  bool hideAnimationBlock = false;
  @override
  void initState() {
    super.initState();
    this._init();
  }

  void _init() async {
    await Future.delayed(Duration(milliseconds: 800));
    final player = AudioCache();
    player.play('notification/success.mp3');
    setState(() {
      animationCompleted = true;
    });
    await Future.delayed(Duration(milliseconds: 1500));
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.bottomToTop,
        child: OrderQuickInfoScreen(
          orderInfo: this.widget.orderInfo,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: hideAnimationBlock == false
          ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 200,
                    child: Hero(
                      tag: 'order-placed-success-img',
                      child: FlareActor(
                        "assets/animation/order_02.flr",
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        controller: _controller,
                      ),
                    ),
                  ),
                  Hero(
                    tag: 'order-placed-text',
                    child: AnimatedDefaultTextStyle(
                      style: animationCompleted
                          ? TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 24)
                          : TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.normal, fontSize: 0),
                      duration: const Duration(milliseconds: 200),
                      child: AutoSizeText(
                        'Order Placed'.toUpperCase(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Container(),
    );
  }
}

class OrderQuickInfoScreen extends StatefulWidget {
  final OrderInfo orderInfo;
  const OrderQuickInfoScreen({Key key, this.orderInfo}) : super(key: key);

  @override
  _OrderQuickInfoScreenState createState() => _OrderQuickInfoScreenState();
}

class _OrderQuickInfoScreenState extends State<OrderQuickInfoScreen> {
  CurrencyFormat inr = CurrencyConst.inrFormat2;

  @override
  Widget build(BuildContext context) {
    print(this.widget.orderInfo.invoiceNo);
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   title: Text(
      //     getTranslate('order_info'),
      //     style: TextStyle(
      //       color: Theme.of(context).primaryColor,
      //     ),
      //   ),
      //   iconTheme: IconThemeData(
      //     color: Theme.of(context).primaryColor, //change your color here
      //   ),
      // ),
      body: SafeArea(
        child: WillPopScope(
          onWillPop: () {
            return Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (Route<dynamic> route) => false);
          },
          child: Container(
            child: Stack(
              children: [
                Container(
                  height: double.maxFinite,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/pPoper.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  color: Colors.transparent,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 30,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Hero(
                                      tag: 'order-placed-success-img',
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          CircleAvatar(
                                            radius: 52,
                                            backgroundColor: Colors.green,
                                          ),
                                          CircleAvatar(
                                            radius: 50,
                                            backgroundColor: Colors.white,
                                            child: Icon(
                                              Icons.check,
                                              color: Colors.green,
                                              size: 90,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Hero(
                                      tag: 'order-placed-text',
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: AutoSizeText(
                                          getTranslate('order_placed').toUpperCase(),
                                          minFontSize: 16,
                                          maxFontSize: 18,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Product-Sans',
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: AutoSizeText(
                                      '${this.widget.orderInfo.orderBenefitQuickInfo.totalItems} ' +
                                          '${(this.widget.orderInfo.orderBenefitQuickInfo.totalItems > 1) ? getTranslate('items') : getTranslate('item')} | ' +
                                          '${this.widget.orderInfo.orderBenefitQuickInfo.orderPayAmount} ${getTranslate('rupees')}  ' +
                                          '${(this.widget.orderInfo.orderPayment.isCOD == true) ? getTranslate('cod') : getTranslate('paid')} | ' +
                                          '${getTranslate('est_delivery_on')}  ${dateFormat(dateTime: this.widget.orderInfo.estDelivery)}',
                                      minFontSize: 14,
                                      maxFontSize: 16,
                                      maxLines: 3,
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                        this.widget.orderInfo.orderBenefitQuickInfo.isHavingDiscount
                            ? Container(
                                padding: EdgeInsets.all(20),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                                        decoration: BoxDecoration(
                                          color: Colors.green.withOpacity(.2),
                                          // borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Expanded(
                                                  flex: 3,
                                                  child: SizedBox(
                                                    width: 60,
                                                    height: 60,
                                                    child: SvgPicture.asset(
                                                      'assets/images/icons/price-tag.svg',
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(),
                                                ),
                                                Expanded(
                                                  flex: 9,
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        this.widget.orderInfo.orderBenefitQuickInfo.walletUsed > 0
                                                            ? AutoSizeText(
                                                                ' ${getTranslate('wallet_discount')} -  ${inr.formatCurrency(this.widget.orderInfo.orderBenefitQuickInfo.walletUsed.toDouble())}',
                                                                minFontSize: 16,
                                                                maxFontSize: 20,
                                                                maxLines: 1,
                                                                style: TextStyle(
                                                                  color: Theme.of(context).primaryColor,
                                                                  fontWeight: FontWeight.bold,
                                                                  fontFamily: 'Product-Sans',
                                                                ),
                                                              )
                                                            : Container(),
                                                        (this.widget.orderInfo.orderBenefitQuickInfo.walletExclusiveDiscount > 0)
                                                            ? AutoSizeText(
                                                                '${getTranslate('exclusive_discount')}  -  ${inr.formatCurrency(this.widget.orderInfo.orderBenefitQuickInfo.walletExclusiveDiscount.toDouble())}',
                                                                minFontSize: 16,
                                                                maxFontSize: 20,
                                                                maxLines: 2,
                                                                style: TextStyle(
                                                                  color: Theme.of(context).primaryColor,
                                                                  fontWeight: FontWeight.bold,
                                                                  fontFamily: 'Product-Sans',
                                                                ),
                                                              )
                                                            : Container(),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        AutoSizeText(
                                                          getTranslate('you_save'),
                                                          style: TextStyle(
                                                            color: Colors.deepOrange,
                                                            fontWeight: FontWeight.bold,
                                                            fontFamily: 'Product-Sans',
                                                          ),
                                                        ),
                                                        Row(
                                                          crossAxisAlignment: CrossAxisAlignment.end,
                                                          children: [
                                                            this.widget.orderInfo.orderBenefitQuickInfo.isHavingDiscount
                                                                ? AutoSizeText(
                                                                    inr.formatCurrency((this.widget.orderInfo.orderBenefitQuickInfo.walletExclusiveDiscount +
                                                                            this.widget.orderInfo.orderBenefitQuickInfo.walletUsed)
                                                                        .toDouble()),
                                                                    minFontSize: 24,
                                                                    maxFontSize: 28,
                                                                    style: TextStyle(
                                                                      color: Colors.deepOrange,
                                                                      fontWeight: FontWeight.bold,
                                                                    ),
                                                                  )
                                                                : Container(),
                                                            Container(
                                                              margin: EdgeInsets.only(bottom: 2),
                                                              child: AutoSizeText(
                                                                getTranslate('on_this_order'),
                                                                style: TextStyle(
                                                                  color: Colors.deepOrange,
                                                                  fontWeight: FontWeight.bold,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                                  decoration: BoxDecoration(
                                    color: Colors.green.withOpacity(.2),
                                    //borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Column(
                                    children: [
                                      InkWell(
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Expanded(
                                              flex: 3,
                                              child: SizedBox(
                                                width: 60,
                                                height: 60,
                                                child: SvgPicture.asset(
                                                  'assets/images/icons/giftbox.svg',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: Container(),
                                            ),
                                            Expanded(
                                              flex: 9,
                                              child: Container(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: [
                                                        Expanded(
                                                          child: Container(
                                                            child: AutoSizeText(
                                                              getTranslate('erned_a_goco_gift_card'),
                                                              minFontSize: 14,
                                                              maxFontSize: 18,
                                                              maxLines: 3,
                                                              style: TextStyle(
                                                                color: Colors.deepOrange,
                                                                fontWeight: FontWeight.bold,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => CashWalletRewardsScreen(),
                                            ),
                                          );
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          margin: EdgeInsets.only(top: 20),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: RaisedButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => OrderStatus(
                                              orderRequestGUID: this.widget.orderInfo.orderRequestGUID,
                                            ),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).primaryColor,
                                      textColor: Colors.white,
                                      child: Container(
                                        padding: EdgeInsets.all(15),
                                        child: AutoSizeText(
                                          'Order Details',
                                          style: TextStyle(
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: RaisedButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (Route<dynamic> route) => false);
                                      },
                                      color: Colors.deepOrange,
                                      textColor: Colors.white,
                                      child: Container(
                                        padding: EdgeInsets.all(15),
                                        child: AutoSizeText(
                                          'Shop More',
                                          style: TextStyle(
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
