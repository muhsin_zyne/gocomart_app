part of 'checkout_screen.dart';

dynamic sel;

class CheckOutScreenContent extends StatefulWidget {
  final Function() notifyParent;
  final bool isCompletedOrder;
  const CheckOutScreenContent({Key key, this.notifyParent, this.isCompletedOrder}) : super(key: key);

  @override
  _CheckOutScreenContentState createState() => _CheckOutScreenContentState();
}

class _CheckOutScreenContentState extends State<CheckOutScreenContent> {
  // ignore: non_constant_identifier_names
  CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  GlobalProvider _globalProvider;
  CartProvider _cartProvider;
  CheckOutServices _checkOutServices;
  AddressProvider _addressProvider;
  bool isReady = false;

  // models

  @override
  void didChangeDependencies() {
    _addressProvider = Provider.of<AddressProvider>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _checkOutServices = CheckOutServices(context: context);
    this._loadClient();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _globalProvider = Provider.of<GlobalProvider>(context);
    _cartProvider = Provider.of<CartProvider>(context);
    this._init();
  }

  _init() async {
    if (this.widget.isCompletedOrder == false) {
      this._loadPaymentOptions();
    }
  }

  _loadPaymentOptions() async {
    var addressId = _addressProvider.currentAddress.address_id;

    QueryResult paymentOptions = await _checkOutServices.getPaymentOption(addressId: addressId);
    if (!paymentOptions.hasException) {
      print(paymentOptions.data);
      setState(() {
        if (paymentOptions.data['codAvailability']['availabity'] == true) {
          _cartProvider.codEnabled = true;
        } else {
          _cartProvider.codEnabled = false;
        }
        _cartProvider.getPaymentOption = GetPaymentOption.fromJson(paymentOptions.data['getPaymentOption']);
        _cartProvider.getPaymentOptionGroup = GetPaymentOptionGroup.fromJson(paymentOptions.data['getPaymentOptionGroup']);
        _cartProvider.getUserPaymentOptions = GetUserPaymentOptions.fromJson(paymentOptions.data['getUserPaymentOptions']);
        _cartProvider.pTPC = ProceedToPaymentCalculation.fromJson(paymentOptions.data['proceedToPaymentCalculation']);
        if (_cartProvider.getPaymentOption.paymentOption.length > 0) {
          setState(() {
            this.isReady = true;
          });
        }
      });
    }
  }

  _applyGoCoWallet() {
    setState(() {
      _cartProvider.togglePTCPWallet();
    });
    this.widget.notifyParent();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (isReady) {
      return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  children: [
                    WalletApplyBlock(width: width),
                    _cartProvider.pTPC.walletUsed
                        ? PriceMetaLabel(
                            width: width,
                            labelColor: Colors.black45,
                            labelItem: "Total Order",
                            labelValue: "${inr.formatCurrency(_cartProvider.pTPC?.totalPayableWithOutOffer?.toDouble())}",
                          )
                        : Container(),
                    _cartProvider.pTPC.walletUsed
                        ? PriceMetaLabel(
                            width: width,
                            labelColor: Colors.green,
                            labelItem: "Wallet Applied Discount",
                            labelValue: "${inr.formatCurrency(_cartProvider.pTPC.paymentInfo.wallet_discount.toDouble())}",
                          )
                        : Container(),
                    _cartProvider.pTPC.exclusiveDiscountUsed
                        ? PriceMetaLabel(
                            width: width,
                            labelColor: Colors.green,
                            labelItem: "Wallet Exclusive discount",
                            labelValue: "${inr.formatCurrency(_cartProvider.pTPC.paymentInfo.walletExclusiveDiscount.toDouble())}",
                          )
                        : Container(),
                    Divider(),
                    PriceMetaLabel(
                      width: width,
                      labelColor: Colors.black45,
                      fontWeight: FontWeight.bold,
                      labelItem: "Total Order Payable",
                      labelValue: "${inr.formatCurrency(_cartProvider.pTPC?.netPay?.toDouble())}",
                    ),
                    _cartProvider.pTPC.orderTotalSavings > 0
                        ? Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                AutoSizeText(
                                  'You save ${inr.formatCurrency(_cartProvider.pTPC.orderTotalSavings.toDouble())} in this order',
                                  style: TextStyle(
                                    color: Colors.black45,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    Divider()
                  ],
                ),
              ),
              _cartProvider.pTPC.freeOrder == false
                  ? Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          AutoSizeText(
                            'CHOSE YOUR PAIMENT MODE',
                            presetFontSizes: [17, 16, 15, 14],
                            style: TextStyle(
                              color: Colors.black45,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(),
              _cartProvider.pTPC.freeOrder == false
                  ? ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _cartProvider.getUserPaymentOptions.userPaymentOptions.length + 1,
                      itemBuilder: (context, index) {
                        if (index == _cartProvider.getUserPaymentOptions.userPaymentOptions.length) {
                          return ListTile(
                            onTap: _addPaymentMethod,
                            leading: Icon(
                              FontAwesomeIcons.plus,
                              color: Theme.of(context).primaryColor,
                            ),
                            title: AutoSizeText(
                              "Add payment method",
                              style: TextStyle(
                                fontSize: width / 22,
                                fontWeight: FontWeight.w300,
                                color: Theme.of(context).primaryColor,
                              ),
                              maxFontSize: 26,
                            ),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              color: Theme.of(context).primaryColor,
                            ),
                          );
                        } else {
                          UserPaymentOption cPaymentOption = _cartProvider.getUserPaymentOptions.userPaymentOptions[index];
                          var paymentLogo = _globalProvider.cdnPoint + 'payment_option/' + cPaymentOption.paymentOption.logo;
//                        var cartProviderPaymentOptionId = 0;
//                        if (_cartProvider.currentPaymentOption != null) {
//                          if (_cartProvider.currentPaymentOption.payment_option_id != null) {
//                            cartProviderPaymentOptionId = _cartProvider.currentPaymentOption.payment_option_id;
//                          }
//                        }
                          var codDisabled = false;
                          if (cPaymentOption.paymentOption.code == 'cod' && _cartProvider.codEnabled == false) {
                            codDisabled = true;
                          }

                          return Column(
                            children: <Widget>[
                              ListTile(
                                onTap: () {
                                  if (codDisabled == false) {
                                    setState(() {
                                      this._cartProvider.currentPaymentOption = cPaymentOption.paymentOption;
                                    });
                                    this.widget.notifyParent();
                                  }
                                },
                                leading: Container(
                                  width: 30,
                                  height: 30,
                                  child: Image.network(paymentLogo),
                                ),
                                title: AutoSizeText(
                                  cPaymentOption.paymentOption.app_label,
                                  style: TextStyle(
                                    fontSize: width / 22,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black54,
                                  ),
                                  maxFontSize: 26,
                                ),
                                trailing: Radio(
                                  groupValue: cPaymentOption.paymentOption.payment_option_id,
                                  value: _cartProvider.currentPaymentOption?.payment_option_id ?? 0,
                                  onChanged: (value) {
                                    if (codDisabled == false) {
                                      setState(() {
                                        this._cartProvider.currentPaymentOption = cPaymentOption.paymentOption;
                                      });
                                      this.widget.notifyParent();
                                    }
                                  },
                                ),
                                subtitle: codDisabled
                                    ? Text(
                                        'Cash on Delivery is not available with your address',
                                        style: TextStyle(
                                          color: Colors.red.withGreen(3),
                                        ),
                                      )
                                    : null,
                              ),
                              Divider()
                            ],
                          );
                        }
                      },
                    )
                  : Container(),
              SizedBox(
                height: 120,
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  void _addPaymentMethod() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChoosePaymentOptionScreen(),
      ),
    );
  }

  void trigger() {}
}
