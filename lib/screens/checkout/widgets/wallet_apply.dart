import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:provider/provider.dart';

class WalletApplyBlock extends StatefulWidget {
  final double width;

  const WalletApplyBlock({Key key, this.width}) : super(key: key);

  @override
  _WalletApplyBlockState createState() => _WalletApplyBlockState();
}

class _WalletApplyBlockState extends State<WalletApplyBlock> {
  CartProvider _cartProvider;

  @override
  void didChangeDependencies() {
    this._cartProvider = Provider.of<CartProvider>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            onTap: () {
              this._cartProvider.togglePTCPWallet();
            },
            leading: Container(
              child: SizedBox(
                height: this.widget.width / 10,
                width: this.widget.width / 10,
                child: SvgPicture.asset(
                  'assets/images/icons/icons8-wallet.svg',
                  fit: BoxFit.contain,
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  (_cartProvider?.pTPC?.walletUsed ?? false) ? 'GOCO Wallet' : 'Apply GOCO Wallet',
                  style: TextStyle(
                    color: Color(
                      0xff6b6b6b,
                    ),
                    fontFamily: 'Product-Sans',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                AutoSizeText(
                  'Balance  ${inr.formatCurrency(_cartProvider.pTPC.netWalletBalance)}',
                  style: TextStyle(
                    color: Color(
                      0xff6b6b6b,
                    ),
                    fontFamily: 'Product-Sans',
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
            subtitle: ((_cartProvider.pTPC.walletUsed == false) && (_cartProvider.pTPC.walletNotUsable == false))
                ? AutoSizeText(
                    'You can use ${inr.formatCurrency(_cartProvider.pTPC?.paymentInfo?.wallet_discount?.toDouble() ?? 0.0)} wallet amount for this order')
                : _cartProvider.pTPC.paymentInfo.wallet_discount.toDouble() > 0
                    ? AutoSizeText(
                        'Wallet Applied',
                        style: TextStyle(
                          fontSize: this.widget.width / 22,
                          color: Colors.green,
                        ),
                      )
                    : AutoSizeText(
                        'Wallet is not Usable',
                        style: TextStyle(
                          fontSize: this.widget.width / 22,
                          color: Colors.red,
                        ),
                      ),
            trailing: Checkbox(
              value: (this._cartProvider?.pTPC?.walletUsed) ?? false,
              onChanged: (value) {
                this._cartProvider.togglePTCPWallet();
              },
            ),
          ),
          _cartProvider.pTPC.exclusiveDiscountUsable == true
              ? ListTile(
                  leading: Container(
                    child: SizedBox(
                      height: this.widget.width / 10,
                      width: this.widget.width / 10,
                      child: SvgPicture.asset(
                        'assets/images/icons/icons8-cash-in-hand.svg',
                        //fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  title: AutoSizeText(
                    'Try your wallet exclusive discount today',
                    style: TextStyle(
                      color: Color(
                        0xff6b6b6b,
                      ),
                      fontFamily: 'Product-Sans',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: AutoSizeText('This discount is only available by today!'),
                  trailing: Checkbox(
                    value: _cartProvider.pTPC.exclusiveDiscountUsed,
                    onChanged: (value) {
                      _cartProvider.toggleExclusiveWallet();
                    },
                  ),
                  onTap: () {
                    _cartProvider.toggleExclusiveWallet();
                  },
                )
              : Container(),
          Divider(),
        ],
      ),
    );
  }
}
