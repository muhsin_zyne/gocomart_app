import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/in_app_notification.dart';
import 'package:gocomartapp/models/checkout/getUserPaymentOptions.dart';
import 'package:gocomartapp/models/checkout/paymentOption.dart';
import 'package:gocomartapp/models/checkout/paymentOptionGroup.dart';
import 'package:gocomartapp/models/checkout/response.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/checkout_services.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
part 'choose_payment_screen.g.dart';

class ChoosePaymentOptionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Options'),
      ),
      body: ChoosePaymentOptionContent(),
    );
  }
}
