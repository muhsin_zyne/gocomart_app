part of 'choose_payment_screen.dart';

class ChoosePaymentOptionContent extends StatefulWidget {
  @override
  _ChoosePaymentOptionContentState createState() => _ChoosePaymentOptionContentState();
}

class _ChoosePaymentOptionContentState extends State<ChoosePaymentOptionContent> {
  CartProvider _cartProvider;
  GlobalProvider _globalProvider;
  CheckOutServices _checkOutServices;
  bool isReady = false;
  @override
  void initState() {
    super.initState();
    _checkOutServices = CheckOutServices(context: context);
    this._loadClient();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _cartProvider = Provider.of<CartProvider>(context);
    _globalProvider = Provider.of<GlobalProvider>(context);
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      this.isReady = true;
    });
  }

  _addUserPaymentOption(PaymentOption currentPaymentOption) async {
    QueryResult result = await _checkOutServices.addNewPaymentOption(paymentOptionId: currentPaymentOption.payment_option_id);
    Response response = Response.fromJson(result.data['addNewPaymentOption']);
    QueryResult userPaymentOptions = await _checkOutServices.getUserPaymentOptions();
    if (!userPaymentOptions.hasException) {
      setState(() {
        _cartProvider.getUserPaymentOptions = GetUserPaymentOptions.fromJson(userPaymentOptions.data['getUserPaymentOptions']);
      });
    }
    currentPaymentOption.isLoading = false;
    InAppNotificator inAppNotificator = new InAppNotificator();
    inAppNotificator.showInAppNotification(
        color: (response.success != null ? Colors.green : Colors.red), message: response.success != null ? response.success : response.error);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return isReady
        ? SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _cartProvider.getPaymentOptionGroup.paymentOptionGroup.length,
                    itemBuilder: (context, i) {
                      PaymentOptionGroup currentOptionGroup = _cartProvider.getPaymentOptionGroup.paymentOptionGroup[i];

                      return Container(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                    child: AutoSizeText(
                                      currentOptionGroup.group_name,
                                      style: TextStyle(
                                        fontSize: width / 19.5,
                                        fontWeight: FontWeight.w300,
                                        color: Colors.black54,
                                      ),
                                      maxFontSize: 26,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: currentOptionGroup.paymentOptions.length,
                              itemBuilder: (context, optionIndex) {
                                PaymentOption currentPaymentOption = currentOptionGroup.paymentOptions[optionIndex];
                                var paymentLogo = _globalProvider.cdnPoint + 'payment_option/' + currentPaymentOption.logo;
//                                var cartProviderPaymentOptionId = 0;
//                                if (_cartProvider.currentPaymentOption != null) {
//                                  if (_cartProvider.currentPaymentOption.payment_option_id != null) {
//                                    cartProviderPaymentOptionId = _cartProvider.currentPaymentOption.payment_option_id;
//                                  }
//                                }
                                return Column(
                                  children: <Widget>[
                                    ListTile(
                                      onTap: () {
                                        setState(() {
                                          currentPaymentOption.isLoading = true;
                                        });
                                        this._addUserPaymentOption(currentPaymentOption);
                                      },
                                      leading: Container(
                                        width: 30,
                                        height: 30,
                                        child: Image.network(paymentLogo),
                                      ),
                                      title: AutoSizeText(
                                        currentPaymentOption.app_label,
                                        style: TextStyle(
                                          fontSize: width / 22,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.black54,
                                        ),
                                        maxFontSize: 26,
                                      ),
                                      trailing: currentPaymentOption.isLoading != true
                                          ? Icon(
                                              Icons.arrow_forward_ios,
                                              size: 20,
                                            )
                                          : CircularProgressIndicator(),
                                    ),
                                    Divider()
                                  ],
                                );
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          )
        : Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
