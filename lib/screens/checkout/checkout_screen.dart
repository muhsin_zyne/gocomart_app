import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/components/widgets/price_meta_label.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/models/checkout/getPaymentOption.dart';
import 'package:gocomartapp/models/checkout/getPaymentOptionGroup.dart';
import 'package:gocomartapp/models/checkout/getUserPaymentOptions.dart';
import 'package:gocomartapp/models/checkout/userPaymentOption.dart';
import 'package:gocomartapp/models/order/RequestOrder.dart';
import 'package:gocomartapp/providers/address_provider.dart';
import 'package:gocomartapp/providers/cart_provider.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/checkout/choose_payment_screen.dart';
import 'package:gocomartapp/screens/checkout/order_confimation_screen.dart';
import 'package:gocomartapp/screens/checkout/widgets/wallet_apply.dart';
import 'package:gocomartapp/screens/widgets/buying_loading%20_animation.dart';
import 'package:gocomartapp/services/checkout_services.dart';
import 'package:graphql/client.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import '../../models/checkout/payment/proceedToPaymentCalculation.dart';
import '../../providers/address_provider.dart';

part 'checkout_screen.g.dart';

class CheckOutScreen extends StatefulWidget {
  @override
  _CheckOutScreenState createState() => _CheckOutScreenState();
}

class _CheckOutScreenState extends State<CheckOutScreen> with TickerProviderStateMixin {
  RequestOrder requestOrder;
  CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  CartProvider _cartProvider;
  AddressProvider _addressProvider;
  bool isReady = false;
  bool hidePrice = false;
  String paymentPrefix = '';
  Razorpay _razorpay;
  bool orderRequesting = false;
  CheckOutServices _checkOutServices;
  int orderRequestId;
  String orderRequestGuid;
  dynamic paymentChannel = {};
  dynamic gocoPaymentData = {};

  // animation handler
  AnimationController _scaleController;
  AnimationController _scale2Controller;
  AnimationController _widthController;
  AnimationController _positionController;

  Animation<double> _scaleAnimation;
  Animation<double> _scale2Animation;
  Animation<double> _widthAnimation;
  Animation<double> _positionAnimation;
  bool hideIcon = false;
  bool hideMainScaffold = false;
  bool isCompletedOrder = false;

  @override
  void initState() {
    super.initState();
    _checkOutServices = CheckOutServices(context: context);
    this._loadClient();

    // Payment gateways loading
    this._initRazorPay();
    this._animationHandle();
  }

  _initRazorPay() {
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _razorpayHandlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _razorpayHandlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _razorpayHandleExternalWallet);
  }

  void _razorpayHandlePaymentSuccess(PaymentSuccessResponse response) {
    this.paymentChannel = {
      "razorpay": {
        "payment_option_id": _cartProvider.currentPaymentOption.payment_option_id,
        "razorpay_order_id": response.orderId,
        "razorpay_payment_id": response.paymentId,
        "razorpay_signature": response.signature
      }
    };
    this.gocoPaymentData = this.loadGoCoPaymentData();

    this._continueOrderRequest(
      goCoPaymentData: this.gocoPaymentData,
      paymentChannel: this.paymentChannel,
      orderRequestGuid: _cartProvider.pTPC.paymentInfo.order_request_guid,
      orderRequestId: _cartProvider.pTPC.paymentInfo.order_request_id,
    );
  }

  void _razorpayHandlePaymentError(PaymentFailureResponse response) {
    this._commonPaymentAfter();
    print(response.message);
  }

  void _razorpayHandleExternalWallet(ExternalWalletResponse response) {
    print(response.toString());
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _cartProvider = Provider.of<CartProvider>(context);
    _addressProvider = Provider.of<AddressProvider>(context);
    this._init();
  }

  _init() async {
    setState(() {
      isReady = true;
    });
  }

  refresh() {
    setState(() {});
  }

  dynamic _razorPayOrderIntRequest() async {
    QueryResult razorPayOrderRequest = await _checkOutServices.razorPayOrderInit(
      paymentOptionId: _cartProvider.currentPaymentOption.payment_option_id,
      amount: _cartProvider.pTPC.netPay,
      orderRequestGuid: _cartProvider.pTPC.paymentInfo.order_request_guid,
    );
    print(razorPayOrderRequest.data);
    if (!razorPayOrderRequest.hasException && razorPayOrderRequest.data['razorpayOrderInit'] != null) {
      if (razorPayOrderRequest.data['razorpayOrderInit']['order'] != null) {
        return razorPayOrderRequest.data['razorpayOrderInit']['order']['id'];
      }
    }
    return null;
  }

  placeOrderRequest() async {
    this._commonPaymentLoadingStart();
    if (_cartProvider.currentPaymentOption.code == 'razorpay') {
      var razorpayOrderId = await this._razorPayOrderIntRequest();
      var amount = (_cartProvider.pTPC.netPay * 100).toInt();
      var razorPayOptions = {
        'key': _cartProvider.currentPaymentOption.public_key,
        'amount': amount, //in the smallest currency sub-unit.
        'name': 'GOCOMART Pvt Ltd',
        'description': 'Purchase',
        'order_id': razorpayOrderId,
        'external': {
          "wallet": ['paytm']
        },
        'prefill': {'contact': '9539935888', 'email': 'muhsin.3009@gmail.com'}
      };
      try {
        _razorpay.open(razorPayOptions);
      } catch (e) {
        print(e);
      }
    } else if (_cartProvider.currentPaymentOption.code == PaymentModeConst.RAZORPAY_UPI) {
      var razorpayOrderId = await this._razorPayOrderIntRequest();
      var amount = (_cartProvider.pTPC.netPay * 100).toInt();
      var razorPayOptions = {
        'key': _cartProvider.currentPaymentOption.public_key,
        'amount': amount, //in the smallest currency sub-unit.
        'name': 'GOCOMART Pvt Ltd',
        'description': 'Purchase',
        'order_id': razorpayOrderId,
        "options": {
          "checkout": {
            "method": {"netbanking": "0", "card": "0", "upi": "1", "wallet": "0"}
          }
        }
      };
      try {
        _razorpay.open(razorPayOptions);
      } catch (e) {
        print(e);
      }
    } else if (_cartProvider.currentPaymentOption.code == 'cod') {
      this.paymentChannel = {
        "cod": {
          "payment_option_id": _cartProvider.currentPaymentOption.payment_option_id,
        }
      };
      this.gocoPaymentData = this.loadGoCoPaymentData();

      this._continueOrderRequest(
        goCoPaymentData: this.gocoPaymentData,
        paymentChannel: this.paymentChannel,
        orderRequestGuid: _cartProvider.pTPC.paymentInfo.order_request_guid,
        orderRequestId: _cartProvider.pTPC.paymentInfo.order_request_id,
      );
    }
  }

  void _pushToOrderDetails() {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.fade,
        child: OrderConfirmationScreen(
          orderInfo: this.requestOrder.orderInfo,
        ),
      ),
    );
  }

  void _continueOrderRequest({
    @required orderRequestId,
    @required orderRequestGuid,
    @required paymentChannel,
    @required goCoPaymentData,
  }) async {
    var addressId = _addressProvider.currentAddress.address_id;
    QueryResult orderRequest = await _checkOutServices.requestOrder(
      billingAddressId: addressId,
      shippingAddressId: addressId,
      orderRequestId: orderRequestId,
      orderRequestGuid: orderRequestGuid,
      paymentChannel: paymentChannel,
      goCoPaymentData: goCoPaymentData,
    );
    if (!orderRequest.hasException) {
      print(orderRequest.data);
      if (orderRequest.data['requestOrder'] != null) {
        // having valid data
        this.requestOrder = RequestOrder.fromJSON(orderRequest.data['requestOrder']);
      } else {}
      setState(() {
        this.isCompletedOrder = true;
        this.orderRequesting = false;
      });

      this.startAnimation();
    } else {
      print(orderRequest.exception);
      print("error");
      // error in order palce
    }
  }

  void startAnimation() async {
    setState(() {
      this.hideMainScaffold = true;
    });
    _scaleController.forward();
  }

  void _commonPaymentLoadingStart() {
    setState(() {
      orderRequesting = true;
    });
  }

  void _commonPaymentAfter() {
    setState(() {
      orderRequesting = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        hideMainScaffold == true ? getAppScaffold() : Container(),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            margin: EdgeInsets.only(top: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                AnimatedBuilder(
                  animation: _scaleController,
                  builder: (context, child) => Transform.scale(
                    scale: _scaleAnimation.value,
                    child: Center(
                      child: AnimatedBuilder(
                        animation: _widthController,
                        builder: (context, child) => Container(
                          width: _widthAnimation.value,
                          height: 60,
                          child: InkWell(
                            onTap: () {
                              _widthController.forward();
                            },
                            child: Stack(
                              children: <Widget>[
                                AnimatedBuilder(
                                  animation: _positionController,
                                  builder: (context, child) => Positioned(
                                    left: _positionAnimation.value,
                                    child: AnimatedBuilder(
                                      animation: _scale2Controller,
                                      builder: (context, child) => Transform.scale(
                                          scale: _scale2Animation.value,
                                          child: Container(
                                            width: 60,
                                            height: 60,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Theme.of(context).primaryColor,
                                            ),
                                            child: Container(),
                                          )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        hideMainScaffold == false ? getAppScaffold() : Container(),
      ],
    );
  }

  Widget getAppScaffold() {
    return LoadingOverlay(
      isLoading: orderRequesting,
      progressIndicator: BuyingLoadingAnimation(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Payment'),
        ),
        body: CheckOutScreenContent(
          notifyParent: refresh,
          isCompletedOrder: this.isCompletedOrder,
        ),
        bottomSheet: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: InkWell(
                onTap: (_cartProvider?.placeOrderPermission == true || _cartProvider?.isOfflinePaymentMode == true)
                    ? () {
                        this.placeOrderRequest();
                        //this.triggerAction();
                      }
                    : null,
                child: Container(
                  height: MediaQuery.of(context).size.width / 6,
                  width: MediaQuery.of(context).size.width,
                  color: _cartProvider?.placeOrderPermission == true ? Theme.of(context).primaryColor : Colors.grey,
                  child: Center(
                    child: isReady && _cartProvider?.pTPCLoaded == true ? paymentButton() : AutoSizeText(''),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget paymentButton() {
    return AutoSizeText(
      ((_cartProvider.pTPC.freeOrder == false && _cartProvider.isOfflinePaymentMode == false) ? "PAY " : "PLACE ORDER") +
          ((_cartProvider.pTPC.freeOrder == false && _cartProvider.isOfflinePaymentMode == false)
              ? "${inr.formatCurrency(_cartProvider?.pTPC?.netPay?.toDouble() ?? 0.0)}"
              : " "),
      style: TextStyle(
        color: Colors.white,
        fontSize: 18,
        fontWeight: FontWeight.w700,
      ),
      maxLines: 1,
    );
  }

  void _animationHandle() {
    _scaleController = AnimationController(vsync: this, duration: Duration(milliseconds: 10));
    _scaleAnimation = Tween<double>(begin: 1.0, end: 0.3).animate(_scaleController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _widthController.forward();
        }
      });
    _widthController = AnimationController(vsync: this, duration: Duration(milliseconds: 0));
    _widthAnimation = Tween<double>(begin: 80.0, end: 700.0).animate(_widthController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _positionController.forward();
        }
      });
    _positionController = AnimationController(vsync: this, duration: Duration(milliseconds: 0));
    _positionAnimation = Tween<double>(begin: 0.0, end: 300.0).animate(_positionController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          setState(() {
            hideIcon = true;
          });
          _scale2Controller.forward();
        }
      });
    _scale2Controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _scale2Animation = Tween<double>(begin: 1.0, end: 90.0).animate(_scale2Controller)
      ..addStatusListener(
        (status) {
          if (status == AnimationStatus.completed) {
            _scale2Controller.reset();
            _widthController.reset();
            _positionController.reset();
            this._pushToOrderDetails();
          }
        },
      );
  }

  loadGoCoPaymentData() {
    var totalAmount = 0.0;
    var walletUsed = 0.0;
    if (_cartProvider.pTPC.walletUsed) {
      totalAmount = _cartProvider.pTPC.netPay + _cartProvider.pTPC.paymentInfo.wallet_discount.toDouble();
      walletUsed = _cartProvider.pTPC.paymentInfo.wallet_discount.toDouble();
    } else {
      totalAmount = _cartProvider.pTPC.netPay.toDouble();
    }

    return {
      "wallet_used": double.parse(walletUsed.toStringAsFixed(2)),
      "netpay": _cartProvider.pTPC.netPay,
      "total_amount": double.parse(totalAmount.toStringAsFixed(2)),
      "delivery_fee": double.parse(_cartProvider.pTPC.paymentInfo.total_delivery_fee.toStringAsFixed(2)),
      "delivery_fee_discount": double.parse(_cartProvider.pTPC.paymentInfo.delivery_fee_discount.toStringAsFixed(2)),
      "wallet_exclusive_discount": double.parse(_cartProvider.pTPC.paymentInfo.walletExclusiveDiscount.toStringAsFixed(2)),
    };
  }
}
