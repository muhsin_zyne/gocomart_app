import 'dart:convert';

class ProductHelper {
  formatProductOption(dynamic json) async {
    dynamic masterList = [];
    dynamic notMasterList = [];
    for (int i = 0; i < json.length; i++) {
      final currentObj = json[i];
      if (currentObj['master_option'] == 0) {
        masterList.add(currentObj);
      } else {
        notMasterList.add(currentObj);
      }
    }

    for (int i = 0; i < masterList.length; i++) {
      final currentMasterList = masterList[i];
      for (int j = 0; j < currentMasterList['optionValues'].length; j++) {
        final currentOptionValue = currentMasterList['optionValues'][j];
        dynamic currentChildList = [];
        currentChildList = await this.getChildOptions(notMasterList, currentMasterList['option_id'], currentOptionValue['product_option_value_id'], null);
        if (currentChildList != null) {
          masterList[i]['optionValues'][j]['child'] = currentChildList;
        }
      }
    }
    return masterList;
  }

  dynamic getChildOptions(dynamic notMasterList, int masterId, int optionValueId, childProperties) async {
    dynamic returnChildList = [];
    for (int i = 0; i < notMasterList.length; i++) {
      if (notMasterList[i]['master_option'] == masterId) {
        if (childProperties == null) {
          childProperties = jsonDecode(jsonEncode(notMasterList[i]));
        }
        final currentOptValues = notMasterList[i]['optionValues'];
        for (int j = 0; j < currentOptValues.length; j++) {
          if (currentOptValues[j]['master_option_value'] == optionValueId) {
            returnChildList.add(currentOptValues[j]);
          }
        }
      }
    }
    if (childProperties != null) {
      childProperties['optionValues'] = [];
      childProperties['childOptionItems'] = returnChildList;
    }
    return childProperties;
  }

  checkColorOptions(dynamic json) async {}

  formatReviewSummary(dynamic json) {
    dynamic jsonReview = [
      {"rating": 1, "count_of_rating": 0},
      {"rating": 2, "count_of_rating": 0},
      {"rating": 3, "count_of_rating": 0},
      {"rating": 4, "count_of_rating": 0},
      {"rating": 5, "count_of_rating": 0},
    ];
    for (int i = 0; i < json.length; i++) {
      var cData = json[i];
      jsonReview[(cData['rating'] - 1)]['count_of_rating'] = cData['count_of_rating'];
    }
    return jsonReview.reversed.toList();
  }
}
