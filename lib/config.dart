class AppFlavors {
  static FlavorValues devFlavor = FlavorValues(
    name: "Dev",
    apiEndPoint: "https://api.dev.gocomart.com/graphql",
    cdnPoint: "https://cdimage.dev.gocomart.com/",
    cdnImagePoint: "https://cdimage.dev.gocomart.com/",
    isTest: true,
    oneSignalAppId: "8801857e-080b-4d49-aad4-5131a192bbd4",
    oneSignalRestApiKey: "ZmFlN2Q2YjctOWNiOC00NDQyLWFmNTgtYTM0NzY4ZmVmMjdi",
  );

  static FlavorValues stageFlavor = FlavorValues(
    name: "Stage",
    apiEndPoint: "https://api.staging.gocomart.com/graphql",
    cdnPoint: "https://cdimage.staging.gocomart.com/",
    cdnImagePoint: "https://cdimage.staging.gocomart.com/",
    isTest: true,
    oneSignalAppId: "bf0d780f-01b4-4dfa-b081-4b13f8e3de5e",
    oneSignalRestApiKey: "ZmFlN2Q2YjctOWNiOC00NDQyLWFmNTgtYTM0NzY4ZmVmMjdi",
  );

  static FlavorValues prodFlavor = FlavorValues(
    name: "Dev",
    apiEndPoint: "https://api.prod.gocomart.com/graphql",
    cdnPoint: "https://cdimage.prod.gocomart.com/",
    cdnImagePoint: "https://cdimage.prod.gocomart.com/",
    isTest: true,
    oneSignalAppId: "8801857e-080b-4d49-aad4-5131a192bbd4",
    oneSignalRestApiKey: "ZmFlN2Q2YjctOWNiOC00NDQyLWFmNTgtYTM0NzY4ZmVmMjdi",
  );
}

enum Flavor { development, staging, production }

class FlavorValues {
  final String name;
  final String oneSignalAppId;
  final String oneSignalRestApiKey;
  final String apiEndPoint;
  final String cdnPoint;
  final String cdnImagePoint;
  final bool isTest;

  FlavorValues({
    this.oneSignalAppId = '',
    this.oneSignalRestApiKey = '',
    this.name = '',
    this.apiEndPoint = '',
    this.cdnPoint = '',
    this.cdnImagePoint = '',
    this.isTest = false,
  });
}

class FlavorConfig {
  Flavor flavor;
  FlavorValues flavorValues;
  static FlavorConfig _instance;

  factory FlavorConfig({Flavor flavor, FlavorValues flavorValues}) {
    _instance ??= FlavorConfig._initialize(flavor, flavorValues);
    return _instance;
  }

  FlavorConfig._initialize(this.flavor, this.flavorValues);
  static FlavorConfig get instance => _instance;
}
