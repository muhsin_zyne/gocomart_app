import 'package:flutter/material.dart';
import 'package:gocomartapp/models/profile_screen_model/get_profile_app_links.dart';

class AppProvider extends ChangeNotifier {
  AppProvider();
  //profile app link provider
  GetProfileAppLinks _getProfileAppLinks;
  GetProfileAppLinks get getProfileAppLinks => _getProfileAppLinks;
  set getProfileAppLinks(GetProfileAppLinks profileAppLinks) {
    this._getProfileAppLinks = profileAppLinks;
    notify();
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
