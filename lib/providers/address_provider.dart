import 'package:flutter/material.dart';
import 'package:gocomartapp/models/address/address.dart';
import 'package:gocomartapp/models/address/get_address.dart';

class AddressProvider extends ChangeNotifier {
  AddressProvider() {
    init();
  }

  init() {
    isReady = true;
  }

  GetAddress _getAddress;
  Address _currentAddress;
  bool _nullAddress = true;
  bool isReady = false;
  bool _currentAddressLoaded = false;

  bool _continuePaymentPage;

  Address get currentAddress => _currentAddress;
  get nullAddress => _nullAddress;
  GetAddress get getAddress => _getAddress;
  bool get continuePaymentPage {
    if (this._currentAddress.address_id != null) {
      _continuePaymentPage = true;
    } else {
      _continuePaymentPage = false;
    }
    return _continuePaymentPage;
  }

  bool get currentAddressLoaded => _currentAddressLoaded;

  set currentAddressLoaded(bool value) {
    this._currentAddressLoaded = value;
    notify();
  }

  set currentAddress(Address address) {
    this._currentAddress = address;
    this.currentAddressLoaded = true;
    notify();
  }

  set getAddress(GetAddress getAddress) {
    this._getAddress = getAddress;
    notify();
  }

  set nullAddress(val) {
    this._nullAddress = val;
    notify();
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
