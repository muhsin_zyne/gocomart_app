import 'package:flutter/material.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';

class ShoppingHomeProvider extends ChangeNotifier {
  ShoppingHomeProvider();

  ShoppingHomeModel _shoppingHomeModel;
  GetAppBanners _homeAppBanner;
  ShoppingHomeModel get shoppingHomeModel => _shoppingHomeModel;
  GetAppBanners get homeAppBanner => _homeAppBanner;

  set shoppingHomeModel(ShoppingHomeModel selected) {
    this._shoppingHomeModel = selected;
    notify();
  }

  set homeAppBanner(GetAppBanners selected) {
    this._homeAppBanner = selected;
    notify();
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
