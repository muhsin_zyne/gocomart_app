import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/models/category/appCategories.dart';
import 'package:gocomartapp/models/category/category.dart';
import 'package:gocomartapp/models/filter-new/filter.dart';
import 'package:gocomartapp/models/filter-new/filterGroup.dart';
import 'package:gocomartapp/models/filter-new/filterGroupProvider.dart';
import 'package:gocomartapp/models/filter/getFiltersByGroup.dart';

class CategoryProvider extends ChangeNotifier {
  CategoryProvider() {
    init();
  }
  init() {
    isReady = true;
  }

  int lastLoadedCategory = 0;
  int _productListSelectedCategoryId = 0;

  AppSortList _selectedSort = AppSortList.Popularity;
  // Implementation Block
  bool isReady = false;
  AppCategories _appCategories;
  int _selectedCategoryId;
  Category _selectedCategory;
  // old filter Group
  GetFiltersByGroup _getFiltersByGroup;
  //new filter group

  FilterGroupProvider _filterGroupProvider;
  FilterGroup _selectedFilterGroup;
  List<String> _selectFilterList = [];
  List<String> _oldFilterList = [];
  bool filterApplied = false;
  // Get Block

  AppCategories get appCategories => _appCategories;
  int get selectedCategoryId => _selectedCategoryId;
  Category get selectedCategory => _selectedCategory;
  GetFiltersByGroup get getFiltersByGroup => _getFiltersByGroup;
  FilterGroup get selectedFilterGroup => _selectedFilterGroup;
  List<String> get selectFilterList => _selectFilterList;
  int get productListSelectedCategoryId => _productListSelectedCategoryId;
  FilterGroupProvider get filterGroupProvider => _filterGroupProvider;
  AppSortList get selectedSort => _selectedSort;
  // Set Block

  set selectedSort(AppSortList data) {
    this._selectedSort = data;
    notify();
  }

  set filterGroupProvider(FilterGroupProvider setterData) {
    this._filterGroupProvider = setterData;
    notify();
  }

  set productListSelectedCategoryId(int value) {
    _productListSelectedCategoryId = value;
    this.notify();
  }

  set appCategories(AppCategories setterData) {
    this._appCategories = setterData;
    this.notify();
  }

  set selectedCategoryId(int id) {
    this._selectedCategoryId = id;
    notify();
  }

  set selectedCategory(Category selected) {
    this._selectedCategory = selected;
    notify();
  }

  set getFiltersByGroup(GetFiltersByGroup data) {
    this._getFiltersByGroup = data;
    notify();
  }

  set selectedFilterGroup(FilterGroup data) {
    this.lastLoadedCategory = this._productListSelectedCategoryId;
    this._selectedFilterGroup = data;
    notify();
  }

  findFilterAppliedByFilterId(String filterId) {
    return this._selectFilterList.contains(filterId);
  }

  oldFindFilterAppliedByFilterId(String filterId) {
    return this._oldFilterList.contains(filterId);
  }

  clearCachedCheckListSelected() async {
    this._selectFilterList = [];
    this._selectedSort = AppSortList.Popularity;
  }

  saveLastFilterState() {
    this._oldFilterList = [];
    this._selectFilterList.forEach((element) {
      this._oldFilterList.add(element);
    });
  }

  copyOldFilterRecord() {
    this._oldFilterList.forEach((element) {
      this._selectFilterList.add(element);
    });
  }

  clearAllFilterItems() {
    this._selectFilterList = [];
    notify();
  }

  cancelFilterAction() async {
    this.filterApplied = false;
    await this.clearCachedCheckListSelected();
    this.copyOldFilterRecord();
    return true;
  }

  filterItemChanged(bool status, Filter changedFilter) {
    if (status == false) {
      this._selectFilterList.removeWhere((filter) => filter == changedFilter.filterId);
      notify();
    } else {
      this._selectFilterList.add(changedFilter.filterId);
      notify();
    }
  }

  // other functions
  applyFilterAction() async {
    this.filterApplied = true;
    return true;
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
