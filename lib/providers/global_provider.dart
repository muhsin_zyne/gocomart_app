import 'package:flutter/material.dart';
import 'package:geolocation/geolocation.dart' as geolocation;
import 'package:geolocator/geolocator.dart';
import 'package:gocomartapp/models/product_detail/p_option_values.dart';
import 'package:gocomartapp/models/profile/user.dart';
import 'package:gocomartapp/models/scratch_card/scratchCard.dart';
import 'package:gocomartapp/src/models/response/firebase/referral_link.dart';
import 'package:gocomartapp/src/models/response/wallet/ScratchCardPromotional.dart';
import 'package:gocomartapp/src/models/response/wallet/WalletAppLinksMixed.dart';
import 'package:graphql/client.dart';
import 'package:location/location.dart' as gps;
import 'package:location_permissions/location_permissions.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';

class GlobalProvider extends ChangeNotifier {
  GlobalProvider() {
    // constructor
    init();
  }

  GlobalKey<State> _keyLoader = new GlobalKey<State>();
  GlobalKey<RefreshIndicatorState> refreshKey;
  Position _position;
  String _authToken = '';
  String _refreshToken = '';
  String _apiEndPoint = '';
  String _cdnPoint = '';
  String _cdnImagePoint = '';
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _cashCurrentScratchState = false;
  ServiceStatus _gpsServiceStatus = ServiceStatus.unknown;
  PermissionStatus _gpsPermissionStatus = PermissionStatus.unknown;
  int _currentOptionValueId = 0;
  POptionValues currentSelectedOption;
  int _cartItemsLength = 0;
  User _currentUser;
  WalletAppLinksMixed _walletAppLinksMixed;

  ReferralLink _referralLink;
  // get objects
  String get authToken => _authToken;
  String get refreshToken => _refreshToken;
  String get apiEndPoint => _apiEndPoint;
  String get cdnPoint => _cdnPoint;
  String get cdnImagePoint => _cdnImagePoint;
  bool get cashCurrentScratchState => _cashCurrentScratchState;
  Position get position => _position;
  GlobalKey get keyLoader => _keyLoader;
  ServiceStatus get gpsServiceStatus => _gpsServiceStatus;
  PermissionStatus get gpsPermissionStatus => _gpsPermissionStatus;
  int get currentOptionValueId => _currentOptionValueId;
  int get cartItemsLength => _cartItemsLength;
  User get currentUser => _currentUser;
  WalletAppLinksMixed get walletAppLinksMixed => _walletAppLinksMixed;
  ReferralLink get referralLink => _referralLink;

  List<ScratchCard> scratchCardRawList = [];
  num scratchTotalCollectedAmount = 0;

  // some cart provider variables

  ScPromotionalText scPromotionalText;

  // scratch card performance
  bool _scratchCardHeroHide = false;
  bool get scratchCardHeroHide => _scratchCardHeroHide;

  set referralLink(ReferralLink data) {
    this._referralLink = data;
    notify();
  }

  set scratchCardHeroHide(bool value) {
    this._scratchCardHeroHide = value;
    notify();
  }

  // init statement which run on constructor trigger
  init() async {
    await setAuthToken();
    await setApiEndPoint();
    refreshKey = GlobalKey<RefreshIndicatorState>();
    notify();
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }

  setCashCurrentScratchState(bool value) {
    _cashCurrentScratchState = value;
  }

  trigger() {
    notify();
  }

  set currentUser(User data) {
    this._currentUser = data;
    notify();
  }

  set authToken(String token) {
    print("global provider auth token update");
    print(token);
    _authToken = token;
    notify();
  }

  set refreshToken(String token) {
    _refreshToken = token;
    notify();
  }

  set currentOptionValueId(int value) {
    _currentOptionValueId = value;
    notify();
  }

  set cartItemsLength(int value) {
    _cartItemsLength = value;
    notify();
  }

  set walletAppLinksMixed(WalletAppLinksMixed mixedData) {
    _walletAppLinksMixed = mixedData;
    notify();
  }

  GraphQLClient apiClient({String token = ''}) {
    if (token == '') {
      token = this.authToken;
    }
    final HttpLink _httpLink = HttpLink(
      uri: apiEndPoint,
    );
    final AuthLink _authLink = AuthLink(
      // ignore: undefined_identifier
      getToken: () => "Bearer: $token",
    );
    final Link _link = _authLink.concat(_httpLink);

    return GraphQLClient(
      cache: InMemoryCache(),
      link: _link,
    );
  }

  // setting auth token from shared pref
  setAuthToken() async {
    final SharedPreferences prefs = await _prefs;
    try {
      _authToken = prefs.getString('auth_token');
    } catch (e) {
      print(e);
      _authToken = '';
    }
  }

  // getting api end point from fire base remote config
  setApiEndPoint() async {
    _apiEndPoint = FlavorConfig.instance.flavorValues.apiEndPoint;
    _cdnImagePoint = FlavorConfig.instance.flavorValues.cdnImagePoint;
    _cdnPoint = FlavorConfig.instance.flavorValues.cdnPoint;
//    print("api end point");
//    try {
//      final RemoteConfig remoteConfig = await RemoteConfig.instance;
//      await remoteConfig.fetch(expiration: Duration(hours: 12));
//      await remoteConfig.activateFetched();
//      _apiEndPoint = remoteConfig.getString('api_endpoint');
//    } catch (exception) {}
  }

  setGeoLocation(Position geoLocationData) {
    _position = geoLocationData;
    notify();
  }

  setGpsServiceStatus(ServiceStatus status) {
    _gpsServiceStatus = status;
    notify();
  }

  setGpsPermissionStatus(PermissionStatus status) {
    _gpsPermissionStatus = status;
    notify();
  }

  checkGpsServiceStatus() async {
    ServiceStatus serviceStatus = await LocationPermissions().checkServiceStatus();
    setGpsServiceStatus(serviceStatus);
  }

  Future initGpsRequest(BuildContext context) async {
    await checkGpsServiceStatus();
    if (_gpsServiceStatus == ServiceStatus.enabled) {
      PermissionStatus permissionStatus = await LocationPermissions().checkPermissionStatus();
      setGpsPermissionStatus(permissionStatus);
      if (_gpsPermissionStatus == PermissionStatus.granted) {
        await loadGeoPoint();
      } else {
        bool status = await this.requestGpsPermissionAction(context);
        print(status);
        print("status camed");
      }
    } else {
      final gps.Location location = gps.Location();
      if (!await location.serviceEnabled()) {
        var locationStatus = await location.requestService();
        if (locationStatus == true) {
          initGpsRequest(context);
        }
        if (_gpsServiceStatus == ServiceStatus.enabled) {
          await loadGeoPoint();
        }
      } else {}
    }
    return true;
  }

  loadGeoPoint() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
    if (position != null) {
      setGeoLocation(position);
    } else {}
  }

  requestGpsPermissionAction(BuildContext context) async {
    await this._showMyDialog(context);
    final geolocation.GeolocationResult result = await geolocation.Geolocation.requestLocationPermission(
      permission: const geolocation.LocationPermission(
        android: geolocation.LocationPermissionAndroid.fine,
        ios: geolocation.LocationPermissionIOS.always,
      ),
      openSettingsIfDenied: true,
    );

    if (result.isSuccessful) {
      await this.loadGeoPoint();
      return true;
    } else {
      return false;
      // location permission is not granted
      // user might have denied, but it's also possible that location service is not enabled, restricted, and user never saw the permission request dialog. Check the result.error.type for details.
    }
  }

  requestGpsPermission() async {
    PermissionStatus permissionStatus = await LocationPermissions().requestPermissions();
    return permissionStatus;
  }

  // set current product option selected

  setcurrentProductOptionSelected(POptionValues cSelectedItem) {
    currentSelectedOption = cSelectedItem;
    notify();
  }

  markProductOpenedAsNew() {
    currentSelectedOption = null;
    this.currentOptionValueId = 0;
  }

  //profile app link provider

  Future<void> _showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Location permission disabled'),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('You have dined location permission'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
