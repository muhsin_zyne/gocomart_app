import 'package:flutter/material.dart';
import 'package:gocomartapp/models/wishlist_screen_model/product_wishlist.dart';

class ScreenCachingProvider extends ChangeNotifier {
  ScreenCachingProvider();

  //wishlist caching
  ProductWishlist _productWishlist;
  ProductWishlist get productWishlist => _productWishlist;
  set productWishlist(val) {
    this._productWishlist = val;
    notify();
  }

//notify listener
  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
