import 'package:flutter/material.dart';
import 'package:gocomartapp/models/cart/buyNowItem.dart';
import 'package:gocomartapp/models/cart/cartResponse.dart';
import 'package:gocomartapp/models/cart/item.dart';
import 'package:gocomartapp/models/cart/productBuyNowPreview.dart';
import 'package:gocomartapp/models/cart/productOptionValue.dart';
import 'package:gocomartapp/models/checkout/getPaymentOption.dart';
import 'package:gocomartapp/models/checkout/getPaymentOptionGroup.dart';
import 'package:gocomartapp/models/checkout/getUserPaymentOptions.dart';
import 'package:gocomartapp/models/checkout/paymentOption.dart';

import '../models/checkout/payment/proceedToPaymentCalculation.dart';

class CartProvider extends ChangeNotifier {
  CartProvider() {
    // constructor
    init();
  }
  init() async {
    this._isOfflinePaymentMode = false;
  }

  CartResponse _cartResponse;
  ProductBuyNowPreview _productBuyNowPreview;
  bool _buyNowPreviewShouldLoad = false;
  int _cartLength = 0;
  double _cartTotal = 0.0;
  bool _firstLoad = false;
  bool isReady = false;
  bool pTPCLoaded = false;
  bool _codEnabled = false;

  CartResponse get cartResponse => _cartResponse;
  ProductBuyNowPreview get productBuyNowPreview => _productBuyNowPreview;
  int get cartLength => _cartLength;
  double get cartTotal => _cartTotal;
  bool get firstLoad => _firstLoad;
  bool get buyNowPreviewShouldLoad => _buyNowPreviewShouldLoad;
  bool _isOfflinePaymentMode;

  set firstLoad(bool value) {
    this._firstLoad = true;
    notify();
  }

  List<PaymentOption> paymentOptions;

  // checkout section variables

  bool _placeOrderPermission = false;
  bool get placeOrderPermission {
    if (this._currentPaymentOption?.payment_option_id != null) {
      if (this._currentPaymentOption?.payment_option_id != 0) {
        this._placeOrderPermission = true;
      } else {
        this._placeOrderPermission = false;
      }
    }
    return this._placeOrderPermission;
  }

  set placeOrderPermission(bool value) {
    this._placeOrderPermission = true;
  }

  GetUserPaymentOptions _getUserPaymentOptions;
  GetPaymentOptionGroup _getPaymentOptionGroup;
  ProceedToPaymentCalculation _pTPC;
  PaymentOption _currentPaymentOption;
  GetPaymentOption _getPaymentOption;
  GetPaymentOption get getPaymentOption => _getPaymentOption;
  PaymentOption get currentPaymentOption => _currentPaymentOption;
  GetUserPaymentOptions get getUserPaymentOptions => _getUserPaymentOptions;
  GetPaymentOptionGroup get getPaymentOptionGroup => _getPaymentOptionGroup;
  ProceedToPaymentCalculation get pTPC => _pTPC;
  bool get codEnabled => _codEnabled;
  bool get isOfflinePaymentMode => _isOfflinePaymentMode;

  void togglePTCPWallet() {
    this._pTPC.walletNotUsable = false;
    if (this._pTPC.walletUsed == false) {
      if (this._pTPC.paymentInfo.wallet_discount.toInt() > 0) {
        this._pTPC.walletUsed = true;
        _pTPC.netWalletBalance = _pTPC.paymentInfo.wallet_balance.toDouble() - _pTPC.paymentInfo.wallet_discount.toDouble();
        if (_pTPC.paymentInfo.walletExclusiveDiscount > 0) {
          _pTPC.exclusiveDiscountUsable = true;
        } else {
          _pTPC.exclusiveDiscountUsable = false;
        }
      } else {
        _pTPC.walletUsed = false;
        _pTPC.netWalletBalance = 0.0;
        this._pTPC.walletNotUsable = true;
      }
      this._pTPCRecalculate();
    } else {
      this._pTPC.walletUsed = false;
      this._pTPC.exclusiveDiscountUsed = false;
      _pTPC.netWalletBalance = _pTPC.paymentInfo.wallet_balance.toDouble();
      _pTPC.exclusiveDiscountUsable = false;
      this._pTPCRecalculate();
    }
  }

  _pTPCRecalculate() {
    _pTPC.netPay = ((_pTPC.paymentInfo.total_price + _pTPC.paymentInfo.total_delivery_fee) - _pTPC.paymentInfo.delivery_fee_discount);
    _pTPC.totalPayableWithOutOffer = ((_pTPC.paymentInfo.total_price + _pTPC.paymentInfo.total_delivery_fee) - _pTPC.paymentInfo.delivery_fee_discount);
    if (_pTPC.walletUsed == true) {
      _pTPC.netPay -= _pTPC.paymentInfo.wallet_discount;
      if (_pTPC.exclusiveDiscountUsed == true) {
        _pTPC.netPay -= _pTPC.paymentInfo.walletExclusiveDiscount;
      }
    }
    if (_pTPC.netPay == 0) {
      _pTPC.freeOrder = true;
    } else {
      _pTPC.freeOrder = false;
    }

    _pTPC.orderTotalSavings = _pTPC.totalPayableWithOutOffer - pTPC.netPay;
    pTPC.netPay = double.parse(_pTPC.netPay.toStringAsFixed(2));
    this.notify();
  }

  toggleExclusiveWallet() {
    this._pTPC.exclusiveDiscountUsable = true;
    if (this._pTPC.exclusiveDiscountUsed == false) {
      this._pTPC.exclusiveDiscountUsed = true;
      _pTPCRecalculate();
      // exclusive apply script
    } else {
      this._pTPC.exclusiveDiscountUsed = false;
      _pTPCRecalculate();
      // exclusive dittach
    }
  }

  set codEnabled(bool value) {
    _codEnabled = value;
    notify();
  }

  set pTPC(ProceedToPaymentCalculation data) {
    this.pTPCLoaded = true;
    this._pTPC = data;
    this.togglePTCPWallet();
  }

  set isOfflinePaymentMode(bool value) {
    _isOfflinePaymentMode = value;
    notify();
  }

  set getUserPaymentOptions(GetUserPaymentOptions data) {
    this._getUserPaymentOptions = data;
    notify();
  }

  set getPaymentOptionGroup(GetPaymentOptionGroup data) {
    this._getPaymentOptionGroup = data;
    notify();
  }

  set getPaymentOption(GetPaymentOption data) {
    this._getPaymentOption = data;
    notify();
  }

  set currentPaymentOption(PaymentOption data) {
    this._currentPaymentOption = data;
    if (this._currentPaymentOption.is_online == false) {
      this.isOfflinePaymentMode = true;
    } else {
      this.isOfflinePaymentMode = false;
    }
    notify();
    // cod checking and approving gose here here
  }

  set buyNowPreviewShouldLoad(bool value) {
    _buyNowPreviewShouldLoad = value;
    notify();
  }

  // Setting new cart list
  set cartResponse(CartResponse data) {
    this._cartResponse = data;
    cartUpdate();
  }

  set productBuyNowPreview(ProductBuyNowPreview data) {
    this._productBuyNowPreview = data;
    this._buyNowPreviewUpdate();
  }

  _buyNowPreviewUpdate() {
    if (this._productBuyNowPreview.items != null) {
      this._productBuyNowPreview.items.forEach((BuyNowItem buyNowItem) {
        this._productBuyNowPreview.totalAmount += buyNowItem.priceInfo.itemTotal;
        this._productBuyNowPreview.deliveryCharges += buyNowItem.priceInfo.totalShippingCost;
        this._productBuyNowPreview.additionalTax += buyNowItem.priceInfo.tax;
        buyNowItem.basePrice = ((buyNowItem.product.price + buyNowItem.priceInfo.hikeTotal) * buyNowItem.quantity);
        buyNowItem.offerAmount = (buyNowItem.basePrice - buyNowItem.priceInfo.itemTotal);
      });

      this._freeDeliveryInputTrigger();
      this._netPayCalculate();
    }
  }

  _netPayCalculate() {
    this._productBuyNowPreview.totalPayableAmount =
        this._productBuyNowPreview.totalAmount + this._productBuyNowPreview.additionalTax + (this._productBuyNowPreview.deliveryCharges + this.productBuyNowPreview.freeDeliveryAdjustment);
    this.notify();
  }

  _freeDeliveryInputTrigger() {
    if (this._productBuyNowPreview.totalAmount > this._productBuyNowPreview.delivery.freeShippingCartTotal) {
      this._productBuyNowPreview.freeDeliveryAdjustment = -this._productBuyNowPreview.deliveryCharges;
      this.notify();
    }
  }

  // cart update common trigger
  cartUpdate() {
    this._cartLength = 0;
    this._cartTotal = 0;
    for (int i = 0; i < this._cartResponse.items.length; i++) {
      Item cItem = this._cartResponse.items[i];
      this._cartLength = this._cartLength + cItem.quantity;

      cItem.basePrice = cItem.product.price.toDouble();
      cItem.specialPrice = cItem.product.special?.price?.toDouble() ?? cItem.basePrice;
      if (cItem.customerPrice != null && cItem.customerPrice.max_quantity >= cItem.quantity) {
        if (cItem.customerPrice.is_flexible_option == true) {
          // offer price override with customer offer
          if (cItem.specialPrice > cItem.customerPrice.offer_price) {
            cItem.specialPrice = cItem.customerPrice.offer_price;
          }
        } else {
          if (cItem.specialPrice > cItem.customerPrice.offer_price) {
            cItem.specialPrice = cItem.customerPrice.offer_price.toDouble();
            cItem.forceCustomerOffer = true;
          }
        }
      }

      cItem.hikePrice = 0;
      if (cItem.productOptionsValues != null && cItem.forceCustomerOffer == false) {
        for (int optionLength = 0; optionLength < cItem.productOptionsValues?.length; optionLength++) {
          ProductOptionValue cItemOpValue = cItem.productOptionsValues[optionLength];
          if (cItemOpValue.productOptionValue.price_prefix == '+') {
            cItem.specialPrice = cItem.specialPrice + cItemOpValue.productOptionValue.price.toDouble();
            cItem.hikePrice = cItem.hikePrice + cItemOpValue.productOptionValue.price.toDouble();
          } else if (cItemOpValue.productOptionValue.price_prefix == '-') {
            cItem.specialPrice = cItem.specialPrice - cItemOpValue.productOptionValue.price.toDouble();
            cItem.hikePrice = cItem.hikePrice - cItemOpValue.productOptionValue.price.toDouble();
          }
        }
      }
      cItem.discountPercentage = this.discountPercentage((cItem.basePrice + cItem.hikePrice), cItem.specialPrice);
      cItem.itemTotal = (cItem.specialPrice * cItem.quantity);

      this._cartTotal = (this._cartTotal + cItem.itemTotal);
    }
    notify();
  }

  // notify statement
  notify() {
    try {
      notifyListeners();
    } catch (e) {
      print(e.toString());
    }
  }

  double discountPercentage(double orgPrice, double offerPrice) {
    double percentage = ((orgPrice - offerPrice) / orgPrice) * 100;
    return percentage;
  }
}
