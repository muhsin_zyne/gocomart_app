import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gocomartapp/models/product_detail/customer_offer/offeredOptions.dart';
import 'package:gocomartapp/models/product_detail/customer_offer/optionValue.dart';
import 'package:gocomartapp/models/product_detail/p_option_values.dart';
import 'package:gocomartapp/models/product_detail/customer_offer/customerOffer.dart';
import 'package:gocomartapp/models/product_detail_new/child_option.dart';

class ProductProvider extends ChangeNotifier {
  ProductProvider() {
    // constructor
    init();
  }
  init() async {}

  // Private variables
  bool ready = true;
  bool _imageLoading = false;
  bool _buyNowPermission = false;
  bool havingOfferCItem = true;
  bool havingOfferCChildItem = true;
  int _currentOptionValueId = 0;
  double _productDisplayPrice = 0.0;
  double _productBaseStdPrice = 0.0;
  double _productBaseSpecialPrice = 0.0;
  double _productStdPrice = 0.0;
  double _productStdPriceDis = 0.0;
  double _productSpPrice = 0.0;
  double _offerPercentage = 0.0;
  double _lastParentPriceDiff = 0.0;
  POptionValues currentSelectedOption;
  int _currentChildOptionValueId;

  List<ChildOption> _childOptions;

  List<String> _pImages = <String>[];
  List<dynamic> pCartOptionData = [
    {
      'product_option_id': 0,
      'product_option_value_id': 0,
    },
    {
      'product_option_id': 0,
      'product_option_value_id': 0,
    }
  ];

  CustomerOffer _customerOffer;

  set childOptions(val) {
    this._childOptions = val;
    notify();
  }

  set imageLoading(bool value) {
    this._imageLoading = value;
    notify();
  }

  set pImages(List<String> data) {
    this._pImages = data;
    notify();
  }

  bool get imageLoading => _imageLoading;
  List<String> get pImages => _pImages;
  get pCartOptionDataApi {
    var pCartOptionApiRaw = [];
    for (int i = 0; i < this.pCartOptionData.length; i++) {
      if (this.pCartOptionData[i]['product_option_id'] != 0 &&
          this.pCartOptionData[i]['product_option_value_id'] != 0) {
        pCartOptionApiRaw.add({
          "product_option_id": pCartOptionData[i]['product_option_id'],
          "product_option_value_id": this.pCartOptionData[i]
              ['product_option_value_id'],
        });
      }
    }
    return pCartOptionApiRaw;
  }

  // Public variables
  get childOptions => _childOptions;
  bool get buyNowPermission => _buyNowPermission;
  int get currentOptionValueId => _currentOptionValueId;
  int get currentChildOptionValueId => _currentChildOptionValueId;
  double get productDisplayPrice => _productDisplayPrice;
  double get productStdPrice => _productStdPrice;
  double get productStdPriceDis => _productStdPriceDis;
  double get productSpPrice => _productSpPrice;
  double get offerPercentage => _offerPercentage;
  double get productBaseStdPrice => _productBaseStdPrice;
  double get productBaseSpecialPrice => _productBaseSpecialPrice;

  double get applicableStdPrice {
    if (_productBaseSpecialPrice > 0 &&
        _productBaseSpecialPrice < _productBaseStdPrice) {
      // checking the customer has better offer price
      if (_customerOffer?.customerPrice?.offer_price != null &&
          havingOfferCItem == true) {
        if (_customerOffer.customerPrice.offer_price <
            _productBaseSpecialPrice) {
          return _customerOffer.customerPrice.offer_price.toDouble();
        }
      }
      return _productBaseSpecialPrice;
    } else {
      return _productBaseStdPrice;
    }
  }

  CustomerOffer get customerOffer => _customerOffer;

  set customerOffer(dynamic data) {
    if (data == null) {
      _customerOffer = CustomerOffer.fromJson({});
    } else {
      _customerOffer = CustomerOffer.fromJson(data);
    }
    notify();
  }

  set buyNowPermission(bool value) {
    _buyNowPermission = value;
    notify();
  }

  set currentOptionValueId(int value) {
    _currentOptionValueId = value;
    if (this._currentOptionValueId != 0) {
      this.buyNowPermission = true;
    } else {
      this.buyNowPermission = false;
    }
    notify();
  }

  set currentChildOptionValueId(int value) {
    _currentChildOptionValueId = value;
    if (this._currentChildOptionValueId != 0) {
      this.buyNowPermission = true;
    } else {
      this.buyNowPermission = false;
    }
    notify();
  }

  set productDisplayPrice(double value) {
    _productDisplayPrice = value;
    priceCalc();
  }

  set productStdPrice(double value) {
    _productStdPrice = value;
    _productStdPriceDis = value;
    priceCalc();
  }

  set productStdPriceDis(double value) {
    _productStdPriceDis = value;
    priceCalc();
  }

  set productSpPrice(double value) {
    _productSpPrice = value;
    priceCalc();
  }

  set productBaseStdPrice(double value) {
    _productBaseStdPrice = value;
    priceCalc();
  }

  set productBaseSpecialPrice(double value) {
    _productBaseSpecialPrice = value;
    priceCalc();
  }

  // notify statement
  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }

  priceCalc() {
    if (_productStdPrice != 0.0 && _productSpPrice != 0.0) {
      _offerPercentage =
          this.discountPercentage(_productStdPriceDis, _productSpPrice);
    }

    if ((_productSpPrice > 0 && (_productSpPrice < _productStdPriceDis)) &&
        _productDisplayPrice != _productSpPrice) {
      productDisplayPrice = _productSpPrice;
    }
    notify();
  }

  discountPercentage(double orgPrice, double offerPrice) {
    double percentage = ((orgPrice - offerPrice) / orgPrice) * 100;
    return percentage;
  }

  setCurrentProductOptionSelected(POptionValues cSelectedItem) {
    currentSelectedOption = cSelectedItem;
    notify();
  }

  markProductOpenedAsNew() {
    productSpPrice = 0;
    _lastParentPriceDiff = 0.0;
    currentSelectedOption = null;
    this.currentOptionValueId = 0;
    this.pCartOptionData = [
      {
        'product_option_id': 0,
        'product_option_value_id': 0,
      },
      {
        'product_option_id': 0,
        'product_option_value_id': 0,
      }
    ];
    pImages = [];
    notify();
  }

  updatePriceInfo(POptionValues cOptionValues) {
    if (cOptionValues.isFlatOffer == true) {
      havingOfferCItem = true;
      productSpPrice = applicableStdPrice;
      productStdPriceDis = productStdPrice;
      _lastParentPriceDiff = 0.0;
    } else {
      if (cOptionValues.price > 0) {
        if (this._customerOffer?.customerPrice?.is_flexible_option != true) {
          havingOfferCItem =
              _findThisProductOptionHasCustomerOffer(cOptionValues);
        } else {
          /// make always true for flexible product options
          havingOfferCItem = true;
        }
        if (cOptionValues.price_prefix == '+') {
          productSpPrice = (applicableStdPrice + cOptionValues.price);
          productStdPriceDis = (productStdPrice + cOptionValues.price);
          _lastParentPriceDiff = cOptionValues.price.toDouble();
        } else if (cOptionValues.price_prefix == '-') {
          productSpPrice = (applicableStdPrice - cOptionValues.price);
          productStdPriceDis = (productStdPrice - cOptionValues.price);
          _lastParentPriceDiff = (cOptionValues.price.toDouble() * -1);
        }
      } else {
        productSpPrice = applicableStdPrice;
        productStdPriceDis = productStdPrice;
        _lastParentPriceDiff = 0.0;
      }
    }
    priceCalc();
  }

  bool _findThisProductOptionHasCustomerOffer(POptionValues pOption) {
    if (_customerOffer.offeredOptions != null) {
      for (int i = 0; i < _customerOffer.offeredOptions.length; i++) {
        OfferedOptions cOfferedOptions = _customerOffer.offeredOptions[i];

        for (int j = 0; j < cOfferedOptions.optionValue.length; j++) {
          OptionValue cOptionValue = cOfferedOptions.optionValue[j];
          if ((pOption.product_option_value_id ==
                  cOptionValue.product_option_value_id) &&
              (pOption.product_option_id == cOptionValue.product_option_id)) {
            return true;
          }
        }
      }
    }
    return false;
  }

  _findThisChildOptionHasCustomerOffer() {
    var tempCartOptionData = jsonEncode(pCartOptionDataApi);
    var copyTempCartOptionData = jsonDecode(tempCartOptionData);
    //print(copyTempCartOptionData);
    if (_customerOffer.offeredOptions != null) {
      for (int i = 0; i < _customerOffer.offeredOptions.length; i++) {
        OfferedOptions cOfferedOptions = _customerOffer.offeredOptions[i];
        var resultCount = 0;
        cOfferedOptions.optionValue.forEach((cOptionValue) {
          var searchResult = copyTempCartOptionData.where((item) =>
              item['product_option_id'] == cOptionValue.product_option_id &&
              item['product_option_value_id'] ==
                  cOptionValue.product_option_value_id);
          if (searchResult.length != 0) {
            resultCount++;
          }
        });
        if (resultCount == copyTempCartOptionData.length) {
          return true;
        }
      }
    }
    return false;
  }

  updatePriceInfoForChild(POptionValues cOptionValues) {
    //print(pCartOptionDataApi);
    if (_customerOffer?.customerPrice?.is_flexible_option == true) {
      /// make always true for all child option if flexible flag is on
      havingOfferCItem = true;
    } else {
      havingOfferCItem = this._findThisChildOptionHasCustomerOffer();
    }
    if (this._customerOffer?.customerPrice?.is_flexible_option == true) {
      if (cOptionValues.price > 0 || _lastParentPriceDiff != 0.0) {
        if (cOptionValues.price_prefix == '+') {
          productSpPrice =
              (applicableStdPrice + cOptionValues.price + _lastParentPriceDiff);
          productStdPriceDis =
              (productStdPrice + cOptionValues.price + _lastParentPriceDiff);
        } else if (cOptionValues.price_prefix == '-') {
          productSpPrice =
              (applicableStdPrice - cOptionValues.price + _lastParentPriceDiff);
          productStdPriceDis =
              (productStdPrice - cOptionValues.price + _lastParentPriceDiff);
        }
      } else {
        productSpPrice = applicableStdPrice;
        productStdPriceDis = productStdPrice;
      }
    } else {
      if (customerOffer.customerPrice != null) {
        productSpPrice = applicableStdPrice;
        productStdPriceDis = productStdPrice;
      } else {
        if (cOptionValues.price > 0 || _lastParentPriceDiff != 0.0) {
          if (cOptionValues.price_prefix == '+') {
            productSpPrice = (applicableStdPrice +
                cOptionValues.price +
                _lastParentPriceDiff);
            productStdPriceDis =
                (productStdPrice + cOptionValues.price + _lastParentPriceDiff);
          } else if (cOptionValues.price_prefix == '-') {
            productSpPrice = (applicableStdPrice -
                cOptionValues.price +
                _lastParentPriceDiff);
            productStdPriceDis =
                (productStdPrice - cOptionValues.price + _lastParentPriceDiff);
          }
        } else {
          productSpPrice = applicableStdPrice;
          productStdPriceDis = productStdPrice;
        }
      }
    }

    priceCalc();
  }

  reloadImage(imageData) async {
    imageLoading = true;
    pImages = [];
    pImages = imageData;
    await autoUnload();
    return true;
  }

  autoUnload() async {
    await Future.delayed(Duration(milliseconds: 500));
    imageLoading = false;
    notify();
  }
}

enum imageReloadType { first, all }
