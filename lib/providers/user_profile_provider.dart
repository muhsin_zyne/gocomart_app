import 'package:flutter/material.dart';
import 'package:gocomartapp/models/profile/user.dart';

class UserProfileProvider extends ChangeNotifier {
  UserProfileProvider();
  User _userDetails;

  User get userDetails => _userDetails;

  set userDetails(val) {
    this._userDetails = val;
    notify();
  }

  notify() {
    try {
      notifyListeners();
    } catch (e) {}
  }
}
