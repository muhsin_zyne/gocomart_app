import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/src/locator.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

import 'config.dart';

export 'theme/hexcolor.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await translator.init(
    localeDefault: LocalizationDefaultType.device,
    languagesList: <String>['en', 'ar'],
    assetsDirectory: 'lang/',
  );

  FlavorConfig(
    flavor: Flavor.production,
    flavorValues: AppFlavors.prodFlavor,
  );
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  setupLocator();
  runApp(new LocalizedApp(
    child: MyApp(),
  ));
}
