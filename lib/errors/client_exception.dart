import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/ui_notifications.dart';

class GeneralClientException implements Exception {
  String cause;
  GeneralClientException(String message) {
    this.cause = message;
    UINotification.showInAppNotification(title: "Error!", message: this.cause, color: Colors.red, duration: Duration(seconds: 10));
  }
}
