import 'package:flutter/material.dart';
import 'package:gocomartapp/graphql_operation/mutations/index.dart'
    as mutations;
import 'package:gocomartapp/graphql_operation/queries/index.dart' as queries;
import 'package:gocomartapp/models/product-list/product_list.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/base/graphql_service.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class ShoppingServices {
  GlobalProvider _globalProvider;
  GraphQLClient _client;
  BuildContext context;
  GraphQLNetwork network;
  ShoppingServices({this.context}) {
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 100));
    loadClient();
  }

  void loadClient() async {
    _globalProvider = Provider.of<GlobalProvider>(context);
    _client = _globalProvider.apiClient();
    network = new GraphQLNetwork(context: context, client: this._client);
  }

  Future<QueryResult> getMixedShoppingHomeProducts() async {
    final QueryOptions options =
        QueryOptions(documentNode: gql(queries.mixedShoppingHome));
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<ShoppingHomeModel> getMixedShoppingHomeProductsV2() async {
    final QueryOptions options =
        QueryOptions(documentNode: gql(queries.mixedShoppingHome));
    final QueryResult result = await network.query(options: options);
    try {
      if (!result.hasException) {
        final ShoppingHomeModel shoppingHome =
            ShoppingHomeModel.fromJSON(result.data);

        return shoppingHome;
      } else {
        ShoppingHomeModel shoppingHome = ShoppingHomeModel();
        shoppingHome.error = true;
        if (result.exception.graphqlErrors.length > 0)
          shoppingHome.message =
              result.exception.graphqlErrors.first.toString();
        return shoppingHome;
      }
    } on Exception catch (e) {
      //TODO
      // unexpected api call error
      throw Exception();
    }
  }

  Future<GetAppBanners> getAppBanners({@required String bannerType}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(
        queries.getAppBanners,
      ),
      variables: <String, dynamic>{'banner_type': bannerType},
    );

    final QueryResult result = await network.query(options: options);

    try {
      if (!result.hasException) {
        final GetAppBanners getAppBanners =
            GetAppBanners.fromJson(result.data["getAppBanners"]);
        print(getAppBanners.banners.name);
        return getAppBanners;
      } else {
        GetAppBanners getAppBanners = GetAppBanners();
        getAppBanners.error = true;
        if (result.exception.graphqlErrors.length > 0)
          getAppBanners.message =
              result.exception.graphqlErrors.first.toString();
        return getAppBanners;
      }
    } on Exception catch (e) {
      //TODO
      // unexpected api call error
      throw Exception();
    }
  }

  Future<QueryResult> newAPICAll() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.me),
      fetchPolicy: FetchPolicy.networkOnly,
    );
    final QueryResult result = await network.query(
      options: options,
    );
    if (result.hasException) {
      print(result.exception);
    } else {
      print(result.data);
    }
    return result;
  }

  //offered Products query
  Future<QueryResult> getOfferProducts() async {
    print(_globalProvider.authToken);
    final QueryOptions options =
        QueryOptions(documentNode: gql(queries.getOfferProducts));

    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getTrendingProducts() async {
    final QueryOptions options =
        QueryOptions(documentNode: gql(queries.getTrendingProducts));
    final QueryResult result = await network.query(options: options);

    return result;
  }

  Future<QueryResult> getRecentViewProducts() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getRecentViewProducts),
    );
    final QueryResult result = await network.query(options: options);

    return result;
  }

  Future<QueryResult> appSearchResult({@required term}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.searchProduct),
      variables: <String, dynamic>{
        'term': term,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getProductDtl({@required productId}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getProductDtl),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    final QueryResult result = await network.query(options: options);

    final MutationOptions mutateOptions = MutationOptions(
      documentNode: gql(mutations.addRecentProduct),
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    network.mutate(options: mutateOptions);

    return result;
  }

  //new
  Future<QueryResult> getProductDtlnew({@required productId}) async {
    print("Calleeeedd successfully");
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getProductDtlNew),
      //fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    final QueryResult result = await network.query(options: options);

    // final MutationOptions mutateOptions = MutationOptions(
    //   documentNode: gql(mutations.addRecentProduct),
    //   variables: <String, dynamic>{
    //     'product_id': productId,
    //   },
    // );
    // network.mutate(options: mutateOptions);
    print("Calleeeedd successfully");
    return result;
  }

  Future<QueryResult> customerOfferByProduct({@required productId}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.customerOfferByProduct),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getFiltersByGroup() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getFiltersByGroupMin),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // get filter group based on category entered
  Future<QueryResult> getFilterGroupByCategory({@required categoryId}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getFilterGroupByCategory),
      variables: <String, dynamic>{
        'categoryId': categoryId,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // get filter group filters only by category and filterGroupId
  Future<QueryResult> getFilterGroupFilters(
      {@required categoryId, @required filterGroupId}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getFilterGroupFilters),
      variables: <String, dynamic>{
        'categoryId': categoryId,
        'filterGroupId': filterGroupId,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getProductReview(
      {@required currentPage,
      @required dataLimit,
      // ignore: non_constant_identifier_names
      @required product_id}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getProducrReview),
      variables: <String, dynamic>{
        'product_id': product_id,
        'page': currentPage,
        'limit': dataLimit
      },
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  //Wish list status
  Future<QueryResult> wishListStatus({@required product}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.wishListStatus),
      variables: <String, dynamic>{'product': product},
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> addToWishList({@required productId}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.addToWishList),
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> removeFromWishList({@required productId}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.removeFromWishlist),
      variables: <String, dynamic>{
        'product_id': productId,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> productWishList() async {
    final QueryOptions options = QueryOptions(
        documentNode: gql(queries.wishlist),
        fetchPolicy: FetchPolicy.networkOnly);
    final QueryResult result = await network.query(options: options);

    return result;
  }

  //Delivery Estimation
  Future<QueryResult> deliveryEstimation({@required postalCode}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.deliveryEstimation),
      variables: <String, dynamic>{"postal_code": postalCode},
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  //product option image
  Future<QueryResult> productOptionImages({
    @required pOptionValueId,
  }) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.productOptionImages),
      variables: <String, dynamic>{
        'product_option_value_id': pOptionValueId,
      },
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  //add Address
  Future<QueryResult> addAddress({
    String company,
    @required String address_1,
    @required String address_2,
    @required String city,
    @required postcode,
    @required postalCodeId,
    @required String type,
    String landmark,
    @required String name,
    @required String mobileNumber,
    String altContact = '',
  }) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.addAddress),
      variables: <String, dynamic>{
        "mobile_number": mobileNumber,
        "address_1": address_1,
        "address_2": address_2,
        "alt_contact": altContact,
        "city": city,
        "name": name,
        "postcode": postcode,
        "postal_code_id": postalCodeId,
        "landmark": landmark,
        "type": type
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  //get address
  Future<QueryResult> getAddress() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getAddress),
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  //editAddress
  Future<QueryResult> editAddress(
      {String company,
      @required addressId,
      @required String address_1,
      @required String address_2,
      @required String city,
      @required postcode,
      @required postalCodeId,
      @required String type,
      String landmark,
      @required String name,
      @required String mobileNumber,
      String altContact}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.editAddress),
      variables: <String, dynamic>{
        "address_id": addressId,
        "mobile_number": mobileNumber,
        "address_1": address_1,
        "address_2": address_2,
        "alt_contact": altContact,
        "city": city,
        "name": name,
        "postcode": postcode,
        "postal_code_id": postalCodeId,
        "landmark": landmark,
        "type": type
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  // ignore: non_constant_identifier_names
  Future deleteAddress({@required address_id}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.deleteAddress),
      variables: <String, dynamic>{
        'address_id': address_id,
      },
    );
    final QueryResult result = await _client.mutate(options);
    return result;
  }

  // ignore: non_constant_identifier_names
  Future<QueryResult> orderPlaceRequest(
      {@required shippingAddressId,
      @required products,
      @required productOptions}) async {
    final MutationOptions options = MutationOptions(
        documentNode: gql(mutations.orderPlaceRequest),
        variables: <String, dynamic>{
          "shipping_address_id": shippingAddressId,
          "products": products,
          "productOptions": productOptions
        });

    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  // Add to cart
  Future<QueryResult> addToCart({@required productId, cartOptionInput}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.addToCart),
      variables: <String, dynamic>{
        "product_id": productId,
        "cartOptionInput": cartOptionInput
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  // Get cart items
  Future<QueryResult> getCartItems() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getCartItems),
      fetchPolicy: FetchPolicy.networkOnly,
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // remove from cart

  Future<QueryResult> removeFromCart(
      {@required productId, cartOptionInput, cartId}) async {
    if (cartId == null) {
      final MutationOptions options = MutationOptions(
        documentNode: gql(mutations.removeFromCart),
        variables: <String, dynamic>{
          "product_id": productId,
          "cartOptionInput": cartOptionInput,
        },
        fetchPolicy: FetchPolicy.networkOnly,
      );
      final QueryResult result = await network.mutate(options: options);
      return result;
    } else {
      final MutationOptions options = MutationOptions(
        documentNode: gql(mutations.removeFromCart),
        variables: <String, dynamic>{
          "product_id": productId,
          "cart_id": cartId
        },
        fetchPolicy: FetchPolicy.networkOnly,
      );
      final QueryResult result = await network.mutate(options: options);
      return result;
    }
  }

  //review of ordered product listing
  Future<QueryResult> getCustomerOrderReview(
      {@required currentPage, @required dataLimit}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.get_customer_order_review),
      variables: <String, dynamic>{'page': currentPage, 'limit': dataLimit},
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  //create review
  Future<QueryResult> createReview({
    // ignore: non_constant_identifier_names
    @required product_id,
    String message,
    @required rating,
    // ignore: non_constant_identifier_names
    @required order_id,
    String title,
    // ignore: non_constant_identifier_names
    @required order_product_id,
  }) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.createReview),
      variables: <String, dynamic>{
        "product_id": product_id,
        "message": message,
        "rating": rating,
        "order_id": order_id,
        "title": title,
        "order_product_id": order_product_id,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  //view review of product
  Future<QueryResult> viewReview({@required reviewId}) async {
    final QueryOptions options = QueryOptions(
        documentNode: gql(queries.viewReview),
        variables: <String, dynamic>{"review_id": reviewId});
    final QueryResult result = await network.query(options: options);
    return result;
  }

  //update review
  Future<QueryResult> updateReview({
    @required reviewId,
    @required rating,
    String message,
    String title,
  }) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.updateReview),
      variables: <String, dynamic>{
        "review_id": reviewId,
        "title": title,
        "message": message,
        "rating": rating,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> singleProductBuyNow({
    @required productId,
    cartOptionInput,
  }) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.singleProductBuyNow),
      variables: <String, dynamic>{
        "product_id": productId,
        "cartOptionInput": cartOptionInput
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> defaultDeliveryAddress() async {
    final QueryOptions options =
        QueryOptions(documentNode: gql(queries.defaultDeliveryAddress));
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> cartProductByNow() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.cartProductByNow),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  //validate pincode
  Future<QueryResult> validatePincode({@required pincode}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.validatePinCode),
      variables: <String, dynamic>{
        "pincode": pincode,
      },
    );

    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> productList(
      {@required categories,
      @required filters,
      @required page,
      @required limit,
      @required sort,
      @required isNetwork}) async {
    if (isNetwork == true) {
      final QueryOptions options = QueryOptions(
        fetchPolicy: FetchPolicy.networkOnly,
        documentNode: gql(queries.productList),
        variables: <String, dynamic>{
          'categories': categories,
          'filters': filters,
          'page': page,
          'limit': limit,
          'sort': sort
        },
      );

      final QueryResult result = await network.query(options: options);
      return result;
    } else {
      final QueryOptions options = QueryOptions(
        documentNode: gql(queries.productList),
        variables: <String, dynamic>{
          'categories': categories,
          'filters': filters,
          'page': page,
          'limit': limit,
          'sort': sort,
        },
      );
      final QueryResult result = await network.query(options: options);
      print(result.exception);
      return result;
    }
  }

  //get orders
  Future<QueryResult> getOrders(
      {@required currentPage, @required dataLimit}) async {
    final QueryOptions options = QueryOptions(
        documentNode: gql(queries.getOrders),
        variables: <String, dynamic>{'page': currentPage, 'limit': dataLimit});

    final QueryResult result = await network.query(options: options);
    return result;
  }

  //get orders
  Future<QueryResult> getOrderDetails({
    @required orderRequestGuid,
  }) async {
    final QueryOptions options = QueryOptions(
        documentNode: gql(queries.getOrderDetails),
        variables: <String, dynamic>{'order_request_guid': orderRequestGuid});

    final QueryResult result = await network.query(options: options);
    return result;
  }

  /// Most Trending Product Based on Category ID

  Future<ProductList> getMostTrendingProductByCategoryId(int categoryId) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getTrendingProductsByCategoryId),
      variables: <String, dynamic>{
        'categoryId': categoryId,
      },
    );
    final QueryResult result = await network.query(options: options);
    print(result.data['trendingProducts']);
    try {
      if (!result.hasException) {
        final ProductList productList =
            ProductList.fromJson(result.data['trendingProducts']);
        return productList;
      } else {
        ProductList productList = ProductList();
        productList.error = true;
        if (result.exception.graphqlErrors.length > 0)
          productList.message = result.exception.graphqlErrors.first.toString();
        return productList;
      }
    } on Exception catch (e) {
      //TODO
      // unexpected api call error
      throw Exception();
    }
  }
}
