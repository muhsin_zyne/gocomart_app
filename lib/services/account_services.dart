import 'package:flutter/material.dart';
import 'package:gocomartapp/graphql_operation/mutations/index.dart' as mutations;
import 'package:gocomartapp/graphql_operation/queries/index.dart' as queries;
import 'package:gocomartapp/models/login_response.dart';
import 'package:gocomartapp/models/otp_validation_response.dart';
import 'package:gocomartapp/models/scratch_card/scratchCards.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/base/graphql_service.dart';
import 'package:gocomartapp/src/models/requests/account/claimScratchCardRequest.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:gocomartapp/src/models/requests/auth/VerifyOtpRequest.dart';
import 'package:gocomartapp/src/models/requests/base/paginationRequest.dart';
import 'package:gocomartapp/src/models/response/base/processStatus.dart';
import 'package:gocomartapp/src/models/response/wallet/ScratchCardPromotional.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class AccountServices {
  GlobalProvider _globalProvider;
  GraphQLClient _client;
  BuildContext context;
  GraphQLNetwork network;
  AccountServices({this.context}) {
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 100));
    loadClient();
  }

  void loadClient() async {
    _globalProvider = Provider.of<GlobalProvider>(context);
    _client = _globalProvider.apiClient();
    network = new GraphQLNetwork(context: context, client: this._client);
  }

  // wallet home screen mixed queries
  Future<QueryResult> walletScreenQuery({
    @required transPage,
    @required transLimit,
  }) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.walletScreenQuery),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'page': transPage,
        'limit': transLimit,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // scratch card clime service request
  Future<ProcessStatus> claimScratchCard(ClaimScratchCardRequest claimScratchCardRequest) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.claimScratchCard),
      variables: claimScratchCardRequest.toJson(),
    );
    final QueryResult result = await network.mutate(options: options);
    try {
      if (!result.hasException) {
        final ProcessStatus processStatus = ProcessStatus.fromJSON(result.data['claimScratchCard']);

        return processStatus;
      } else {
        ProcessStatus processStatus = ProcessStatus();
        processStatus.error = true;
        if (result.exception.graphqlErrors.length > 0) processStatus.message = result.exception.graphqlErrors.first.toString();
        return processStatus;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }

  Future<QueryResult> updateProfile({String firstName, String lastName, @required String dob, @required String email, @required String name}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.updateProfile),
      variables: <String, dynamic>{'first_name': firstName, 'last_name': lastName, 'email': email, 'dob': dob, 'name': name},
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> userDetails() async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.userDetails),
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> getAddress() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getAddress),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // CashWallet  transaction history
  Future<QueryResult> cashWalletTransactions({
    @required transPage,
    @required transLimit,
  }) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.cashWalletTransactionHistory),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'page': transPage,
        'limit': transLimit,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // CashWallet  transaction history
  Future<QueryResult> coinWalletTransactions({
    @required transPage,
    @required transLimit,
  }) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.coinWalletTransactionHistory),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'page': transPage,
        'limit': transLimit,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // Scratch cards

  Future<ScratchCards> getScratchCards({PaginationRequest pagination}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getScratchCards),
      variables: pagination.toJson(),
    );
    final QueryResult result = await network.query(options: options);
    try {
      if (!result.hasException) {
        final ScratchCards scratchCards = ScratchCards.fromJson(result.data['getScratchCards']);
        if (result.data['totalCashCollected'] != null) {
          scratchCards.collectedAmount = result.data['totalCashCollected']['amount'].toDouble();
        }
        return scratchCards;
      } else {
        ScratchCards scratchCards = ScratchCards();
        scratchCards.error = true;
        if (result.exception.graphqlErrors.length > 0) scratchCards.message = result.exception.graphqlErrors.first.toString();
        return scratchCards;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }

  // Scratch card promotional text

  Future<ScPromotionalText> scPromotionalText() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.scPromotionalText),
    );
    final QueryResult result = await network.query(options: options);
    try {
      if (!result.hasException) {
        final ScPromotionalText scPromotionalText = ScPromotionalText.fromJSON(result.data['scPromotionalText']);
        return scPromotionalText;
      } else {
        ScPromotionalText scPromotionalText = ScPromotionalText();
        scPromotionalText.error = true;
        if (result.exception.graphqlErrors.length > 0) scPromotionalText.message = result.exception.graphqlErrors.first.toString();
        return scPromotionalText;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }

  // action Login
  Future<LoginResponse> actionLogin(LoginScreenRequest loginScreenRequest) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.accountLogin),
      variables: loginScreenRequest.toJson(),
    );
    final QueryResult result = await network.mutate(options: options);
    print(result.exception);
    try {
      if (!result.hasException) {
        final LoginResponse loginResponse = LoginResponse.fromJson(result.data['login']);
        return loginResponse;
      } else {
        LoginResponse loginResponse = LoginResponse();
        loginResponse.error = true;
        if (result.exception.graphqlErrors.length > 0) loginResponse.message = result.exception.graphqlErrors.first.toString();
        return loginResponse;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }

  Future<OtpValidationResponse> accountVerifyOTP(VerifyOtpRequest verifyOtpRequest) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.accountVerifyOTP),
      variables: verifyOtpRequest.toJson(),
    );
    final QueryResult result = await network.mutate(options: options);
    try {
      if (!result.hasException) {
        final OtpValidationResponse validationResponse = OtpValidationResponse.fromJson(result.data['verifyOTP']);
        return validationResponse;
      } else {
        OtpValidationResponse validationResponse = OtpValidationResponse();
        validationResponse.error = true;
        if (result.exception.graphqlErrors.length > 0) validationResponse.message = result.exception.graphqlErrors.first.toString();
        return validationResponse;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }
}
