import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gocomartapp/graphql_operation/mutations/index.dart' as mutations;
import 'package:gocomartapp/graphql_operation/queries/index.dart' as queries;
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/base/graphql_service.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class CheckOutServices {
  GlobalProvider _globalProvider;
  GraphQLClient _client;
  BuildContext context;
  GraphQLNetwork network;

  CheckOutServices({this.context}) {
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 1));
    loadClient();
  }

  void loadClient() async {
    _globalProvider = Provider.of<GlobalProvider>(context);
    _client = _globalProvider.apiClient();
    network = new GraphQLNetwork(context: context, client: this._client);
  }

  Future<QueryResult> getPaymentOption({@required addressId}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getPaymentOption),
      variables: <String, dynamic>{
        'addressId': addressId,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> addNewPaymentOption({@required paymentOptionId}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.addNewPaymentOption),
      variables: <String, dynamic>{
        'payment_option_id': paymentOptionId,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> getUserPaymentOptions() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getUserPaymentOptions),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> razorPayOrderInit({@required paymentOptionId, @required amount, @required orderRequestGuid}) async {
    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.razorPayOrderInit),
      variables: <String, dynamic>{
        'payment_option_id': paymentOptionId,
        'amount': amount,
        'order_request_guid': orderRequestGuid,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }

  Future<QueryResult> requestOrder({
    @required billingAddressId,
    @required shippingAddressId,
    @required orderRequestId,
    @required orderRequestGuid,
    @required paymentChannel,
    @required goCoPaymentData,
  }) async {
    var jsondata = {
      'billing_address_id': billingAddressId,
      'shipping_address_id': shippingAddressId,
      'order_request_id': orderRequestId,
      'order_request_guid': orderRequestGuid,
      'paymentchannel': paymentChannel,
      'goco_paymentdata': goCoPaymentData
    };

    print(jsonEncode(jsondata));

    print(_globalProvider.authToken);

    final MutationOptions options = MutationOptions(
      documentNode: gql(mutations.requestOrder),
      variables: <String, dynamic>{
        'billing_address_id': billingAddressId,
        'shipping_address_id': shippingAddressId,
        'order_request_id': orderRequestId,
        'order_request_guid': orderRequestGuid,
        'paymentchannel': paymentChannel,
        'goco_paymentdata': goCoPaymentData,
      },
    );
    final QueryResult result = await network.mutate(options: options);
    return result;
  }
}
