import 'package:flutter/material.dart';
import 'package:gocomartapp/graphql_operation/queries/index.dart' as queries;
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/base/graphql_service.dart';
import 'package:gocomartapp/src/models/response/wallet/WalletAppLinksMixed.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class AppServices {
  GlobalProvider _globalProvider;
  GraphQLClient _client;
  BuildContext context;
  GraphQLNetwork network;
  AppServices({this.context}) {
    init();
  }

  void init() async {
    await Future.delayed(Duration(microseconds: 1));
    loadClient();
  }

  void loadClient() async {
    _globalProvider = Provider.of<GlobalProvider>(context);
    _client = _globalProvider.apiClient();
    network = new GraphQLNetwork(context: context, client: this._client);
  }

  testQuery() async {}

  Future<QueryResult> appConstLoad() async {
    final QueryOptions options = QueryOptions(documentNode: gql(queries.appDynamicConst));
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getWalletAppLinks({@required transPage, @required transLimit}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getWalletAppLinks),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'page': transPage,
        'limit': transLimit,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<WalletAppLinksMixed> getWalletAppLinksModel({@required transPage, @required transLimit}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getWalletAppLinks),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: <String, dynamic>{
        'page': transPage,
        'limit': transLimit,
      },
    );
    final QueryResult result = await network.query(options: options);
    print(result.exception);
    try {
      if (!result.hasException) {
        WalletAppLinksMixed walletAppLinksMixed = WalletAppLinksMixed.fromJSON(result.data);
        return walletAppLinksMixed;
      } else {
        WalletAppLinksMixed walletAppLinksMixed = WalletAppLinksMixed();
        walletAppLinksMixed.error = true;
        if (result.exception.graphqlErrors.length > 0) walletAppLinksMixed.message = result.exception.graphqlErrors.first.toString();
        return walletAppLinksMixed;
      }
    } on Exception catch (e) {
      throw Exception();
    }
  }

  Future<QueryResult> getWalletOffers({@required walletType}) async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.getWalletOffers),
      variables: <String, dynamic>{
        'wallet_type': walletType,
      },
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  Future<QueryResult> getProfileAppLinks() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.profileAppLinks),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }

  // app Categories fetch first 3 level
  Future<QueryResult> appCategories() async {
    final QueryOptions options = QueryOptions(
      documentNode: gql(queries.appCategories),
    );
    final QueryResult result = await network.query(options: options);
    return result;
  }
}
