import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:gocomartapp/errors/client_exception.dart';
import 'package:gocomartapp/errors/token_expired.dart';
import 'package:gocomartapp/graphql_operation/mutations/index.dart' as mutations;
import 'package:gocomartapp/models/auth/RefreshTokenResponse.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:graphql/client.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_const.dart';

class GraphQLNetwork {
  GraphQLNetwork({this.context, GraphQLClient client}) {
    this._client = client;
    _init();
  }

  GraphQLClient _client;
  BuildContext context;
  GlobalProvider _globalProvider;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  _init() {
    Future.delayed(Duration(microseconds: 100));
    _globalProvider = Provider.of<GlobalProvider>(context);
  }

  set client(GraphQLClient client) {
    this._client = client;
  }

  GraphQLClient get client => _client;

  /// Query Modification gose here
  ///
  ///

  Future query({@required QueryOptions options, int attempt = 1}) async {
    try {
      QueryResult result = await this.client.query(options).timeout(GraphQLSettings.API_TIME_OUT_GRAPH);
      //print(result.data);
      //print(result.exception);
      if (result.hasException) {
        if (result.exception.clientException != null) {
          throw GeneralClientException(result.exception.clientException.message);
        }
        if (result.exception.graphqlErrors != []) {
          print(result.exception.graphqlErrors);
          try {
            if (result.exception.graphqlErrors.first.message == GraphQLExceptionStringConst.TOKEN_HAS_EXPIRED) {
              /// retry for 3 time
              if (attempt <= 3) {
                GraphQLClient updatedClient = await this._refreshToken();
                if (updatedClient != null) {
                  result = await this.query(options: options, attempt: (attempt + 1));
                }
              }

              /// retry eliminated token expired exception
              if (!result.hasException) {
                return result;
              }

              /// reached 3 attempts and still has exception
              if (attempt >= 3 && result.hasException) {
                print(result.exception);
                throw TokenInvalidException();
              }
            }
          } catch (Exception) {}
        }
        return result;
      } else {
        return result;
      }
    } on TimeoutException catch (e) {
      // exception handle for time out
    } on TokenInvalidException catch (e) {
      // logout action gose here
    } on Exception catch (e) {}
  }

  Future mutate({@required MutationOptions options, int attempt = 1}) async {
    try {
      QueryResult result = await this.client.mutate(options).timeout(GraphQLSettings.API_TIME_OUT_GRAPH);
      //print(result.data);
      //print(result.exception);
      if (result.hasException) {
        if (result.exception.clientException != null) {
          throw GeneralClientException(result.exception.clientException.message);
        }
        if (result.exception.graphqlErrors != []) {
          print(result.exception.graphqlErrors);
          try {
            if (result.exception.graphqlErrors.first.message == GraphQLExceptionStringConst.TOKEN_HAS_EXPIRED) {
              /// retry for 3 time
              if (attempt <= 3) {
                GraphQLClient updatedClient = await this._refreshToken();
                if (updatedClient != null) {
                  result = await this.mutate(options: options, attempt: (attempt + 1));
                }
              }

              /// retry eliminated token expired exception
              if (!result.hasException) {
                return result;
              }

              /// reached 3 attempts and still has exception
              if (attempt >= 3 && result.hasException) {
                print(result.exception);
                throw TokenInvalidException();
              }
            }
          } catch (Exception) {}
        }
        return result;
      } else {
        return result;
      }
    } on TimeoutException catch (e) {
      // exception handle for time out
    } on TokenInvalidException catch (e) {
      // logout action gose here
    } on Exception catch (e) {}
  }

  Future _refreshToken() async {
    final SharedPreferences prefs = await _prefs;
    try {
      var refreshTokenString = prefs.getString('refreshToken');
      if (refreshTokenString != null) {
        final MutationOptions options = MutationOptions(
          documentNode: gql(mutations.refreshToken),
          variables: <String, dynamic>{
            'refreshToken': refreshTokenString,
          },
        );
        QueryResult refreshTokenDetails = await this.client.mutate(options);

        if (!refreshTokenDetails.hasException) {
          RefreshTokenResponse refreshTokenResponse = RefreshTokenResponse.fromJSON(refreshTokenDetails.data);
          if (refreshTokenResponse.refreshToken.refreshToken != null && refreshTokenResponse.refreshToken.authToken != null) {
            prefs.setString('auth_token', refreshTokenResponse.refreshToken.authToken);
            prefs.setString('refreshToken', refreshTokenResponse.refreshToken.refreshToken);
            _globalProvider.authToken = refreshTokenResponse.refreshToken.authToken;
            _globalProvider.refreshToken = refreshTokenResponse.refreshToken.refreshToken;
            this.client = _globalProvider.apiClient(token: _globalProvider.authToken);
            return this.client;
          } else {
            return null;
          }
        }
        return null;
      }
    } on Exception catch (e) {
      print("refresh token exception");
      return null;
    }

    return false;
  }

  void notValidTokenError() async {}
}
