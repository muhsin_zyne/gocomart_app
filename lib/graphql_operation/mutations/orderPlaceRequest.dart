String orderPlaceRequest = r'''
mutation orderPlaceRequest($shipping_address_id:Int!,$products: [OrderProductsInput!]!,$productOptions:[OrderProductOptionsInput]
){
  orderPlaceRequest(shipping_address_id: $shipping_address_id,  billing_address_id: $shipping_address_id,products: $products,productOptions: $productOptions
  ){
    orderInfo{
      order_id
      invoice_no
      status{
        name
      }
    }
  }
}
''';