const String accountVerifyOTP = r'''
mutation verifyOTP($mobile: Float!, $code: Float!) {
  verifyOTP(mobile: $mobile, code: $code){
    auth_token
    refresh_token
      user {
        id
        mobile_no
        user_id
        status
      }
      error
      message
	}
}
''';
