String createReview = r'''
mutation createReview($product_id: Int!, $message: String, $rating: Int!, $order_id: Int!, $title: String, $order_product_id: Int!) {
  createReview(product_id: $product_id, message:$message, rating: $rating, order_id: $order_id, order_product_id: $order_product_id title: $title) {
  	error
    success
    review {
     	productInfo {
        image
        description {
          name
        }
        manufacturer {
          name
        }
      }
      review_id
    	product_id
    	customer_id
    	order_id
    	author
    	title
    	text
    	rating
    	date_added
    	date_modified
      orderInfo {
        date_modified
        order_status_id
        status {
          name
        }
      }
      
    }
  }
}
''';
