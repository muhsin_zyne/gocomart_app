const String requestOrder = r'''
mutation requestOrder(
  $order_request_id: Int, 
  $order_request_guid: String,
  $goco_paymentdata: GocoPaymentData!
  $paymentchannel: PaymentChannelData
  $billing_address_id: Int
  $shipping_address_id: Int
) {
  requestOrder(
    order_request_id: $order_request_id, 
    order_request_guid: $order_request_guid,
    goco_paymentdata: $goco_paymentdata,
    paymentchannel:$paymentchannel,
    billing_address_id: $billing_address_id
    shipping_address_id: $shipping_address_id
  ) {
    error
    message
    orderInfo {
      order_request_guid
      order_id
      invoice_no
      invoice_prefix
      est_delivery
      orderBenefitQuickInfo{
        totalItems
        orderPayAmount
        walletUsed
        walletExclusiveDiscount
      }
      orderPayment {
        is_cod
        order_payment_id
        order_id
        payment_option_id
        paymentOption {
          app_label
        }
      }
    }
  }
}
''';
