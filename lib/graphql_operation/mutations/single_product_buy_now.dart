const String singleProductBuyNow = r'''
mutation singleProductBuyNow($product_id: Int!, $cartOptionInput: [CartOptionInput]) {
  singleProductBuyNow(product_id: $product_id, option: $cartOptionInput) {
    success
    error
        items {
        product_id
      	cartImage
        quantity
        priceInfo {
          itemTotal
          hikeTotal
          totalShippingCost
          tax
        }
        product {
          price
          description {
            name
          }
          manufacturer{
            name
          }
        }
        
        productOptionsValues {
          productOption{
            option {
              description{
                name
              }
            }
          }
          productOptionValue{
            description {
              name
            }
          }
        }
      }
    	delivery {
        freeShippingCartTotal
      }
  }

}
''';
