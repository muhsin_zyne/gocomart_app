String razorPayOrderInit = r'''
mutation razorpayOrderInit($payment_option_id: Int!, $amount: Float!, $order_request_guid: String!) {
  razorpayOrderInit(payment_option_id: $payment_option_id, amount: $amount, order_request_guid: $order_request_guid) {
  order {
    id
    receipt
    amount
    attempts
    created_at
    status
  }
  }
}
''';
