const String addNewPaymentOption = r'''
mutation addNewPaymentOption($payment_option_id: Int!){
  addNewPaymentOption(payment_option_id:$payment_option_id ) {
    success
    error
  }
}
''';
