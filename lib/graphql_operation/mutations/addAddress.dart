String addAddress = r'''
mutation addAddress( $company: String, $address_1: String!, $address_2: String!, $city: String!, $postcode: Float!, $postal_code_id: Int!, $type: String!, $landmark: String,$name:String!,$mobile_number:String!, $alt_contact:String ) {
  addAddress(company: $company, address_1: $address_1, address_2: $address_2,city: $city, postcode: $postcode,type: $type, landmark: $landmark, name:$name,mobile_number:$mobile_number, alt_contact: $alt_contact, postal_code_id: $postal_code_id) {
    success
    error
    address {
      address_id
      customer_id
      firstname
      lastname
      company
      address_1
      address_2
      city
      postcode
      country_id
      zone_id
      custom_field
      type
      landmark
      name
      mobile_number
      alt_contact
    }
  }
}
''';
