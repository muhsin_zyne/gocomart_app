const String removeFromWishlist=r'''
mutation removeFromWishlist($product_id:Int!){
  removeFromWishlist(product_id:$product_id){
    success
    error
  }
}
''';