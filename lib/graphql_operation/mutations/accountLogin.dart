const String accountLogin = r'''
  mutation accountLogin($mobile_code: String!, $mobile: Float!, $unique_id: String!, $client_type: String!, $signature: String, $referral: String) {
  login(mobile_code: $mobile_code, mobile: $mobile, unique_id: $unique_id, client_type: $client_type, signature: $signature, referral: $referral){
	  message
	  error
	  otp
	}
}
''';
