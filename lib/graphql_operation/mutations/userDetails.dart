String userDetails = r'''

mutation userDetailS{
  me{
    user{
      id
      mobile_no
      user_id
      goco_id
      customerInfo{
        firstname
        lastname
        name
        email
        telephone
        profile_image
      	dob
      }
    }
  }
}
''';
