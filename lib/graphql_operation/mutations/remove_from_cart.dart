String removeFromCart = r'''
mutation removeFromCart($product_id: Int!, $cartOptionInput: [CartOptionInput], $cart_id: Int) {
  removeFromCart(product_id: $product_id, option: $cartOptionInput, cart_id: $cart_id) {
    error
    message
    items{
      cart_id
      product_id
      quantity
      cartImage
      customerPrice{
        offer_price
        is_flexible_option
        valid_to
        max_quantity
      }
      product{
        model
        sku
        price
        special {
          price
        }
        description {
          name
        }
        manufacturer{
          name
        }
      }
      productOptionsValues {
        productOption{
          product_option_id
          option {
            description{
              name
            }
          }
        }
        productOptionValue{
          product_option_value_id
          description {
            name
          }
          price_prefix
          price
        }
      }
    }
  }
}
''';
