const String claimScratchCard = r'''
  mutation claimScratchCard ($scratch_id: Int!, $latitude: Float!, $longitude: Float!) {
    claimScratchCard(scratch_id: $scratch_id, latitude: $latitude, longitude: $longitude) {
      error
      message
    }
  }
''';
