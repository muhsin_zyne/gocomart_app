String deleteAddress=r'''
mutation deleteAddress($address_id: Int!){
  deleteAddress(address_id:$address_id){
    success
    error
  }
}
''';