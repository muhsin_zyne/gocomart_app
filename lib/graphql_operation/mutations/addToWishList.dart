String addToWishList = r'''
mutation addToWishList($product_id:Int!){
  addToWishList(product_id:$product_id){
    product{
      product_id
    }
    error
  }
}
''';