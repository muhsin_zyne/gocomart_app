String refreshToken = r'''
mutation refreshToken($refreshToken: String!){
  refreshToken(token: $refreshToken){
    auth_token
    refresh_token
      user {
        id
        mobile_no
        user_id
        status
      }
  }
}
''';
