String updateProfile = r'''
mutation updateProfile($first_name:String,$last_name:String,$dob:Date,$email:String,$name:String!){
  updateProfile(first_name:$first_name,last_name:$last_name,email:,$email,dob:$dob,name:$name){
    customer{
      firstname
      lastname
      email
      name
    }
  }
}
''';
