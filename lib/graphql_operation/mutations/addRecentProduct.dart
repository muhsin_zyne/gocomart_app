String addRecentProduct = r'''
mutation addToRecentProducts($product_id:Int!){
  addToRecentProducts(product_id:$product_id){
    product{
      product_id
      productDetails{
        model
      }
      app_user_id
    }
  }
}
''';
