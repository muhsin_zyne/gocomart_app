String getCartItems = r'''
query {
    getCartItems {
    items{
      cart_id
      product_id
      quantity
      cartImage
      customerPrice{
        offer_price
        is_flexible_option
        valid_to
        max_quantity
      }
      product{
        model
        sku
        price
        special {
          price
        }
        description {
          name
        }
        manufacturer{
          name
        }
      }
      productOptionsValues {
        productOption{
          product_option_id
          option {
            description{
              name
            }
          }
        }
        productOptionValue{
          product_option_value_id
          description {
            name
          }
          price_prefix
          price
        }
      }
    }
  }
}
''';
