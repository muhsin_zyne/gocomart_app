const String me = r'''
query {
  me{
    user{
      id
      mobile_no
      goco_id
      is_goco_prime
      status
    }
    
    token
  }
}
''';
