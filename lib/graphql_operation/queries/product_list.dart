const String productList = r'''
query productList($categories: [Int], $filters: ProductFilterInput, $page:Int, $limit: Int, $sort: String){
  productList(categories:$categories, filters: $filters, page: $page, limit: $limit, sort: $sort) {
    error
    message
    total
    products{
      product_id
      sku
      description{
        name
      }
      image
      manufacturer{
        name
      }
      avlColorCount
      price
      special {
        price
      }
      customerPrice{
        offer_price
      }
    }
  }
}
''';
