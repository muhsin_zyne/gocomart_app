String getProducrReview=r'''
query getProductReviews($product_id:Int!,$page:Int!,$limit:Int!){
  getProductReviews(product_id:$product_id,page:$page,limit:$limit){
    reviews{
      author
      rating
      text
      title
      date_added
    }
  }
}
''';