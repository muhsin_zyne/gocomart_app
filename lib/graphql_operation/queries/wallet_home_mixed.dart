const String walletScreenQuery = r'''
  query getScratchCards($page: Int!, $limit: Int!) {
    transactionHistory(page:$page, limit: $limit) {
  	  total
      data{
        type
        scratch_card_id
        amount
        description
        created_at
      }
    }
    currentBalance {
      amount
    }
    totalCashCollected{
      amount
    }
    getUnscratchedCount {
      count
    }
  }
    
''';
