const String getProductDtlNew = r'''
query productDetail($product_id: ID!) {
  product(product_id: $product_id) {
     images{
      image
    }
    image
    description{
      name
    }
  	avgRating{
      avg_rating
    }
  	productOptionNew{
      option_id
      type
  		name
     	optionValuesNew{
        product_option_id
        product_option_value_id
        name
        image
        quantity
        childOptions {
          product_option_id
          option_id
          type
          name
          optionValuesNewChild {
            master_option_value
            product_option_id
            product_option_value_id
            name
            image
          }
        }
      }
    }
  }
}
''';
