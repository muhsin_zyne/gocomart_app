const String getScratchCards = r'''
 query getScratchCards($page: Int!, $limit: Int!) {
  getScratchCards(page: $page, limit: $limit) {
    total
    cards {
      id
      value
      is_scratched
      reward_text
      reward_promo_text
      generated_at
      scratched_on
      expires_on
      card_color
      texture_png
      reweal_on
      card_scheme
      advertiser {
        id
        name
      }
      advertiserImage {
        id
        advertiser_id
        image
      }
    }
  }
  totalCashCollected {
      amount
    }
}
''';
