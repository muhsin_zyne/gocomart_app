const String getUserPaymentOptions = r'''
query {
  getUserPaymentOptions {
      success
    	error
      userPaymentOptions{
        id
        paymentOption{
          payment_option_id
          name
          code
          app_label
          is_online
          public_key
          logo
        }
      }
  }
  proceedToPaymentCalculation {
    paymentInfo {
      order_request_id
      order_request_guid
      total_price
      total_delivery_fee
      wallet_discount
      delivery_fee_discount
      wallet_balance
    }
  }
}
''';
