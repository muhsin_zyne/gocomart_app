const String cartProductByNow = r'''
query {
  cartProductByNow {
    error
    message
        items {
        product_id
      	cartImage
        quantity
        priceInfo {
          itemTotal
          hikeTotal
          totalShippingCost
          tax
        }
        product {
          price
          description {
            name
          }
          manufacturer{
            name
          }
        }
        
        productOptionsValues {
          productOption{
            option {
              description{
                name
              }
            }
          }
          productOptionValue{
            description {
              name
            }
          }
        }
      }
    	delivery {
        freeShippingCartTotal
      }
  }
  defaultDeliveryAddress{
    address{
      address_id
      customer_id
      firstname
      lastname
      company
      address_1
      address_2
      city
      postcode
      country_id
      zone_id
      custom_field
      type
      landmark
      name
      mobile_number
      alt_contact
      postalZone{
        admin_name1
        country{
          country_name
        }
      }
    }
  }
}
''';
