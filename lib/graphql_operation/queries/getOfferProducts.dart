const String getOfferProducts = r'''
 query getOfferProducts{
   getOfferProducts{
		  	products {
          product_id
          price
          details {
            image
            price
            description {
              name
            }
            manufacturer {
              name
            }
          }
        }
  }
}
''';
