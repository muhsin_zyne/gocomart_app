const String productOptionImages = r'''
query productOptionImages($product_option_value_id:Int!) {
  productOptionImages(option: $product_option_value_id) {
    image
    images
  }
}
''';
