String customerOfferByProduct = r'''
query customerOfferByProduct($product_id: Int!) {
  customerOfferByProduct(product_id: $product_id) {
    success
    error
    customerOffer {
      customerPrice{
        ac_group_product_id
        product_id
        max_quantity
        offer_price
        is_flexible_option
        valid_to
        option_length
      }
      offeredOptions {
        ac_group_product_option_id
        ac_group_product_id
        product_id
        optionValue {
          ac_group_product_option_value_id
          product_option_id
          product_option_value_id
        }
      }
    }
  }
}
''';
