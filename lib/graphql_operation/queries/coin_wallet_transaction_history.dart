const String coinWalletTransactionHistory = r'''
  query transactionHistory($page: Int!, $limit: Int!) {
  coinTransactionHistory(page:$page, limit: $limit) {
  	  total
      transactions{
        type
        coin_scratch_card_id
        coin_value
        description
        created_at
      }
    }
   } 
''';
