const String getTrendingProductsByCategoryId = r'''
query trendingProducts($categoryId: Int!){
  trendingProducts(category_id: $categoryId) {
    products{
      product_id
      price
      image
      description{
        name
      }
      special{
        price
      }
      manufacturer{
        name
      }
    }
  }
}
''';
