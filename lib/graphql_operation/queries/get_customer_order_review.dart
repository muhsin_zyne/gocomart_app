// ignore: non_constant_identifier_names
String get_customer_order_review = r'''
  query getCustomerOrderReview($page:Int!,$limit:Int){
  getCustomerOrderReview(page: $page, limit: $limit){
    orders{
      order_product_id
      order_id
      order_status_id
      customer_id
      product_id
      name
      image
      last_order_history_time
      rating
      review_id
    }
  }
}
''';
