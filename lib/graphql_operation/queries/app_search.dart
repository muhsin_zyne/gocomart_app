const String searchProduct = r'''
 query searchProduct($term: String!){
  searchProduct(term:$term) {
     category {
      category_id
      name
      getParentName
    }
    products {
      product_id
      name
      details {
        image
      }
    }
  }
}
''';
