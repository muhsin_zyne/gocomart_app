String validatePinCode=r'''
query validatePinCode($pincode:Float!){
  validatePinCode(pincode: $pincode) {
    success
    error
    postals {
      id
      country_code
      postal_code
      place_name
      admin_name1
      admin_name2
      admin_name3
      country {
        country_name
        
      }
      
    }
  }
}
''';