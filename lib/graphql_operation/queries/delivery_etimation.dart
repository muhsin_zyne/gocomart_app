String deliveryEstimation = r'''
query deliveryEstimation($postal_code: Int!) {
 deliveryEstimation(postal_code: $postal_code) {
  error
  postalDetails{
    postal_code
    place_name
    admin_name1
    admin_name2
  }
  bestDeliveryEstimate {
    id
    radius_in_km
    min_days
    max_days
    faster_than_actual
    est_days
    distance
  }
}
}
''';
