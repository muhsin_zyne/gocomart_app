String profileAppLinks = r'''
query {
  getProfileAppLinks{
    error
    message
    links
    {
      id
      title
      title
      link
      icon
      description
      primary_color
      secondary_color
    }
  }
}
''';
