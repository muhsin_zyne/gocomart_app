const String getFilterGroupFilters = r'''
query getFilterGroupFilters($categoryId: Int!, $filterGroupId: Int){
  getFilterGroupFilters(category_id: $categoryId, filter_group_id: $filterGroupId) {
    filters{
      filter_id
      name
    }
  }
}
''';
