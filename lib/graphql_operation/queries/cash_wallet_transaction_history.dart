const String cashWalletTransactionHistory = r'''
  query transactionHistory($page: Int!, $limit: Int!) {
    transactionHistory(page:$page, limit: $limit) {
  	  total
      data{
        type
        scratch_card_id
        amount
        description
        created_at
      }
    }
  }
    
''';
