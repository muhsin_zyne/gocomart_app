String mixedShoppingHome = r'''
query getHomeProducts{
  #banners
  getAppBanners(banner_type: "app_banner") {
    banners{
      banner_id
      name
      images {
        banner_image_id
        title
        link
        image
      }
    }
  }
  
  appHomeCategories{
    category_id
    name
    image
  }
  
  currentTrends{
    trendingCategory{
      category_id
      name
    }
  }
  
  trendingBrands {
    total
    brands{
      manufacturer_id
      name
      image
      
    }
  }
  
  #getOfferProducts
  getOfferProducts{
		  	products {
          product_id
          price
          details {
            image
            price
            description {
              name
            }
            manufacturer {
              name
            }
          }
      }
  }
  
  #getTrendingProducts
   getTrendingProducts{
    products{
      product_id
      name
      model
      
      productDetails{
        image
        manufacturer_id
        manufacturer{
          name
        }
      }
    }
  }
  
  #recentProducts
  recentProducts{
    products{
      product_id
      app_user_id
      id
      productDetails{
        image
        description{
          name
        }
        manufacturer{
          name
        }
      }
    }
  }
  
}
''';
