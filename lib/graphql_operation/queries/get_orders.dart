String getOrders = r'''
query getOrders($page:Int!,$limit:Int){
  getOrders(page: $page, limit: $limit){

    time
    total
    orders{
      order_id
      invoice_no
      invoice_prefix
      order_request_guid
      date_added
      products{
        currentProductStatus{
          name
          order_status_id
          code
        }
        productDetails{
          description{
            name
          }
          manufacturer{
            name
          }
        }
        est_delivery
        dispatched_at
        act_delivery
        return_policy_end_at
        name
        model
        price
        total
        image
      }
    }
    total
  
  }
}
''';
