const String getWalletOffers = r'''
query getWalletOffers($wallet_type: String!) {
  getWalletOffers(wallet_type: $wallet_type) {
    error
    message
    offers {
      id
      title
      description
      content
      action_label
      action
      offer_from
      valid_from
      valid_to
      offer_card_image
      card_color
      priority
    }
  }
}
''';
