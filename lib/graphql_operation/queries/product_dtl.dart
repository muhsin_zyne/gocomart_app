const String getProductDtl = r'''
query productDetails($product_id: ID!) {
  product(product_id: $product_id) {
  	model
    sku
    price
    shipping
    date_available
    minimum
    status
    quantity
    manufacturer{
      name
    }
    attributes{
      attribute_id
      text
      getAttributeName
    }
    description {
      name
    }
    image
    images{
      product_image_id
      image
    }
    stock{
      stock_status_id
      name
    }
    product_option {
      option_id
      option {
        option_id
        type
        description {
          name
        }
      }
      master_option
      master_option_value
      optionValues {
        product_option_value_id
        product_option_id
        description {
          option_value_id
          name
        }
        optionImage {
          image
        }
        quantity
        price
        price_prefix
        master_option_value
      }
			value
      required
    
    }
    special{
      price
    }
    tax_class{
      title
      tax_class_id
    }
    avgRating{
      avg_rating
    }
    ratingSummary{
      rating
      count_of_rating
    }
    productReviewRatingCount{
      review_count
      rating
    }
  }
}
''';
