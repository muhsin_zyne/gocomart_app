const String getWalletAppLinks = r'''
query walletQueryMixed($page: Int!, $limit: Int!) {
  getWalletAppLinks{
    error
    message
    walletAppLinks {
      cashWallet{
        label
        links {
          id
          title
          description
          link
          icon
          primary_color
          secondary_color
          bg_color
          exclusiveMark
        }
      }
      coinWallet{
        label
        links {
          id
          title
          description
          link
          icon
          primary_color
          secondary_color
          bg_color
          exclusiveMark
        }
      }
    }
  }
  transactionHistory(page:$page, limit: $limit) {
    total
    data{
      type
      scratch_card_id
      amount
      description
      created_at
    }
  } 
  coinTransactionHistory(page:$page, limit: $limit) {
    total
    transactions{
      type
      coin_scratch_card_id
      coin_value
      description
      created_at
    }
  }
  currentBalance {
    amount
  }
  currentCoinBalance {
    coin
  }
  me {
    user {
      id
      mobile_no
      goco_id
      is_goco_prime
      status
      customerInfo{
        firstname
        lastname
        email
        telephone
        profile_image
      	dob
      }
    }
  }
}
''';
