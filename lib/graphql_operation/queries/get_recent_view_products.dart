String getRecentViewProducts = r'''
  query getRecentProducts{
  recentProducts{
    products{
      product_id
      app_user_id
      id
      productDetails{
        image
        description{
          name
        }
        manufacturer{
          name
        }
      }
    }
  }
}
''';