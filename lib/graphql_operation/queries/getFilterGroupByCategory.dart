const String getFilterGroupByCategory = r'''
query getFilterGroupByCategory($categoryId: Int!){
  getFilterGroupByCategory(category_id: $categoryId) {
    filterGroups{
      filter_group_id
      sort_order
      description {
        name
      }
    }
    filters{
      filter_id
      language_id
      filter_group_id
      name
    }
  }
}
''';
