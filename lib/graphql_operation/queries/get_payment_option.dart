String getPaymentOption = r'''
query queryFunction($addressId: Int) {

  codAvailability(address_id: $addressId){
		availabity
  }
  
  getPaymentOption{
    success
    error
    paymentOption{
      payment_option_id
      name
      app_label
      is_online
      logo
      public_key
    }
  }
  getPaymentOptionGroup {
    success
    error
    paymentOptionGroup{
      id
      group_name
      status
      paymentOptions {
        payment_option_id
        name
        app_label
        is_online
        public_key
        logo
      }
    }
  }
  getUserPaymentOptions {
      userPaymentOptions{
        id
        paymentOption{
          payment_option_id
          name
          code
          app_label
          is_online
          public_key
          logo
        }
      }
  }
  proceedToPaymentCalculation {
    paymentInfo {
      order_request_id
      order_request_guid
      total_price
      total_delivery_fee
      wallet_discount
      delivery_fee_discount
      wallet_balance
      wallet_exclusive_discount
    }
  }
}
''';
