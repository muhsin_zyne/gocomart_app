String appCategories = r'''
query {
appCategories {
  error
  message
  categories {
    category_id
    image
    description {
      name
      category_id
    }
    banner
    children {
      category_id
      image
      banner
      description {
        name
      }
      children {
        category_id
        image
        banner
        description {
          name
        }
    	}
    }
  }
}
}
''';
