const String getAppBanners = r'''
query getAppBanners($banner_type:String!){
  getAppBanners(banner_type:$banner_type) {
    banners{
      banner_id
      name
      images {
        banner_image_id
        title
        link
        image
      }
    }
  }
}
''';
