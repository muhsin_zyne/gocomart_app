String wishlist=r'''
query productWishlist{
  productWishlist{
    products{
      id
      product_id
      productDetails{
        image
        stock{
          stock_status_id
          name
        }
        price
        description{
          name
        }
        manufacturer{
          name
        }
        special{
          price
        }
      }
    }
    error
  }
}
''';