String viewReview=r'''
query viewReview($review_id:Int!){
  viewReview(review_id: $review_id) {
    error
    success
    review {
     	productInfo {
        image
        description {
          name
        }
        manufacturer {
          name
        }
      }
      review_id
    	product_id
    	customer_id
    	order_id
    	author
    	title
    	text
    	rating
    	date_added
    	date_modified
      orderInfo {
        date_modified
        order_status_id
        status {
          name
        }
      }
      
    }
  }
}
''';