const String getFiltersByGroupMin = r'''
query {
  getFiltersByGroup {
    filterGroups{
      filter_group_id
      sort_order
      description {
        name
      }
      filters{
        filter_id
        name
      }
    }
  }
}
''';
