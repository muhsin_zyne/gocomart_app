import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

import 'config.dart';
import 'controllers/curency_format.dart';

export 'theme/hexcolor.dart';

CurrencyFormat inr = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await translator.init(
    localeDefault: LocalizationDefaultType.device,
    languagesList: <String>['en', 'ar'],
    assetsDirectory: 'lang/',
  );

  FlavorConfig(
    flavor: Flavor.staging,
    flavorValues: AppFlavors.stageFlavor,
  );

  await OneSignal.shared.setRequiresUserPrivacyConsent(true);
  await OneSignal.shared.consentGranted(true);
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  OneSignal.shared.init(FlavorConfig.instance.flavorValues.oneSignalAppId, iOSSettings: {OSiOSSettings.autoPrompt: false, OSiOSSettings.inAppLaunchUrl: false});
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(new MyApp());
}
