import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:html_unescape/html_unescape.dart';

class AppSortList {
  final int id;
  final String name;
  final String value;
  const AppSortList({
    this.name,
    this.id,
    this.value,
  });

  static const AppSortList Relevance = AppSortList(id: 1, name: "Relevance", value: "relevance");
  static const AppSortList Popularity = AppSortList(id: 2, name: "Popularity", value: "popularity");
  static const AppSortList PriceLow = AppSortList(id: 3, name: "Price Low to High", value: "price_low_to_high");
  static const AppSortList PriceHigh = AppSortList(id: 4, name: "Price High to Low", value: "price_high_to_low");
  static const AppSortList Newest = AppSortList(id: 5, name: "Newest First", value: "new_first");

  static const ALL = [Relevance, Popularity, PriceLow, PriceHigh, Newest];
}

class GraphQLExceptionStringConst {
  static const String TOKEN_HAS_EXPIRED = 'Token has expired';
  static const String INVALID_REFRESH_TOKEN = 'Invalid Refresh Token';
}

class GraphQLSettings {
  static const Duration API_TIME_OUT_GRAPH = Duration(seconds: 10);
}

class CurrencyConst {
  static CurrencyFormat inrFormat1 = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '0#');
  static CurrencyFormat inrFormat2 = CurrencyFormat(format: "##,##,##0", symbol: '₹', decimal: '##');
}

class PushNotificationTypeConst {
  static const String NAVIGATOR = 'navigator';
}

class PaymentModeConst {
  static const String RAZORPAY = 'razorpay';
  static const String RAZORPAY_UPI = 'razorpay-upi';
}

HtmlUnescape htmlUnescape = HtmlUnescape();

class PaginationConst {
  static const CASH_REWARDS_PER_PAGE = 12;
}
