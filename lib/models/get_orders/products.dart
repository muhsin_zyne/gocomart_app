import 'package:gocomartapp/models/get_orders/current_product_status.dart';
import 'package:json_annotation/json_annotation.dart';
import "productDetails.dart";
part 'products.g.dart';

@JsonSerializable()
class Products {
  Products();

  ProductDetails productDetails;
  CurrentProductStatus currentProductStatus;
  String name;
  String model;
  num price;
  num total;
  String image;
  String estDelivery;
  String dispatchedAt;
  String actDelivery;
  String returnPolicyEndAt;

  factory Products.fromJson(Map<String, dynamic> json) =>
      _$ProductsFromJson(json);
  Map<String, dynamic> toJson() => _$ProductsToJson(this);
}
