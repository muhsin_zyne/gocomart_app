import 'package:json_annotation/json_annotation.dart';
import "description.dart";
import "manufacturer.dart";
part 'productDetails.g.dart';

@JsonSerializable()
class ProductDetails {
    ProductDetails();

    Description description;
    Manufacturer manufacturer;
    
    factory ProductDetails.fromJson(Map<String,dynamic> json) => _$ProductDetailsFromJson(json);
    Map<String, dynamic> toJson() => _$ProductDetailsToJson(this);
}
