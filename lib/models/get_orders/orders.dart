import 'package:json_annotation/json_annotation.dart';
import "products.dart";
part 'orders.g.dart';

@JsonSerializable()
class Orders {
    Orders();

    String order_id;
    num invoice_no;
    String invoice_prefix;
    String order_request_guid;
    String date_added;
    List<Products> products;
    
    factory Orders.fromJson(Map<String,dynamic> json) => _$OrdersFromJson(json);
    Map<String, dynamic> toJson() => _$OrdersToJson(this);
}
