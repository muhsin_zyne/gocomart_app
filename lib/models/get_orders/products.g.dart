// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Products _$ProductsFromJson(Map<String, dynamic> json) {
  return Products()
    ..productDetails = json['productDetails'] == null
        ? null
        : ProductDetails.fromJson(
            json['productDetails'] as Map<String, dynamic>)
    ..currentProductStatus = json['currentProductStatus'] == null
        ? null
        : CurrentProductStatus.fromJSON(
            json['currentProductStatus'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..model = json['model'] as String
    ..price = json['price'] as num
    ..total = json['total'] as num
    ..image = json['image'] as String
    ..estDelivery = json['est_delivery'] as String
    ..dispatchedAt = json['dispatched_at'] as String
    ..actDelivery = json['act_delivery'] as String
    ..returnPolicyEndAt = json['return_policy_end_at'] as String;
}

Map<String, dynamic> _$ProductsToJson(Products instance) => <String, dynamic>{
      'productDetails': instance.productDetails,
      'name': instance.name,
      'model': instance.model,
      'price': instance.price,
      'total': instance.total,
      'image': instance.image
    };
