// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productDetails.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetails _$ProductDetailsFromJson(Map<String, dynamic> json) {
  return ProductDetails()
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacturer.fromJson(json['manufacturer'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductDetailsToJson(ProductDetails instance) =>
    <String, dynamic>{
      'description': instance.description,
      'manufacturer': instance.manufacturer
    };
