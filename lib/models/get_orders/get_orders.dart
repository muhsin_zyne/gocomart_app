import 'package:json_annotation/json_annotation.dart';
import "orders.dart";
part 'get_orders.g.dart';

@JsonSerializable()
class Get_orders {
    Get_orders();

    num time;
    num total;
    List<Orders> orders;
    
    factory Get_orders.fromJson(Map<String,dynamic> json) => _$Get_ordersFromJson(json);
    Map<String, dynamic> toJson() => _$Get_ordersToJson(this);
}
