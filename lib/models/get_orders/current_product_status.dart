class CurrentProductStatus {
  String name;
  String order_status_id;
  String code;

  CurrentProductStatus.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'];
    this.code = json['code'];
    this.order_status_id = json['order_status_id'];
  }
}
