// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Get_orders _$Get_ordersFromJson(Map<String, dynamic> json) {
  return Get_orders()
    ..time = json['time'] as num
    ..total = json['total'] as num
    ..orders = (json['orders'] as List)
        ?.map((e) =>
            e == null ? null : Orders.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Get_ordersToJson(Get_orders instance) =>
    <String, dynamic>{
      'time': instance.time,
      'total': instance.total,
      'orders': instance.orders
    };
