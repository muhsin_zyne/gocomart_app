// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Orders _$OrdersFromJson(Map<String, dynamic> json) {
  return Orders()
    ..order_id = json['order_id'] as String
    ..invoice_no = json['invoice_no'] as num
    ..invoice_prefix = json['invoice_prefix'] as String
    ..order_request_guid = json['order_request_guid'] as String
    ..date_added = json['date_added'] as String
    ..products = (json['products'] as List)
        ?.map((e) =>
            e == null ? null : Products.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$OrdersToJson(Orders instance) => <String, dynamic>{
      'order_id': instance.order_id,
      'invoice_no': instance.invoice_no,
      'invoice_prefix': instance.invoice_prefix,
      'order_request_guid': instance.order_request_guid,
      'date_added': instance.date_added,
      'products': instance.products
    };
