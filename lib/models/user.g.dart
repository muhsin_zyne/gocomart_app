// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..id = json['id'] as String
    ..mobile_no = json['mobile_no'] as num
    ..user_id = json['user_id'] as num
    ..status = json['status'] as num;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'mobile_no': instance.mobile_no,
      'user_id': instance.user_id,
      'status': instance.status
    };
