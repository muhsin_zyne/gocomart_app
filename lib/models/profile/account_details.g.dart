// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountDetails _$AccountDetailsFromJson(Map<String, dynamic> json) {
  return AccountDetails()
    ..firstname = json['firstname'] as String
    ..dob = json['dob'] as String
    ..name = json['name'] as String
    ..lastname = json['lastname'] as String
    ..email = json['email'] as String
    ..profileImage = json['profile_image']
    ..telephone = json['telephone'] as String;
}

Map<String, dynamic> _$AccountDetailsToJson(AccountDetails instance) =>
    <String, dynamic>{
      'firstname': instance.firstname,
      'name': instance.name,
      'lastname': instance.lastname,
      'email': instance.email,
      'profile_image': instance.profileImage,
      'telephone': instance.telephone
    };
