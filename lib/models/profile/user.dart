import 'package:json_annotation/json_annotation.dart';

import "account_details.dart";

part 'user.g.dart';

@JsonSerializable()
class User {
  User();

  String id;
  // ignore: non_constant_identifier_names
  num mobile_no;
  // ignore: non_constant_identifier_names
  String goco_id;
  // ignore: non_constant_identifier_names
  num user_id;

  AccountDetails accountDetails;
  // ignore: non_constant_identifier_names
  bool is_goco_prime;
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
