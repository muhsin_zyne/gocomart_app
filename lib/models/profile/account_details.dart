import 'package:json_annotation/json_annotation.dart';

part 'account_details.g.dart';

@JsonSerializable()
class AccountDetails {
  AccountDetails();

  String firstname;
  String lastname;
  String name;
  String email;
  String telephone;
  String profileImage;
  String dob;
  factory AccountDetails.fromJson(Map<String, dynamic> json) =>
      _$AccountDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$AccountDetailsToJson(this);
}
