// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..id = json['id'] as String
    ..mobile_no = json['mobile_no'] as num
    ..goco_id = json['goco_id'] as String
    ..user_id = json['user_id'] as num
    ..is_goco_prime = json['is_goco_prime'] as bool

    //note the changes
    ..accountDetails = json['customerInfo'] == null
        ? null
        : AccountDetails.fromJson(
            json['customerInfo'] as Map<String, dynamic>,
          );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'mobile_no': instance.mobile_no,
      'goco_id': instance.goco_id,
      'user_id': instance.user_id,
      'is_goco_prime': instance.is_goco_prime,
      'customerInfo': instance.accountDetails,
    };
