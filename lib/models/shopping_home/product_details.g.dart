// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetails _$ProductDetailsFromJson(Map<String, dynamic> json) {
  return ProductDetails()
    ..image = json['image'] as String
    ..manufacturer_id = json['manufacturer_id'] as num
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacture.fromJson(json['manufacturer'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductDetailsToJson(ProductDetails instance) =>
    <String, dynamic>{
      'image': instance.image,
      'manufacturer_id': instance.manufacturer_id,
      'manufacturer': instance.manufacturer
    };
