// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recent_product_description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecentProductDescription _$RecentProductDescriptionFromJson(
    Map<String, dynamic> json) {
  return RecentProductDescription()..name = json['name'] as String;
}

Map<String, dynamic> _$RecentProductDescriptionToJson(
        RecentProductDescription instance) =>
    <String, dynamic>{'name': instance.name};
