import 'package:gocomartapp/src/models/response/home/AppHomeCategory.dart';
import 'package:gocomartapp/src/models/response/home/TrendingBrands.dart';
import 'package:gocomartapp/src/models/response/home/TrendingCategory.dart';

import 'get_app_banners.dart';
import 'get_offered_products.dart';
import 'get_recent_product.dart';
import 'get_trending_products.dart';

class ShoppingHomeModel {
  ShoppingHomeModel();
  bool error = false;
  String message = '';
  GetAppBanners getAppBanners;
  GetOfferedProducts getOfferedProducts;
  GetTrendingProducts getTrendingProducts;
  GetRecentProduct getRecentProduct;
  List<AppHomeCategory> appHomeCategories;
  TrendingBrands trendingBrands;
  List<TrendingCategory> trendingCategories;

  ShoppingHomeModel.fromJSON(Map<String, dynamic> json) {
    this.trendingBrands = json['trendingBrands'] == null ? null : TrendingBrands.fromJSON(json['trendingBrands']);
    this.appHomeCategories =
        (json['appHomeCategories'] as List<dynamic>).map((e) => e == null ? null : AppHomeCategory.fromJSON(e as Map<String, dynamic>))?.toList();

    if (json['currentTrends'] != null) {
      this.trendingCategories = (json['currentTrends']['trendingCategory'] as List<dynamic>)
          .map((e) => e == null ? null : TrendingCategory.fromJSON(e as Map<String, dynamic>))
          .toList();
    }

    this.getAppBanners = json['getAppBanners'] == null ? null : GetAppBanners.fromJson(json['getAppBanners']);
    this.getOfferedProducts = json['getOfferProducts'] == null ? null : GetOfferedProducts.fromJson(json['getOfferProducts']);

    this.getTrendingProducts = json['getTrendingProducts'] == null ? null : GetTrendingProducts.fromJson(json['getTrendingProducts']);

    this.getRecentProduct = json['recentProducts'] == null ? null : GetRecentProduct.fromJson(json['recentProducts']);
  }
}
