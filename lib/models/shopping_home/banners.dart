import 'package:json_annotation/json_annotation.dart';
import "images.dart";
part 'banners.g.dart';

@JsonSerializable()
class Banners {
  Banners();
  // ignore: non_constant_identifier_names
  String banner_id;
  String name;
  List<Images> images;

  factory Banners.fromJson(Map<String, dynamic> json) =>
      _$BannersFromJson(json);
  Map<String, dynamic> toJson() => _$BannersToJson(this);
}
