// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_recent_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRecentProduct _$GetRecentProductFromJson(Map<String, dynamic> json) {
  return GetRecentProduct()
    ..products = (json['products'] as List)
        ?.map((e) => e == null
            ? null
            : RecentProducts.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetRecentProductToJson(GetRecentProduct instance) =>
    <String, dynamic>{'products': instance.products};
