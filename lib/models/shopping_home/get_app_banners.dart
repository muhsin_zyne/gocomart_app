import 'package:json_annotation/json_annotation.dart';
import "banners.dart";
part 'get_app_banners.g.dart';

@JsonSerializable()
class GetAppBanners {
  GetAppBanners();
  bool error = false;
  Banners banners;
  String message = '';
  factory GetAppBanners.fromJson(Map<String, dynamic> json) =>
      _$GetAppBannersFromJson(json);
  Map<String, dynamic> toJson() => _$GetAppBannersToJson(this);
}
