// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recent_product_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecentProductDetails _$RecentProductDetailsFromJson(Map<String, dynamic> json) {
  return RecentProductDetails()
    ..image = json['image'] as String
    ..description = json['description'] == null
        ? null
        : RecentProductDescription.fromJson(
            json['description'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null
        ? null
        : RecentProductManufacturer.fromJson(
            json['manufacturer'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RecentProductDetailsToJson(
        RecentProductDetails instance) =>
    <String, dynamic>{
      'image': instance.image,
      'description': instance.description,
      'manufacturer': instance.manufacturer
    };
