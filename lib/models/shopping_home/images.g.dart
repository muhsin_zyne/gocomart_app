// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images()
    ..banner_image_id = json['banner_image_id'] as String
    ..title = json['title'] as String
    ..link = json['link'] as String
    ..image = json['image'] as String;
}

Map<String, dynamic> _$ImagesToJson(Images instance) => <String, dynamic>{
      'banner_image_id': instance.banner_image_id,
      'title': instance.title,
      'link': instance.link,
      'image': instance.image
    };
