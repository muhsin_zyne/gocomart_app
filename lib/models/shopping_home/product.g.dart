// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrendingProduct _$TrendingProductFromJson(Map<String, dynamic> json) {
  return TrendingProduct()
    ..product_id = json['product_id'] as num
    ..name = json['name'] as String
    ..model = json['model'] as String
    ..productDetails = json['productDetails'] == null
        ? null
        : ProductDetails.fromJson(
            json['productDetails'] as Map<String, dynamic>);
}

Map<String, dynamic> _$TrendingProductToJson(TrendingProduct instance) =>
    <String, dynamic>{
      'product_id': instance.product_id,
      'name': instance.name,
      'model': instance.model,
      'productDetails': instance.productDetails
    };
