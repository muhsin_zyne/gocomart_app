import 'package:json_annotation/json_annotation.dart';
import "manufacturer.dart";
import "description.dart";
part 'details.g.dart';

@JsonSerializable()
class Details {
    Details();

    String image;
    num price;
    Manufacturer manufacturer;
    Description description;
    
    factory Details.fromJson(Map<String,dynamic> json) => _$DetailsFromJson(json);
    Map<String, dynamic> toJson() => _$DetailsToJson(this);
}
