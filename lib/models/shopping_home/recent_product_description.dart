import 'package:json_annotation/json_annotation.dart';

part 'recent_product_description.g.dart';

@JsonSerializable()
class RecentProductDescription {
    RecentProductDescription();

    String name;
    
    factory RecentProductDescription.fromJson(Map<String,dynamic> json) => _$RecentProductDescriptionFromJson(json);
    Map<String, dynamic> toJson() => _$RecentProductDescriptionToJson(this);
}
