import 'package:json_annotation/json_annotation.dart';
import "products.dart";
part 'get_offered_products.g.dart';

@JsonSerializable()
class GetOfferedProducts {
    GetOfferedProducts();

    List<Products> products;
    
    factory GetOfferedProducts.fromJson(Map<String,dynamic> json) => _$GetOfferedProductsFromJson(json);
    Map<String, dynamic> toJson() => _$GetOfferedProductsToJson(this);
}
