import 'package:json_annotation/json_annotation.dart';

part 'manufacture.g.dart';

@JsonSerializable()
class Manufacture {
    Manufacture();

    String name;
    
    factory Manufacture.fromJson(Map<String,dynamic> json) => _$ManufactureFromJson(json);
    Map<String, dynamic> toJson() => _$ManufactureToJson(this);
}
