import 'package:json_annotation/json_annotation.dart';

part 'recent_product_manufacturer.g.dart';

@JsonSerializable()
class RecentProductManufacturer {
    RecentProductManufacturer();

    String name;
    
    factory RecentProductManufacturer.fromJson(Map<String,dynamic> json) => _$RecentProductManufacturerFromJson(json);
    Map<String, dynamic> toJson() => _$RecentProductManufacturerToJson(this);
}
