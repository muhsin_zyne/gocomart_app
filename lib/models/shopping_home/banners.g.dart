// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banners.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Banners _$BannersFromJson(Map<String, dynamic> json) {
  return Banners()
    ..banner_id = json['banner_id'] as String
    ..name = json['name'] as String
    ..images = (json['images'] as List)
        ?.map((e) =>
            e == null ? null : Images.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$BannersToJson(Banners instance) => <String, dynamic>{
      'banner_id': instance.banner_id,
      'name': instance.name,
      'images': instance.images
    };
