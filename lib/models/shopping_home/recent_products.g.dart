// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recent_products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecentProducts _$RecentProductsFromJson(Map<String, dynamic> json) {
  return RecentProducts()
    ..product_id = json['product_id'] as num
    ..app_user_id = json['app_user_id'] as String
    ..id = json['id'] as String
    ..productDetails = json['productDetails'] == null
        ? null
        : RecentProductDetails.fromJson(
            json['productDetails'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RecentProductsToJson(RecentProducts instance) =>
    <String, dynamic>{
      'product_id': instance.product_id,
      'app_user_id': instance.app_user_id,
      'id': instance.id,
      'productDetails': instance.productDetails
    };
