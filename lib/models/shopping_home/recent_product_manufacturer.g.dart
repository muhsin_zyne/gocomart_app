// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recent_product_manufacturer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecentProductManufacturer _$RecentProductManufacturerFromJson(
    Map<String, dynamic> json) {
  return RecentProductManufacturer()..name = json['name'] as String;
}

Map<String, dynamic> _$RecentProductManufacturerToJson(
        RecentProductManufacturer instance) =>
    <String, dynamic>{'name': instance.name};
