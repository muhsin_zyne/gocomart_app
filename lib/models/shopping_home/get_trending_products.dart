import 'package:json_annotation/json_annotation.dart';
import "product.dart";
part 'get_trending_products.g.dart';

@JsonSerializable()
class GetTrendingProducts {
  GetTrendingProducts();

  List<TrendingProduct> products;

  factory GetTrendingProducts.fromJson(Map<String, dynamic> json) =>
      _$GetTrendingProductsFromJson(json);
  Map<String, dynamic> toJson() => _$GetTrendingProductsToJson(this);
}
