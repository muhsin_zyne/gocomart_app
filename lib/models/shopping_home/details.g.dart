// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Details _$DetailsFromJson(Map<String, dynamic> json) {
  return Details()
    ..image = json['image'] as String
    ..price = json['price'] as num
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacturer.fromJson(json['manufacturer'] as Map<String, dynamic>)
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DetailsToJson(Details instance) => <String, dynamic>{
      'image': instance.image,
      'price': instance.price,
      'manufacturer': instance.manufacturer,
      'description': instance.description
    };
