// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_offered_products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetOfferedProducts _$GetOfferedProductsFromJson(Map<String, dynamic> json) {
  return GetOfferedProducts()
    ..products = (json['products'] as List)
        ?.map((e) =>
            e == null ? null : Products.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetOfferedProductsToJson(GetOfferedProducts instance) =>
    <String, dynamic>{'products': instance.products};
