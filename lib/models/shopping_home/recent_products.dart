import 'package:json_annotation/json_annotation.dart';
import "recent_product_details.dart";
part 'recent_products.g.dart';

@JsonSerializable()
class RecentProducts {
    RecentProducts();

    // ignore: non_constant_identifier_names
    num product_id;
    // ignore: non_constant_identifier_names
    String app_user_id;
    String id;
    RecentProductDetails productDetails;
    
    factory RecentProducts.fromJson(Map<String,dynamic> json) => _$RecentProductsFromJson(json);
    Map<String, dynamic> toJson() => _$RecentProductsToJson(this);
}
