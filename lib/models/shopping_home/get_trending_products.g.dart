// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_trending_products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetTrendingProducts _$GetTrendingProductsFromJson(Map<String, dynamic> json) {
  return GetTrendingProducts()
    ..products = (json['products'] as List)
        ?.map((e) => e == null
            ? null
            : TrendingProduct.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetTrendingProductsToJson(
        GetTrendingProducts instance) =>
    <String, dynamic>{'products': instance.products};
