import 'package:json_annotation/json_annotation.dart';
import "recent_product_description.dart";
import "recent_product_manufacturer.dart";
part 'recent_product_details.g.dart';

@JsonSerializable()
class RecentProductDetails {
    RecentProductDetails();

    String image;
    RecentProductDescription description;
    RecentProductManufacturer manufacturer;
    
    factory RecentProductDetails.fromJson(Map<String,dynamic> json) => _$RecentProductDetailsFromJson(json);
    Map<String, dynamic> toJson() => _$RecentProductDetailsToJson(this);
}
