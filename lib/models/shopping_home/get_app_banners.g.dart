// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_app_banners.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAppBanners _$GetAppBannersFromJson(Map<String, dynamic> json) {
  return GetAppBanners()
    ..banners = json['banners'] == null
        ? null
        : Banners.fromJson(json['banners'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GetAppBannersToJson(GetAppBanners instance) =>
    <String, dynamic>{'banners': instance.banners};
