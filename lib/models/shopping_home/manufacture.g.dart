// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manufacture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Manufacture _$ManufactureFromJson(Map<String, dynamic> json) {
  return Manufacture()..name = json['name'] as String;
}

Map<String, dynamic> _$ManufactureToJson(Manufacture instance) =>
    <String, dynamic>{'name': instance.name};
