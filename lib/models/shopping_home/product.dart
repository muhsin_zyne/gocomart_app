import 'package:json_annotation/json_annotation.dart';
import "product_details.dart";
part 'product.g.dart';

@JsonSerializable()
class TrendingProduct {
  TrendingProduct();

  // ignore: non_constant_identifier_names
  num product_id;
  String name;
  String model;
  ProductDetails productDetails;

  factory TrendingProduct.fromJson(Map<String, dynamic> json) =>
      _$TrendingProductFromJson(json);
  Map<String, dynamic> toJson() => _$TrendingProductToJson(this);
}
