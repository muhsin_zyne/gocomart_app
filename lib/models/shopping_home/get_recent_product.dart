import 'package:json_annotation/json_annotation.dart';
import "recent_products.dart";
part 'get_recent_product.g.dart';

@JsonSerializable()
class GetRecentProduct {
    GetRecentProduct();

    List<RecentProducts> products;
    
    factory GetRecentProduct.fromJson(Map<String,dynamic> json) => _$GetRecentProductFromJson(json);
    Map<String, dynamic> toJson() => _$GetRecentProductToJson(this);
}
