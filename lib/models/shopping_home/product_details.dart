import 'package:json_annotation/json_annotation.dart';
import "manufacture.dart";
part 'product_details.g.dart';

@JsonSerializable()
class ProductDetails {
  ProductDetails();

  String image;
  // ignore: non_constant_identifier_names
  num manufacturer_id;
  Manufacture manufacturer;

  factory ProductDetails.fromJson(Map<String, dynamic> json) =>
      _$ProductDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$ProductDetailsToJson(this);
}
