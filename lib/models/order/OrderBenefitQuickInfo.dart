class OrderBenefitQuickInfo {
  num orderPayAmount;
  int totalItems;
  num walletExclusiveDiscount;
  num walletUsed;
  bool isHavingDiscount;

  OrderBenefitQuickInfo.fromJSON(Map<String, dynamic> json) {
    this.orderPayAmount = json['orderPayAmount'] as num;
    this.totalItems = json['totalItems'];
    this.walletExclusiveDiscount = json['walletExclusiveDiscount'] as num;
    this.walletUsed = json['walletUsed'] as num;
    if (this.walletUsed > 0 || this.walletExclusiveDiscount > 0) {
      this.isHavingDiscount = true;
    }else {
      this.isHavingDiscount = false;
    }
  }

  toJson() {
    return <String, dynamic>{
      'orderPayAmount': this.orderPayAmount,
      'totalItems': this.totalItems,
      'walletExclusiveDiscount': this.walletExclusiveDiscount,
      'walletUsed': this.walletUsed,
    };
  }
}
