class PaymentOption {
  String appLabel;

  PaymentOption.fromJSON(Map<String, dynamic> parsedJson) {
    this.appLabel = parsedJson['app_label'];
  }
}
