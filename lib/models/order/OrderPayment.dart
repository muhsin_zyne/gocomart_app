import 'package:gocomartapp/models/order/PaymentOption.dart';

class OrderPayment {
  bool isCOD = false;
  int orderId;
  int orderPaymentId;
  int paymentOptionId;
  PaymentOption paymentOption;

  OrderPayment.fromJSON(Map<String, dynamic> json) {
    this.isCOD = json['is_cod'];
    this.orderId = json['order_id'];
    this.orderPaymentId = json['order_payment_id'];
    this.paymentOptionId = json['payment_option_id'];
    this.paymentOption = json['paymentOption'] == null ? null : PaymentOption.fromJSON(json['paymentOption']);
  }
}
