import 'package:gocomartapp/models/order/OrderBenefitQuickInfo.dart';
import 'package:gocomartapp/models/order/OrderPayment.dart';

class OrderInfo {
  String orderRequestGUID;
  String estDelivery;
  int invoiceNo;
  String invoicePrefix;
  String orderId;
  OrderBenefitQuickInfo orderBenefitQuickInfo;
  OrderPayment orderPayment;

  OrderInfo.fromJSON(Map<String, dynamic> json) {
    this.orderRequestGUID = json['order_request_guid'];
    this.estDelivery = json['est_delivery'];
    this.invoiceNo = json['invoice_no'];
    this.invoicePrefix = json['invoice_prefix'];
    this.orderId = json['order_id'];
    this.orderBenefitQuickInfo = json['orderBenefitQuickInfo'] == null ? null : OrderBenefitQuickInfo.fromJSON(json['orderBenefitQuickInfo']);
    this.orderPayment = json['orderPayment'] == null ? null : OrderPayment.fromJSON(json['orderPayment']);
  }
}
