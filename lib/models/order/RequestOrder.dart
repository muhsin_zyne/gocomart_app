import 'package:gocomartapp/models/order/OrderInfo.dart';

class RequestOrder {
  OrderInfo orderInfo;

  RequestOrder.fromJSON(Map<String, dynamic> json) {
    this.orderInfo = json['orderInfo'] == null ? null : OrderInfo.fromJSON(json['orderInfo']);
  }
}
