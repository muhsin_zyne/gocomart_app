import 'package:json_annotation/json_annotation.dart';
import "products.dart";
part 'product_wishlist.g.dart';

@JsonSerializable()
class ProductWishlist {
    ProductWishlist();

    List<Products> products;
    String error;
    
    factory ProductWishlist.fromJson(Map<String,dynamic> json) => _$ProductWishlistFromJson(json);
    Map<String, dynamic> toJson() => _$ProductWishlistToJson(this);
}
