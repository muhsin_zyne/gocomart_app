import 'package:json_annotation/json_annotation.dart';
import "productDetails.dart";
part 'products.g.dart';

@JsonSerializable()
class Products {
  Products();

  String id;
  // ignore: non_constant_identifier_names
  num product_id;
  ProductDetails productDetails;

  factory Products.fromJson(Map<String, dynamic> json) =>
      _$ProductsFromJson(json);
  Map<String, dynamic> toJson() => _$ProductsToJson(this);
}
