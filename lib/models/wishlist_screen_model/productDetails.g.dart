// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productDetails.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetails _$ProductDetailsFromJson(Map<String, dynamic> json) {
  return ProductDetails()
    ..stock = json['stock'] == null
        ? null
        : Stock.fromJson(json['stock'] as Map<String, dynamic>)
    ..image = json['image'] as String
    ..price = json['price'] as num
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacturer.fromJson(json['manufacturer'] as Map<String, dynamic>)
    ..special = json['special'] == null
        ? null
        : Special.fromJson(json['special'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductDetailsToJson(ProductDetails instance) =>
    <String, dynamic>{
      'stock': instance.stock,
      'image': instance.image,
      'price': instance.price,
      'description': instance.description,
      'manufacturer': instance.manufacturer,
      'special': instance.special
    };
