import 'package:json_annotation/json_annotation.dart';
import "stock.dart";
import "description.dart";
import "manufacturer.dart";
import "special.dart";
part 'productDetails.g.dart';

@JsonSerializable()
class ProductDetails {
    ProductDetails();

    Stock stock;
    String image;
    num price;
    Description description;
    Manufacturer manufacturer;
    Special special;
    
    factory ProductDetails.fromJson(Map<String,dynamic> json) => _$ProductDetailsFromJson(json);
    Map<String, dynamic> toJson() => _$ProductDetailsToJson(this);
}
