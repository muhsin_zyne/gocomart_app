// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'special.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Special _$SpecialFromJson(Map<String, dynamic> json) {
  return Special()..price = json['price'] as num;
}

Map<String, dynamic> _$SpecialToJson(Special instance) =>
    <String, dynamic>{'price': instance.price};
