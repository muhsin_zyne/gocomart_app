// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_wishlist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductWishlist _$ProductWishlistFromJson(Map<String, dynamic> json) {
  return ProductWishlist()
    ..products = (json['products'] as List)
        ?.map((e) =>
            e == null ? null : Products.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..error = json['error'] as String;
}

Map<String, dynamic> _$ProductWishlistToJson(ProductWishlist instance) =>
    <String, dynamic>{'products': instance.products, 'error': instance.error};
