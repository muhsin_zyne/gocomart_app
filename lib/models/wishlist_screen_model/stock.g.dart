// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stock.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Stock _$StockFromJson(Map<String, dynamic> json) {
  return Stock()
    ..stock_status_id = json['stock_status_id'] as num
    ..name = json['name'] as String;
}

Map<String, dynamic> _$StockToJson(Stock instance) => <String, dynamic>{
      'stock_status_id': instance.stock_status_id,
      'name': instance.name
    };
