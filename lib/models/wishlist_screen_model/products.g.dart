// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Products _$ProductsFromJson(Map<String, dynamic> json) {
  return Products()
    ..id = json['id'] as String
    ..product_id = json['product_id'] as num
    ..productDetails = json['productDetails'] == null
        ? null
        : ProductDetails.fromJson(
            json['productDetails'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductsToJson(Products instance) => <String, dynamic>{
      'id': instance.id,
      'product_id': instance.product_id,
      'productDetails': instance.productDetails
    };
