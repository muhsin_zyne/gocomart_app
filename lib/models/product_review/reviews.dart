import 'package:json_annotation/json_annotation.dart';

part 'reviews.g.dart';

@JsonSerializable()
class Reviews {
  Reviews();

  String author;
  num rating;
  String text;
  String title;
  // ignore: non_constant_identifier_names
  dynamic date_added;
  factory Reviews.fromJson(Map<String, dynamic> json) => _$ReviewsFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewsToJson(this);
}
