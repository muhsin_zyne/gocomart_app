// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_product_reviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProductReviews _$GetProductReviewsFromJson(Map<String, dynamic> json) {
  return GetProductReviews()
    ..reviews = (json['reviews'] as List)
        ?.map((e) =>
            e == null ? null : Reviews.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetProductReviewsToJson(GetProductReviews instance) =>
    <String, dynamic>{'reviews': instance.reviews};
