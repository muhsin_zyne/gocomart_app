import 'package:json_annotation/json_annotation.dart';
import "reviews.dart";
part 'get_product_reviews.g.dart';

@JsonSerializable()
class GetProductReviews {
    GetProductReviews();

    List<Reviews> reviews;
    
    factory GetProductReviews.fromJson(Map<String,dynamic> json) => _$GetProductReviewsFromJson(json);
    Map<String, dynamic> toJson() => _$GetProductReviewsToJson(this);
}
