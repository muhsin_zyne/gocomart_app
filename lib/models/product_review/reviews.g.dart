// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reviews _$ReviewsFromJson(Map<String, dynamic> json) {
  return Reviews()
    ..author = json['author'] as String
    ..rating = json['rating'] as num
    ..text = json['text'] as String
    ..title = json['title'] as String
    ..date_added=json['date_added'] as dynamic;
}

Map<String, dynamic> _$ReviewsToJson(Reviews instance) => <String, dynamic>{
      'author': instance.author,
      'rating': instance.rating,
      'text': instance.text,
      'title':instance.title,
      'date_added':instance.date_added
    };
