import 'package:json_annotation/json_annotation.dart';

part 'response.g.dart';

@JsonSerializable()
class Response {
    Response();

    String success;
    String error;
    
    factory Response.fromJson(Map<String,dynamic> json) => _$ResponseFromJson(json);
    Map<String, dynamic> toJson() => _$ResponseToJson(this);
}
