import 'package:json_annotation/json_annotation.dart';
import 'paymentOption.dart';
part 'userPaymentOption.g.dart';

@JsonSerializable()
class UserPaymentOption {
  UserPaymentOption();

  num id;
  PaymentOption paymentOption;

  factory UserPaymentOption.fromJson(Map<String, dynamic> json) => _$UserPaymentOptionFromJson(json);
  Map<String, dynamic> toJson() => _$UserPaymentOptionToJson(this);
}
