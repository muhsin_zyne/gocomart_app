// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getPaymentOption.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetPaymentOption _$GetPaymentOptionFromJson(Map<String, dynamic> json) {
  return GetPaymentOption()
    ..success = json['success'] as String
    ..error = json['error'] as String
    ..paymentOption = (json['paymentOption'] as List)
        ?.map((e) => e == null
            ? null
            : PaymentOption.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetPaymentOptionToJson(GetPaymentOption instance) =>
    <String, dynamic>{
      'success': instance.success,
      'error': instance.error,
      'paymentOption': instance.paymentOption
    };
