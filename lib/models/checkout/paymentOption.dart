import 'package:json_annotation/json_annotation.dart';

part 'paymentOption.g.dart';

@JsonSerializable()
class PaymentOption {
  PaymentOption();

  // ignore: non_constant_identifier_names
  num payment_option_id;
  String name;
  String code;
  // ignore: non_constant_identifier_names
  String app_label;
  // ignore: non_constant_identifier_names
  bool is_online;
  String logo;
  // ignore: non_constant_identifier_names
  String public_key;

  // dynamic variables

  bool isLoading = false;

  factory PaymentOption.fromJson(Map<String, dynamic> json) => _$PaymentOptionFromJson(json);
  Map<String, dynamic> toJson() => _$PaymentOptionToJson(this);
}
