// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getPaymentOptionGroup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetPaymentOptionGroup _$GetPaymentOptionGroupFromJson(
    Map<String, dynamic> json) {
  return GetPaymentOptionGroup()
    ..success = json['success'] as String
    ..error = json['error'] as String
    ..paymentOptionGroup = (json['paymentOptionGroup'] as List)
        ?.map((e) => e == null
            ? null
            : PaymentOptionGroup.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetPaymentOptionGroupToJson(
        GetPaymentOptionGroup instance) =>
    <String, dynamic>{
      'success': instance.success,
      'error': instance.error,
      'paymentOptionGroup': instance.paymentOptionGroup
    };
