// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getUserPaymentOptions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUserPaymentOptions _$GetUserPaymentOptionsFromJson(
    Map<String, dynamic> json) {
  return GetUserPaymentOptions()
    ..success = json['success'] as String
    ..error = json['error'] as String
    ..userPaymentOptions = (json['userPaymentOptions'] as List)
        ?.map((e) => e == null
            ? null
            : UserPaymentOption.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetUserPaymentOptionsToJson(
        GetUserPaymentOptions instance) =>
    <String, dynamic>{
      'success': instance.success,
      'error': instance.error,
      'userPaymentOptions': instance.userPaymentOptions
    };
