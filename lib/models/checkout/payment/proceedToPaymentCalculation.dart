import 'package:json_annotation/json_annotation.dart';

import "paymentInfo.dart";

part 'proceedToPaymentCalculation.g.dart';

@JsonSerializable()
class ProceedToPaymentCalculation {
  ProceedToPaymentCalculation();

  PaymentInfo paymentInfo;

  num netWalletBalance = 0;
  num totalPayableWithOutOffer = 0;
  num orderTotalSavings = 0;
  num netPay = 0;
  bool walletUsed = false;
  bool freeOrder = false;
  bool walletNotUsable = false;
  bool exclusiveDiscountUsable = false;
  bool exclusiveDiscountUsed = false;

  factory ProceedToPaymentCalculation.fromJson(Map<String, dynamic> json) => _$ProceedToPaymentCalculationFromJson(json);
  Map<String, dynamic> toJson() => _$ProceedToPaymentCalculationToJson(this);
}
