// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proceedToPaymentCalculation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProceedToPaymentCalculation _$ProceedToPaymentCalculationFromJson(Map<String, dynamic> json) {
  return ProceedToPaymentCalculation()..paymentInfo = json['paymentInfo'] == null ? null : PaymentInfo.fromJson(json['paymentInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProceedToPaymentCalculationToJson(ProceedToPaymentCalculation instance) => <String, dynamic>{
      'paymentInfo': instance.paymentInfo,
      'netWalletBalance': instance.netWalletBalance,
      'netPay': instance.netPay,
    };
