import 'package:json_annotation/json_annotation.dart';

part 'paymentInfo.g.dart';

@JsonSerializable()
class PaymentInfo {
  PaymentInfo();

  // ignore: non_constant_identifier_names
  num order_request_id;
  // ignore: non_constant_identifier_names
  String order_request_guid;
  // ignore: non_constant_identifier_names
  num total_price;
  // ignore: non_constant_identifier_names
  num total_delivery_fee;
  // ignore: non_constant_identifier_names
  num wallet_discount;
  // ignore: non_constant_identifier_names
  num delivery_fee_discount;
  // ignore: non_constant_identifier_names
  num wallet_balance;
  num walletExclusiveDiscount = 0;

  factory PaymentInfo.fromJson(Map<String, dynamic> json) => _$PaymentInfoFromJson(json);
  Map<String, dynamic> toJson() => _$PaymentInfoToJson(this);
}
