// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paymentInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentInfo _$PaymentInfoFromJson(Map<String, dynamic> json) {
  return PaymentInfo()
    ..order_request_guid = json['order_request_guid'] as String
    ..order_request_id = json['order_request_id'] as num
    ..total_price = json['total_price'] as num
    ..total_delivery_fee = json['total_delivery_fee'] as num
    ..wallet_discount = json['wallet_discount'] as num
    ..delivery_fee_discount = json['delivery_fee_discount'] as num
    ..wallet_balance = json['wallet_balance'] as num
    ..walletExclusiveDiscount = json['wallet_exclusive_discount'] as num;
}

Map<String, dynamic> _$PaymentInfoToJson(PaymentInfo instance) => <String, dynamic>{
      'order_request_id': instance.order_request_id,
      'order_request_guid': instance.order_request_guid,
      'total_price': instance.total_price,
      'total_delivery_fee': instance.total_delivery_fee,
      'wallet_discount': instance.wallet_discount,
      'delivery_fee_discount': instance.delivery_fee_discount,
      'wallet_balance': instance.wallet_balance,
      'wallet_exclusive_discount': instance.walletExclusiveDiscount,
    };
