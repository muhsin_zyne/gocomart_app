// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paymentOption.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentOption _$PaymentOptionFromJson(Map<String, dynamic> json) {
  return PaymentOption()
    ..payment_option_id = json['payment_option_id'] as num
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..app_label = json['app_label'] as String
    ..is_online = json['is_online'] as bool
    ..logo = json['logo'] as String
    ..public_key = json['public_key'] as String
    ..isLoading = json['isLoading'] as bool;
}

Map<String, dynamic> _$PaymentOptionToJson(PaymentOption instance) => <String, dynamic>{
      'payment_option_id': instance.payment_option_id,
      'name': instance.name,
      'code': instance.code,
      'app_label': instance.app_label,
      'is_online': instance.is_online,
      'logo': instance.logo,
      'public_key': instance.public_key,
      'isLoading': instance.isLoading
    };
