// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userPaymentOption.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserPaymentOption _$UserPaymentOptionFromJson(Map<String, dynamic> json) {
  return UserPaymentOption()
    ..id = json['id'] as num
    ..paymentOption = json['paymentOption'] == null
        ? null
        : PaymentOption.fromJson(json['paymentOption'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserPaymentOptionToJson(UserPaymentOption instance) =>
    <String, dynamic>{
      'id': instance.id,
      'paymentOption': instance.paymentOption
    };
