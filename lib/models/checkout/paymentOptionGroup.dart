import 'package:gocomartapp/models/checkout/paymentOption.dart';
import 'package:json_annotation/json_annotation.dart';

part 'paymentOptionGroup.g.dart';

@JsonSerializable()
class PaymentOptionGroup {
  PaymentOptionGroup();

  num id;
  // ignore: non_constant_identifier_names
  String group_name;
  bool status;
  List<PaymentOption> paymentOptions;

  factory PaymentOptionGroup.fromJson(Map<String, dynamic> json) => _$PaymentOptionGroupFromJson(json);
  Map<String, dynamic> toJson() => _$PaymentOptionGroupToJson(this);
}
