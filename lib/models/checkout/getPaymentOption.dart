import 'package:json_annotation/json_annotation.dart';
import "paymentOption.dart";
part 'getPaymentOption.g.dart';

@JsonSerializable()
class GetPaymentOption {
    GetPaymentOption();

    String success;
    String error;
    List<PaymentOption> paymentOption;
    
    factory GetPaymentOption.fromJson(Map<String,dynamic> json) => _$GetPaymentOptionFromJson(json);
    Map<String, dynamic> toJson() => _$GetPaymentOptionToJson(this);
}
