import 'package:json_annotation/json_annotation.dart';
import "userPaymentOption.dart";
part 'getUserPaymentOptions.g.dart';

@JsonSerializable()
class GetUserPaymentOptions {
    GetUserPaymentOptions();

    String success;
    String error;
    List<UserPaymentOption> userPaymentOptions;
    
    factory GetUserPaymentOptions.fromJson(Map<String,dynamic> json) => _$GetUserPaymentOptionsFromJson(json);
    Map<String, dynamic> toJson() => _$GetUserPaymentOptionsToJson(this);
}
