import 'package:gocomartapp/models/checkout/paymentOptionGroup.dart';
import 'package:json_annotation/json_annotation.dart';

part 'getPaymentOptionGroup.g.dart';

@JsonSerializable()
class GetPaymentOptionGroup {
  GetPaymentOptionGroup();

  String success;
  String error;
  List<PaymentOptionGroup> paymentOptionGroup;

  factory GetPaymentOptionGroup.fromJson(Map<String, dynamic> json) => _$GetPaymentOptionGroupFromJson(json);
  Map<String, dynamic> toJson() => _$GetPaymentOptionGroupToJson(this);
}
