// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Response _$ResponseFromJson(Map<String, dynamic> json) {
  return Response()
    ..success = json['success'] as String
    ..error = json['error'] as String;
}

Map<String, dynamic> _$ResponseToJson(Response instance) =>
    <String, dynamic>{'success': instance.success, 'error': instance.error};
