// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paymentOptionGroup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentOptionGroup _$PaymentOptionGroupFromJson(Map<String, dynamic> json) {
  return PaymentOptionGroup()
    ..id = json['id'] as num
    ..group_name = json['group_name'] as String
    ..status = json['status'] as bool
    ..paymentOptions = (json['paymentOptions'] as List)
        ?.map((e) => e == null
            ? null
            : PaymentOption.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$PaymentOptionGroupToJson(PaymentOptionGroup instance) =>
    <String, dynamic>{
      'id': instance.id,
      'group_name': instance.group_name,
      'status': instance.status,
      'paymentOptions': instance.paymentOptions
    };
