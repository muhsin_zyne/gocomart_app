import 'package:json_annotation/json_annotation.dart';

part 'advertiser.g.dart';

@JsonSerializable()
class Advertiser {
    Advertiser();

    String id;
    String name;
    
    factory Advertiser.fromJson(Map<String,dynamic> json) => _$AdvertiserFromJson(json);
    Map<String, dynamic> toJson() => _$AdvertiserToJson(this);
}
