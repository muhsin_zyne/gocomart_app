// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scratchCard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScratchCard _$ScratchCardFromJson(Map<String, dynamic> json) {
  return ScratchCard()
    ..id = json['id'] as String
    ..value = json['value'] as num
    ..isScratched = json['is_scratched'] as bool
    ..rewardText = json['reward_text'] as String
    ..rewardPromoText = json['reward_promo_text'] as String
    ..generatedAt = json['generated_at'] as num
    ..scratchedOn = json['scratched_on'] as num
    ..expiresOn = json['expires_on'] as num
    ..cardColor = json['card_color'] as String
    ..texturePng = json['texture_png'] as String
    ..rewealOn = json['reweal_on'] as num
    ..cardScheme = json['card_scheme'] as String
    ..advertiser = json['advertiser'] == null ? null : Advertiser.fromJson(json['advertiser'] as Map<String, dynamic>)
    ..advertiserImage = json['advertiserImage'] == null ? null : AdvertiserImage.fromJson(json['advertiserImage'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ScratchCardToJson(ScratchCard instance) => <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
      'is_scratched': instance.isScratched,
      'reward_text': instance.rewardText,
      'reward_promo_text': instance.rewardPromoText,
      'generated_at': instance.generatedAt,
      'scratched_on': instance.scratchedOn,
      'expires_on': instance.expiresOn,
      'card_color': instance.cardColor,
      'texture_png': instance.texturePng,
      'reweal_on': instance.rewealOn,
      'advertiser': instance.advertiser,
      'advertiserImage': instance.advertiserImage,
      'card_scheme': instance.cardScheme,
    };
