// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertiser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Advertiser _$AdvertiserFromJson(Map<String, dynamic> json) {
  return Advertiser()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$AdvertiserToJson(Advertiser instance) =>
    <String, dynamic>{'id': instance.id, 'name': instance.name};
