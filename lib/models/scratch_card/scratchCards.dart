import 'package:json_annotation/json_annotation.dart';

import "scratchCard.dart";

part 'scratchCards.g.dart';

@JsonSerializable()
class ScratchCards {
  ScratchCards();

  num total;
  List<ScratchCard> cards;
  bool error = false;
  String message = '';
  num collectedAmount = 0;
  factory ScratchCards.fromJson(Map<String, dynamic> json) => _$ScratchCardsFromJson(json);
  Map<String, dynamic> toJson() => _$ScratchCardsToJson(this);
}
