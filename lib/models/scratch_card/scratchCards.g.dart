// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scratchCards.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScratchCards _$ScratchCardsFromJson(Map<String, dynamic> json) {
  return ScratchCards()
    ..total = json['total'] as num
    ..cards = (json['cards'] as List)?.map((e) => e == null ? null : ScratchCard.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$ScratchCardsToJson(ScratchCards instance) => <String, dynamic>{
      'total': instance.total,
      'cards': instance.cards,
      'error': instance.error,
      'message': instance.message,
    };
