import 'package:gocomartapp/app_main.dart';
import 'package:json_annotation/json_annotation.dart';

import "advertiser.dart";
import "advertiserImage.dart";

part 'scratchCard.g.dart';

@JsonSerializable()
class ScratchCard {
  ScratchCard();

  String id;
  num value;
  // ignore: non_constant_identifier_names
  bool isScratched;
  // ignore: non_constant_identifier_names
  String rewardText;
  // ignore: non_constant_identifier_names
  num generatedAt;
  // ignore: non_constant_identifier_names
  num scratchedOn;
  // ignore: non_constant_identifier_names
  num expiresOn;
  // ignore: non_constant_identifier_names
  String cardColor;
  // ignore: non_constant_identifier_names
  String texturePng;
  // ignore: non_constant_identifier_names
  num rewealOn;
  Advertiser advertiser;
  AdvertiserImage advertiserImage;
  // ignore: non_constant_identifier_names
  String rewardPromoText;
  String cardScheme;

  String get scratchCardSchemeLabel {
    var date = new DateTime.fromMillisecondsSinceEpoch(this.rewealOn * 1000);
    var generatedDateFor = dateFormat(dateTime: date.toString());
    return 'GOCO ' + capitalize(this.cardScheme) + ' Scratch Card for ' + generatedDateFor;
  }

  factory ScratchCard.fromJson(Map<String, dynamic> json) => _$ScratchCardFromJson(json);
  Map<String, dynamic> toJson() => _$ScratchCardToJson(this);
}
