import 'package:json_annotation/json_annotation.dart';

part 'advertiserImage.g.dart';

@JsonSerializable()
class AdvertiserImage {
  AdvertiserImage();

  String id;
  // ignore: non_constant_identifier_names
  String advertiser_id;
  String image;

  factory AdvertiserImage.fromJson(Map<String, dynamic> json) =>
      _$AdvertiserImageFromJson(json);
  Map<String, dynamic> toJson() => _$AdvertiserImageToJson(this);
}
