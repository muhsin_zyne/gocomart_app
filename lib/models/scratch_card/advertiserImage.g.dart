// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertiserImage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdvertiserImage _$AdvertiserImageFromJson(Map<String, dynamic> json) {
  return AdvertiserImage()
    ..id = json['id'] as String
    ..advertiser_id = json['advertiser_id'] as String
    ..image = json['image'] as String;
}

Map<String, dynamic> _$AdvertiserImageToJson(AdvertiserImage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'advertiser_id': instance.advertiser_id,
      'image': instance.image
    };
