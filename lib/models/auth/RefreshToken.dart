import 'package:gocomartapp/models/auth/User.dart';

class RefreshToken {
  String authToken;
  String refreshToken;
  User user;

  RefreshToken.fromJSON(Map<String, dynamic> json) {
    this.authToken = json['auth_token'];
    this.refreshToken = json['refresh_token'];
    this.user = json['user'] == null ? null : User.fromJSON(json['user']);
  }
}
