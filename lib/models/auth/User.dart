class User {
  String id;
  int mobileNo;
  int status;
  int userId;

  User.fromJSON(Map<String, dynamic> parsedJson) {
    this.id = parsedJson['id'];
    this.mobileNo = parsedJson['mobile_no'];
    this.status = parsedJson['status'];
    this.userId = parsedJson['user_id'];
  }
}
