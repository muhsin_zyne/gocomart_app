import 'package:gocomartapp/models/auth/RefreshToken.dart';

class RefreshTokenResponse {
  RefreshToken refreshToken;

  RefreshTokenResponse.fromJSON(Map<String, dynamic> json) {
    this.refreshToken = json['refreshToken'] == null ? null : RefreshToken.fromJSON(json['refreshToken']);
  }
}
