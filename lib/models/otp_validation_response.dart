import 'package:json_annotation/json_annotation.dart';

import "user.dart";

part 'otp_validation_response.g.dart';

@JsonSerializable()
class OtpValidationResponse {
  OtpValidationResponse();

  // ignore: non_constant_identifier_names
  String auth_token;
  String refreshToken;
  String testName;
  User user;
  bool error;
  String message;

  factory OtpValidationResponse.fromJson(Map<String, dynamic> json) => _$OtpValidationResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OtpValidationResponseToJson(this);
}
