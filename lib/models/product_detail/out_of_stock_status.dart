import 'package:json_annotation/json_annotation.dart';

part 'out_of_stock_status.g.dart';

@JsonSerializable()
class OutOfStockStatus {
  OutOfStockStatus();

  // ignore: non_constant_identifier_names
  num stock_status_id;
  String name;

  factory OutOfStockStatus.fromJson(Map<String, dynamic> json) =>
      _$OutOfStockStatusFromJson(json);
  Map<String, dynamic> toJson() => _$OutOfStockStatusToJson(this);
}
