import 'package:json_annotation/json_annotation.dart';

part 'ratingSummary.g.dart';

@JsonSerializable()
class RatingSummary {
  RatingSummary();

  num rating;
  // ignore: non_constant_identifier_names
  num count_of_rating;

  factory RatingSummary.fromJson(Map<String, dynamic> json) =>
      _$RatingSummaryFromJson(json);
  Map<String, dynamic> toJson() => _$RatingSummaryToJson(this);
}
