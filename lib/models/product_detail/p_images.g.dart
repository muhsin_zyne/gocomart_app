// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'p_images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PImages _$PImagesFromJson(Map<String, dynamic> json) {
  return PImages()
    ..product_image_id = json['product_image_id'] as num
    ..image = json['image'] as String;
}

Map<String, dynamic> _$PImagesToJson(PImages instance) => <String, dynamic>{
      'product_image_id': instance.product_image_id,
      'image': instance.image
    };
