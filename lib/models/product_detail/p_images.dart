import 'package:json_annotation/json_annotation.dart';

part 'p_images.g.dart';

@JsonSerializable()
class PImages {
  PImages();

  // ignore: non_constant_identifier_names
  num product_image_id;
  String image;

  factory PImages.fromJson(Map<String, dynamic> json) =>
      _$PImagesFromJson(json);
  Map<String, dynamic> toJson() => _$PImagesToJson(this);
}
