import 'package:json_annotation/json_annotation.dart';
import "description.dart";
part 'p_option_dtl.g.dart';

@JsonSerializable()
class POptionDtl {
  POptionDtl();

  // ignore: non_constant_identifier_names
  num option_id;
  String type;
  Description description;

  factory POptionDtl.fromJson(Map<String, dynamic> json) =>
      _$POptionDtlFromJson(json);
  Map<String, dynamic> toJson() => _$POptionDtlToJson(this);
}
