import 'package:json_annotation/json_annotation.dart';
import "description.dart";
import "p_images.dart";
import "out_of_stock_status.dart";
import "p_option.dart";
import 'attributes.dart';
import 'special.dart';
import 'avgRating.dart';
import 'ratingSummary.dart';
import 'productReviewRatingCount.dart';
import 'package:gocomartapp/models/shopping_home/manufacturer.dart';
part 'product_detail.g.dart';

@JsonSerializable()
class ProductDetail {
  ProductDetail();

  String model;
  String sku;
  num price;
  num shipping;
  // ignore: non_constant_identifier_names
  String date_available;
  num minimum;
  num status;
  num quantity;
  List<Attributes> attributes;
  Description description;
  String image;
  List<PImages> images;
  OutOfStockStatus stock;
  // ignore: non_constant_identifier_names
  List<POption> product_option;
  Special special;
  AvgRating avgRating;
  ProductReviewRatingCount productReviewRatingCount;
  List<RatingSummary> ratingSummary;
  Manufacturer manufacturer;

  factory ProductDetail.fromJson(Map<String, dynamic> json) => _$ProductDetailFromJson(json);
  Map<String, dynamic> toJson() => _$ProductDetailToJson(this);
}
