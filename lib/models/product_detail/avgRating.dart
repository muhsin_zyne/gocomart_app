import 'package:json_annotation/json_annotation.dart';

part 'avgRating.g.dart';

@JsonSerializable()
class AvgRating {
  AvgRating();
  // ignore: non_constant_identifier_names
  double avg_rating;

  factory AvgRating.fromJson(Map<String, dynamic> json) =>
      _$AvgRatingFromJson(json);
  Map<String, dynamic> toJson() => _$AvgRatingToJson(this);
}
