import 'package:json_annotation/json_annotation.dart';

part 'description.g.dart';

@JsonSerializable()
class Description {
    Description();

    String name;
    
    factory Description.fromJson(Map<String,dynamic> json) => _$DescriptionFromJson(json);
    Map<String, dynamic> toJson() => _$DescriptionToJson(this);
}
