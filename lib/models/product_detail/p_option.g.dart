// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'p_option.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

POption _$POptionFromJson(Map<String, dynamic> json) {
  return POption()
    ..option_id = json['option_id'] as num
    ..option = json['option'] == null
        ? null
        : POptionDtl.fromJson(json['option'] as Map<String, dynamic>)
    ..master_option = json['master_option'] as num
    ..master_option_value = json['master_option_value'] as num
    ..optionValues = (json['optionValues'] as List)
        ?.map((e) => e == null
            ? null
            : POptionValues.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..value = json['value'] as String
    ..required = json['required'] as num
    ..childOptionItems = (json['childOptionItems'] as List)
        ?.map((e) => e == null
            ? null
            : POptionValues.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$POptionToJson(POption instance) => <String, dynamic>{
      'option_id': instance.option_id,
      'option': instance.option,
      'master_option': instance.master_option,
      'master_option_value': instance.master_option_value,
      'optionValues': instance.optionValues,
      'value': instance.value,
      'required': instance.required,
      'childOptionItems': instance.childOptionItems
    };
