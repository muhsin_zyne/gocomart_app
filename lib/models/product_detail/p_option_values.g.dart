// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'p_option_values.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

POptionValues _$POptionValuesFromJson(Map<String, dynamic> json) {
  return POptionValues()
    ..product_option_value_id = json['product_option_value_id'] as num
    ..product_option_id = json['product_option_id'] as num
    ..description = json['description'] == null
        ? null
        : POptionValuesDescription.fromJson(
            json['description'] as Map<String, dynamic>)
    ..quantity = json['quantity'] as num
    ..price = json['price'] as num
    ..price_prefix = json['price_prefix'] as String
    ..master_option_value = json['master_option_value'] as num
    ..child = json['child'] == null
        ? null
        : POption.fromJson(json['child'] as Map<String, dynamic>)
    ..optionImage = json['optionImage'] == null
        ? null
        : OpImage.fromJson(json['optionImage'] as Map<String, dynamic>);
}

Map<String, dynamic> _$POptionValuesToJson(POptionValues instance) =>
    <String, dynamic>{
      'product_option_value_id': instance.product_option_value_id,
      'product_option_id': instance.product_option_id,
      'description': instance.description,
      'quantity': instance.quantity,
      'price': instance.price,
      'price_prefix': instance.price_prefix,
      'master_option_value': instance.master_option_value,
      'child': instance.child,
      'optionImage': instance.optionImage,
      'isFlatOffer': instance.isFlatOffer,
    };
