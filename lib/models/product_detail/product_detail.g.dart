// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetail _$ProductDetailFromJson(Map<String, dynamic> json) {
  return ProductDetail()
    ..model = json['model'] as String
    ..sku = json['sku'] as String
    ..price = json['price'] as num
    ..shipping = json['shipping'] as num
    ..date_available = json['date_available'] as String
    ..minimum = json['minimum'] as num
    ..status = json['status'] as num
    ..quantity = json['quantity'] as num
    ..attributes = (json['attributes'] as List)
        ?.map((e) =>
            e == null ? null : Attributes.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..image = json['image'] as String
    ..images = (json['images'] as List)
        ?.map((e) =>
            e == null ? null : PImages.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..stock = json['stock'] == null
        ? null
        : OutOfStockStatus.fromJson(json['stock'] as Map<String, dynamic>)
    ..product_option = (json['product_option'] as List)
        ?.map((e) =>
            e == null ? null : POption.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..special = json['special'] == null
        ? null
        : Special.fromJson(json['special'] as Map<String, dynamic>)
    ..avgRating = json['avgRating'] == null
        ? null
        : AvgRating.fromJson(json['avgRating'] as Map<String, dynamic>)
        ..productReviewRatingCount = json['productReviewRatingCount'] == null
        ? null
        : ProductReviewRatingCount.fromJson(json['productReviewRatingCount'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacturer.fromJson(json['manufacturer'] as Map<String, dynamic>)
    ..ratingSummary = (json['ratingSummary'] as List)
        ?.map((e) => e == null
            ? null
            : RatingSummary.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ProductDetailToJson(ProductDetail instance) =>
    <String, dynamic>{
      'model': instance.model,
      'sku': instance.sku,
      'price': instance.price,
      'shipping': instance.shipping,
      'date_available': instance.date_available,
      'minimum': instance.minimum,
      'status': instance.status,
      'quantity': instance.quantity,
      'attributes': instance.attributes,
      'description': instance.description,
      'image': instance.image,
      'images': instance.images,
      'stock': instance.stock,
      'product_option': instance.product_option,
      'special': instance.special,
      'avgRating': instance.avgRating,
      'ratingSummary': instance.ratingSummary,
      'manufacturer': instance.manufacturer
    };
