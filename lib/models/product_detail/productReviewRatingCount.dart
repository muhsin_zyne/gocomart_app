import 'package:json_annotation/json_annotation.dart';

part 'productReviewRatingCount.g.dart';

@JsonSerializable()
class ProductReviewRatingCount {
  ProductReviewRatingCount();
  // ignore: non_constant_identifier_names
  num rating;
  // ignore: non_constant_identifier_names
  num review_count;

  factory ProductReviewRatingCount.fromJson(Map<String, dynamic> json) => _$ProductReviewRatingCountFromJson(json);
  Map<String, dynamic> toJson() => _$ProductReviewRatingCountToJson(this);
}
