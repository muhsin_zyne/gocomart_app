// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ratingSummary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingSummary _$RatingSummaryFromJson(Map<String, dynamic> json) {
  return RatingSummary()
    ..rating = json['rating'] as num
    ..count_of_rating = json['count_of_rating'] as num;
}

Map<String, dynamic> _$RatingSummaryToJson(RatingSummary instance) =>
    <String, dynamic>{
      'rating': instance.rating,
      'count_of_rating': instance.count_of_rating
    };
