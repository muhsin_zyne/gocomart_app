// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'p_option_dtl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

POptionDtl _$POptionDtlFromJson(Map<String, dynamic> json) {
  return POptionDtl()
    ..option_id = json['option_id'] as num
    ..type = json['type'] as String
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>);
}

Map<String, dynamic> _$POptionDtlToJson(POptionDtl instance) =>
    <String, dynamic>{
      'option_id': instance.option_id,
      'type': instance.type,
      'description': instance.description
    };
