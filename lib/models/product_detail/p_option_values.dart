import 'package:gocomartapp/models/product_detail/p_option.dart';
import 'package:json_annotation/json_annotation.dart';
import "p_option_values_description.dart";
import "opImage.dart";
part 'p_option_values.g.dart';

@JsonSerializable()
class POptionValues {
  POptionValues();

  // ignore: non_constant_identifier_names
  num product_option_value_id;
  // ignore: non_constant_identifier_names
  num product_option_id;
  POptionValuesDescription description;
  num quantity;
  num price;
  // ignore: non_constant_identifier_names
  String price_prefix;
  // ignore: non_constant_identifier_names
  num master_option_value;
  POption child;
  OpImage optionImage;

  //internal variables
  bool isFlatOffer = false;

  factory POptionValues.fromJson(Map<String, dynamic> json) => _$POptionValuesFromJson(json);
  Map<String, dynamic> toJson() => _$POptionValuesToJson(this);
}
