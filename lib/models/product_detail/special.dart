import 'package:json_annotation/json_annotation.dart';

part 'special.g.dart';

@JsonSerializable()
class Special {
  Special();

  // ignore: non_constant_identifier_names
  num price;

  factory Special.fromJson(Map<String, dynamic> json) =>
      _$SpecialFromJson(json);
  Map<String, dynamic> toJson() => _$SpecialToJson(this);
}