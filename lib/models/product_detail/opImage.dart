import 'package:json_annotation/json_annotation.dart';

part 'opImage.g.dart';

@JsonSerializable()
class OpImage {
    OpImage();

    String image;
    
    factory OpImage.fromJson(Map<String,dynamic> json) => _$OpImageFromJson(json);
    Map<String, dynamic> toJson() => _$OpImageToJson(this);
}
