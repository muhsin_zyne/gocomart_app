// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productReviewRatingCount.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductReviewRatingCount _$ProductReviewRatingCountFromJson(Map<String, dynamic> json) {
  return ProductReviewRatingCount() 
  ..review_count = json['review_count'] as num
  ..rating = json['rating'] as num;
}

Map<String, dynamic> _$ProductReviewRatingCountToJson(ProductReviewRatingCount instance) =>
    <String, dynamic>{
      'review_count': instance.review_count,
      'rating': instance.rating
    };
