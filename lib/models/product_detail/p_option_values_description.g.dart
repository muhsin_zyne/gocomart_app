// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'p_option_values_description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

POptionValuesDescription _$POptionValuesDescriptionFromJson(
    Map<String, dynamic> json) {
  return POptionValuesDescription()
    ..option_value_id = json['option_value_id'] as num
    ..name = json['name'] as String;
}

Map<String, dynamic> _$POptionValuesDescriptionToJson(
        POptionValuesDescription instance) =>
    <String, dynamic>{
      'option_value_id': instance.option_value_id,
      'name': instance.name
    };
