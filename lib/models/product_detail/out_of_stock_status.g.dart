// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'out_of_stock_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutOfStockStatus _$OutOfStockStatusFromJson(Map<String, dynamic> json) {
  return OutOfStockStatus()
    ..stock_status_id = json['stock_status_id'] as num
    ..name = json['name'] as String;
}

Map<String, dynamic> _$OutOfStockStatusToJson(OutOfStockStatus instance) =>
    <String, dynamic>{
      'stock_status_id': instance.stock_status_id,
      'name': instance.name
    };
