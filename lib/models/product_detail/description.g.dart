// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Description _$DescriptionFromJson(Map<String, dynamic> json) {
  return Description()..name = json['name'] as String;
}

Map<String, dynamic> _$DescriptionToJson(Description instance) =>
    <String, dynamic>{'name': instance.name};
