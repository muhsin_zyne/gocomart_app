// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'avgRating.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AvgRating _$AvgRatingFromJson(Map<String, dynamic> json) {
  return AvgRating()..avg_rating = (json['avg_rating'] as num)?.toDouble();
}

Map<String, dynamic> _$AvgRatingToJson(AvgRating instance) =>
    <String, dynamic>{'avg_rating': instance.avg_rating};
