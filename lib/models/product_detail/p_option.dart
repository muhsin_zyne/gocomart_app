import 'package:json_annotation/json_annotation.dart';
import "p_option_dtl.dart";
import "p_option_values.dart";
part 'p_option.g.dart';

@JsonSerializable()
class POption {
  POption();

  // ignore: non_constant_identifier_names
  num option_id;
  POptionDtl option;
  // ignore: non_constant_identifier_names
  num master_option;
  // ignore: non_constant_identifier_names
  num master_option_value;
  List<POptionValues> optionValues;
  String value;
  num required;
  List<POptionValues> childOptionItems;

  factory POption.fromJson(Map<String, dynamic> json) =>
      _$POptionFromJson(json);
  Map<String, dynamic> toJson() => _$POptionToJson(this);
}
