import 'package:json_annotation/json_annotation.dart';

part 'best_delivery_estimate.g.dart';

@JsonSerializable()
class BestDeliveryEstimate {
  BestDeliveryEstimate();

  num id;
  // ignore: non_constant_identifier_names
  num radius_in_km;
  // ignore: non_constant_identifier_names
  num min_days;
  // ignore: non_constant_identifier_names
  num max_days;
  // ignore: non_constant_identifier_names
  num faster_than_actual;
  // ignore: non_constant_identifier_names
  num est_days;
  // ignore: non_constant_identifier_names
  num distance;

  factory BestDeliveryEstimate.fromJson(Map<String, dynamic> json) =>
      _$BestDeliveryEstimateFromJson(json);
  Map<String, dynamic> toJson() => _$BestDeliveryEstimateToJson(this);
}
