import 'package:json_annotation/json_annotation.dart';
import "post_details.dart";
import "best_delivery_estimate.dart";
part 'delivery_estimation.g.dart';

@JsonSerializable()
class DeliveryEstimation {
    DeliveryEstimation();

    String error;
    PostDetails postalDetails;
    BestDeliveryEstimate bestDeliveryEstimate;
    
    factory DeliveryEstimation.fromJson(Map<String,dynamic> json) => _$DeliveryEstimationFromJson(json);
    Map<String, dynamic> toJson() => _$DeliveryEstimationToJson(this);
}
