// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostDetails _$PostDetailsFromJson(Map<String, dynamic> json) {
  return PostDetails()
    ..postal_code = json['postal_code'] as String
    ..place_name = json['place_name'] as String
    ..admin_name1 = json['admin_name1'] as String
    ..admin_name2 = json['admin_name2'] as String;
}

Map<String, dynamic> _$PostDetailsToJson(PostDetails instance) =>
    <String, dynamic>{
      'postal_code': instance.postal_code,
      'place_name': instance.place_name,
      'admin_name1': instance.admin_name1,
      'admin_name2': instance.admin_name2
    };
