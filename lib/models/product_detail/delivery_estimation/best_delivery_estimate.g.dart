// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'best_delivery_estimate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BestDeliveryEstimate _$BestDeliveryEstimateFromJson(Map<String, dynamic> json) {
  return BestDeliveryEstimate()
    ..id = json['id'] as num
    ..radius_in_km = json['radius_in_km'] as num
    ..min_days = json['min_days'] as num
    ..max_days = json['max_days'] as num
    ..faster_than_actual = json['faster_than_actual'] as num
    ..est_days = json['est_days'] as num
    ..distance = json['distance'] as num;
}

Map<String, dynamic> _$BestDeliveryEstimateToJson(
        BestDeliveryEstimate instance) =>
    <String, dynamic>{
      'id': instance.id,
      'radius_in_km': instance.radius_in_km,
      'min_days': instance.min_days,
      'max_days': instance.max_days,
      'faster_than_actual': instance.faster_than_actual,
      'est_days': instance.est_days,
      'distance': instance.distance
    };
