import 'package:json_annotation/json_annotation.dart';

part 'post_details.g.dart';

@JsonSerializable()
class PostDetails {
  PostDetails();
  // ignore: non_constant_identifier_names
  String postal_code;
  // ignore: non_constant_identifier_names
  String place_name;
  // ignore: non_constant_identifier_names
  String admin_name1;
  // ignore: non_constant_identifier_names
  String admin_name2;

  factory PostDetails.fromJson(Map<String, dynamic> json) =>
      _$PostDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$PostDetailsToJson(this);
}
