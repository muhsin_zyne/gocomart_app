// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delivery_estimation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryEstimation _$DeliveryEstimationFromJson(Map<String, dynamic> json) {
  return DeliveryEstimation()
    ..error = json['error'] as String
    ..postalDetails = json['postalDetails'] == null
        ? null
        : PostDetails.fromJson(json['postalDetails'] as Map<String, dynamic>)
    ..bestDeliveryEstimate = json['bestDeliveryEstimate'] == null
        ? null
        : BestDeliveryEstimate.fromJson(
            json['bestDeliveryEstimate'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeliveryEstimationToJson(DeliveryEstimation instance) =>
    <String, dynamic>{
      'error': instance.error,
      'postalDetails': instance.postalDetails,
      'bestDeliveryEstimate': instance.bestDeliveryEstimate
    };
