// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'opImage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OpImage _$OpImageFromJson(Map<String, dynamic> json) {
  return OpImage()..image = json['image'] as String;
}

Map<String, dynamic> _$OpImageToJson(OpImage instance) =>
    <String, dynamic>{'image': instance.image};
