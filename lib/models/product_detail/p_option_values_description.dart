import 'package:json_annotation/json_annotation.dart';

part 'p_option_values_description.g.dart';

@JsonSerializable()
class POptionValuesDescription {
  POptionValuesDescription();

  // ignore: non_constant_identifier_names
  num option_value_id;
  String name;

  factory POptionValuesDescription.fromJson(Map<String, dynamic> json) =>
      _$POptionValuesDescriptionFromJson(json);
  Map<String, dynamic> toJson() => _$POptionValuesDescriptionToJson(this);
}
