// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attributes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Attributes _$AttributesFromJson(Map<String, dynamic> json) {
  return Attributes()
    ..attribute_id = json['attribute_id'] as num
    ..text = json['text'] as String
    ..getAttributeName = json['getAttributeName'] as String;
}

Map<String, dynamic> _$AttributesToJson(Attributes instance) =>
    <String, dynamic>{
      'attribute_id': instance.attribute_id,
      'text': instance.text,
      'getAttributeName': instance.getAttributeName
    };
