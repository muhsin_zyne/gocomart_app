import 'package:json_annotation/json_annotation.dart';
import "optionValue.dart";
part 'offeredOptions.g.dart';

@JsonSerializable()
class OfferedOptions {
  OfferedOptions();
  // ignore: non_constant_identifier_names
  num ac_group_product_option_id;
  // ignore: non_constant_identifier_names
  num ac_group_product_id;
  // ignore: non_constant_identifier_names
  num product_id;
  // ignore: non_constant_identifier_names
  List<OptionValue> optionValue;

  factory OfferedOptions.fromJson(Map<String, dynamic> json) => _$OfferedOptionsFromJson(json);
  Map<String, dynamic> toJson() => _$OfferedOptionsToJson(this);
}
