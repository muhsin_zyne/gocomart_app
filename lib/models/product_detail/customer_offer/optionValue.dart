import 'package:json_annotation/json_annotation.dart';

part 'optionValue.g.dart';

@JsonSerializable()
class OptionValue {
  OptionValue();
  // ignore: non_constant_identifier_names
  num ac_group_product_option_value_id;
  // ignore: non_constant_identifier_names
  num product_option_id;
  // ignore: non_constant_identifier_names
  num product_option_value_id;

  factory OptionValue.fromJson(Map<String, dynamic> json) => _$OptionValueFromJson(json);
  Map<String, dynamic> toJson() => _$OptionValueToJson(this);
}
