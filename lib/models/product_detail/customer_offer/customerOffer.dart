import 'package:json_annotation/json_annotation.dart';
import "customerPrice.dart";
import "offeredOptions.dart";
part 'customerOffer.g.dart';

@JsonSerializable()
class CustomerOffer {
    CustomerOffer();

    CustomerPrice customerPrice;
    List<OfferedOptions> offeredOptions;
    
    factory CustomerOffer.fromJson(Map<String,dynamic> json) => _$CustomerOfferFromJson(json);
    Map<String, dynamic> toJson() => _$CustomerOfferToJson(this);
}
