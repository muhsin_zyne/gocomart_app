// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customerPrice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerPrice _$CustomerPriceFromJson(Map<String, dynamic> json) {
  return CustomerPrice()
    ..ac_group_product_id = json['ac_group_product_id'] as num
    ..product_id = json['product_id'] as num
    ..max_quantity = json['max_quantity'] as num
    ..offer_price = json['offer_price'] as num
    ..is_flexible_option = json['is_flexible_option'] as bool
    ..valid_to = json['valid_to'] as String
    ..option_length = json['option_length'] as num;
}

Map<String, dynamic> _$CustomerPriceToJson(CustomerPrice instance) =>
    <String, dynamic>{
      'ac_group_product_id': instance.ac_group_product_id,
      'product_id': instance.product_id,
      'max_quantity': instance.max_quantity,
      'offer_price': instance.offer_price,
      'is_flexible_option': instance.is_flexible_option,
      'valid_to': instance.valid_to,
      'option_length': instance.option_length
    };
