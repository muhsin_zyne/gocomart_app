// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customerOffer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerOffer _$CustomerOfferFromJson(Map<String, dynamic> json) {
  return CustomerOffer()
    ..customerPrice = json['customerPrice'] == null
        ? null
        : CustomerPrice.fromJson(json['customerPrice'] as Map<String, dynamic>)
    ..offeredOptions = (json['offeredOptions'] as List)
        ?.map((e) => e == null
            ? null
            : OfferedOptions.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$CustomerOfferToJson(CustomerOffer instance) =>
    <String, dynamic>{
      'customerPrice': instance.customerPrice,
      'offeredOptions': instance.offeredOptions
    };
