// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offeredOptions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferedOptions _$OfferedOptionsFromJson(Map<String, dynamic> json) {
  return OfferedOptions()
    ..ac_group_product_option_id = json['ac_group_product_option_id'] as num
    ..ac_group_product_id = json['ac_group_product_id'] as num
    ..product_id = json['product_id'] as num
    ..optionValue = (json['optionValue'] as List)
        ?.map((e) =>
            e == null ? null : OptionValue.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$OfferedOptionsToJson(OfferedOptions instance) =>
    <String, dynamic>{
      'ac_group_product_option_id': instance.ac_group_product_option_id,
      'ac_group_product_id': instance.ac_group_product_id,
      'product_id': instance.product_id,
      'optionValue': instance.optionValue
    };
