// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'optionValue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionValue _$OptionValueFromJson(Map<String, dynamic> json) {
  return OptionValue()
    ..ac_group_product_option_value_id =
        json['ac_group_product_option_value_id'] as num
    ..product_option_id = json['product_option_id'] as num
    ..product_option_value_id = json['product_option_value_id'] as num;
}

Map<String, dynamic> _$OptionValueToJson(OptionValue instance) =>
    <String, dynamic>{
      'ac_group_product_option_value_id':
          instance.ac_group_product_option_value_id,
      'product_option_id': instance.product_option_id,
      'product_option_value_id': instance.product_option_value_id
    };
