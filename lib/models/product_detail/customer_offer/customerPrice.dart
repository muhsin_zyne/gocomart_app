import 'package:json_annotation/json_annotation.dart';

part 'customerPrice.g.dart';

@JsonSerializable()
class CustomerPrice {
  CustomerPrice();
  // ignore: non_constant_identifier_names
  num ac_group_product_id;
  // ignore: non_constant_identifier_names
  num product_id;
  // ignore: non_constant_identifier_names
  num max_quantity;
  // ignore: non_constant_identifier_names
  num offer_price;
  // ignore: non_constant_identifier_names
  bool is_flexible_option;
  // ignore: non_constant_identifier_names
  String valid_to;
  // ignore: non_constant_identifier_names
  num option_length;

  factory CustomerPrice.fromJson(Map<String, dynamic> json) => _$CustomerPriceFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerPriceToJson(this);
}
