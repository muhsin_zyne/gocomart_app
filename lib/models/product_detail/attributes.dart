import 'package:json_annotation/json_annotation.dart';
part 'attributes.g.dart';

@JsonSerializable()

class Attributes{
  Attributes();

  // ignore: non_constant_identifier_names
  num attribute_id;
  String text;
  String getAttributeName;

  factory Attributes.fromJson(Map<String, dynamic> json) =>
      _$AttributesFromJson(json);
  Map<String, dynamic> toJson() => _$AttributesToJson(this);
}