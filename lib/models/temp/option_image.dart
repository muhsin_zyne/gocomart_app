import 'package:json_annotation/json_annotation.dart';
import '../product_detail/opImage.dart';
part 'option_image.g.dart';

@JsonSerializable()
class OptionImage {
  OptionImage();

  OpImage optionImage;

  factory OptionImage.fromJson(Map<String, dynamic> json) =>
      _$OptionImageFromJson(json);
  Map<String, dynamic> toJson() => _$OptionImageToJson(this);
}
