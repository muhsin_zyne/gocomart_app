// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'option_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionImage _$OptionImageFromJson(Map<String, dynamic> json) {
  return OptionImage()
    ..optionImage = json['optionImage'] == null
        ? null
        : OpImage.fromJson(json['optionImage'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OptionImageToJson(OptionImage instance) =>
    <String, dynamic>{'optionImage': instance.optionImage};
