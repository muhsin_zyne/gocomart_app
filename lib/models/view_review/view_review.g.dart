// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'view_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ViewReview _$ViewReviewFromJson(Map<String, dynamic> json) {
  return ViewReview()
    ..error = json['error'] as String
    ..success = json['success'] as String
    ..review = json['review'] == null
        ? null
        : Review.fromJson(json['review'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ViewReviewToJson(ViewReview instance) =>
    <String, dynamic>{
      'error': instance.error,
      'success': instance.success,
      'review': instance.review
    };
