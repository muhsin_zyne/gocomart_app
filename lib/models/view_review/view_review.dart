import 'package:json_annotation/json_annotation.dart';
import "review.dart";
part 'view_review.g.dart';

@JsonSerializable()
class ViewReview {
    ViewReview();

    String error;
    String success;
    Review review;
    
    factory ViewReview.fromJson(Map<String,dynamic> json) => _$ViewReviewFromJson(json);
    Map<String, dynamic> toJson() => _$ViewReviewToJson(this);
}
