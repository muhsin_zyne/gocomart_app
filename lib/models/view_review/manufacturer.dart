import 'package:json_annotation/json_annotation.dart';

part 'manufacturer.g.dart';

@JsonSerializable()
class Manufacturer {
    Manufacturer();

    String name;
    
    factory Manufacturer.fromJson(Map<String,dynamic> json) => _$ManufacturerFromJson(json);
    Map<String, dynamic> toJson() => _$ManufacturerToJson(this);
}
