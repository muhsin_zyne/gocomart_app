// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductInfo _$ProductInfoFromJson(Map<String, dynamic> json) {
  return ProductInfo()
    ..image = json['image'] as String
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null
        ? null
        : Manufacturer.fromJson(json['manufacturer'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductInfoToJson(ProductInfo instance) =>
    <String, dynamic>{
      'image': instance.image,
      'description': instance.description,
      'manufacturer': instance.manufacturer
    };
