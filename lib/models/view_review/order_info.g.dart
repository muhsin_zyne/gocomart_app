// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderInfo _$OrderInfoFromJson(Map<String, dynamic> json) {
  return OrderInfo()
    ..date_modified = json['date_modified'] as String
    ..order_status_id = json['order_status_id'] as num
    ..status = json['status'] == null
        ? null
        : Status.fromJson(json['status'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrderInfoToJson(OrderInfo instance) =>
    <String, dynamic>{
      'date_modified': instance.date_modified,
      'order_status_id': instance.order_status_id,
      'status': instance.status
    };
