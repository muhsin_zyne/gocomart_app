import 'package:json_annotation/json_annotation.dart';
import "status.dart";
part 'order_info.g.dart';

@JsonSerializable()
class OrderInfo {
  OrderInfo();
  // ignore: non_constant_identifier_names
  String date_modified;
  // ignore: non_constant_identifier_names
  num order_status_id;
  Status status;

  factory OrderInfo.fromJson(Map<String, dynamic> json) => _$OrderInfoFromJson(json);
  Map<String, dynamic> toJson() => _$OrderInfoToJson(this);
}
