import 'package:json_annotation/json_annotation.dart';
import "product_info.dart";
import "order_info.dart";
part 'review.g.dart';

@JsonSerializable()
class Review {
  Review();

  ProductInfo productInfo;
  // ignore: non_constant_identifier_names
  num review_id;
  // ignore: non_constant_identifier_names
  num product_id;
  // ignore: non_constant_identifier_names
  num customer_id;
  // ignore: non_constant_identifier_names
  num order_id;
  // ignore: non_constant_identifier_names
  String author;
  String title;
  String text;
  num rating;
  // ignore: non_constant_identifier_names
  String date_added;
  // ignore: non_constant_identifier_names
  String date_modified;
  OrderInfo orderInfo;

  factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewToJson(this);
}
