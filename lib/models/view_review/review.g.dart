// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) {
  return Review()
    ..productInfo = json['productInfo'] == null
        ? null
        : ProductInfo.fromJson(json['productInfo'] as Map<String, dynamic>)
    ..review_id = json['review_id'] as num
    ..product_id = json['product_id'] as num
    ..customer_id = json['customer_id'] as num
    ..order_id = json['order_id'] as num
    ..author = json['author'] as String
    ..title = json['title'] as String
    ..text = json['text'] as String
    ..rating = json['rating'] as num
    ..date_added = json['date_added'] as String
    ..date_modified = json['date_modified'] as String
    ..orderInfo = json['orderInfo'] == null
        ? null
        : OrderInfo.fromJson(json['orderInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'productInfo': instance.productInfo,
      'review_id': instance.review_id,
      'product_id': instance.product_id,
      'customer_id': instance.customer_id,
      'order_id': instance.order_id,
      'author': instance.author,
      'title': instance.title,
      'text': instance.text,
      'rating': instance.rating,
      'date_added': instance.date_added,
      'date_modified': instance.date_modified,
      'orderInfo': instance.orderInfo
    };
