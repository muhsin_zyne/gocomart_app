import 'package:json_annotation/json_annotation.dart';
import "description.dart";
import "manufacturer.dart";
part 'product_info.g.dart';

@JsonSerializable()
class ProductInfo {
    ProductInfo();

    String image;
    Description description;
    Manufacturer manufacturer;
    
    factory ProductInfo.fromJson(Map<String,dynamic> json) => _$ProductInfoFromJson(json);
    Map<String, dynamic> toJson() => _$ProductInfoToJson(this);
}
