// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coinWalletAppLink.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoinWalletAppLink _$CoinWalletAppLinkFromJson(Map<String, dynamic> json) {
  return CoinWalletAppLink()
    ..label = json['label'] as String
    ..links = (json['links'] as List)
        ?.map(
            (e) => e == null ? null : Links.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$CoinWalletAppLinkToJson(CoinWalletAppLink instance) =>
    <String, dynamic>{'label': instance.label, 'links': instance.links};
