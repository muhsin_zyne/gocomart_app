// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coinTransaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoinTransaction _$CoinTransactionFromJson(Map<String, dynamic> json) {
  return CoinTransaction()
    ..type = json['type'] as String
    ..coin_scratch_card_id = json['coin_scratch_card_id'] as num
    ..coin_value = json['coin_value'] as num
    ..description = json['description'] as String
    ..created_at = json['created_at'] as num;
}

Map<String, dynamic> _$CoinTransactionToJson(CoinTransaction instance) =>
    <String, dynamic>{
      'type': instance.type,
      'coin_scratch_card_id': instance.coin_scratch_card_id,
      'coin_value': instance.coin_value,
      'description': instance.description,
      'created_at': instance.created_at
    };
