// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'walletAppLinks.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WalletAppLinks _$WalletAppLinksFromJson(Map<String, dynamic> json) {
  return WalletAppLinks()
    ..cashWallet = (json['cashWallet'] as List)?.map((e) => e == null ? null : WalletAppLink.fromJson(e as Map<String, dynamic>))?.toList()
    ..coinWallet = (json['coinWallet'] as List)?.map((e) => e == null ? null : WalletAppLink.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$WalletAppLinksToJson(WalletAppLinks instance) => <String, dynamic>{'cashWallet': instance.cashWallet, 'coinWallet': instance.coinWallet};
