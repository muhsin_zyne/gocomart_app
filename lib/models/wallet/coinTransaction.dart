import 'package:json_annotation/json_annotation.dart';

part 'coinTransaction.g.dart';

@JsonSerializable()
class CoinTransaction {
  CoinTransaction();

  String type;
  // ignore: non_constant_identifier_names
  num coin_scratch_card_id;
  // ignore: non_constant_identifier_names
  num coin_value;
  String description;
  // ignore: non_constant_identifier_names
  num created_at;

  factory CoinTransaction.fromJson(Map<String, dynamic> json) => _$CoinTransactionFromJson(json);
  Map<String, dynamic> toJson() => _$CoinTransactionToJson(this);
}
