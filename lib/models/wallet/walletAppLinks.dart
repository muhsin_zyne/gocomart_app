import 'package:json_annotation/json_annotation.dart';
import "cashWalletAppLink.dart";
part 'walletAppLinks.g.dart';

@JsonSerializable()
class WalletAppLinks {
  WalletAppLinks();

  List<WalletAppLink> cashWallet;
  List<WalletAppLink> coinWallet;

  factory WalletAppLinks.fromJson(Map<String, dynamic> json) => _$WalletAppLinksFromJson(json);
  Map<String, dynamic> toJson() => _$WalletAppLinksToJson(this);
}
