// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'links.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Links _$LinksFromJson(Map<String, dynamic> json) {
  return Links()
    ..id = json['id'] as num
    ..title = json['title'] as String
    ..description = json['description'] as String
    ..link = json['link'] as String
    ..icon = json['icon'] as String
    ..primary_color = json['primary_color'] as String
    ..secondary_color = json['secondary_color'] as String
    ..bg_color = json['bg_color'] as String
    ..exclusiveMark = json['exclusiveMark'] as num;
}

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'link': instance.link,
      'icon': instance.icon,
      'primary_color': instance.primary_color,
      'secondary_color': instance.secondary_color,
      'bg_color': instance.bg_color,
      'exclusiveMark': instance.exclusiveMark
    };
