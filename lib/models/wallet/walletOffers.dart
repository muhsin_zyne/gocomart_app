import 'package:json_annotation/json_annotation.dart';
import "walletOffer.dart";
part 'walletOffers.g.dart';

@JsonSerializable()
class WalletOffers {
    WalletOffers();

    List<WalletOffer> offers;
    
    factory WalletOffers.fromJson(Map<String,dynamic> json) => _$WalletOffersFromJson(json);
    Map<String, dynamic> toJson() => _$WalletOffersToJson(this);
}
