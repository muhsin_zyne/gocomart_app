import 'package:json_annotation/json_annotation.dart';
import "links.dart";
part 'cashWalletAppLink.g.dart';

@JsonSerializable()
class WalletAppLink {
  WalletAppLink();

  String label;
  List<Links> links;

  factory WalletAppLink.fromJson(Map<String, dynamic> json) => _$CashWalletAppLinkFromJson(json);
  Map<String, dynamic> toJson() => _$CashWalletAppLinkToJson(this);
}
