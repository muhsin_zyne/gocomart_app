// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coinTransactionHistory.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoinTransactionHistory _$CoinTransactionHistoryFromJson(
    Map<String, dynamic> json) {
  return CoinTransactionHistory()
    ..total = json['total'] as num
    ..transactions = (json['transactions'] as List)
        ?.map((e) => e == null
            ? null
            : CoinTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$CoinTransactionHistoryToJson(
        CoinTransactionHistory instance) =>
    <String, dynamic>{
      'total': instance.total,
      'transactions': instance.transactions
    };
