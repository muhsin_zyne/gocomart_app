// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'walletOffers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WalletOffers _$WalletOffersFromJson(Map<String, dynamic> json) {
  return WalletOffers()
    ..offers = (json['offers'] as List)
        ?.map((e) =>
            e == null ? null : WalletOffer.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$WalletOffersToJson(WalletOffers instance) =>
    <String, dynamic>{'offers': instance.offers};
