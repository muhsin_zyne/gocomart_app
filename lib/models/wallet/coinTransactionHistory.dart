import 'package:json_annotation/json_annotation.dart';
import "coinTransaction.dart";
part 'coinTransactionHistory.g.dart';

@JsonSerializable()
class CoinTransactionHistory {
    CoinTransactionHistory();

    num total;
    List<CoinTransaction> transactions;
    
    factory CoinTransactionHistory.fromJson(Map<String,dynamic> json) => _$CoinTransactionHistoryFromJson(json);
    Map<String, dynamic> toJson() => _$CoinTransactionHistoryToJson(this);
}
