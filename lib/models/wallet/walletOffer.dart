import 'package:json_annotation/json_annotation.dart';

part 'walletOffer.g.dart';

@JsonSerializable()
class WalletOffer {
  WalletOffer();

  num id;
  String title;
  String description;
  String content;
  // ignore: non_constant_identifier_names
  String action_label;
  String action;
  // ignore: non_constant_identifier_names
  String offer_from;
  // ignore: non_constant_identifier_names
  String valid_from;
  // ignore: non_constant_identifier_names
  String valid_to;
  // ignore: non_constant_identifier_names
  String offer_card_image;
  // ignore: non_constant_identifier_names
  String card_color;
  num priority;

  factory WalletOffer.fromJson(Map<String, dynamic> json) => _$WalletOfferFromJson(json);
  Map<String, dynamic> toJson() => _$WalletOfferToJson(this);
}
