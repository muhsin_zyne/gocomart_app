import 'package:json_annotation/json_annotation.dart';
import "links.dart";
part 'coinWalletAppLink.g.dart';

@JsonSerializable()
class CoinWalletAppLink {
    CoinWalletAppLink();

    String label;
    List<Links> links;
    
    factory CoinWalletAppLink.fromJson(Map<String,dynamic> json) => _$CoinWalletAppLinkFromJson(json);
    Map<String, dynamic> toJson() => _$CoinWalletAppLinkToJson(this);
}
