// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cashWalletAppLink.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WalletAppLink _$CashWalletAppLinkFromJson(Map<String, dynamic> json) {
  return WalletAppLink()
    ..label = json['label'] as String
    ..links = (json['links'] as List)?.map((e) => e == null ? null : Links.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$CashWalletAppLinkToJson(WalletAppLink instance) => <String, dynamic>{'label': instance.label, 'links': instance.links};
