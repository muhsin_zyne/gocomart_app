// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'walletOffer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WalletOffer _$WalletOfferFromJson(Map<String, dynamic> json) {
  return WalletOffer()
    ..id = json['id'] as num
    ..title = json['title'] as String
    ..description = json['description'] as String
    ..content = json['content'] as String
    ..action_label = json['action_label'] as String
    ..action = json['action'] as String
    ..offer_from = json['offer_from'] as String
    ..valid_from = json['valid_from'] as String
    ..valid_to = json['valid_to'] as String
    ..offer_card_image = json['offer_card_image'] as String
    ..card_color = json['card_color'] as String
    ..priority = json['priority'] as num;
}

Map<String, dynamic> _$WalletOfferToJson(WalletOffer instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'content': instance.content,
      'action_label': instance.action_label,
      'action': instance.action,
      'offer_from': instance.offer_from,
      'valid_from': instance.valid_from,
      'valid_to': instance.valid_to,
      'offer_card_image': instance.offer_card_image,
      'card_color': instance.card_color,
      'priority': instance.priority
    };
