import 'package:json_annotation/json_annotation.dart';

part 'links.g.dart';

@JsonSerializable()
class Links {
  Links();

  num id;
  String title;
  String description;
  String link;
  String icon;
  // ignore: non_constant_identifier_names
  String primary_color;
  // ignore: non_constant_identifier_names
  String secondary_color;
  // ignore: non_constant_identifier_names
  String bg_color;
  num exclusiveMark = 0;

  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);
  Map<String, dynamic> toJson() => _$LinksToJson(this);
}
