import 'option_value_new.dart';

class ProductOptionNew {
  String name;
  int optionId;
  List<OptionValuesNew> optionValuesNew;
  String type;

  ProductOptionNew.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'] as String;
    this.optionId = json['option_id'] as int;
    // this.optionValuesNew = parsedJson['optionValuesNew'];
    this.optionValuesNew = (json['optionValuesNew'] as List)
        ?.map((e) => e == null
            ? null
            : OptionValuesNew.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.type = json['type'] as String;
  }
}
