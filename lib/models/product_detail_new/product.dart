import 'product_option_new.dart';

class Product {
  List<ProductOptionNew> productOptionNew;
  String image;
  List<PImages> images;
  Description description;
  AvgRating avgRating;

  Product.fromJSON(Map<String, dynamic> json) {
    // this.productOptionNew = json['productOptionNew'];

    this.productOptionNew = (json['productOptionNew'] as List)
        ?.map((e) => e == null
            ? null
            : ProductOptionNew.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.image = json['image'] as String;
    this.images = (json['images'] as List)
        ?.map((e) =>
            e == null ? null : PImages.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.description = json['description'] == null
        ? null
        : Description.fromJSON(json['description'] as Map<String, dynamic>);
  }
}

class PImages {
  String image;
  PImages.fromJSON(Map<String, dynamic> json) {
    this.image = json['image'] as String;
  }
}

class Description {
  String name;
  Description.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'] as String;
  }
}

class AvgRating {
  num avg_rating;
  AvgRating.fromJSON(Map<String, dynamic> json) {
    this.avg_rating = json['avg_rating'] as num;
  }
}
