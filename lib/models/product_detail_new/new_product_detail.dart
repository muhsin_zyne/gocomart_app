import 'product.dart';

class NewProductDetail {
  Product product;
  

  NewProductDetail.fromJSON(Map<String, dynamic> json) {
    this.product = json['product'] == null
        ? null
        : Product.fromJSON(json['product'] as Map<String, dynamic>);
  
  }
}

