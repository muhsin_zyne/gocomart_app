import 'package:gocomartapp/models/product_detail_new/child_option.dart';

class OptionValuesNew {
  List<ChildOption> childOptions;
  String image;
  String name;
  int productOptionId;
  int productOptionValueId;
  int quantity;

  OptionValuesNew.fromJSON(Map<String, dynamic> json) {
    //this.childOptions = parsedJson['childOptions'];
    this.childOptions = (json['childOptions'] as List)
        ?.map((e) =>
            e == null ? null : ChildOption.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.image = json['image'];
    this.name = json['name'];
    this.productOptionId = json['product_option_id'];
    this.productOptionValueId = json['product_option_value_id'];
    this.quantity = json['quantity'];
  }
}
