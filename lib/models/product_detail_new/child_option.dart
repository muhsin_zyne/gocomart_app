import 'option_value_new_child.dart';

class ChildOption {
  String name;
  int optionId;
  List<OptionValuesNewChild> optionValuesNewChild;
  int productOptionId;
  String type;

  ChildOption.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'];
    this.optionId = json['option_id'];
    //this.optionValuesNewChild = parsedJson['optionValuesNewChild'];
    this.optionValuesNewChild = (json['optionValuesNewChild'] as List)
        ?.map((e) => e == null
            ? null
            : OptionValuesNewChild.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.productOptionId = json['product_option_id'];
    this.type = json['type'];
  }
}
