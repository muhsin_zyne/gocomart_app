class OptionValuesNewChild {
  String image;
  int masterOptionValue;
  String name;
  int productOptionId;
  int productOptionValueId;

  OptionValuesNewChild.fromJSON(Map<String, dynamic> json) {
    this.image = json['image'];
    this.masterOptionValue = json['master_option_value'];
    this.name = json['name'];
    this.productOptionId = json['product_option_id'];
    this.productOptionValueId = json['product_option_value_id'];
  }
}
