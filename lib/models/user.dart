import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  User();

  String id;
  // ignore: non_constant_identifier_names
  num mobile_no;
  // ignore: non_constant_identifier_names
  num user_id;
  num status;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
