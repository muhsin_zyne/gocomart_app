import 'package:json_annotation/json_annotation.dart';

part 'current_balance.g.dart';

@JsonSerializable()
class CurrentBalance {
  CurrentBalance();

  num amount;

  factory CurrentBalance.fromJson(Map<String, dynamic> json) =>
      _$CurrentBalanceFromJson(json);
  Map<String, dynamic> toJson() => _$CurrentBalanceToJson(this);
}
