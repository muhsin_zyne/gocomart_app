import 'package:json_annotation/json_annotation.dart';

part 'transaction.g.dart';

@JsonSerializable()
class Transaction {
  Transaction();

  String type;
  // ignore: non_constant_identifier_names
  num scratch_card_id;
  num amount;
  String description;
  // ignore: non_constant_identifier_names
  num created_at;

  factory Transaction.fromJson(Map<String, dynamic> json) =>
      _$TransactionFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionToJson(this);
}
