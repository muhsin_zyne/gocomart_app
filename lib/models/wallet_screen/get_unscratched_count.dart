import 'package:json_annotation/json_annotation.dart';

part 'get_unscratched_count.g.dart';

@JsonSerializable()
class GetUnscratchedCount {
  GetUnscratchedCount();

  num count;

  factory GetUnscratchedCount.fromJson(Map<String, dynamic> json) =>
      _$GetUnscratchedCountFromJson(json);
  Map<String, dynamic> toJson() => _$GetUnscratchedCountToJson(this);
}
