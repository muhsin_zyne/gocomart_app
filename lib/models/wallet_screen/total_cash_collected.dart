import 'package:json_annotation/json_annotation.dart';

part 'total_cash_collected.g.dart';

@JsonSerializable()
class TotalCashCollected {
  TotalCashCollected();

  num amount;

  factory TotalCashCollected.fromJson(Map<String, dynamic> json) =>
      _$TotalCashCollectedFromJson(json);
  Map<String, dynamic> toJson() => _$TotalCashCollectedToJson(this);
}
