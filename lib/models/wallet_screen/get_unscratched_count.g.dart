// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_unscratched_count.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUnscratchedCount _$GetUnscratchedCountFromJson(Map<String, dynamic> json) {
  return GetUnscratchedCount()..count = json['count'] as num;
}

Map<String, dynamic> _$GetUnscratchedCountToJson(
        GetUnscratchedCount instance) =>
    <String, dynamic>{'count': instance.count};
