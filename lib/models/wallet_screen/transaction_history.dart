import 'package:json_annotation/json_annotation.dart';
import "transaction.dart";
part 'transaction_history.g.dart';

@JsonSerializable()
class TransactionHistory {
  TransactionHistory();

  num total;
  List<Transaction> data;

  factory TransactionHistory.fromJson(Map<String, dynamic> json) =>
      _$TransactionHistoryFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionHistoryToJson(this);
}
