// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_balance.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentBalance _$CurrentBalanceFromJson(Map<String, dynamic> json) {
  return CurrentBalance()..amount = json['amount'] as num;
}

Map<String, dynamic> _$CurrentBalanceToJson(CurrentBalance instance) =>
    <String, dynamic>{'amount': instance.amount};
