// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) {
  return Transaction()
    ..type = json['type'] as String
    ..scratch_card_id = json['scratch_card_id'] as num
    ..amount = json['amount'] as num
    ..description = json['description'] as String
    ..created_at = json['created_at'] as num;
}

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'type': instance.type,
      'scratch_card_id': instance.scratch_card_id,
      'amount': instance.amount,
      'description': instance.description,
      'created_at': instance.created_at
    };
