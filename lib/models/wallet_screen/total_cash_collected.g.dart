// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'total_cash_collected.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TotalCashCollected _$TotalCashCollectedFromJson(Map<String, dynamic> json) {
  return TotalCashCollected()..amount = json['amount'] as num;
}

Map<String, dynamic> _$TotalCashCollectedToJson(TotalCashCollected instance) =>
    <String, dynamic>{'amount': instance.amount};
