// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appCategories.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppCategories _$AppCategoriesFromJson(Map<String, dynamic> json) {
  return AppCategories()
    ..error = json['error'] as bool
    ..message = json['message'] as String
    ..categories = (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$AppCategoriesToJson(AppCategories instance) =>
    <String, dynamic>{
      'error': instance.error,
      'message': instance.message,
      'categories': instance.categories
    };
