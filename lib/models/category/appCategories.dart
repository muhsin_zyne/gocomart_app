import 'package:json_annotation/json_annotation.dart';
import "category.dart";
part 'appCategories.g.dart';

@JsonSerializable()
class AppCategories {
    AppCategories();

    bool error;
    String message;
    List<Category> categories;
    
    factory AppCategories.fromJson(Map<String,dynamic> json) => _$AppCategoriesFromJson(json);
    Map<String, dynamic> toJson() => _$AppCategoriesToJson(this);
}
