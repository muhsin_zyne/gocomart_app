import 'package:json_annotation/json_annotation.dart';

import "description.dart";

part 'category.g.dart';

@JsonSerializable()
class Category {
  Category();

  num categoryId;
  String image;
  Description description;
  String banner;
  List<Category> children;

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
