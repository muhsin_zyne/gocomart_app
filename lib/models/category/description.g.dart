// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Description _$DescriptionFromJson(Map<String, dynamic> json) {
  return Description()
    ..name = json['name'] as String
    ..categoryId = json['category_id'] as num;
}

Map<String, dynamic> _$DescriptionToJson(Description instance) => <String, dynamic>{'name': instance.name, 'category_id': instance.categoryId};
