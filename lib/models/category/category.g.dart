// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category()
    ..categoryId = json['category_id'] as num
    ..image = json['image'] as String
    ..description = json['description'] == null ? null : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..banner = json['banner'] as String
    ..children = (json['children'] as List)?.map((e) => e == null ? null : Category.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'category_id': instance.categoryId,
      'image': instance.image,
      'description': instance.description,
      'banner': instance.banner,
      'children': instance.children
    };
