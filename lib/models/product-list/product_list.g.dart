// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductList _$ProductListFromJson(Map<String, dynamic> json) {
  return ProductList()
    ..total = json['total'] as num
    ..products = (json['products'] as List)
        ?.map((e) => e == null ? null : Product.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ProductListToJson(ProductList instance) =>
    <String, dynamic>{'total': instance.total, 'products': instance.products};
