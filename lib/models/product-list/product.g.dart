// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product()
    ..productId = json['product_id'] as String
    ..price = json['price'] as num
    ..sku = json['sku'] as String
    ..description = json['description'] == null ? null : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..special = json['special'] == null ? null : Special.fromJson(json['special'] as Map<String, dynamic>)
    ..manufacturer = json['manufacturer'] == null ? null : Manufacture.fromJson(json['manufacturer'] as Map<String, dynamic>)
    ..customerPrice = json['customerPrice'] == null ? null : CustomerPrice.fromJson(json['customerPrice'] as Map<String, dynamic>)
    ..avlColorCount = json['avlColorCount'] as num
    ..image = json['image'] as String;
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'product_id': instance.productId,
      'sku': instance.sku,
      'description': instance.description,
      'image': instance.image,
      'manufacture': instance.manufacturer,
      'special': instance.special,
      'avlColorCount': instance.avlColorCount,
      'customerPrice': instance.customerPrice,
      'price': instance.price,
    };
