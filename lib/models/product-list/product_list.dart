import 'package:json_annotation/json_annotation.dart';

import "product.dart";

part 'product_list.g.dart';

@JsonSerializable()
class ProductList {
  bool error = false;
  String message = '';
  ProductList();
  num total;
  List<Product> products;

  factory ProductList.fromJson(Map<String, dynamic> json) => _$ProductListFromJson(json);
  Map<String, dynamic> toJson() => _$ProductListToJson(this);
}
