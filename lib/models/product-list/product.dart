import 'package:gocomartapp/models/cart/customerPrice.dart';
import 'package:gocomartapp/models/product_detail/special.dart';
import 'package:gocomartapp/models/shopping_home/manufacture.dart';
import 'package:json_annotation/json_annotation.dart';

import "description.dart";

part 'product.g.dart';

@JsonSerializable()
class Product {
  Product();

  String productId;
  String sku;
  num price;
  Description description;
  String image;
  Manufacture manufacturer;
  Special special;
  num avlColorCount;
  CustomerPrice customerPrice;

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
