// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address()
    ..address_id = json['address_id'] as num
    ..customer_id = json['customer_id'] as num
    ..firstName = json['firstname'] as String
    ..lastName = json['lastname'] as String
    ..company = json['company'] as String
    ..address_1 = json['address_1'] as String
    ..address_2 = json['address_2'] as String
    ..city = json['city'] as String
    ..postcode = json['postcode'] as num
    ..postalCodeId = json['postal_code_id'] as num
    ..countryId = json['country_id'] as num
    ..zoneId = json['zone_id'] as num
    ..customField = json['custom_field'] as String
    ..type = json['type'] as String
    ..landmark = json['landmark'] as String
    ..name = json['name'] as String
    ..mobileNumber = json['mobile_number'] as String
    ..altContact = json['alt_contact'] as String
    ..postalZone = json['postalZone'] == null ? null : PostalZone.fromJson(json['postalZone'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'address_id': instance.address_id,
      'customer_id': instance.customer_id,
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'company': instance.company,
      'address_1': instance.address_1,
      'address_2': instance.address_2,
      'city': instance.city,
      'postcode': instance.postcode,
      'postal_code_id': instance.postalCodeId,
      'country_id': instance.countryId,
      'zone_id': instance.zoneId,
      'custom_field': instance.customField,
      'type': instance.type,
      'landmark': instance.landmark,
      'name': instance.name,
      'mobile_number': instance.mobileNumber,
      'alt_contact': instance.altContact,
      'postalZone': instance.postalZone
    };
