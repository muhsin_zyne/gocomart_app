import 'package:json_annotation/json_annotation.dart';

import 'postalZone.dart';

part 'address.g.dart';

@JsonSerializable()
class Address {
  Address();

  // ignore: non_constant_identifier_names
  num address_id;
  // ignore: non_constant_identifier_names
  num customer_id;
  String firstName;
  String lastName;
  String company;
  String address_1;
  String address_2;
  String city;
  num postcode;
  num postalCodeId;
  num countryId;
  num zoneId;
  String customField;
  String type;
  String landmark;
  String name;
  String mobileNumber;
  String altContact;
  PostalZone postalZone;

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);
  Map<String, dynamic> toJson() => _$AddressToJson(this);
}
