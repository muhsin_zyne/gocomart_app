// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'postalZone.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostalZone _$PostalZoneFromJson(Map<String, dynamic> json) {
  return PostalZone()
    ..adminName1 = json['admin_name1'] as String
    ..country = json['country'] == null ? null : Country.fromJson(json['country'] as Map<String, dynamic>);
}

Map<String, dynamic> _$PostalZoneToJson(PostalZone instance) => <String, dynamic>{'admin_name1': instance.adminName1, 'country': instance.country};
