// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAddress _$GetAddressFromJson(Map<String, dynamic> json) {
  return GetAddress()
    ..address = (json['address'] as List)
        ?.map((e) =>
            e == null ? null : Address.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetAddressToJson(GetAddress instance) =>
    <String, dynamic>{'address': instance.address};
