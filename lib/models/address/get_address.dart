import 'package:json_annotation/json_annotation.dart';
import "address.dart";
part 'get_address.g.dart';

@JsonSerializable()
class GetAddress {
    GetAddress();

    List<Address> address;
    
    factory GetAddress.fromJson(Map<String,dynamic> json) => _$GetAddressFromJson(json);
    Map<String, dynamic> toJson() => _$GetAddressToJson(this);
}
