// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Country _$CountryFromJson(Map<String, dynamic> json) {
  return Country()..countryName = json['country_name'] as String;
}

Map<String, dynamic> _$CountryToJson(Country instance) => <String, dynamic>{'country_name': instance.countryName};
