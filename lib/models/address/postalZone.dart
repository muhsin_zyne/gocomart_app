import 'package:json_annotation/json_annotation.dart';

import 'country.dart';

part 'postalZone.g.dart';

@JsonSerializable()
class PostalZone {
  PostalZone();

  String adminName1;
  Country country;

  factory PostalZone.fromJson(Map<String, dynamic> json) => _$PostalZoneFromJson(json);
  Map<String, dynamic> toJson() => _$PostalZoneToJson(this);
}
