// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validate_pin_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ValidatePinCode _$ValidatePinCodeFromJson(Map<String, dynamic> json) {
  return ValidatePinCode()
    ..success = json['success'] as String
    ..error = json['error'] as String
    ..postals = (json['postals'] as List)?.map((e) => e == null ? null : Postals.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$ValidatePinCodeToJson(ValidatePinCode instance) =>
    <String, dynamic>{'success': instance.success, 'error': instance.error, 'postals': instance.postals};
