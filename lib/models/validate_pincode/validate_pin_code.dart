import 'package:json_annotation/json_annotation.dart';

import "postals.dart";

part 'validate_pin_code.g.dart';

@JsonSerializable()
class ValidatePinCode {
  ValidatePinCode();

  String success;
  String error;
  List<Postals> postals;

  factory ValidatePinCode.fromJson(Map<String, dynamic> json) => _$ValidatePinCodeFromJson(json);
  Map<String, dynamic> toJson() => _$ValidatePinCodeToJson(this);
}
