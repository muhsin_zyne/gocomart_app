// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'postals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Postals _$PostalsFromJson(Map<String, dynamic> json) {
  return Postals()
    ..id = json['id'] as num
    ..countryCode = json['country_code'] as String
    ..postalCode = json['postal_code'] as String
    ..placeName = json['place_name'] as String
    ..adminName1 = json['admin_name1'] as String
    ..adminName2 = json['admin_name2'] as String
    ..adminName3 = json['admin_name3'] as String
    ..country = json['country'] == null ? null : Country.fromJson(json['country'] as Map<String, dynamic>);
}

Map<String, dynamic> _$PostalsToJson(Postals instance) => <String, dynamic>{
      'id': instance.id,
      'country_code': instance.countryCode,
      'postal_code': instance.postalCode,
      'place_name': instance.placeName,
      'admin_name1': instance.adminName1,
      'admin_name2': instance.adminName2,
      'admin_name3': instance.adminName3,
      'country': instance.country
    };
