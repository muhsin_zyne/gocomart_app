import 'package:json_annotation/json_annotation.dart';

import "country.dart";

part 'postals.g.dart';

@JsonSerializable()
class Postals {
  Postals();

  num id;
  String countryCode;
  String postalCode;
  String placeName;
  String adminName1;
  String adminName2;
  String adminName3;
  Country country;

  factory Postals.fromJson(Map<String, dynamic> json) => _$PostalsFromJson(json);
  Map<String, dynamic> toJson() => _$PostalsToJson(this);
}
