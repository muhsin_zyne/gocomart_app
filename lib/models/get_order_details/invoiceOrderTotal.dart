class InvoiceOrderTotal {
  String title;
  num value;
  String code;

  InvoiceOrderTotal.fromJSON(Map<String, dynamic> json) {
    this.title = json['title'] as String;
    this.value = json['value'] as num;
    this.code = json['code'] as String;
  }
}
