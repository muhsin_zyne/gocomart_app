//
//  ProcessingProduct.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on October 21, 2020

import 'option.dart';

class ProcessingProduct {
  String model;
  String name;
  int orderProductId;
  String image;
  List<Option> option;
  num price;
  num quantity;
  ProcessingProduct.fromJSON(Map<String, dynamic> json) {
    this.model = json['model'];
    this.name = json['name'];
    this.image = json['image'];
    this.orderProductId = json['order_product_id'];
    this.price = json['price'] as num;
    this.quantity = json['quantity'] as num;
    this.option = (json['option'] as List)
        .map((e) =>
            e == null ? null : Option.fromJSON(e as Map<String, dynamic>))
        ?.toList();
  }
}
