import 'orderAddress.dart';
import 'ProcessingProduct.dart';
import 'ShipmentGroupProduct.dart';
import 'invoiceOrderTotal.dart';

class Order {
  String orderId;
  List<ProcessingProduct> processingProducts;
  List<ShipmentGroupProduct> shipmentGroupProducts;
  String dateAdded;
  int invoiceNo;
  String invoicePrefix;
  String invoicePdf;
  List<InvoiceOrderTotal> invoiceOrderTotal;
  OrderAddress orderAddress;
  Order.fromJSON(Map<String, dynamic> json) {
    this.orderId = json['order_id'];
    this.invoicePdf = json['invoicePdf'];
    this.dateAdded = json['date_added'];
    this.invoiceNo = json['invoice_no'];
    this.invoicePrefix = json['invoice_prefix'];
    this.processingProducts = (json['processingProducts'] as List)
        ?.map((e) => e == null
            ? null
            : ProcessingProduct.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.shipmentGroupProducts = (json['shipmentGroupProducts'] as List)
        .map((e) => e == null
            ? null
            : ShipmentGroupProduct.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.orderAddress = json['orderAddress'] == null
        ? null
        : OrderAddress.fromJSON(json['orderAddress']);
    this.invoiceOrderTotal = (json['invoiceOrderTotal'] as List)
        ?.map((e) => e == null
            ? null
            : InvoiceOrderTotal.fromJSON(e as Map<String, dynamic>))
        ?.toList();
  }
}
