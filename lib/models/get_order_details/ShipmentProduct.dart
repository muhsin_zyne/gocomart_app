//
//  ShipmentProduct.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on October 21, 2020

import 'OrderProduct.dart';

class ShipmentProduct {
  int id;
  int orderProductId;
  int orderShipmentId;
  OrderProduct orderProduct;
  int productId;
  int quantity;

  ShipmentProduct.fromJSON(Map<String, dynamic> json) {
    this.id = json['id'];
    this.orderProductId = json['order_product_id'];
    this.orderShipmentId = json['order_shipment_id'];
    this.orderProduct = json['orderProduct'] == null ? null : OrderProduct.fromJSON(json['orderProduct']);
    this.productId = json['product_id'];
    this.quantity = json['quantity'];
  }
}
