import 'logs.dart';

class OrderShipmentHistory {
  String name;
  String time;
  String code;
  List<Logs> logs;

  OrderShipmentHistory.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'];
    this.time = json['time'];
    this.code = json['code'];
    this.logs = (json['logs'] as List)
        ?.map(
            (e) => e == null ? null : Logs.fromJSON(e as Map<String, dynamic>))
        ?.toList();
  }
}
