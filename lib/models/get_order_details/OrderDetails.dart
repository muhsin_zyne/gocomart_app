//
//  OrderDetails.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on October 21, 2020

import 'Order.dart';

class OrderDetails {
  Order order;

  OrderDetails.fromJSON(Map<String, dynamic> json) {
    this.order = json['order'] == null ? null : Order.fromJSON(json['order']);
  }
}
