class Logs {
  String log;
  String time;

  Logs.fromJSON(Map<String, dynamic> json) {
    this.log = json['log'];
    this.time = json['time'];
  }
}
