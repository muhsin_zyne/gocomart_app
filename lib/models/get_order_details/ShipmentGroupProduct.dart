//
//  ShipmentGroupProduct.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on October 21, 2020

import 'order_shipment_history.dart';

import 'ShipmentProduct.dart';

class ShipmentGroupProduct {
  String gocoShipmentCode;
  int orderShipmentId;
  int processRequestId;
  List<ShipmentProduct> shipmentProducts;
  int shippingCourierId;
  String trackingId;
  List<OrderShipmentHistory> orderShipmentHistory;

  ShipmentGroupProduct.fromJSON(Map<String, dynamic> json) {
    this.gocoShipmentCode = json['goco_shipment_code'];
    this.orderShipmentId = json['order_shipment_id'];
    this.processRequestId = json['process_request_id'];
    this.orderShipmentHistory = (json['orderShipmentHistroy'] as List)
        ?.map((e) => e == null
            ? null
            : OrderShipmentHistory.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.shipmentProducts = (json['shipmentProducts'] as List)
        ?.map((e) => e == null
            ? null
            : ShipmentProduct.fromJSON(e as Map<String, dynamic>))
        ?.toList();
    this.shippingCourierId = json['shipping_courier_id'];
    this.trackingId = json['tracking_id'];
  }
}
