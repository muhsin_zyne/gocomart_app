class OrderAddress {
  String shippingName;
  String shippingAddress_1;
  String shippingAddress_2;
  String shippingCity;
  String shippingMobileNumber;
  String shippingPostcode;
  OrderAddress.fromJSON(Map<String, dynamic> json) {
    this.shippingName = json['shipping_name'];
    this.shippingAddress_1 = json['shipping_address_1'];
    this.shippingAddress_2 = json['shipping_address_2'];
    this.shippingCity = json['shipping_city'];
    this.shippingMobileNumber = json['shipping_mobile_number'];
    this.shippingPostcode = json['shipping_postcode'];
  }
}
