import 'option.dart';

class OrderProduct {
  String estDelivery;
  String model;
  String name;
  String image;
  int orderId;
  int orderProductId;
  num price;
  int quantity;
  List<Option> option;

  OrderProduct.fromJSON(Map<String, dynamic> json) {
    this.estDelivery = json['est_delivery'];
    this.model = json['model'];
    this.name = json['name'];
    this.image = json['image'];
    this.orderId = json['order_id'];
    this.orderProductId = json['order_product_id'];
    this.price = json['price'] as num;
    this.quantity = json['quantity'];
    this.option = (json['option'] as List)
        .map((e) =>
            e == null ? null : Option.fromJSON(e as Map<String, dynamic>))
        ?.toList();
  }
}
