// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wishlist_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WishlistStatus _$WishlistStatusFromJson(Map<String, dynamic> json) {
  return WishlistStatus()
    ..isTrue = json['true'] as bool
    ..isFalse = json['false'] as bool;
}

Map<String, dynamic> _$WishlistStatusToJson(WishlistStatus instance) =>
    <String, dynamic>{'true': instance.isTrue, 'false': instance.isFalse};
