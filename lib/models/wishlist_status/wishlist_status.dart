import 'package:json_annotation/json_annotation.dart';

part 'wishlist_status.g.dart';

@JsonSerializable()
class WishlistStatus {
    WishlistStatus();

    bool isTrue;
    bool isFalse;
    
    factory WishlistStatus.fromJson(Map<String,dynamic> json) => _$WishlistStatusFromJson(json);
    Map<String, dynamic> toJson() => _$WishlistStatusToJson(this);
}
