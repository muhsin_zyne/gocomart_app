// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse()
    ..message = json['message'] as String
    ..otp = json['otp'] as num
    ..error = json['error'] as bool;
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) => <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
      'otp': instance.otp,
    };
