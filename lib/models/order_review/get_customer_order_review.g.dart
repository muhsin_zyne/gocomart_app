// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_customer_order_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

// ignore: non_constant_identifier_names
GetCustomerOrderReview _$GetCustomerOrderReviewFromJson(Map<String, dynamic> json) {
  return GetCustomerOrderReview()..orders = (json['orders'] as List)?.map((e) => e == null ? null : Orders.fromJson(e as Map<String, dynamic>))?.toList();
}

// ignore: non_constant_identifier_names
Map<String, dynamic> _$GetCustomerOrderReviewToJson(GetCustomerOrderReview instance) => <String, dynamic>{'orders': instance.orders};
