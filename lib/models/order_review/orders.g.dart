// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Orders _$OrdersFromJson(Map<String, dynamic> json) {
  return Orders()
    ..order_product_id = json['order_product_id'] as num
    ..order_id = json['order_id'] as num
    ..order_status_id = json['order_status_id'] as num
    ..customer_id = json['customer_id'] as num
    ..product_id = json['product_id'] as num
    ..name = json['name'] as String
    ..image = json['image'] as String
    ..last_order_history_time = json['last_order_history_time'] as String
    ..rating = json['rating'] as num
    ..review_id = json['review_id'];
}

Map<String, dynamic> _$OrdersToJson(Orders instance) => <String, dynamic>{
      'order_product_id': instance.order_product_id,
      'order_id': instance.order_id,
      'order_status_id': instance.order_status_id,
      'customer_id': instance.customer_id,
      'product_id': instance.product_id,
      'name': instance.name,
      'image': instance.image,
      'last_order_history_time': instance.last_order_history_time,
      'rating': instance.rating,
      'review_id': instance.review_id
    };
