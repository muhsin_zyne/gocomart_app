import 'package:json_annotation/json_annotation.dart';

part 'orders.g.dart';

@JsonSerializable()
class Orders {
  Orders();

  // ignore: non_constant_identifier_names
  num order_product_id;
  // ignore: non_constant_identifier_names
  num order_id;
  // ignore: non_constant_identifier_names
  num order_status_id;
  // ignore: non_constant_identifier_names
  num customer_id;
  // ignore: non_constant_identifier_names
  num product_id;
  String name;
  String image;
  // ignore: non_constant_identifier_names
  String last_order_history_time;
  num rating;
  // ignore: non_constant_identifier_names
  dynamic review_id;

  factory Orders.fromJson(Map<String, dynamic> json) => _$OrdersFromJson(json);
  Map<String, dynamic> toJson() => _$OrdersToJson(this);
}
