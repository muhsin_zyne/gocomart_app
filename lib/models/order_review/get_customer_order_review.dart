import 'package:json_annotation/json_annotation.dart';
import "orders.dart";
part 'get_customer_order_review.g.dart';

@JsonSerializable()
// ignore: camel_case_types
class GetCustomerOrderReview {
  GetCustomerOrderReview();

  List<Orders> orders;

  factory GetCustomerOrderReview.fromJson(Map<String, dynamic> json) => _$GetCustomerOrderReviewFromJson(json);
  Map<String, dynamic> toJson() => _$GetCustomerOrderReviewToJson(this);
}
