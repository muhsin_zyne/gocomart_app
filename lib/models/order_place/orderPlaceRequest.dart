import 'package:json_annotation/json_annotation.dart';
import "orderInfo.dart";
part 'orderPlaceRequest.g.dart';

@JsonSerializable()
class OrderPlaceRequest {
    OrderPlaceRequest();

    OrderInfo orderInfo;
    
    factory OrderPlaceRequest.fromJson(Map<String,dynamic> json) => _$OrderPlaceRequestFromJson(json);
    Map<String, dynamic> toJson() => _$OrderPlaceRequestToJson(this);
}
