// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orderInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderInfo _$OrderInfoFromJson(Map<String, dynamic> json) {
  return OrderInfo()
    ..order_id = json['order_id'] as String
    ..invoice_no = json['invoice_no'] as num
    ..status = json['status'] == null
        ? null
        : Status.fromJson(json['status'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrderInfoToJson(OrderInfo instance) => <String, dynamic>{
      'order_id': instance.order_id,
      'invoice_no': instance.invoice_no,
      'status': instance.status
    };
