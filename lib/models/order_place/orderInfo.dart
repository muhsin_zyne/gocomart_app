import 'package:json_annotation/json_annotation.dart';
import "status.dart";
part 'orderInfo.g.dart';

@JsonSerializable()
class OrderInfo {
  OrderInfo();
  // ignore: non_constant_identifier_names
  String order_id;
  // ignore: non_constant_identifier_names
  num invoice_no;
  Status status;

  factory OrderInfo.fromJson(Map<String, dynamic> json) =>
      _$OrderInfoFromJson(json);
  Map<String, dynamic> toJson() => _$OrderInfoToJson(this);
}
