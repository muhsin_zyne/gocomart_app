// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orderPlaceRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderPlaceRequest _$OrderPlaceRequestFromJson(Map<String, dynamic> json) {
  return OrderPlaceRequest()
    ..orderInfo = json['orderInfo'] == null
        ? null
        : OrderInfo.fromJson(json['orderInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrderPlaceRequestToJson(OrderPlaceRequest instance) =>
    <String, dynamic>{'orderInfo': instance.orderInfo};
