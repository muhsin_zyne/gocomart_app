// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_validation_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OtpValidationResponse _$OtpValidationResponseFromJson(Map<String, dynamic> json) {
  return OtpValidationResponse()
    ..auth_token = json['auth_token'] as String
    ..refreshToken = json['refresh_token'] as String
    ..testName = json['testName'] as String
    ..user = json['user'] == null ? null : User.fromJson(json['user'] as Map<String, dynamic>)
    ..error = json['error'] as bool
    ..message = json['message'] as String;
}

Map<String, dynamic> _$OtpValidationResponseToJson(OtpValidationResponse instance) => <String, dynamic>{
      'auth_token': instance.auth_token,
      'refresh_token': instance.refreshToken,
      'testName': instance.testName,
      'user': instance.user,
      'error': instance.error,
      'message': instance.message
    };
