//
//  PushNotificationData.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 5, 2021

import 'package:gocomartapp/models/core/Route.dart';

class PushNotificationData {
  Route route;
  String type;

  PushNotificationData();

  PushNotificationData.fromJSON(Map<String, dynamic> parsedJson) {
    this.route = parsedJson['route'] == null ? null : Route.fromJSON(parsedJson['route']);
    this.type = parsedJson['type'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'route': this.route,
        'type': this.type,
      };
}
