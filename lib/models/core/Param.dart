//
//  Param.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 5, 2021

class Params {
  String couponCode;
  int id;
  int productId;
  String orderRequestGUID;
  Params();
  Params.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.couponCode = parsedJson['coupon_code'];
    this.id = parsedJson['id'];
    this.productId = parsedJson['product_id'];
    this.orderRequestGUID = parsedJson['order_request_guid'];
  }

  dynamic getArguments() {
    var response = {};
    if (this.couponCode != null) {
      response['coupon_code'] = this.couponCode;
    }
    if (this.orderRequestGUID != null) {
      response['orderRequestGUID'] = this.orderRequestGUID;
    }

    if (this.productId != null) {
      response['productId'] = this.productId;
    }

    if (this.id != null) {
      response['id'] = this.id;
    }

    return response;
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'coupon_code': this.couponCode,
        'id': this.id,
        'product_id': this.productId,
        'orderRequestGUID': this.orderRequestGUID,
      };
}
