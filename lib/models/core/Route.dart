//
//  Route.dart
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 5, 2021

import 'package:gocomartapp/models/core/Param.dart';

class Route {
  Params params;
  String screenId;
  Route();

  Route.fromJSON(Map<dynamic, dynamic> parsedJson) {
    //print(parsedJson);
    this.params = parsedJson['params'] == null ? null : Params.fromJSON(parsedJson['params']);
    this.screenId = parsedJson['screen_id'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'params': this.params,
        'screen_id': this.screenId,
      };
}
