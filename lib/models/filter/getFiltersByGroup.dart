import 'package:json_annotation/json_annotation.dart';
import "filterGroup.dart";
part 'getFiltersByGroup.g.dart';

@JsonSerializable()
class GetFiltersByGroup {
    GetFiltersByGroup();

    List<FilterGroup> filterGroups;
    
    factory GetFiltersByGroup.fromJson(Map<String,dynamic> json) => _$GetFiltersByGroupFromJson(json);
    Map<String, dynamic> toJson() => _$GetFiltersByGroupToJson(this);
}
