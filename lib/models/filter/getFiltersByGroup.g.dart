// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getFiltersByGroup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetFiltersByGroup _$GetFiltersByGroupFromJson(Map<String, dynamic> json) {
  return GetFiltersByGroup()
    ..filterGroups = (json['filterGroups'] as List)
        ?.map((e) =>
            e == null ? null : FilterGroup.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetFiltersByGroupToJson(GetFiltersByGroup instance) =>
    <String, dynamic>{'filterGroups': instance.filterGroups};
