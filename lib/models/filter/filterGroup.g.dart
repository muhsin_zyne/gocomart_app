// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filterGroup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterGroup _$FilterGroupFromJson(Map<String, dynamic> json) {
  return FilterGroup()
    ..filterGroupId = json['filter_group_id'] as String
    ..sortOrder = json['sort_order'] as num
    ..description = json['description'] == null ? null : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..filters = (json['filters'] as List)?.map((e) => e == null ? null : Filter.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$FilterGroupToJson(FilterGroup instance) => <String, dynamic>{
      'filter_group_id': instance.filterGroupId,
      'sort_order': instance.sortOrder,
      'description': instance.description,
      'filters': instance.filters
    };
