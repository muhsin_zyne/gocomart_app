import 'package:json_annotation/json_annotation.dart';

import "description.dart";
import "filter.dart";

part 'filterGroup.g.dart';

@JsonSerializable()
class FilterGroup {
  FilterGroup();

  String filterGroupId;
  num sortOrder;
  Description description;
  List<Filter> filters;

  factory FilterGroup.fromJson(Map<String, dynamic> json) => _$FilterGroupFromJson(json);
  Map<String, dynamic> toJson() => _$FilterGroupToJson(this);
}
