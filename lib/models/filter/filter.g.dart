// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Filter _$FilterFromJson(Map<String, dynamic> json) {
  return Filter()
    ..filterId = json['filter_id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$FilterToJson(Filter instance) => <String, dynamic>{
      'filter_id': instance.filterId,
      'name': instance.name,
      'value': instance.value,
    };
