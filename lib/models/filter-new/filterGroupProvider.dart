import 'package:json_annotation/json_annotation.dart';
import "filterGroup.dart";
import "filter.dart";
part 'filterGroupProvider.g.dart';

@JsonSerializable()
class FilterGroupProvider {
    FilterGroupProvider();

    List<FilterGroup> filterGroups;
    List<Filter> filters;
    
    factory FilterGroupProvider.fromJson(Map<String,dynamic> json) => _$FilterGroupProviderFromJson(json);
    Map<String, dynamic> toJson() => _$FilterGroupProviderToJson(this);
}
