// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filterGroupProvider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterGroupProvider _$FilterGroupProviderFromJson(Map<String, dynamic> json) {
  return FilterGroupProvider()
    ..filterGroups = (json['filterGroups'] as List)
        ?.map((e) =>
            e == null ? null : FilterGroup.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..filters = (json['filters'] as List)
        ?.map((e) =>
            e == null ? null : Filter.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$FilterGroupProviderToJson(
        FilterGroupProvider instance) =>
    <String, dynamic>{
      'filterGroups': instance.filterGroups,
      'filters': instance.filters
    };
