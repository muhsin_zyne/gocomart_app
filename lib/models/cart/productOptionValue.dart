import 'package:json_annotation/json_annotation.dart';
import "optionDtl.dart";
import "optionValueDtl.dart";
part 'productOptionValue.g.dart';

@JsonSerializable()
class ProductOptionValue {
    ProductOptionValue();

    OptionDtl productOption;
    OptionValueDtl productOptionValue;
    
    factory ProductOptionValue.fromJson(Map<String,dynamic> json) => _$ProductOptionValueFromJson(json);
    Map<String, dynamic> toJson() => _$ProductOptionValueToJson(this);
}
