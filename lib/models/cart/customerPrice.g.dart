// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customerPrice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerPrice _$CustomerPriceFromJson(Map<String, dynamic> json) {
  return CustomerPrice()
    ..offer_price = json['offer_price'] as num
    ..is_flexible_option = json['is_flexible_option'] as bool
    ..valid_to = json['valid_to'] as String
    ..max_quantity = json['max_quantity'] as num;
}

Map<String, dynamic> _$CustomerPriceToJson(CustomerPrice instance) =>
    <String, dynamic>{
      'offer_price': instance.offer_price,
      'is_flexible_option': instance.is_flexible_option,
      'valid_to': instance.valid_to,
      'max_quantity': instance.max_quantity
    };
