import 'package:json_annotation/json_annotation.dart';
import "priceInfo.dart";
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'package:gocomartapp/models/cart/productOptionValue.dart';
part 'buyNowItem.g.dart';

@JsonSerializable()
class BuyNowItem {
  BuyNowItem();

  // ignore: non_constant_identifier_names
  num product_id;
  String cartImage;
  num quantity;
  PriceInfo priceInfo;

  ProductDetail product;
  List<ProductOptionValue> productOptionsValues;

  // external variables

  num basePrice = 0;
  num offerAmount = 0;

  factory BuyNowItem.fromJson(Map<String, dynamic> json) => _$BuyNowItemFromJson(json);
  Map<String, dynamic> toJson() => _$BuyNowItemToJson(this);
}
