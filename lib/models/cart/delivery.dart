import 'package:json_annotation/json_annotation.dart';

part 'delivery.g.dart';

@JsonSerializable()
class Delivery {
    Delivery();

    num freeShippingCartTotal;
    
    factory Delivery.fromJson(Map<String,dynamic> json) => _$DeliveryFromJson(json);
    Map<String, dynamic> toJson() => _$DeliveryToJson(this);
}
