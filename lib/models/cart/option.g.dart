// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'option.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Option _$OptionFromJson(Map<String, dynamic> json) {
  return Option()
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OptionToJson(Option instance) =>
    <String, dynamic>{'description': instance.description};
