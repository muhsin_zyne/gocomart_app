// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cartResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartResponse _$CartResponseFromJson(Map<String, dynamic> json) {
  return CartResponse()
    ..items = (json['items'] as List)?.map((e) => e == null ? null : Item.fromJson(e as Map<String, dynamic>))?.toList()
    ..message = json['message'] as String
    ..error = json['error'] as bool;
}

Map<String, dynamic> _$CartResponseToJson(CartResponse instance) => <String, dynamic>{
      'items': instance.items,
      'message': instance.message,
      'error': instance.error,
    };
