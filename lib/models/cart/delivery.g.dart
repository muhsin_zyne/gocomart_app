// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delivery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Delivery _$DeliveryFromJson(Map<String, dynamic> json) {
  return Delivery()
    ..freeShippingCartTotal = json['freeShippingCartTotal'] as num;
}

Map<String, dynamic> _$DeliveryToJson(Delivery instance) =>
    <String, dynamic>{'freeShippingCartTotal': instance.freeShippingCartTotal};
