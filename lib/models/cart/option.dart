import 'package:json_annotation/json_annotation.dart';
import "description.dart";
part 'option.g.dart';

@JsonSerializable()
class Option {
    Option();

    Description description;
    
    factory Option.fromJson(Map<String,dynamic> json) => _$OptionFromJson(json);
    Map<String, dynamic> toJson() => _$OptionToJson(this);
}
