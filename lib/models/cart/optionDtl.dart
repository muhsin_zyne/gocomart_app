import 'package:json_annotation/json_annotation.dart';
import "option.dart";
part 'optionDtl.g.dart';

@JsonSerializable()
class OptionDtl {
  OptionDtl();
  // ignore: non_constant_identifier_names
  num product_option_id;
  Option option;

  factory OptionDtl.fromJson(Map<String, dynamic> json) => _$OptionDtlFromJson(json);
  Map<String, dynamic> toJson() => _$OptionDtlToJson(this);
}
