import 'package:json_annotation/json_annotation.dart';
import "description.dart";
part 'optionValueDtl.g.dart';

@JsonSerializable()
class OptionValueDtl {
  OptionValueDtl();
  // ignore: non_constant_identifier_names
  num product_option_value_id;
  Description description;
  // ignore: non_constant_identifier_names
  String price_prefix;
  num price;

  factory OptionValueDtl.fromJson(Map<String, dynamic> json) => _$OptionValueDtlFromJson(json);
  Map<String, dynamic> toJson() => _$OptionValueDtlToJson(this);
}
