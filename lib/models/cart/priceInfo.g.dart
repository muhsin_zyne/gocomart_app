// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'priceInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PriceInfo _$PriceInfoFromJson(Map<String, dynamic> json) {
  return PriceInfo()
    ..itemTotal = json['itemTotal'] as num
    ..hikeTotal = json['hikeTotal'] as num
    ..totalShippingCost = json['totalShippingCost'] as num
    ..tax = json['tax'] as num;
}

Map<String, dynamic> _$PriceInfoToJson(PriceInfo instance) => <String, dynamic>{
      'itemTotal': instance.itemTotal,
      'hikeTotal': instance.hikeTotal,
      'totalShippingCost': instance.totalShippingCost,
      'tax': instance.tax,
    };
