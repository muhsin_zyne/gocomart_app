import 'package:json_annotation/json_annotation.dart';

part 'priceInfo.g.dart';

@JsonSerializable()
class PriceInfo {
  PriceInfo();

  num itemTotal;
  num hikeTotal;
  num totalShippingCost;
  num tax;

  factory PriceInfo.fromJson(Map<String, dynamic> json) => _$PriceInfoFromJson(json);
  Map<String, dynamic> toJson() => _$PriceInfoToJson(this);
}
