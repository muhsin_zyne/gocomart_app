import 'package:json_annotation/json_annotation.dart';
import "productOptionValue.dart";
import 'package:gocomartapp/models/product_detail/product_detail.dart';
import 'customerPrice.dart';
part 'item.g.dart';

@JsonSerializable()
class Item {
  Item();
  // ignore: non_constant_identifier_names
  num cart_id;
  // ignore: non_constant_identifier_names
  num product_id;
  num quantity;
  String cartImage;
  CustomerPrice customerPrice;
  ProductDetail product;
  List<ProductOptionValue> productOptionsValues;

  // internal modal values

  bool isLoading = false;
  bool forceCustomerOffer = false;
  num basePrice = 0;
  num specialPrice = 0;
  num discountPercentage = 0;
  num hikePrice = 0;

  num itemTotal = 0;
  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);
  Map<String, dynamic> toJson() => _$ItemToJson(this);
}
