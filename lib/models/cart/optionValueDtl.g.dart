// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'optionValueDtl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionValueDtl _$OptionValueDtlFromJson(Map<String, dynamic> json) {
  return OptionValueDtl()
    ..product_option_value_id = json['product_option_value_id'] as num
    ..description = json['description'] == null
        ? null
        : Description.fromJson(json['description'] as Map<String, dynamic>)
    ..price_prefix = json['price_prefix'] as String
    ..price = json['price'] as num;
}

Map<String, dynamic> _$OptionValueDtlToJson(OptionValueDtl instance) =>
    <String, dynamic>{
      'product_option_value_id': instance.product_option_value_id,
      'description': instance.description,
      'price_prefix': instance.price_prefix,
      'price': instance.price
    };
