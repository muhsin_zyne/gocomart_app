// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'optionDtl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionDtl _$OptionDtlFromJson(Map<String, dynamic> json) {
  return OptionDtl()
    ..product_option_id = json['product_option_id'] as num
    ..option = json['option'] == null
        ? null
        : Option.fromJson(json['option'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OptionDtlToJson(OptionDtl instance) => <String, dynamic>{
      'product_option_id': instance.product_option_id,
      'option': instance.option
    };
