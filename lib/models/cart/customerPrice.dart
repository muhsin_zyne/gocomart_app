import 'package:json_annotation/json_annotation.dart';

part 'customerPrice.g.dart';

@JsonSerializable()
class CustomerPrice {
  CustomerPrice();

  // ignore: non_constant_identifier_names
  num offer_price;
  // ignore: non_constant_identifier_names
  bool is_flexible_option;
  // ignore: non_constant_identifier_names
  String valid_to;
  // ignore: non_constant_identifier_names
  num max_quantity;

  factory CustomerPrice.fromJson(Map<String, dynamic> json) =>
      _$CustomerPriceFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerPriceToJson(this);
}
