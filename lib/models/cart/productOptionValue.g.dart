// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productOptionValue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductOptionValue _$ProductOptionValueFromJson(Map<String, dynamic> json) {
  return ProductOptionValue()
    ..productOption = json['productOption'] == null
        ? null
        : OptionDtl.fromJson(json['productOption'] as Map<String, dynamic>)
    ..productOptionValue = json['productOptionValue'] == null
        ? null
        : OptionValueDtl.fromJson(
            json['productOptionValue'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductOptionValueToJson(ProductOptionValue instance) =>
    <String, dynamic>{
      'productOption': instance.productOption,
      'productOptionValue': instance.productOptionValue
    };
