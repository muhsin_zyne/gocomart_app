// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buyNowItem.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuyNowItem _$BuyNowItemFromJson(Map<String, dynamic> json) {
  return BuyNowItem()
    ..product_id = json['product_id'] as num
    ..cartImage = json['cartImage'] as String
    ..quantity = json['quantity'] as num
    ..priceInfo = json['priceInfo'] == null
        ? null
        : PriceInfo.fromJson(json['priceInfo'] as Map<String, dynamic>)
    ..product = json['product'] == null
        ? null
        : ProductDetail.fromJson(json['product'] as Map<String, dynamic>)
    ..productOptionsValues = (json['productOptionsValues'] as List)
        ?.map((e) => e == null
            ? null
            : ProductOptionValue.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$BuyNowItemToJson(BuyNowItem instance) =>
    <String, dynamic>{
      'product_id': instance.product_id,
      'cartImage': instance.cartImage,
      'quantity': instance.quantity,
      'priceInfo': instance.priceInfo,
      'product': instance.product,
      'productOptionsValues': instance.productOptionsValues,
      'basePrice': instance.basePrice,
      'offerAmount': instance.offerAmount
    };
