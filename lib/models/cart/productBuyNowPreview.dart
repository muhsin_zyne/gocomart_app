import 'package:json_annotation/json_annotation.dart';

import "buyNowItem.dart";
import "delivery.dart";

part 'productBuyNowPreview.g.dart';

@JsonSerializable()
class ProductBuyNowPreview {
  ProductBuyNowPreview();

  String message;
  bool error;
  List<BuyNowItem> items;
  Delivery delivery;

  num totalAmount = 0;
  num deliveryCharges = 0;
  num totalPayableAmount = 0;
  num freeDeliveryAdjustment = 0;
  num additionalTax = 0;

  factory ProductBuyNowPreview.fromJson(Map<String, dynamic> json) => _$SingleProductBuyNowFromJson(json);
  Map<String, dynamic> toJson() => _$SingleProductBuyNowToJson(this);
}
