// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  return Item()
    ..cart_id = json['cart_id'] as num
    ..product_id = json['product_id'] as num
    ..quantity = json['quantity'] as num
    ..cartImage = json['cartImage'] as String
    ..customerPrice = json['customerPrice'] == null
        ? null
        : CustomerPrice.fromJson(json['customerPrice'] as Map<String, dynamic>)
    ..product = json['product'] == null
        ? null
        : ProductDetail.fromJson(json['product'] as Map<String, dynamic>)
    ..productOptionsValues = (json['productOptionsValues'] as List)
        ?.map((e) => e == null
            ? null
            : ProductOptionValue.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'cart_id': instance.cart_id,
      'product_id': instance.product_id,
      'quantity': instance.quantity,
      'cartImage': instance.cartImage,
      'customerPrice': instance.customerPrice,
      'product': instance.product,
      'productOptionsValues': instance.productOptionsValues,
      'isLoading': instance.isLoading,
      'itemTotal': instance.itemTotal,
      'basePrice': instance.basePrice,
      'specialPrice': instance.specialPrice,
      'discountPercentage': instance.discountPercentage,
      'hikePrice': instance.hikePrice,
      'forceCustomerOffer': instance.forceCustomerOffer
    };
