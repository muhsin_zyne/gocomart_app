import 'package:json_annotation/json_annotation.dart';

import "item.dart";

part 'cartResponse.g.dart';

@JsonSerializable()
class CartResponse {
  CartResponse();

  List<Item> items;
  String message;
  bool error;
  factory CartResponse.fromJson(Map<String, dynamic> json) => _$CartResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CartResponseToJson(this);
}
