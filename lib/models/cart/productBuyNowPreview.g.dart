// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productBuyNowPreview.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductBuyNowPreview _$SingleProductBuyNowFromJson(Map<String, dynamic> json) {
  return ProductBuyNowPreview()
    ..message = json['message'] as String
    ..error = json['error'] as bool
    ..items = (json['items'] as List)?.map((e) => e == null ? null : BuyNowItem.fromJson(e as Map<String, dynamic>))?.toList()
    ..delivery = json['delivery'] == null ? null : Delivery.fromJson(json['delivery'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SingleProductBuyNowToJson(ProductBuyNowPreview instance) => <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
      'items': instance.items,
      'delivery': instance.delivery,
      'totalPayableAmount': instance.totalPayableAmount,
      'deliveryCharges': instance.deliveryCharges,
      'totalAmount': instance.totalAmount,
      'freeDeliveryAdjustment': instance.freeDeliveryAdjustment,
      'additionalTax': instance.additionalTax,
    };
