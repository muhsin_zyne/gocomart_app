import 'package:json_annotation/json_annotation.dart';

import 'links.dart';

part 'get_profile_app_links.g.dart';

@JsonSerializable()
class GetProfileAppLinks {
  GetProfileAppLinks();

  String error;
  String message;
  List<Links> links;

  factory GetProfileAppLinks.fromJson(Map<String, dynamic> json) => _$GetProfileAppLinksFromJson(json);
  Map<String, dynamic> toJson() => _$GetProfileAppLinksToJson(this);
}
