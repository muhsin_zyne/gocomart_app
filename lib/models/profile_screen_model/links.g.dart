// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'links.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Links _$LinksFromJson(Map<String, dynamic> json) {
  return Links()
    ..id = json['id'] as num
    ..title = json['title'] as String
    ..link = json['link'] as String
    ..icon = json['icon'] as String
    ..description = json['description'] as String
    ..primaryColor = json['primary_color'] as String
    ..secondaryColor = json['secondary_color'] as String;
}

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'link': instance.link,
      'icon': instance.icon,
      'description': instance.description,
      'primary_color': instance.primaryColor,
      'secondary_color': instance.secondaryColor
    };
