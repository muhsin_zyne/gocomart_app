// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_profile_app_links.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProfileAppLinks _$GetProfileAppLinksFromJson(Map<String, dynamic> json) {
  return GetProfileAppLinks()
    ..error = json['error'] as String
    ..message = json['message'] as String
    ..links = (json['links'] as List)?.map((e) => e == null ? null : Links.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$GetProfileAppLinksToJson(GetProfileAppLinks instance) =>
    <String, dynamic>{'error': instance.error, 'message': instance.message, 'links': instance.links};
