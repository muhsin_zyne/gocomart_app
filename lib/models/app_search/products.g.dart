// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Products _$ProductsFromJson(Map<String, dynamic> json) {
  return Products()
    ..product_id = json['product_id'] as num
    ..name = json['name'] as String
    ..details = json['details'] == null
        ? null
        : ProductDetails.fromJson(json['details'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductsToJson(Products instance) => <String, dynamic>{
      'product_id': instance.product_id,
      'name': instance.name,
      'details': instance.details
    };
