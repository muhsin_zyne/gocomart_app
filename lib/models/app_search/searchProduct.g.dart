// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'searchProduct.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchProduct _$SearchProductFromJson(Map<String, dynamic> json) {
  return SearchProduct()
    ..category = (json['category'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..products = (json['products'] as List)
        ?.map((e) =>
            e == null ? null : Products.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SearchProductToJson(SearchProduct instance) =>
    <String, dynamic>{
      'category': instance.category,
      'products': instance.products
    };
