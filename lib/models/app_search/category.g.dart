// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category()
    ..category_id = json['category_id'] as num
    ..name = json['name'] as String
    ..getParentName = json['getParentName'] as String;
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'category_id': instance.category_id,
      'name': instance.name,
      'getParentName': instance.getParentName
    };
