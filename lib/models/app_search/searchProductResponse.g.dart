// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'searchProductResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchProductResponse _$SearchProductResponseFromJson(
    Map<String, dynamic> json) {
  return SearchProductResponse()
    ..searchProduct = json['searchProduct'] == null
        ? null
        : SearchProduct.fromJson(json['searchProduct'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SearchProductResponseToJson(
        SearchProductResponse instance) =>
    <String, dynamic>{'searchProduct': instance.searchProduct};
