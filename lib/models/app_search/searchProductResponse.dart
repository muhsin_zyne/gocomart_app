import 'package:json_annotation/json_annotation.dart';
import "searchProduct.dart";
part 'searchProductResponse.g.dart';

@JsonSerializable()
class SearchProductResponse {
    SearchProductResponse();

    SearchProduct searchProduct;
    
    factory SearchProductResponse.fromJson(Map<String,dynamic> json) => _$SearchProductResponseFromJson(json);
    Map<String, dynamic> toJson() => _$SearchProductResponseToJson(this);
}
