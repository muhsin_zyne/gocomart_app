// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'productDetails.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetails _$ProductDetailsFromJson(Map<String, dynamic> json) {
  return ProductDetails()..image = json['image'] as String;
}

Map<String, dynamic> _$ProductDetailsToJson(ProductDetails instance) =>
    <String, dynamic>{'image': instance.image};
