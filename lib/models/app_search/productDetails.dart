import 'package:json_annotation/json_annotation.dart';

part 'productDetails.g.dart';

@JsonSerializable()
class ProductDetails {
    ProductDetails();

    String image;
    
    factory ProductDetails.fromJson(Map<String,dynamic> json) => _$ProductDetailsFromJson(json);
    Map<String, dynamic> toJson() => _$ProductDetailsToJson(this);
}
