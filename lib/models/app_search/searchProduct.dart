import 'package:json_annotation/json_annotation.dart';
import "category.dart";
import "products.dart";
part 'searchProduct.g.dart';

@JsonSerializable()
class SearchProduct {
    SearchProduct();

    List<Category> category;
    List<Products> products;
    
    factory SearchProduct.fromJson(Map<String,dynamic> json) => _$SearchProductFromJson(json);
    Map<String, dynamic> toJson() => _$SearchProductToJson(this);
}
