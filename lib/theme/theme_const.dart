import 'package:flutter/material.dart';

const kThemePrimaryColor = Color(0xff3b5998);
const kCashWalletLabel = TextStyle(
  fontFamily: 'Product-Sans',
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
const kCashWalletSmallLabel = TextStyle(
  fontFamily: 'Product-Sans',
  //fontSize: 16.0,
  color: Colors.white,
);

const kToastMessage = TextStyle(
  fontFamily: 'Product-Sans',
  fontSize: 16.0,
  color: Colors.white,
);

const kGeneralText = TextStyle(
  fontFamily: 'Product-Sans',
  color: Colors.white,
);
const kScrachCardLabel = TextStyle(fontSize: 14.0, color: Colors.black);

const kRewardedBy = TextStyle(fontSize: 12);
const kRewardCompany = TextStyle(fontSize: 14);

const kOutOfStockLabelText = TextStyle(
  color: Colors.red,
);
