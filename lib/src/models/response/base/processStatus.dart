class ProcessStatus {
  bool error;
  String message;
  ProcessStatus();
  ProcessStatus.fromJSON(Map<String, dynamic> parsedJson) {
    this.error = parsedJson['error'];
    this.message = parsedJson['message'];
  }
}
