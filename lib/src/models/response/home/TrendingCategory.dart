class TrendingCategory {
  int categoryId;
  String name;

  TrendingCategory.fromJSON(Map<String, dynamic> parsedJson) {
    this.categoryId = parsedJson['category_id'];
    this.name = parsedJson['name'];
  }
}
