class AppHomeCategory {
  int categoryId;
  String image;
  String name;

  AppHomeCategory.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.categoryId = parsedJson['category_id'];
    this.image = parsedJson['image'];
    this.name = parsedJson['name'];
  }
}
