class Brand {
  String image;
  int manufacturerId;
  String name;

  Brand.fromJSON(Map<String, dynamic> parsedJson) {
    this.image = parsedJson['image'];
    this.manufacturerId = parsedJson['manufacturer_id'];
    this.name = parsedJson['name'];
  }
}
