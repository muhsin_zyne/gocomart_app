import 'package:gocomartapp/src/models/response/home/Brand.dart';

class TrendingBrands {
  List<Brand> brands;
  int total;
  TrendingBrands();
  TrendingBrands.fromJSON(Map<String, dynamic> parsedJson) {
    this.brands = (parsedJson['brands'] as List<dynamic>).map((e) => e == null ? null : Brand.fromJSON(e as Map<String, dynamic>))?.toList();
    this.total = parsedJson['total'];
  }
}
