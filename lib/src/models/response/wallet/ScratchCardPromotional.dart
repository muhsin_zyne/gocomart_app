class ScratchCardPromotional {
  ScPromotionalText scPromotionalText;

  ScratchCardPromotional();
  ScratchCardPromotional.fromJSON(Map<String, dynamic> parsedJson) {
    this.scPromotionalText = parsedJson['scPromotionalText'] == null ? null : ScPromotionalText.fromJSON(parsedJson['scPromotionalText']);
  }
}

class ScPromotionalText {
  String content;
  bool error;
  String message;
  ScPromotionalText();
  ScPromotionalText.fromJSON(Map<String, dynamic> parsedJson) {
    this.content = parsedJson['content'];
    this.error = parsedJson['error'];
  }
}
