import 'package:gocomartapp/models/profile/user.dart';
import 'package:gocomartapp/models/wallet/coinTransactionHistory.dart';
import 'package:gocomartapp/models/wallet/walletAppLinks.dart';
import 'package:gocomartapp/models/wallet_screen/transaction_history.dart';

class WalletAppLinksMixed {
  bool error = false;
  String message = '';
  CoinTransactionHistory shortCoinTransactionHistory;
  TransactionHistory shortTransactionHistory;
  WalletAppLinks walletAppLinks;
  User currentUser;
  num cashAmount;
  num currentCoin;

  WalletAppLinksMixed();

  WalletAppLinksMixed.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.walletAppLinks =
        parsedJson['getWalletAppLinks']['walletAppLinks'] == null ? null : WalletAppLinks.fromJson(parsedJson['getWalletAppLinks']['walletAppLinks']);
    this.shortTransactionHistory = parsedJson['transactionHistory'] == null ? null : TransactionHistory.fromJson(parsedJson['transactionHistory']);
    this.shortCoinTransactionHistory =
        parsedJson['coinTransactionHistory'] == null ? null : CoinTransactionHistory.fromJson(parsedJson['coinTransactionHistory']);
    this.currentUser = parsedJson['me']['user'] == null ? null : User.fromJson(parsedJson['me']['user']);
    this.cashAmount = parsedJson['currentBalance']['amount'] as num;
    this.currentCoin = parsedJson['currentCoinBalance']['coin'] as num;
  }
}
