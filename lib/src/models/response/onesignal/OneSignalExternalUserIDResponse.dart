import 'package:gocomartapp/src/models/response/onesignal/Push.dart';

class OneSignalExternalUserIDResponse {
  Push push;
  OneSignalExternalUserIDResponse();

  OneSignalExternalUserIDResponse.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.push = parsedJson['push'] == null ? null : Push.fromJSON(parsedJson['push']);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'push': this.push,
      };
}
