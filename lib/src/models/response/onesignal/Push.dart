class Push {
  bool success;
  Push();

  Push.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.success = parsedJson['success'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'success': this.success,
      };
}
