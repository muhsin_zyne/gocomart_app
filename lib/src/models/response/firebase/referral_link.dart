class ReferralLink {
  String code;
  ReferralLink();

  ReferralLink.fromJSON(Map<dynamic, dynamic> parsedJson) {
    this.code = parsedJson['code'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'code': this.code,
      };
}
