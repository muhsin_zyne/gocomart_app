class ClaimScratchCardRequest {
  num latitude;
  num longitude;
  String id;

  ClaimScratchCardRequest();

  Map<String, dynamic> toJson() => <String, dynamic>{
        'scratch_id': this.id,
        'latitude': this.latitude,
        'longitude': this.longitude,
      };
}
