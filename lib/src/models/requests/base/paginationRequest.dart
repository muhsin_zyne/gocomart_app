class PaginationRequest {
  int page;
  int limit;

  PaginationRequest();

  Map<String, dynamic> toJson() => <String, dynamic>{
        'page': this.page,
        'limit': this.limit,
      };
}
