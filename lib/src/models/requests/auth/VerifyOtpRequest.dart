class VerifyOtpRequest {
  int code;
  int mobile;

  VerifyOtpRequest();
  VerifyOtpRequest.fromJSON(Map<String, dynamic> parsedJson) {
    this.code = parsedJson['code'];
    this.mobile = parsedJson['mobile'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'code': this.code,
        'mobile': this.mobile,
      };
}
