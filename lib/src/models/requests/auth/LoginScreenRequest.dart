class LoginScreenRequest {
  String clientType;
  int mobile;
  String mobileCode;
  String signature;
  String uniqueId;
  String referral;
  LoginScreenRequest();

  LoginScreenRequest.fromJSON(Map<String, dynamic> parsedJson) {
    this.clientType = parsedJson['client_type'];
    this.mobile = parsedJson['mobile'];
    this.mobileCode = parsedJson['mobile_code'];
    this.signature = parsedJson['signature'];
    this.uniqueId = parsedJson['unique_id'];
    this.referral = parsedJson['referral'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'client_type': this.clientType,
        'mobile': this.mobile,
        'mobile_code': this.mobileCode,
        'signature': this.signature,
        'unique_id': this.uniqueId,
        'referral': this.referral,
      };
}
