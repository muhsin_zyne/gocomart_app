import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/wallet/WalletAppLinksMixed.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WalletHomeScreenPresenter {
  static const int TRANS_LIMIT = 5;
  static const int TRANS_PAGE = 1;
  WalletHomeScreenPresenter({this.context, this.view}) {
    _init();
  }
  BuildContext context;
  IWalletHomeScreenView view;
  DeviseInfo deviseInfo = new DeviseInfo();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  AccountServices _accountServices;
  GlobalProvider _globalProvider;
  AppServices _appServices;
  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _accountServices = AccountServices(context: context);
    _appServices = AppServices(context: context);
    _globalProvider = Provider.of<GlobalProvider>(context);
  }

  void getWalletAppLinksHandler() async {
    this._assignCachedContent();
    WalletAppLinksMixed mixed = await _appServices.getWalletAppLinksModel(transLimit: TRANS_LIMIT, transPage: TRANS_PAGE);
    _globalProvider.walletAppLinksMixed = mixed;
    this.view.onWalletHomeApiCallDone(mixed);
  }

  void _assignCachedContent() {
    try {
      if (_globalProvider?.walletAppLinksMixed?.walletAppLinks?.cashWallet?.isNotEmpty ?? false) {
        WalletAppLinksMixed mixed = _globalProvider.walletAppLinksMixed;
        this.view.onWalletHomeApiCallDone(mixed);
      }
    } on Exception catch (e) {
      print("this catch block triggered");
      //throw Exception();
    }
  }
}
