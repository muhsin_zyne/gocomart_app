import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/controllers/ui_notifications.dart';
import 'package:gocomartapp/main.dart';
import 'package:gocomartapp/models/wallet/cashWalletAppLink.dart';
import 'package:gocomartapp/models/wallet/coinTransaction.dart';
import 'package:gocomartapp/models/wallet/coinTransactionHistory.dart';
import 'package:gocomartapp/models/wallet/walletAppLinks.dart';
import 'package:gocomartapp/models/wallet_screen/transaction.dart';
import 'package:gocomartapp/models/wallet_screen/transaction_history.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/screens/wallet/wallet_transaction.dart';
import 'package:gocomartapp/screens/wallet/widgets/transactionTile.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/wallet/WalletAppLinksMixed.dart';
import 'package:gocomartapp/src/screens/wallet/wallet_home_presenter.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/util/enum.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:provider/provider.dart';
import 'package:skeleton_text/skeleton_text.dart';

import '../../../components/widgets/swipe_card.dart';
import '../../../providers/global_provider.dart';

class WalletHomeScreen extends StatelessWidget {
  static const String id = 'wallet_home_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          getTranslate('wallet'),
        ),
        elevation: 0.0,
      ),
      body: WalletScreenHomeContent(),
    );
  }
}

class WalletScreenHomeContent extends StatefulWidget {
  @override
  _WalletScreenHomeContentState createState() => _WalletScreenHomeContentState();
}

class _WalletScreenHomeContentState extends State<WalletScreenHomeContent> with TickerProviderStateMixin, BaseStatefulScreen implements IWalletHomeScreenView {
  GlobalProvider _globalProvider;
  CurrencyFormat inr = CurrencyConst.inrFormat1;
  WalletHomeScreenPresenter _presenter;
  AppServices _appServices;
  WalletAppLinks walletAppLinks;
  bool pageReady = false;
  GOCOWallet currentWallet = GOCOWallet.cashWallet;
  List<WalletAppLink> currentWalletAppLinks = [];

  TransactionHistory shortTransactionHistory;
  CoinTransactionHistory shortCoinTransactionHistory;
  ScrollController _cardScroll = ScrollController();
  AnimationController _cashBalanceAnimation;
  AnimationController _coinBalanceAnimation;

  double _screenWith;
  int _currentCard = 0;
  double cashBalance = 0.0;
  double coinBalance = 0.0;

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    _appServices = AppServices(context: context);
    _loadClient();
    super.initState();
  }

  _loadClient() async {
    await Future.delayed(Duration(microseconds: 500));
    _presenter = WalletHomeScreenPresenter(context: context, view: this);
    await Future.delayed(Duration(microseconds: 500));
    this._init();
    await _globalProvider.initGpsRequest(context);
    if ((_globalProvider.gpsServiceStatus != ServiceStatus.enabled) && (_globalProvider.gpsPermissionStatus != PermissionStatus.granted)) {
      locationDisabledException();
    }
  }

  cashBalanceAnimate({@required amount, from = 0.0}) {
    if (amount != 0 && amount > 0 && amount != from) {
      if (amount < from) {
        _cashBalanceAnimation = AnimationController(
          duration: Duration(milliseconds: 800),
          vsync: this,
          lowerBound: amount.toDouble(),
          upperBound: from,
        );
      } else {
        _cashBalanceAnimation = AnimationController(
          duration: Duration(milliseconds: 800),
          vsync: this,
          lowerBound: from,
          upperBound: amount.toDouble(),
        );
      }

      if (amount > from) {
        _cashBalanceAnimation.forward();
      } else {
        _cashBalanceAnimation.reverse(from: from);
      }
      _cashBalanceAnimation.addListener(() {
        setState(() {
          cashBalance = double.parse(_cashBalanceAnimation.value.toStringAsFixed(1));
        });
      });
    }
  }

  coinBalanceAnimation({@required coin, from = 0.0}) async {
    if (coin != 0 && coin > 0 && coin != from) {
      if (coin < from) {
        _coinBalanceAnimation = AnimationController(
          duration: Duration(milliseconds: 800),
          vsync: this,
          lowerBound: coin.toDouble(),
          upperBound: from,
        );
      } else {
        _coinBalanceAnimation = AnimationController(
          duration: Duration(milliseconds: 800),
          vsync: this,
          lowerBound: from,
          upperBound: coin.toDouble(),
        );
      }

      if (coin > from) {
        _coinBalanceAnimation.forward();
      } else {
        _coinBalanceAnimation.reverse(from: from);
      }
      _coinBalanceAnimation.addListener(() async {
        setState(() {
          coinBalance = double.parse(_coinBalanceAnimation.value.toStringAsFixed(1));
        });
      });
    }
  }

  locationDisabledException() async {
    UINotification.showInAppNotification(
        color: Colors.red, title: getTranslate('oops'), message: getTranslate('wallet_features_disabled_message'), duration: Duration(seconds: 5));
    HomeScreen.setPreviousTabAsSelected(context);
  }

  _init() async {
    this._getWalletAppLinks();
    this.setCardScrollFunction();
  }

  void setCardScrollFunction() async {
    await new Future.delayed(const Duration(microseconds: 100));
    _screenWith = MediaQuery.of(context).size.width;
    _cardScroll.addListener(
      () {
        if (_cardScroll.position.pixels > _screenWith * .8 - 150) {
          if (_currentCard == 0) {
            setState(() {
              currentWallet = GOCOWallet.coinWallet;
              currentWalletAppLinks = walletAppLinks.coinWallet;
              _currentCard = 1;
            });
          }
        }
        if (_currentCard == 1 && _cardScroll.position.pixels < (_screenWith * .8 / 10)) {
          if (_currentCard == 1) {
            setState(() {
              currentWallet = GOCOWallet.cashWallet;
              currentWalletAppLinks = walletAppLinks.cashWallet;
              _currentCard = 0;
            });
          }
        }
      },
    );
  }

  @override
  void onWalletHomeApiCallDone(WalletAppLinksMixed walletAppLinksMixed) {
    setState(() {
      walletAppLinks = walletAppLinksMixed.walletAppLinks;
      shortTransactionHistory = walletAppLinksMixed.shortTransactionHistory;
      shortCoinTransactionHistory = walletAppLinksMixed.shortCoinTransactionHistory;
      currentWalletAppLinks = walletAppLinks.cashWallet;
      _globalProvider.currentUser = walletAppLinksMixed.currentUser;
      pageReady = true;
    });
    cashBalanceAnimate(
      from: cashBalance,
      amount: walletAppLinksMixed.cashAmount ?? 0,
    );

    coinBalanceAnimation(
      from: coinBalance,
      coin: walletAppLinksMixed.currentCoin ?? 0,
    );
  }

  _getWalletAppLinks() async {
    if (_globalProvider.walletAppLinksMixed != null) {
      walletAppLinks = _globalProvider.walletAppLinksMixed.walletAppLinks;
      shortTransactionHistory = _globalProvider.walletAppLinksMixed.shortTransactionHistory;
      shortCoinTransactionHistory = _globalProvider.walletAppLinksMixed.shortCoinTransactionHistory;
      currentWalletAppLinks = walletAppLinks.cashWallet;
      _globalProvider.currentUser = _globalProvider.walletAppLinksMixed.currentUser;
      if (walletAppLinks.cashWallet.isNotEmpty || walletAppLinks.coinWallet.isNotEmpty) {
        setState(() {
          pageReady = true;
        });
      }
    }
    _presenter.getWalletAppLinksHandler();
  }

  _navigateToTransaction() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WalletTransaction(
          walletType: currentWallet,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return pageReady
        ? RefreshIndicator(
            onRefresh: () async {
              await _getWalletAppLinks();
            },
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 300,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                color: Theme.of(context).primaryColor,
                                height: 180,
                                child: Container(
                                  margin: EdgeInsets.only(
                                    left: 25,
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 25,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          currentWallet == GOCOWallet.cashWallet
                                              ? AutoSizeText(
                                                  'Cash Balance',
                                                  style: TextStyle(
                                                    color: Colors.white70,
                                                    fontSize: 15,
                                                  ),
                                                )
                                              : AutoSizeText(
                                                  'Coin Balance',
                                                  style: TextStyle(
                                                    color: Colors.white70,
                                                    fontSize: 15,
                                                  ),
                                                ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          currentWallet == GOCOWallet.cashWallet
                                              ? AutoSizeText(
                                                  '${inr.formatCurrency(cashBalance)}',
                                                  style: TextStyle(
                                                    color: Colors.white70,
                                                    fontSize: 40,
                                                  ),
                                                )
                                              : AutoSizeText(
                                                  '${inr.formatToNumber(coinBalance)}',
                                                  style: TextStyle(
                                                    color: Colors.white70,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          top: 100,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: <Widget>[
                                this.getSwipeCardBlock(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    this.appLinkIcons(),
                    this.getQuickTransactionData(),
                  ],
                ),
              ),
            ),
          )
        : pageLoader();
  }

  Widget getSwipeCardBlock() {
    //print(_globalProvider.currentUser.goco_id);
    return Container(
      height: 160,
      margin: EdgeInsets.only(bottom: 20),
      child: ListView(
        controller: _cardScroll,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          InkWell(
            onTap: () {
              setState(() {
                _cardScroll.jumpTo(_cardScroll.position.minScrollExtent);
              });
            },
            child: SwipeCard(
              color: Colors.blueAccent,
              cardLabel: 'GOCO CASH',
              nameOnCard: "${_globalProvider.currentUser.accountDetails.firstname} ${_globalProvider.currentUser.accountDetails.lastname}",
              cardID: "${_globalProvider.currentUser.goco_id} A",
              enabled: _currentCard == 0 ? true : false,
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                _cardScroll.jumpTo(_cardScroll.position.maxScrollExtent);
              });
            },
            child: SwipeCard(
              color: Colors.pinkAccent,
              cardLabel: 'GOCO COIN',
              nameOnCard: "${_globalProvider.currentUser.accountDetails.firstname} ${_globalProvider.currentUser.accountDetails.lastname}",
              cardID: "${_globalProvider.currentUser.goco_id} B",
              enabled: _currentCard == 1 ? true : false,
            ),
          ),
        ],
      ),
    );
  }

  Widget getQuickTransactionData() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 6,
                child: AutoSizeText(
                  'TRANSACTIONS',
                  presetFontSizes: [16, 18, 20],
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: _navigateToTransaction,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        //color: Colors.blue,
                        child: Icon(
                          Icons.arrow_forward,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          currentWallet == GOCOWallet.cashWallet
              ? ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    Transaction cTransaction = shortTransactionHistory.data[index];
                    return Column(
                      children: <Widget>[
                        TransactionTile(
                          transactionType: cTransaction.type == 'cr' ? TransactionType.cr : TransactionType.dr,
                          transDescription: cTransaction.description ?? '',
                          transDate: formatTime(cTransaction.created_at),
                          transAmount: '${inr.formatCurrency(cTransaction.amount.toDouble())}',
                        ),
                        Divider(),
                      ],
                    );
                  },
                  itemCount: shortTransactionHistory?.data?.length ?? 0,
                )
              : ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    CoinTransaction cTransaction = shortCoinTransactionHistory.transactions[index];
                    return Column(
                      children: <Widget>[
                        TransactionTile(
                          transactionType: cTransaction.type == 'cr' ? TransactionType.cr : TransactionType.dr,
                          transDescription: cTransaction.description ?? '',
                          transDate: formatTime(cTransaction.created_at),
                          transAmount: '${inr.formatToNumber(cTransaction.coin_value.toDouble())}',
                        ),
                        Divider(),
                      ],
                    );
                  },
                  itemCount: shortCoinTransactionHistory?.transactions?.length ?? 0,
                ),
        ],
      ),
    );
  }

  Widget appLinkIcons() {
    return Container(
      padding: EdgeInsets.all(15),
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          WalletAppLink cAppLinkCategory = currentWalletAppLinks[index];

          List<Widget> menuList = [];
          cAppLinkCategory.links.forEach((link) {
            menuList.add(RoundedNavButton(
              context: context,
              iconPath: 'assets/images/icons/${link.icon}',
              bgColor: HexColor(link.bg_color),
              isHighlighter: link.exclusiveMark > 0 ? true : false,
              label: '${link.title}',
              onTap: () {
                Navigator.pushNamed(context, link.link).whenComplete(() {
                  this._getWalletAppLinks();
                });
              },
            ));
            menuList.add(SizedBox(
              width: 10,
            ));
          });
          return Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: AutoSizeText(
                        '${cAppLinkCategory.label.toUpperCase()}',
                        presetFontSizes: [17, 16, 15, 14, 13, 12],
                        maxLines: 1,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      child: Wrap(
                        children: menuList,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          );
        },
        itemCount: currentWalletAppLinks.length,
      ),
    );
  }

  Widget pageLoader() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 270,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        height: 200,
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 25,
                          ),
                          child: Column(
                            children: <Widget>[],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: 100,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 160,
                          margin: EdgeInsets.only(bottom: 20),
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              SwipeCardSkeletonAnimation(),
                              SwipeCardSkeletonAnimation(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              margin: EdgeInsets.only(bottom: 25),
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * .28,
                        child: SkeletonAnimation(
                          child: Container(
                            height: 15,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Wrap(
                          children: <Widget>[
                            RadiusMenuSkeletonAnimation(),
                            SizedBox(
                              width: 20,
                            ),
                            RadiusMenuSkeletonAnimation(),
                            SizedBox(
                              width: 20,
                            ),
                            RadiusMenuSkeletonAnimation(),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * .28,
                        child: SkeletonAnimation(
                          child: Container(
                            height: 15,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Wrap(
                          children: <Widget>[
                            RadiusMenuSkeletonAnimation(),
                            SizedBox(
                              width: 20,
                            ),
                            RadiusMenuSkeletonAnimation(),
                            SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  TransactionSkeletonAnimation(),
                  TransactionSkeletonAnimation(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RoundedNavButton extends StatelessWidget {
  final String iconPath;
  final Color bgColor;
  final bool isHighlighter;
  final String label;
  final Function onTap;
  const RoundedNavButton(
      {Key key, this.label = '', this.isHighlighter = false, this.bgColor = Colors.blue, @required this.iconPath, @required this.context, @required this.onTap})
      : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              CircleAvatar(
                backgroundColor: bgColor,
                radius: 30,
                child: Image.asset(
                  iconPath,
                  scale: 2,
                ),
              ),
              isHighlighter
                  ? Positioned(
                      top: 5,
                      right: 0,
                      child: CircleAvatar(
                        backgroundColor: Color(0xfffafafa),
                        radius: 9,
                      ),
                    )
                  : Container(),
              isHighlighter
                  ? Positioned(
                      top: 7,
                      right: 2,
                      child: CircleAvatar(
                        backgroundColor: GocoMartAppTheme.buildLightTheme().primaryColor,
                        radius: 7,
                      ),
                    )
                  : Container()
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            width: MediaQuery.of(context).size.width * .24,
            child: AutoSizeText(
              label,
              presetFontSizes: [15, 14, 13, 12],
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
