import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/components/ui_animations/fireworks.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/scratch_card/scratchCard.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/wallet/scratch_open_modal.dart';
import 'package:gocomartapp/screens/wallet/widgets/reward_card_detail.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/plugins/scratcher/widgets.dart';
import 'package:gocomartapp/src/screens/wallet/cashwallet/cash_wallet_rewards_detail_presenter.dart';

class CashWalletRewardsDetailV2 extends StatefulWidget {
  final ScratchCard scratchCard;
  final GlobalProvider globalProvider;

  const CashWalletRewardsDetailV2({
    Key key,
    this.scratchCard,
    this.globalProvider,
  }) : super(key: key);
  @override
  _CashWalletRewardsDetailV2State createState() => _CashWalletRewardsDetailV2State();
}

class _CashWalletRewardsDetailV2State extends State<CashWalletRewardsDetailV2> with BaseStatefulScreen implements ICashWalletRewardsDetail {
  bool locationDisabled = false;
  String scratchCardDisabledInfo = '';
  final scratchKey2 = GlobalKey<ScratcherState>();

  CashWalletRewardsDetailPresenter _presenter;

  @override
  void initState() {
    super.initState();
    this._init();
    loadClient();
  }

  void _init() async {
    await Future.delayed(Duration(microseconds: 10));
    _presenter = CashWalletRewardsDetailPresenter(view: this, context: context);
    await Future.delayed(Duration(microseconds: 10));
  }

  void loadClient() async {
    await Future.delayed(Duration(milliseconds: 10));
    if (this.widget.globalProvider.position == null) {
      setState(() {
        locationDisabled = true;
        scratchCardDisabledInfo = 'Please enable your location service to scratch this offer';
      });
    } else {
      setState(() {
        locationDisabled = false;
      });
    }
    this.widget.globalProvider.scratchCardHeroHide = true;
  }

  @override
  void onScratchCardScratchedDone() {
    setState(() {
      this.widget.scratchCard.isScratched = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    bool currentOpenCard = this.widget.scratchCard.isScratched;

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    child: AutoSizeText(
                      'Congratulations!',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                        color: Colors.black87,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    child: AutoSizeText(
                      'You won a scratch card!',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black87,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Material(
              child: (currentOpenCard == false && locationDisabled == false)
                  ? FireWorkBox(
                      duration: Duration(seconds: 1),
                      particle: FreeOrderParticles(),
                      firedWidgetBuilder: (context, controller) {
                        return Scratcher(
                          key: scratchKey2,
                          accuracy: ScratchAccuracy.low,
                          brushSize: 60,
                          threshold: 30,
                          color: HexColor("#${this.widget.scratchCard.cardColor}"),
                          image: Image.asset('assets/images/texture/${this.widget.scratchCard.texturePng}'),
                          onChange: (value) {
                            //print(value);
                          },
                          onThreshold: () {
                            controller.forward();
                            scratchKey2.currentState.reveal(duration: Duration(milliseconds: 1000));
                            this.widget.globalProvider.setCashCurrentScratchState(true);
                            _presenter.onScratchCardRevealRequest(position: this.widget.globalProvider.position, scratchCardId: this.widget.scratchCard.id);
                          },
                          child: RewardCardDetail(
                            isScratched: this.widget.scratchCard.isScratched,
                            brandLogoNetworkLink:
                                "${FlavorConfig.instance.flavorValues.cdnPoint}scratchcard/brandads/${this.widget.scratchCard.advertiserImage.image}",
                            offerText: this.widget.scratchCard.rewardText,
                            offerProvider: this.widget.scratchCard.advertiser.name,
                            scratchedValue: this.widget.scratchCard.value.toString(),
                          ),
                          radius: 30,
                        );
                      },
                    )
                  : RewardCardDetail(
                      isScratched: this.widget.scratchCard.isScratched,
                      brandLogoNetworkLink:
                          "${FlavorConfig.instance.flavorValues.cdnPoint}scratchcard/brandads/${this.widget.scratchCard.advertiserImage.image}",
                      offerText: this.widget.scratchCard.rewardText,
                      offerProvider: this.widget.scratchCard.advertiser.name,
                      scratchedValue: this.widget.scratchCard.value.toString(),
                      unScratchedColor: HexColor("#${this.widget.scratchCard.cardColor}"),
                      locationDisabled: locationDisabled,
                      cardDisabledText: locationDisabled == true ? scratchCardDisabledInfo : '',
                    ),
            ),
            SizedBox(
              height: 40,
            ),
            AnimatedSwitcher(
              duration: Duration(milliseconds: 500),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(child: child, scale: animation);
              },
              child: this.widget.scratchCard.isScratched
                  ? Container(
                      key: ValueKey(1),
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Icon(
                                    Icons.radio_button_off,
                                    color: Colors.orange,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 10,
                                child: Container(
                                  child: AutoSizeText(
                                    'Added to your Goco wallet, ready to use in the next purchase,',
                                    style: TextStyle(color: Colors.black45),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Icon(
                                    Icons.radio_button_off,
                                    color: Colors.green,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 10,
                                child: Container(
                                  child: Row(
                                    children: [
                                      AutoSizeText(
                                        'Explore More',
                                        style: TextStyle(color: Colors.green),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.green,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Container(
                      key: ValueKey(1),
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Icon(
                                    Icons.radio_button_off,
                                    color: Colors.orange,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 10,
                                child: Container(
                                  child: AutoSizeText(
                                    '${this.widget.scratchCard.scratchCardSchemeLabel}',
                                    style: TextStyle(color: Colors.black45),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Icon(
                                    Icons.radio_button_off,
                                    color: Colors.orange,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 10,
                                child: Container(
                                  child: AutoSizeText(
                                    'No Worries!!! never says better luck next time,',
                                    style: TextStyle(color: Colors.black45),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
            ),
          ],
        ),
      ),
      bottomSheet: this.widget.scratchCard.isScratched == true
          ? Container(
              color: Theme.of(context).primaryColor,
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      child: SizedBox.expand(
                        child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {},
                          child: AutoSizeText(
                            'Tell your Friend',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 19,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          : Container(
              height: 0,
            ),
    );
  }
}
