import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/models/scratch_card/scratchCard.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/requests/account/claimScratchCardRequest.dart';
import 'package:gocomartapp/src/models/requests/base/paginationRequest.dart';
import 'package:gocomartapp/src/models/response/base/processStatus.dart';
import 'package:provider/provider.dart';

class CashWalletRewardsDetailPresenter {
  CashWalletRewardsDetailPresenter({this.context, this.view}) {
    _init();
  }

  BuildContext context;
  ICashWalletRewardsDetail view;
  DeviseInfo deviseInfo = new DeviseInfo();
  AccountServices _accountServices;
  GlobalProvider globalProvider;
  PaginationRequest pagination;
  double totalRewardsCollected = 0.0;

  List<ScratchCard> scratchCardRawList = [];
  int totalCards = 0;
  bool fetchMore = false;
  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _accountServices = AccountServices(context: context);
    globalProvider = Provider.of<GlobalProvider>(context);
  }

  void onScratchCardRevealRequest({String scratchCardId, Position position}) async {
    ClaimScratchCardRequest cardRequest = ClaimScratchCardRequest();
    cardRequest.id = scratchCardId;
    cardRequest.latitude = position.latitude;
    cardRequest.longitude = position.longitude;
    cardRequest.id = scratchCardId;
    ProcessStatus status = await _accountServices.claimScratchCard(cardRequest);
    if (status.error == false) {
      this.view.onScratchCardScratchedDone();
    } else {}
  }
}
