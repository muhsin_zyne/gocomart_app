import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/controllers/curency_format.dart';
import 'package:gocomartapp/controllers/hero_route.dart';
import 'package:gocomartapp/models/scratch_card/scratchCard.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/modules/skeleton_animations.dart';
import 'package:gocomartapp/screens/wallet/widgets/reward_card.dart';
import 'package:gocomartapp/screens/wallet/widgets/sliver_flip.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/screens/wallet/cashwallet/cash_wallet_reward_detail.dart';
import 'package:gocomartapp/src/screens/wallet/cashwallet/cash_wallet_rewards_presenter.dart';
import 'package:gocomartapp/theme/hexcolor.dart';
import 'package:provider/provider.dart';

class CashWalletRewardsScreen extends StatefulWidget {
  static const String id = 'cash_wallet_rewards_screen';

  @override
  _CashWalletRewardsScreenState createState() => _CashWalletRewardsScreenState();
}

class _CashWalletRewardsScreenState extends State<CashWalletRewardsScreen>
    with TickerProviderStateMixin, BaseStatefulScreen
    implements ICashWalletRewardsScreen {
  CashWalletRewardsPresenter presenter;
  CurrencyFormat inr = CurrencyConst.inrFormat1;
  ScrollController _controller = ScrollController();
  GlobalProvider _globalProvider;
  AccountServices _accountServices;
  bool pageReady = false;
  bool titleEnabled = false;
  bool loading = true;
  bool currentOpenCard = false;

  List<ScratchCard> scratchCardRawList = [];
  double totalRewardsCollected = 0.0;
  AnimationController _totalRewardsCollectedAnimation;

  @override
  void initState() {
    super.initState();
    this._init();
    this._scrollControlHandler();
  }

  @override
  void onRewardsListFetchDone(scratchCardRawList, totalCollected) {
    setState(() {
      this.scratchCardRawList = scratchCardRawList;
      pageReady = true;
      presenter.fetchMore = false;
    });
    _totalCashRewardsAnimate(
      from: totalRewardsCollected,
      amount: totalCollected ?? 0,
    );
  }

  void _init() async {
    await Future.delayed(Duration(microseconds: 10));
    presenter = CashWalletRewardsPresenter(context: context, view: this);
    _globalProvider = Provider.of<GlobalProvider>(context);
    await Future.delayed(Duration(microseconds: 10));
    this._loadClient();
  }

  void _loadClient() async {
    presenter.onScratchCardRequest();
    presenter.onPromotionalContentRequest();
  }

  void _paginationRequest() {
    if (presenter.scratchCardRawList.length < presenter.totalCards && presenter.fetchMore == false) {
      setState(() {
        presenter.fetchMore = true;
      });
      presenter.nextRequest();
    } else {
      print("pagination completed");
    }
  }

  _totalCashRewardsAnimate({@required amount, from = 0.0}) {
    if (amount != 0 && amount > 0 && amount != from) {
      if (amount < from) {
        _totalRewardsCollectedAnimation = AnimationController(
          duration: Duration(milliseconds: 300),
          vsync: this,
          lowerBound: amount.toDouble(),
          upperBound: from,
        );
      } else {
        _totalRewardsCollectedAnimation = AnimationController(
          duration: Duration(milliseconds: 300),
          vsync: this,
          lowerBound: from,
          upperBound: amount.toDouble(),
        );
      }
      if (amount > from) {
        _totalRewardsCollectedAnimation.forward();
      } else {
        _totalRewardsCollectedAnimation.reverse(from: from);
      }
      _totalRewardsCollectedAnimation.addListener(() {
        setState(() {
          totalRewardsCollected = double.parse(_totalRewardsCollectedAnimation.value.toStringAsFixed(1));
        });
      });
    }
  }

  void _scrollControlHandler() {
    _controller.addListener(() {
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
      if (_controller.position.pixels > 150) {
        setState(() {
          titleEnabled = true;
        });
      } else {
        setState(() {
          titleEnabled = false;
        });
      }

      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        _paginationRequest();
      }
    });
  }

  actionMarkAsScratched(key) {
    setState(() {
      scratchCardRawList[key].isScratched = true;
      var rewardsIncrement = scratchCardRawList[key].value.toDouble();
      var oldReward = totalRewardsCollected;
      _totalCashRewardsAnimate(from: totalRewardsCollected, amount: (rewardsIncrement + oldReward));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: _controller,
        slivers: <Widget>[
          SliverAppBar(
            primary: true,
            title: titleEnabled
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Hero(
                        tag: 'total_rewards',
                        child: Text(
                          "Total Rewards",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Hero(
                        tag: 'rewards_total',
                        child: Text("${inr.formatCurrency(totalRewardsCollected)}"),
                      ),
                    ],
                  )
                : Text(''),
            expandedHeight: 60,
            pinned: true,
            elevation: 0,
          ),
          SliverFlip(
            child: Container(
              height: 200,
              color: Theme.of(context).primaryColor,
              child: Stack(
                children: <Widget>[
                  Container(
                    //color: Colors.red,
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Image.asset(
                              'assets/images/walletBalance-05-40.png',
                              scale: 1.5,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: titleEnabled == false
                                  ? pageReady
                                      ? Hero(
                                          tag: 'total_rewards',
                                          child: AutoSizeText(
                                            'Total Rewards',
                                            presetFontSizes: [16, 15],
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        )
                                      : Container()
                                  : AutoSizeText(''),
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: titleEnabled == false
                                  ? pageReady == true
                                      ? Hero(
                                          tag: 'rewards_total',
                                          child: AutoSizeText(
                                            "${inr.formatCurrency(totalRewardsCollected)}",
                                            presetFontSizes: [32, 31, 30, 29, 28, 27],
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        )
                                      : Container()
                                  : Container(),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.all(15),
            sliver: SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  if (index >= this.scratchCardRawList.length) {
                    return ScratchCardSkeletonAnimation();
                  }
                  ScratchCard currentScratchCard = scratchCardRawList[index];

                  _globalProvider.setCashCurrentScratchState(false);
                  return RewardCard(
                    heroTag: 'scratchCard' + currentScratchCard.id.toString(),
                    brandLogoNetworkLink: "${_globalProvider.cdnPoint}scratchcard/brandads/${currentScratchCard.advertiserImage.image}",
                    texturePng: currentScratchCard.texturePng,
                    isScratched: currentScratchCard.isScratched,
                    unScratchedColor: HexColor("#${currentScratchCard.cardColor}"),
                    offerText: currentScratchCard.rewardText,
                    offerProvider: currentScratchCard.advertiser.name,
                    scratchedValue: currentScratchCard.value.toString(),
                    onTap: () {
                      Navigator.push(
                        context,
                        HeroDialogRoute(
                            builder: (context) => CashWalletRewardsDetailV2(
                                  scratchCard: currentScratchCard,
                                  globalProvider: _globalProvider,
                                ),
                            beforePop: () {
                              setState(() {
                                _globalProvider.scratchCardHeroHide = false;
                              });
                            }),
                      ).whenComplete(() async {
                        await _globalProvider.trigger();
                        if (_globalProvider.cashCurrentScratchState == true) {
                          actionMarkAsScratched(index);
                        }
                      });
                    },
                  );
                },
                childCount: scratchCardRawList.length + ((presenter?.fetchMore == true || pageReady == false) ? 4 : 0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
