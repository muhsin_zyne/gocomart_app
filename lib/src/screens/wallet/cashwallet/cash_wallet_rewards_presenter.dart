import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/models/scratch_card/scratchCard.dart';
import 'package:gocomartapp/models/scratch_card/scratchCards.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/services/app_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/requests/base/paginationRequest.dart';
import 'package:gocomartapp/src/models/response/wallet/ScratchCardPromotional.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CashWalletRewardsPresenter {
  CashWalletRewardsPresenter({this.context, this.view}) {
    _init();
  }

  BuildContext context;
  ICashWalletRewardsScreen view;
  DeviseInfo deviseInfo = new DeviseInfo();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  AccountServices _accountServices;
  GlobalProvider globalProvider;
  AppServices _appServices;
  PaginationRequest pagination;
  double totalRewardsCollected = 0.0;

  List<ScratchCard> scratchCardRawList = [];
  int totalCards = 0;
  bool fetchMore = false;
  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _accountServices = AccountServices(context: context);
    globalProvider = Provider.of<GlobalProvider>(context);
    pagination = new PaginationRequest();
    pagination.limit = PaginationConst.CASH_REWARDS_PER_PAGE;
    pagination.page = 1;
  }

  void nextRequest() {
    pagination.page++;
    this.onScratchCardRequest();
  }

  void _loadFromCache() {
    if (this.globalProvider.scratchCardRawList.length > 0) {
      this.view.onRewardsListFetchDone(this.globalProvider.scratchCardRawList, this.globalProvider.scratchTotalCollectedAmount);
    }
  }

  void onScratchCardRequest() async {
    if (this.pagination.page == 1) {
      this._loadFromCache();
    }
    ScratchCards scratchCards = await _accountServices.getScratchCards(pagination: pagination);
    if (scratchCards.error == false) {
      totalCards = scratchCards.total;
      if (scratchCards.cards.length > 0) {
        scratchCardRawList = [scratchCardRawList, scratchCards.cards].expand((x) => x).toList();
        this.globalProvider.scratchCardRawList = scratchCardRawList;
        this.globalProvider.scratchTotalCollectedAmount = scratchCards.collectedAmount;
      }
      this.view.onRewardsListFetchDone(scratchCardRawList, scratchCards.collectedAmount);
    }
  }

  void onPromotionalContentRequest() async {
    ScPromotionalText scPromotionalText = await this._accountServices.scPromotionalText();
    print(scPromotionalText.content);
    if (scPromotionalText.error == false) {
      globalProvider.scPromotionalText = scPromotionalText;
    }
  }
}
