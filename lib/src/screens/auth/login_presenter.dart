import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/models/login_response.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreenPresenter {
  LoginScreenPresenter({this.context, this.view}) {
    _init();
  }

  GlobalProvider globalProvider;
  BuildContext context;
  ILoginScreenView view;
  DeviseInfo deviseInfo = new DeviseInfo();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  AccountServices _accountServices;
  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _accountServices = AccountServices(context: context);
    globalProvider = Provider.of<GlobalProvider>(
      context,
    );
    deviseInfo.fetchInfo();
  }

  void actionLoginHandler(LoginScreenRequest loginScreenRequest) async {
    loginScreenRequest.uniqueId = deviseInfo.uniqueID;
    loginScreenRequest.clientType = deviseInfo.type;
    if (globalProvider?.referralLink?.code != null) {
      loginScreenRequest.referral = globalProvider.referralLink.code;
    }
    final LoginResponse loginResponse = await _accountServices.actionLogin(loginScreenRequest);
    if (loginResponse.error == false) {
      view.onLoginScreenDone(loginScreenRequest, loginResponse);
    } else {
      view.onLoginScreenError(loginResponse);
    }
  }
}
