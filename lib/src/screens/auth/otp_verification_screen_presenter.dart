import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/models/otp_validation_response.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/requests/auth/VerifyOtpRequest.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OtpVerificationScreenPresenter {
  OtpVerificationScreenPresenter({this.context, this.view}) {
    _init();
  }
  BuildContext context;
  IOtpVerificationScreenView view;
  DeviseInfo deviseInfo = new DeviseInfo();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  AccountServices _accountServices;
  GlobalProvider _globalProvider;
  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _accountServices = AccountServices(context: context);
    _globalProvider = Provider.of<GlobalProvider>(context);
    deviseInfo.fetchInfo();
  }

  void otpVerificationHandler(VerifyOtpRequest verifyOtpRequest) async {
    OtpValidationResponse otpValidationResponse = await _accountServices.accountVerifyOTP(verifyOtpRequest);
    if (otpValidationResponse.error == false) {
      try {
        if (otpValidationResponse.auth_token != null) {
          final SharedPreferences prefs = await _prefs;
          prefs.setString('auth_token', otpValidationResponse.auth_token);
          prefs.setString('refreshToken', otpValidationResponse.refreshToken);
          prefs.setString('userData', jsonEncode(otpValidationResponse.user.toJson()));
          _globalProvider.init();
          OneSignal.shared.setExternalUserId(otpValidationResponse.user.user_id.toString());
          this.view.onOtpVerificationScreenDone(otpValidationResponse);
        } else {
          throw Exception();
        }
      } on Exception catch (e) {
        OtpValidationResponse otpValidationResponse = new OtpValidationResponse();
        otpValidationResponse.error = true;
        otpValidationResponse.message = 'Internal Server Error';
        this.view.onOtpVerificationScreenError(otpValidationResponse);
      }
    } else {
      print(otpValidationResponse.message);
      this.view.onOtpVerificationScreenError(otpValidationResponse);
    }
  }
}
