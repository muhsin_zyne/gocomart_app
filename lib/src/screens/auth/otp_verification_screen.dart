import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/login_response.dart';
import 'package:gocomartapp/models/otp_validation_response.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/auth/widgets/resend_block.dart';
import 'package:gocomartapp/screens/home_screen.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:gocomartapp/src/models/requests/auth/VerifyOtpRequest.dart';
import 'package:gocomartapp/src/screens/auth/otp_verification_screen_presenter.dart';
import 'package:gocomartapp/theme/theme_const.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms_autofill/sms_autofill.dart';

class OtpVerificationScreen extends StatelessWidget {
  static const String id = 'otpverification_screen';
  OtpVerificationScreen({Key key, this.loginScreenRequest, this.mobileNo, this.loginResponse}) : super(key: key);

  final int mobileNo;
  final LoginScreenRequest loginScreenRequest;
  final LoginResponse loginResponse;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        //backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: OtpVerificationContent(
        mobileNo: mobileNo,
        loginScreenRequest: loginScreenRequest,
        loginResponse: loginResponse,
      ),
    );
  }
}

class OtpVerificationContent extends StatefulWidget {
  OtpVerificationContent({Key key, this.loginScreenRequest, this.mobileNo, this.loginResponse}) : super(key: key);
  final int mobileNo;
  final LoginScreenRequest loginScreenRequest;
  final LoginResponse loginResponse;
  @override
  _OtpVerificationContentState createState() => _OtpVerificationContentState();
}

class _OtpVerificationContentState extends State<OtpVerificationContent> with BaseStatefulScreen implements IOtpVerificationScreenView {
  TextEditingController _controller = TextEditingController(); // ignore: unused_field
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  GlobalProvider _globalProvider;
  GraphQLClient _client; // ignore: unused_field
  String _code = '';
  String signature = "";

  int _otpAttemptCount = 0; // ignore: unused_field
  Timer _timer; // ignore: unused_field
  int _start = 30; // ignore: unused_field
  int _otpCode;
  bool _otpVerificationProgress = false;

  AccountServices _accountServices;

  OtpVerificationScreenPresenter _presenter;

  @override
  void initState() {
    super.initState();
    this._init();
  }

  void _init() {
    this._loadClient();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 100));
    _presenter = OtpVerificationScreenPresenter(context: context, view: this);
    _globalProvider = Provider.of<GlobalProvider>(context);
    this._setAutoVerifyListener();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _setAutoVerifyListener() async {
    await Future.delayed(Duration(seconds: 1));
    if (FlavorConfig.instance.flavorValues.isTest) {
      if (this.widget.loginResponse.otp != null) {
        var _codeInt = int.parse('${this.widget.loginResponse.otp}');
        if (_otpCode != _codeInt) {
          _otpCode = _codeInt;
          attemptVerification();
        } else {}
      }
    }
  }

  attemptVerification() async {
    setState(() {
      _otpVerificationProgress = true;
    });
    final VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest();
    verifyOtpRequest.mobile = widget.mobileNo;
    verifyOtpRequest.code = _otpCode;
    this._presenter.otpVerificationHandler(verifyOtpRequest);
  }

  void showSnackBar(String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: kToastMessage,
        ),
      ),
    );
  }

  Widget getHeaderBlock() {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ClipPath(
        clipper: MyClipper(),
        child: Container(
          height: MediaQuery.of(context).size.height * .3,
          decoration: BoxDecoration(
            color: Color(0xff3b5998),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Image.asset(
                      'assets/images/otp_verification.png',
                      width: MediaQuery.of(context).size.width * .2,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0.0),
                  child: AutoSizeText(
                    "We have sent you go-co code via SMS for mobile number verification",
                    maxLines: 3,
                    maxFontSize: 20,
                    minFontSize: 16,
                    textAlign: TextAlign.center,
                    style: kGeneralText,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              getHeaderBlock(),
              Positioned(
                bottom: 20,
                left: MediaQuery.of(context).size.width * .05,
                child: Text(
                  'Otp Verfication',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Lato',
                    fontSize: 30.0,
                  ),
                ),
              ),
            ],
          ),
          GestureDetector(
            onTap: () {},
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 24.0),
                  child: Container(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
                      child: PinFieldAutoFill(
                        // decoration: UnderlineDecoration(
                        //   textStyle: TextStyle(
                        //     fontSize: 20,
                        //     color: Theme.of(context).primaryColor,
                        //   ),
                        //   color: Theme.of(context).primaryColor,
                        // ),
                        currentCode: _code,
                        codeLength: 4,
                        autofocus: true,
                        onCodeChanged: (code) {
                          if (code.length > 3) {
                            var _codeInt = int.parse('$code');
                            if (_otpCode != _codeInt) {
                              _otpCode = _codeInt;
                              attemptVerification();
                            } else {}
                          }
                        },
                      ),
                    ),
                  ),
                ),
                _otpVerificationProgress == true
                    ? Positioned.fill(
                        top: 15,
                        child: Container(
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SpinKitWave(
                                    color: Theme.of(context).primaryColor,
                                    type: SpinKitWaveType.start,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    : Positioned.fill(
                        child: Container(),
                      ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ResentCodeBlock(
            otpVerificationProgress: _otpVerificationProgress,
            loginScreenRequest: this.widget.loginScreenRequest,
          ),
        ],
      ),
    );
  }

  @override
  void onOtpVerificationScreenDone(OtpValidationResponse otpValidationResponse) {
    setState(() {
      _otpVerificationProgress = false;
    });
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => HomeScreen(),
      ),
      (r) => false,
    );
  }

  @override
  void onOtpVerificationScreenError(OtpValidationResponse otpValidationResponse) {
    setState(() {
      _otpVerificationProgress = false;
    });
    showSnackBar(otpValidationResponse.message);
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height);
    var controllPoint = Offset(10, size.height);
    var endPoint = Offset(size.width / 2, size.height);
    path.quadraticBezierTo(controllPoint.dx, controllPoint.dy, endPoint.dx, endPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement should Reclip
    return true;
  }
}
