import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gocomartapp/controllers/devise_info.dart';
import 'package:gocomartapp/models/login_response.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/screens/auth/widgets/country_picker.dart';
import 'package:gocomartapp/services/account_services.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:gocomartapp/src/screens/auth/login_presenter.dart';
import 'package:gocomartapp/src/screens/auth/otp_verification_screen.dart';
import 'package:gocomartapp/theme/gocomart_app_theme.dart';
import 'package:gocomartapp/theme/theme_const.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../contracts.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  dynamic type = '';
  dynamic link = 'not alter';

  @override
  void initState() {
    print('LoginScreen starts now | ${DateTime.now()} ');
    super.initState();
  }

//  void initDynamicLinks() async {
//    final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
//    final Uri deepLink = data?.link;
//    if (deepLink != null) {
//      setState(() {
//        type = 'old data';
//        link = deepLink.toString();
//      });
//
//      triggerAlert();
//
//      //Navigator.pushNamed(context, deepLink.path);
//    }
//
//    FirebaseDynamicLinks.instance.onLink(
//      onSuccess: (PendingDynamicLinkData dynamicLink) async {
//        final Uri deepLink = dynamicLink?.link;
//
//        if (deepLink != null) {}
//      },
//      onError: (OnLinkErrorException e) async {
//        print('onLinkError');
//        //print(e.message);
//      },
//    );
//  }

  triggerAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("$type"),
          content: Text("$link"),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoginScreenContent(),
    );
  }
}

class LoginScreenContent extends StatefulWidget {
  @override
  _LoginScreenContentState createState() => _LoginScreenContentState();
}

class _LoginScreenContentState extends State<LoginScreenContent> with BaseStatefulScreen implements ILoginScreenView {
  GlobalProvider _globalProvider; // ignore: unused_field
  AccountServices _accountServices;
  dynamic loginRequestData;

  Country _selectedCountry = Country.IN;
  int _mobile;
  bool _continuePermission = false;
  bool _isLoading = false;
  String code;
  String signature = '';
  DeviseInfo deviseInfo;

  LoginScreenPresenter _presenter;

  @override
  void initState() {
    super.initState();
    this._init();
  }

  @override
  void didChangeDependencies() {
    _globalProvider = Provider.of<GlobalProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  void _init() async {
    this._loadClient();
  }

  void _loadClient() async {
    await Future.delayed(Duration(microseconds: 10));
    SmsAutoFill smsAutoFill = SmsAutoFill();
    _presenter = new LoginScreenPresenter(context: context, view: this);
    signature = await smsAutoFill.getAppSignature;
  }

  void attemptLogin() async {
    setState(() {
      _isLoading = true;
    });
    final LoginScreenRequest loginScreenRequest = new LoginScreenRequest();
    loginScreenRequest.signature = signature;
    loginScreenRequest.mobileCode = '+' + _selectedCountry.dialingCode.toString();
    loginScreenRequest.mobile = _mobile;
    this._presenter.actionLoginHandler(loginScreenRequest);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              getHeaderBlock(),
              Positioned(
                bottom: 20,
                left: MediaQuery.of(context).size.width * .05,
                child: Text(
                  'Login Account',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Lato',
                    fontSize: 30.0,
                  ),
                ),
              ),
            ],
          ),
          getMobileNumberBlock(),
        ],
      ),
    );
  }

  void showSnackBar(String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: kToastMessage,
        ),
      ),
    );
  }

  Widget getHeaderBlock() {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ClipPath(
        clipper: MyClipper(),
        child: Container(
          height: MediaQuery.of(context).size.height * .8,
          decoration: BoxDecoration(
            color: Color(0xff3b5998),
          ),
          child: Center(
            child: Stack(
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: MediaQuery.of(context).size.width * .3,
                ),
                Positioned(
                  left: 7,
                  top: 7,
                  child: CircleAvatar(
                    backgroundColor: Color(0xff3b5998),
                    radius: MediaQuery.of(context).size.width * .3 - 7,
                    child: Image.asset(
                      'assets/images/logo-invert.png',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getMobileNumberBlock() {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height * .2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
            child: Row(
              children: <Widget>[
                getCountryPickerBlock(),
                getMobileNumberInputBlock(),
                AnimatedSwitcher(
                  duration: Duration(milliseconds: 300),
                  transitionBuilder: (Widget child, Animation<double> animation) {
                    return ScaleTransition(child: child, scale: animation);
                  },
                  child: _isLoading == false ? getTapButtonBlock() : getLoadingWidget(),
                ),
              ],
            ),
          ),
          //getFooterMobileUI(),
        ],
      ),
    );
  }

  Widget getLoadingWidget() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: CircularProgressIndicator(),
    );
  }

  Widget getTapButtonBlock() {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(38.0),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(color: Colors.grey.withOpacity(0.4), offset: Offset(0, 2), blurRadius: 8.0),
        ],
      ),
      child: Material(
        color: _continuePermission == true ? Theme.of(context).primaryColor : Colors.grey.shade500,
        child: InkWell(
          borderRadius: BorderRadius.all(
            Radius.circular(32.0),
          ),
          onTap: _continuePermission == true
              ? () {
                  this.attemptLogin();
                  FocusScope.of(context).requestFocus(FocusNode());
                }
              : null,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(
              FontAwesomeIcons.arrowRight,
              size: 20,
              color: GocoMartAppTheme.buildLightTheme().backgroundColor,
            ),
          ),
        ),
      ),
    );
  }

  Widget getMobileNumberInputBlock() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(right: 16, top: 8, bottom: 8),
        child: TextField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(10),
          ],
          obscureText: false,
          keyboardType: TextInputType.numberWithOptions(),
          onChanged: (String number) {
            _mobile = int.parse('$number');
            setState(() {
              validateScreen();
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Mobile No',
          ),
        ),
      ),
    );
  }

  void validateScreen() {
    if (_mobile.toString().length > 9) {
      _continuePermission = true;
    } else {
      _continuePermission = false;
    }
  }

  Widget getCountryPickerBlock() {
    return CountryPickerCustom(
      onChanged: (selected) {
        code = selected;
      },
    );
  }

  @override
  void onLoginScreenDone(LoginScreenRequest loginScreenRequest, LoginResponse loginResponse) {
    setState(() {
      this._isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OtpVerificationScreen(
          mobileNo: _mobile,
          loginScreenRequest: loginScreenRequest,
          loginResponse: loginResponse,
        ),
      ),
    );
  }

  @override
  void onLoginScreenError(LoginResponse loginResponse) {
    this.showSnackBar(loginResponse.message);
    setState(() {
      this._isLoading = false;
    });
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height);
    var controllPoint = Offset(10, size.height);
    var endPoint = Offset(size.width / 2, size.height);
    path.quadraticBezierTo(controllPoint.dx, controllPoint.dy, endPoint.dx, endPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
