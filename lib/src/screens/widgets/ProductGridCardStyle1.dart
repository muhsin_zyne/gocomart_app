import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/models/shopping_home/products.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';

class ProductGridCardStyle1 extends StatelessWidget {
  const ProductGridCardStyle1({
    Key key,
    @required this.imagePath,
    @required Products product,
    @required this.discountPercent,
  })  : _product = product,
        super(key: key);

  final String imagePath;
  final Products _product;
  final int discountPercent;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductDetailScreen(
              productId: _product.product_id,
            ),
          ),
        );
      },
      child: Stack(
        children: <Widget>[
          Card(
            color: Colors.white,
            clipBehavior: Clip.antiAlias,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width / 2.2,
                  height: MediaQuery.of(context).size.width / 3.2,
                  child: CachedNetworkImage(
                    imageUrl: "${imagePath + _product.details.image}",
                    placeholder: (context, url) => ImagePreLoader(),
                    errorWidget: (context, url, error) => ImageError(),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                Expanded(
                    child: Container(
                  padding: const EdgeInsets.only(
                    left: 10,
                    top: 12,
                  ),
                  width: MediaQuery.of(context).size.width / 2.2,
                  color: Colors.grey[700],
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.width / 21,
                            width: MediaQuery.of(context).size.width / 3.3,
                            //color: Colors.red,
                            child: Container(
                              child: Text(
                                "${_product.details.manufacturer.name}",
                                //textWidthBasis: TextWidthBasis.parent,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 25,
                                ),
                                maxLines: 1,

                                //presetFontSizes: [13, 10, 9, 8, 7],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.width / 11.5,
                            //color: Colors.red,
                            child: AutoSizeText(
                              "${_product.details.description.name}",
                              style: TextStyle(
                                //height: 1.1,
                                color: Colors.white70,
                              ),
                              presetFontSizes: [
                                15,
                                12,
                              ],
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.bottomCenter,
                              height: MediaQuery.of(context).size.width / 21,
                              child: AutoSizeText(
                                "${inr.formatCurrency(_product.price.toDouble())}",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                presetFontSizes: [
                                  19,
                                  18,
                                  17,
                                  16,
                                  15,
                                  14,
                                  13,
                                  11,
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 8, top: 3),
                              //alignment: Alignment.bottomCenter,
                              height: MediaQuery.of(context).size.width / 27,
                              child: AutoSizeText(
                                "${inr.formatCurrency(_product.details.price.toDouble())}",
                                style: TextStyle(
                                  color: Colors.white60,
                                  //fontWeight: FontWeight.w800,
                                  decoration: TextDecoration.lineThrough,
                                  decorationColor: Colors.white70,
                                ),
                                maxLines: 1,
                                presetFontSizes: [12, 11, 10, 9, 8, 7],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ))
              ],
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.width / 3.8,
            left: MediaQuery.of(context).size.width / 3,
            //height: MediaQuery.of(context).size.width/25,
            //width: 30,
            child: Container(
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.width / 8.5,
              width: MediaQuery.of(context).size.width / 8.5,
              decoration:
                  BoxDecoration(color: Colors.white, shape: BoxShape.circle),
              child: Container(
                // padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.red[800], shape: BoxShape.circle),
                //width:MediaQuery.of(context).size.width/10,
                //padding: EdgeInsets.only(left: 6, right: 5),
                // height: MediaQuery.of(context).size.width / 14,
                child: Container(
                  height: MediaQuery.of(context).size.width / 8,
                  padding: const EdgeInsets.all(9.0),
                  child: AutoSizeText(
                    "$discountPercent% \n OFF",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w600),
                    presetFontSizes: [16, 11, 9, 8, 7],
                    maxLines: 2,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
