import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';

class RoundedImageButton extends StatelessWidget {
  const RoundedImageButton({
    Key key,
    @required this.width,
    @required this.imagePath,
    this.label = '',
    this.onTap,
  }) : super(key: key);

  final double width;
  final String imagePath;
  final String label;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: width * .18,
          height: width * .18,
          margin: EdgeInsets.symmetric(horizontal: 10),
          padding: const EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: CachedNetworkImage(
                imageUrl: imagePath,
                placeholder: (context, url) => ImagePreLoader(),
                errorWidget: (context, url, error) => ImageError(),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          width: width * .18,
          child: Row(
            children: [
              Expanded(
                child: AutoSizeText(
                  this.label,
                  textAlign: TextAlign.center,
                  //overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  minFontSize: 12,
                  maxFontSize: 13,
                ),
              )
            ],
          ),
        )
        // Container(
        //   width: width * .15,
        //   margin: const EdgeInsets.only(top: 8),
        //   child: AutoSizeText(
        //     this.label,
        //     textAlign: TextAlign.center,
        //     maxFontSize: 18,
        //     overflow: TextOverflow.ellipsis,
        //     maxLines: 2,
        //     minFontSize: 10,
        //     style: TextStyle(
        //       color: const Color(0xff757171),
        //     ),
        //   ),
        // )
      ],
    );
  }
}
