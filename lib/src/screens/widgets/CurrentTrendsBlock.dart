import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/models/product-list/product.dart';
import 'package:gocomartapp/models/product-list/product_list.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/home/TrendingCategory.dart';
import 'package:gocomartapp/src/screens/home/current_trending_presenter.dart';
import 'package:gocomartapp/screens/product/product_detail_screen.dart';

class CurrentTrendsBlock extends StatefulWidget {
  final String imagePath;
  final BuildContext context;
  final List<TrendingCategory> trendingCategories;
  const CurrentTrendsBlock({
    Key key,
    this.trendingCategories,
    this.context,
    this.imagePath,
  }) : super(key: key);

  @override
  _CurrentTrendsBlockState createState() => _CurrentTrendsBlockState();
}

class _CurrentTrendsBlockState extends State<CurrentTrendsBlock>
    with BaseStatefulScreen
    implements ICurrentTrendingBlockView {
  CurrentTrendingPresenter _presenter;
  List _currentTrendList = <Widget>[];
  List dummyList = <Widget>[];
  bool dummyListEnabled = true;
  TrendingCategory cSelectedCat;

  @override
  void initState() {
    super.initState();
    this._init();
  }

  void _init() async {
    this._presenter =
        CurrentTrendingPresenter(view: this, context: this.widget.context);
    await Future.delayed(Duration(microseconds: 20));
    this.actionCurrentTrends(this.widget.trendingCategories.first);
  }

  void actionCurrentTrends(TrendingCategory trendingCategory) {
    setState(() {
      this.cSelectedCat = trendingCategory;
      this._currentTrendList.clear();
      this.dummyListEnabled = true;
    });
    _presenter.onFetchCurrentTrendingHandler(trendingCategory.categoryId);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 18),
      // height: 100,
      child: Container(
        child: Card(
          borderOnForeground: true,
          elevation: 0.5,
          child: Column(
            children: <Widget>[
              //current trend text
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.width / 12,
                      //height: 20,
                      //color: Colors.blue,
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: AutoSizeText(
                        "Current Trend",
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontWeight: FontWeight.w700),
                        presetFontSizes: [22, 20, 16, 14, 12, 10],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 15),
                      child: InkWell(
                        onTap: () {},
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.grey[700],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //end of current trend text

              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    height: 100,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: this.widget.trendingCategories.length,
                      itemBuilder: (context, index) {
                        final TrendingCategory cTrendingCategory =
                            this.widget.trendingCategories[index];
                        return Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              InkWell(
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: cSelectedCat.categoryId ==
                                                cTrendingCategory.categoryId
                                            ? Theme.of(context).primaryColor
                                            : Colors.black54,
                                      ),
                                      color: cSelectedCat.categoryId ==
                                              cTrendingCategory.categoryId
                                          ? Theme.of(context).primaryColor
                                          : Colors.black54,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Text(
                                    htmlUnescape
                                        .convert(cTrendingCategory.name),
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                onTap: () =>
                                    this.actionCurrentTrends(cTrendingCategory),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 30),
                    child: dummyListEnabled == true
                        ? Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.end,
                            spacing: 10,
                            runSpacing: 10,
                            children: this._currentTrendList,
                          ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onCurrentListChanges(ProductList productList) {
    productList.products.forEach((Product product) {
      double actualPrice = product.price.toDouble();
      double cutPrice = 0.0;
      if (product?.special != null) {
        if (product.special.price != null) {
          if (product.special.price < product.price &&
              product.special.price != 0) {
            actualPrice = product.special.price.toDouble();
            cutPrice = product.price.toDouble();
          }
        }
      }

      _currentTrendList.add(
        Container(
          width: MediaQuery.of(context).size.width / 2.1,
          height: MediaQuery.of(context).size.width / 1.8,

          //text and image
          child: InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProductDetailScreen(
                    productId: product.productId,
                  ),
                ),
              );
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              width: MediaQuery.of(context).size.width / 2,

              //color: Colors.red,
              child: Card(
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          //text + aling
                          Center(
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 13),
                              width: MediaQuery.of(context).size.width / 2.1,
                              height: MediaQuery.of(context).size.width / 2.8,
                              child: Container(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Image.network(
                                  "${this.widget.imagePath + product.image}",
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width / 1.5,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(6),
                            bottomRight: Radius.circular(6)),
                        gradient: LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: [
                            Colors.black87,
                            Colors.black38,
                            Colors.white10
                          ],
                          stops: [0.0, 0.4, 0.6],
                        ),
                        // boxShadow: [
                        //   BoxShadow(
                        //     color: const Color(0x29000000),
                        //     offset: Offset(0, 1),
                        //     blurRadius: 1,
                        //   ),
                        // ],
                      ),
                      child: Container(
                        padding: const EdgeInsets.only(left: 10),
                        width: MediaQuery.of(context).size.width / 2.3,
                        child: Container(
                          height: MediaQuery.of(context).size.width / 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: MediaQuery.of(context).size.width / 20,
                                child: AutoSizeText(
                                  "${htmlUnescape.convert(product.manufacturer.name ?? '')}",
                                  presetFontSizes: [16, 14, 13],
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ),
                              Container(
                                //color: Colors.red,
                                width: MediaQuery.of(context).size.width / 2.4,
                                height: MediaQuery.of(context).size.width / 25,
                                child: AutoSizeText(
                                  "${htmlUnescape.convert(product.description.name ?? '')}",
                                  presetFontSizes: [
                                    14,
                                    12,
                                    11,
                                  ],
                                  textAlign: TextAlign.start,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                      letterSpacing: -0.5),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 8),

                                    height:
                                        MediaQuery.of(context).size.width / 20,
                                    //height: MediaQuery.of(context).size.width / 20,
                                    child: AutoSizeText(
                                      inr.formatCurrency(actualPrice),
                                      presetFontSizes: [16, 14, 13],
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white),
                                    ),
                                  ),
                                  cutPrice != 0.0
                                      ? Container(
                                          margin:
                                              EdgeInsets.only(top: 8, left: 10),

                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24,
                                          //height: MediaQuery.of(context).size.width / 20,
                                          child: AutoSizeText(
                                            inr.formatCurrency(cutPrice),
                                            presetFontSizes: [13, 11, 10],
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                color: Colors.white),
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );

      // _currentTrendList.add(Container(
      //   width: MediaQuery.of(context).size.width * .45,
      //   child: InkWell(
      //     onTap: () {},
      //     child: Container(
      //       margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      //       //color: Colors.red,
      //       child: Card(
      //         elevation: 3,
      //         child: Container(
      //           decoration: BoxDecoration(
      //             borderRadius: BorderRadius.circular(6.0),
      //             gradient: LinearGradient(
      //               begin: Alignment(0.0, 0.11),
      //               end: Alignment(0.0, 1.0),
      //               colors: [
      //                 Colors.white,
      //                 Colors.grey[400],
      //                 const Color(0xff000000)
      //               ],
      //               stops: [0.0, 0.288, 1.0],
      //             ),
      //             boxShadow: [
      //               BoxShadow(
      //                 color: const Color(0x29000000),
      //                 offset: Offset(0, 1),
      //                 blurRadius: 1,
      //               ),
      //             ],
      //           ),
      //           child: Column(
      //             mainAxisAlignment: MainAxisAlignment.start,
      //             children: <Widget>[
      //               Row(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: [
      //                   Expanded(
      //                     child: Container(
      //                       height: MediaQuery.of(context).size.width * .4,
      //                       padding: const EdgeInsets.all(15),
      //                       child: Container(
      //                         padding: EdgeInsets.only(bottom: 10),
      //                         child: CachedNetworkImage(
      //                           imageUrl:
      //                               "${this.widget.imagePath + product.image}",
      //                           placeholder: (context, url) => ImagePreLoader(),
      //                           errorWidget: (context, url, error) =>
      //                               ImageErrorDummy(),
      //                           fit: BoxFit.contain,
      //                         ),
      //                       ),
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //               Row(
      //                 children: [
      //                   Expanded(
      //                     child: Container(
      //                       padding: EdgeInsets.symmetric(horizontal: 10),
      //                       child: AutoSizeText(
      //                         "${htmlUnescape.convert(product.manufacturer.name ?? '')}",
      //                         presetFontSizes: [16, 14, 13],
      //                         textAlign: TextAlign.left,
      //                         overflow: TextOverflow.ellipsis,
      //                         maxLines: 1,
      //                         style: TextStyle(
      //                           fontWeight: FontWeight.w600,
      //                         ),
      //                       ),
      //                     ),
      //                   )
      //                 ],
      //               ),
      //               Row(
      //                 children: [
      //                   Expanded(
      //                     child: Container(
      //                       margin: EdgeInsets.symmetric(horizontal: 10),
      //                       child: AutoSizeText(
      //                         "${htmlUnescape.convert(product.description.name ?? '')}",
      //                         presetFontSizes: [
      //                           16,
      //                           14,
      //                           12,
      //                           11,
      //                         ],
      //                         textAlign: TextAlign.start,
      //                         overflow: TextOverflow.ellipsis,
      //                         maxLines: 1,
      //                         style: TextStyle(
      //                             fontWeight: FontWeight.w700,
      //                             letterSpacing: -0.5),
      //                       ),
      //                     ),
      //                   )
      //                 ],
      //               ),
      //               Container(
      //                 margin: EdgeInsets.symmetric(horizontal: 10),
      //                 child: Row(
      //                   children: [
      //                     Expanded(
      //                       flex: 2,
      //                       child: Container(
      //                         child: AutoSizeText(
      //                           inr.formatCurrency(actualPrice),
      //                           presetFontSizes: [16, 14, 13],
      //                           textAlign: TextAlign.left,
      //                           overflow: TextOverflow.ellipsis,
      //                           maxLines: 1,
      //                           style: TextStyle(fontWeight: FontWeight.w600),
      //                         ),
      //                       ),
      //                     ),
      //                     cutPrice != 0.0
      //                         ? Expanded(
      //                             flex: 1,
      //                             child: Container(
      //                               child: AutoSizeText(
      //                                 inr.formatCurrency(cutPrice),
      //                                 presetFontSizes: [13, 11, 10],
      //                                 textAlign: TextAlign.left,
      //                                 overflow: TextOverflow.ellipsis,
      //                                 maxLines: 1,
      //                                 style: TextStyle(
      //                                     fontWeight: FontWeight.w500,
      //                                     decoration:
      //                                         TextDecoration.lineThrough,
      //                                     color: Colors.grey[300]),
      //                               ),
      //                             ),
      //                           )
      //                         : Container()
      //                   ],
      //                 ),
      //               ),
      //             ],
      //           ),
      //         ),
      //       ),
      //     ),
      //   ),
      // ));
    });
    if (_currentTrendList.length == 1) {
      _currentTrendList.add(Container(
        width: MediaQuery.of(context).size.width * .45,
      ));
    }
    setState(() {
      dummyListEnabled = false;
    });
  }
}
