import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/src/models/response/home/Brand.dart';
import 'package:gocomartapp/src/models/response/home/TrendingBrands.dart';
import 'package:gocomartapp/src/screens/widgets/BrandListItem.dart';

class TrendingBrandsVerticalScroll extends StatelessWidget {
  final double width;
  final TrendingBrands trendingBrands;
  final String imagePath;
  const TrendingBrandsVerticalScroll({Key key, this.width, this.trendingBrands, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width / 12,
                //height: 20,
                //color: Colors.blue,
                padding: EdgeInsets.only(left: 10, top: 5),
                child: AutoSizeText(
                  "Trending brands",
                  style: TextStyle(color: Colors.grey[700], fontWeight: FontWeight.w700),
                  presetFontSizes: [22, 20, 16, 14, 12, 10],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {},
                  child: Icon(
                    Icons.arrow_forward,
                    color: Colors.grey[700],
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          height: width / 3.5,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: this.trendingBrands.brands.length ?? 0,
              itemBuilder: (context, index) {
                final Brand cBrand = this.trendingBrands.brands[index];
                return Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.black12),
                      color: Colors.white,
                    ),
                    margin: const EdgeInsets.only(left: 10),
                    child: BrandListItem(
                      width: width,
                      brand: cBrand,
                      imagePath: imagePath,
                    ));
              }),
        ),
      ],
    );
  }
}
