import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_main.dart' as main;
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/models/shopping_home/products.dart';
import 'package:gocomartapp/src/screens/widgets/ProductGridCardStyle1.dart';
import 'package:skeleton_text/skeleton_text.dart';

class HomeScreenLatestOffer extends StatelessWidget {
  const HomeScreenLatestOffer({
    Key key,
    this.shoppingProductLoaded,
    this.shoppingHomeModel,
    this.imagePath,
    this.width,
  }) : super(key: key);

  final bool shoppingProductLoaded;
  final ShoppingHomeModel shoppingHomeModel;
  final String imagePath;
  final double width;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width / 12,
                //height: 20,
                //color: Colors.blue,
                padding: EdgeInsets.only(left: 10, top: 5),
                child: AutoSizeText(
                  main.getTranslate('latest_offer'),
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontWeight: FontWeight.w700,
                  ),
                  presetFontSizes: [22, 20, 16, 14, 12, 10],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {},
                  child: Icon(
                    Icons.arrow_forward,
                    color: Colors.grey[700],
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          //  padding: EdgeInsets.only(bottom: 20),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.width / 1.75,
          child: shoppingProductLoaded == false
              ? Container(
                  child: offerProductShimmer(width),
                )
              : ListView.builder(
                  itemCount: shoppingHomeModel.getOfferedProducts.products.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, i) {
                    final Products _product = shoppingHomeModel.getOfferedProducts.products[i];
                    final int discountPercent = main.discountPercentage(_product.details.price, _product.price);
                    return ProductGridCardStyle1(imagePath: imagePath, product: _product, discountPercent: discountPercent);
                  },
                ),
        ),
      ],
    );
  }

  offerProductShimmer(double width) {
    return ListView.builder(
      itemCount: 4,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) {
        return Container(
          width: MediaQuery.of(context).size.width / 2.2,
          margin: const EdgeInsets.only(left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2.2,
                height: MediaQuery.of(context).size.width / 3.2,
                child: SkeletonAnimation(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
              ),
              Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.width / 3.9,
                  width: MediaQuery.of(context).size.width / 2.2,
                  child:
                      Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.start, children: shimmerLines(3, width)))
            ],
          ),
        );
      },
    );
  }

  shimmerLines(int n, double width) {
    List skeltons = <Widget>[];
    for (int i = 0; i < n; i++) {
      skeltons.add(SkeletonAnimation(
        child: Container(
          height: 10,
          width: width / 2.8,
          decoration: BoxDecoration(
            color: Colors.grey[300],
          ),
        ),
      ));
    }
    return skeltons;
  }
}
