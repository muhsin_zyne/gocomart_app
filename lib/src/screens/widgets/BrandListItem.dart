import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/src/models/response/home/Brand.dart';

class BrandListItem extends StatelessWidget {
  const BrandListItem({
    Key key,
    @required this.width,
    this.brand,
    this.imagePath,
  }) : super(key: key);

  final double width;
  final Brand brand;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: width / 6,
          width: width / 3.5,
          child: CachedNetworkImage(
            imageUrl: "${imagePath + brand?.image}",
            placeholder: (context, url) => ImagePreLoader(),
            errorWidget: (context, url, error) => ImageErrorDummy(),
            fit: BoxFit.fitHeight,
          ),
        ),
        Container(
          width: width / 4.5,
          height: width / 20,
          margin: const EdgeInsets.only(right: 20, top: 8),
          padding: const EdgeInsets.only(top: 2, bottom: 2, left: 3),
          color: Colors.orange,
          child: AutoSizeText(
            htmlUnescape.convert(brand.name),
            maxFontSize: 18,
            overflow: TextOverflow.ellipsis,
            minFontSize: 10,
            style: TextStyle(
              fontSize: width / 40,
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
          ),
        )
      ],
    );
  }
}
