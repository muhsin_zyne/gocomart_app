import 'package:flutter/material.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';

class ShoppingHomePresenter {
  ShoppingHomePresenter({this.context, this.view}) {
    _init();
  }

  BuildContext context;
  IShoppingHomeView view;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  ShoppingServices _shoppingServices;

  void _init() async {
    Future.delayed(Duration(microseconds: 10));
    _shoppingServices = ShoppingServices(context: context);
  }

  void onHomeScreenInitialApiCallHandler() async {
    ShoppingHomeModel responseModel =
        await _shoppingServices.getMixedShoppingHomeProductsV2();
    if (responseModel.error == false) {
      this.view.onShoppingHomeDone(responseModel);
    }
  }

  void onHomeScreenInitAppBannerHandler() async {
    GetAppBanners responseModel =
        await _shoppingServices.getAppBanners(bannerType: "home_banner_2");
    if (responseModel.error == false) {
      print(responseModel.banners.images.length);
      this.view.onShoppinHomeBannersDone(responseModel);
    }
  }
}
