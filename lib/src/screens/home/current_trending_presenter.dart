import 'package:flutter/material.dart';
import 'package:gocomartapp/models/product-list/product_list.dart';
import 'package:gocomartapp/services/shopping_services.dart';
import 'package:gocomartapp/src/contracts.dart';

class CurrentTrendingPresenter {
  CurrentTrendingPresenter({this.context, this.view}) {
    _init();
  }

  BuildContext context;
  ICurrentTrendingBlockView view;
  ShoppingServices _shoppingServices;

  void _init() async {
    _shoppingServices = ShoppingServices(context: context);
    await Future.delayed(Duration(microseconds: 10));
  }

  onFetchCurrentTrendingHandler(int categoryId) async {
    ProductList productList = await this._shoppingServices.getMostTrendingProductByCategoryId(categoryId);
    if (productList.error == false) {
      this.view.onCurrentListChanges(productList);
    } else {}
  }
}
