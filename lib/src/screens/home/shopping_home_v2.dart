import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gocomartapp/appTheme.dart';
import 'package:gocomartapp/app_const.dart';
import 'package:gocomartapp/app_main.dart';
import 'package:gocomartapp/config.dart';
import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/providers/shopping_home_provider.dart';
import 'package:gocomartapp/screens/modules/product_search.dart';
import 'package:gocomartapp/screens/ui_components/loading_components.dart';
import 'package:gocomartapp/src/base/base_screen.dart';
import 'package:gocomartapp/src/contracts.dart';
import 'package:gocomartapp/src/models/response/home/AppHomeCategory.dart';
import 'package:gocomartapp/src/screens/home/shopping_home_presenter.dart';
import 'package:gocomartapp/src/screens/widgets/CurrentTrendsBlock.dart';
import 'package:gocomartapp/src/screens/widgets/HomeScreenLatestOffer.dart';
import 'package:gocomartapp/src/screens/widgets/TrendingBrandsVerticalScroll.dart';
import 'package:gocomartapp/src/screens/widgets/roundedImageButton.dart';
import 'package:provider/provider.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:gocomartapp/screens/product-list/product_list_screen.dart';
import 'package:html_unescape/html_unescape.dart';

class ShoppingHomeV2 extends StatefulWidget {
  @override
  _ShoppingHomeV2State createState() => _ShoppingHomeV2State();
}

class _ShoppingHomeV2State extends State<ShoppingHomeV2>
    with BaseStatefulScreen
    implements IShoppingHomeView {
  ShoppingHomeProvider _shoppingHomeProvider;
  ShoppingHomePresenter _presenter;
  ScrollController _controller = ScrollController();
  bool showSearchOnAppBar = false;
  List _appBannerImages = <CachedNetworkImage>[];
  List _homeBannerImages = <CachedNetworkImage>[];
  bool shoppingProductLoaded = false;
  bool homeAppBannerLoaded = false;
  int recentViewLength = 0;
  ShoppingHomeModel shoppingHomeModel;
  final imagePath = FlavorConfig.instance.flavorValues.cdnImagePoint;
  double width;
  double height;
  HtmlUnescape _htmlUnescape = HtmlUnescape();
  GetAppBanners _homeAppBanner;

  @override
  void initState() {
    super.initState();
    this._init();
  }

  void _init() async {
    await Future.delayed(Duration(microseconds: 10));
    _presenter = ShoppingHomePresenter(context: context, view: this);
    await Future.delayed(Duration(microseconds: 10));
    this._loadClient();
  }

  void _loadClient() async {
    _presenter.onHomeScreenInitialApiCallHandler();
    _presenter.onHomeScreenInitAppBannerHandler();
    setState(() {
      shoppingHomeModel = _shoppingHomeProvider.shoppingHomeModel;
      _homeAppBanner = _shoppingHomeProvider.homeAppBanner;
    });
    if (shoppingHomeModel != null) {
      setState(() {
        shoppingProductLoaded = true;
        this.setAppBanners();
        recentViewLength = shoppingHomeModel.getRecentProduct.products.length;

        //buildCurrentTrendDetail();
      });
    }

    if (_homeAppBanner != null) {
      setState(() {
        homeAppBannerLoaded = true;
        this.setHomeBanner();
      });
    }
    this._scrollControlHandler();
  }

  void _scrollControlHandler() {
    _controller.addListener(() {
      if (_controller.position.pixels ==
          _controller.position.maxScrollExtent) {}
      if (_controller.position.pixels > 245) {
        setState(() {
          showSearchOnAppBar = true;
        });
      } else {
        setState(() {
          showSearchOnAppBar = false;
        });
      }
    });
  }

  @override
  void onShoppingHomeDone(ShoppingHomeModel responseModel) {
    _shoppingHomeProvider.shoppingHomeModel = responseModel;
    setState(() {
      shoppingHomeModel = _shoppingHomeProvider.shoppingHomeModel;
    });
    this.setAppBanners();
    setState(() {
      shoppingProductLoaded = true;
      recentViewLength = shoppingHomeModel.getRecentProduct.products.length;
    });
  }

  @override
  void onShoppinHomeBannersDone(GetAppBanners responseModel) {
    print("Working");
    _shoppingHomeProvider.homeAppBanner = responseModel;
    setState(() {
      _homeAppBanner = _shoppingHomeProvider.homeAppBanner;
      this.setHomeBanner();
      homeAppBannerLoaded = true;
    });
  }

  void setAppBanners() {
    setState(() {
      _appBannerImages.clear();
    });
    for (int i = 0;
        i < shoppingHomeModel.getAppBanners.banners.images.length;
        i++) {
      _appBannerImages.add(
        CachedNetworkImage(
          imageUrl:
              "${imagePath + shoppingHomeModel.getAppBanners.banners.images[i].image}",
          width: width / 2.2,
          height: height / 3.2,
          placeholder: (context, url) => ImagePreLoader(),
          errorWidget: (context, url, error) => ImageError(),
          fit: BoxFit.fill,
        ),
      );
    }
  }

  void setHomeBanner() {
    print("--------------------------------");
    print(_homeAppBanner.banners.images.length);
    setState(() {
      _homeBannerImages.clear();
    });
    for (int i = 0; i < _homeAppBanner.banners.images.length; i++) {
      _homeBannerImages.add(
        CachedNetworkImage(
          imageUrl: "${imagePath + _homeAppBanner.banners.images[i].image}",
          width: width / 2.2,
          height: height / 3.2,
          placeholder: (context, url) => ImagePreLoader(),
          errorWidget: (context, url, error) => ImageError(),
          fit: BoxFit.fill,
        ),
      );
    }
  }

  @override
  void didChangeDependencies() {
    _shoppingHomeProvider =
        Provider.of<ShoppingHomeProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              //primary: true,
              floating: true,
              pinned: true,
              elevation: 0,
              backgroundColor: Colors.white,
              title: !showSearchOnAppBar
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          'assets/images/icons/icons8-document.svg',
                          height: 50,
                        ),
                        Text(
                          getTranslate('app_name'),
                          style: AppTheme.kAppLogoText,
                          textAlign: TextAlign.left,
                        )
                      ],
                    )
                  : searchWidget(width),
            ),
            SliverFillRemaining(
              child: SingleChildScrollView(
                controller: _controller,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    //carousal
                    Container(
                      color: Colors.transparent,
                      height: MediaQuery.of(context).size.width / 1.9,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(left: 5, right: 5),
                      child: Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        elevation: 4,
                        child: (shoppingProductLoaded == true &&
                                _appBannerImages.length > 0)
                            ? Carousel(
                                onImageTap: (index) {},
                                boxFit: BoxFit.fill,
                                indicatorBgPadding: 3.0,
                                noRadiusForIndicator: true,
                                overlayShadowColors: Colors.transparent,
                                showIndicator: true,
                                animationCurve: Curves.easeInOut,
                                animationDuration: Duration(seconds: 1),
                                autoplay: true,
                                dotIncreaseSize: 1.5,
                                dotVerticalPadding: 5,
                                overlayShadow: false,
                                dotBgColor: Colors.transparent,
                                images: _appBannerImages,
                              )
                            : Container(
                                child: SkeletonAnimation(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ),
                    //search
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: searchWidget(width),
                    ),

                    //categories
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 12),
                      child: Center(
                        child: Container(
                          padding: const EdgeInsets.only(top: 15.0),
                          height: 130,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            color: const Color(0xffe9e7e7),
                          ),
                          child: ListView.builder(
                            itemCount:
                                shoppingHomeModel?.appHomeCategories?.length ??
                                    0,
                            scrollDirection: Axis.horizontal,
                            // padding: EdgeInsets,
                            itemBuilder: (BuildContext context, int index) {
                              final AppHomeCategory _appHomeCategory =
                                  shoppingHomeModel.appHomeCategories[index];
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ProductListScreen(
                                        title: _htmlUnescape.convert(
                                            _appHomeCategory.name.toString()),
                                        categoryId: _appHomeCategory.categoryId,
                                      ),
                                    ),
                                  );
                                },
                                child: RoundedImageButton(
                                  onTap: () {
                                    print("Hii");
                                  },
                                  width: width,
                                  imagePath:
                                      "${imagePath + _appHomeCategory.image}",
                                  label: htmlUnescape
                                      .convert(_appHomeCategory.name),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),

                    //latest product
                    HomeScreenLatestOffer(
                      shoppingProductLoaded: this.shoppingProductLoaded,
                      imagePath: this.imagePath,
                      shoppingHomeModel: this.shoppingHomeModel,
                      width: width,
                    ),

                    //trending brands
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 8.0,
                      ),
                      child: (shoppingHomeModel?.trendingBrands != null)
                          ? TrendingBrandsVerticalScroll(
                              width: MediaQuery.of(context).size.width,
                              trendingBrands: shoppingHomeModel.trendingBrands,
                              imagePath: imagePath,
                            )
                          : Container(),
                    ),
                    //end of latest product
                    Container(
                      color: Colors.transparent,
                      height: width / 3.9,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(left: 5, right: 5, top: 18),
                      child: Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: homeAppBannerLoaded == true
                            ? Carousel(
                                onImageTap: (index) {},
                                boxFit: BoxFit.fill,
                                indicatorBgPadding: 3.0,
                                noRadiusForIndicator: true,
                                overlayShadowColors: Colors.transparent,
                                showIndicator: true,
                                animationCurve: Curves.easeInOut,
                                animationDuration: Duration(seconds: 1),
                                autoplay: true,
                                dotIncreaseSize: 1.5,
                                dotVerticalPadding: 5,
                                overlayShadow: false,
                                dotBgColor: Colors.transparent,
                                images: _homeBannerImages,
                              )
                            : Container(
                                child: SkeletonAnimation(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ),
                    (this.shoppingHomeModel?.trendingCategories?.length ?? 0) >
                            0
                        ? CurrentTrendsBlock(
                            trendingCategories:
                                this.shoppingHomeModel?.trendingCategories ??
                                    [],
                            context: this.context,
                            imagePath: imagePath,
                          )
                        : Container()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  InkWell searchWidget(width) {
    return InkWell(
      onTap: () {
        showSearch(context: context, delegate: ProductSearch());
      },
      child: Container(
        width: width / 1.05,
        height: width / 9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(22.0),
          color: const Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              color: const Color(0x29000000),
              offset: Offset(0, 0),
              blurRadius: 2,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              color: Colors.grey[600],
            ),
            Text(
              'Search products, brands and more',
              style: AppTheme.kAppSearchStyle,
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    );
  }
}
