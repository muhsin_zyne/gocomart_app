import 'package:flutter/material.dart';
import 'package:gocomartapp/src/models/response/onesignal/OneSignalExternalUserIDResponse.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

class OneSignalService {
  BuildContext context;
  OneSignalService({this.context}) {
    _init();
  }

  void _init() async {
    await Future.delayed(Duration(microseconds: 10));
    this._loadClient();
  }

  void _loadClient() async {}

  // Removing User ID Service
  Future<OneSignalExternalUserIDResponse> removeExternalUserId() async {
    var response = await OneSignal.shared.removeExternalUserId();
    try {
      final OneSignalExternalUserIDResponse oneSignalExternalUserIDResponse = OneSignalExternalUserIDResponse.fromJSON(response);
      return oneSignalExternalUserIDResponse;
    } on Exception catch (e) {
      final OneSignalExternalUserIDResponse oneSignalExternalUserIDResponse = OneSignalExternalUserIDResponse();
      return oneSignalExternalUserIDResponse;
    }
  }
}
