import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:gocomartapp/providers/global_provider.dart';
import 'package:gocomartapp/src/models/response/firebase/referral_link.dart';
import 'package:provider/provider.dart';

class DynamicLinkService {
  GlobalProvider globalProvider;

  Future handleDynamicLinks(BuildContext context) async {
    globalProvider = Provider.of<GlobalProvider>(context, listen: true);

    final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
    _handleDeepLink(data);

    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData dynamicLinkData) async {
        _handleDeepLink(dynamicLinkData);
      },
      onError: (OnLinkErrorException error) async {
        print('Dynamic Link Failed : ${error.message}');
      },
    );
  }

  void _handleDeepLink(PendingDynamicLinkData data) {
    final Uri deepLink = data?.link;
    if (deepLink != null) {
      print('_handleDeepLink | deepLink: $deepLink');
      var isInvite = deepLink.pathSegments.contains('referral');
      if (isInvite) {
        final ReferralLink referralLink = new ReferralLink.fromJSON(deepLink.queryParameters);
        globalProvider.referralLink = referralLink;
      }
    }
  }
}
