import 'package:gocomartapp/screens/profile/order/order_status.dart';

class PageDynamicRouteController {
  ///  push replacement named block
  static pushReplacementNamed(dynamic navigatorKey, String screenId, dynamic arguments) {
    switch (screenId) {
      case OrderStatus.id:
        {
          OrderStatusScreenArguments orderStatusScreenArguments = OrderStatusScreenArguments(arguments['orderRequestGUID']);
          navigatorKey.currentState.pushReplacementNamed(screenId, arguments: orderStatusScreenArguments);
        }
        break;

      default:
        {
          navigatorKey.currentState.pushReplacementNamed(screenId);
        }
        break;
    }
  }

  /// push named block
  static pushNamed(dynamic navigatorKey, String screenId, dynamic arguments) {
    switch (screenId) {
      case OrderStatus.id:
        {
          OrderStatusScreenArguments orderStatusScreenArguments = OrderStatusScreenArguments(arguments['orderRequestGUID']);
          navigatorKey.currentState.pushNamed(screenId, arguments: orderStatusScreenArguments);
        }
        break;

      default:
        {
          navigatorKey.currentState.pushNamed(screenId);
        }
        break;
    }
  }
}

class OrderStatusScreenArguments {
  final String orderRequestGUID;
  OrderStatusScreenArguments(this.orderRequestGUID);
}
