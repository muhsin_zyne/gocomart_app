import 'package:gocomartapp/models/login_response.dart';
import 'package:gocomartapp/models/otp_validation_response.dart';
import 'package:gocomartapp/models/product-list/product_list.dart';
import 'package:gocomartapp/models/shopping_home/get_app_banners.dart';
import 'package:gocomartapp/models/shopping_home/mixedShoppingHome.dart';
import 'package:gocomartapp/src/models/requests/auth/LoginScreenRequest.dart';
import 'package:gocomartapp/src/models/response/onesignal/OneSignalExternalUserIDResponse.dart';
import 'package:gocomartapp/src/models/response/wallet/WalletAppLinksMixed.dart';

abstract class IBaseViewContract {
  void onError();
  void onRequestErrorUnhandled(String message);
}

abstract class IProfileScreen extends IBaseViewContract {
  void onLogoutDone(OneSignalExternalUserIDResponse oneSignalExternalUserIDResponse);
}

abstract class ILoginScreenView extends IBaseViewContract {
  void onLoginScreenDone(LoginScreenRequest loginScreenRequest, LoginResponse loginResponse);
  void onLoginScreenError(LoginResponse loginResponse);
}

abstract class IOtpVerificationScreenView extends IBaseViewContract {
  void onOtpVerificationScreenDone(OtpValidationResponse otpValidationResponse);
  void onOtpVerificationScreenError(OtpValidationResponse otpValidationResponse);
}

abstract class IWalletHomeScreenView extends IBaseViewContract {
  void onWalletHomeApiCallDone(WalletAppLinksMixed walletAppLinksMixed);
}

abstract class IShoppingHomeView extends IBaseViewContract {
  void onShoppingHomeDone(ShoppingHomeModel responseModel);
  void onShoppinHomeBannersDone(GetAppBanners responseModel);
}

abstract class ICurrentTrendingBlockView extends IBaseViewContract {
  void onCurrentListChanges(ProductList productList);
}

abstract class ICashWalletRewardsScreen extends IBaseViewContract {
  void onRewardsListFetchDone(List scratchcardRawList, double totalCollected);
}

abstract class ICashWalletRewardsDetail extends IBaseViewContract {
  void onScratchCardScratchedDone() {}
}
